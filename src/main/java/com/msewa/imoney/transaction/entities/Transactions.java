package com.msewa.imoney.transaction.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.entity.AbstractEntity;
import com.msewa.imoney.enums.Status;

@Entity
@Table(name="Transactions")
public class Transactions extends AbstractEntity<Long>{

	@Column(nullable=false)
	private double amount;
	@Column(nullable=false,unique=true)
	private String transaction_ref_no;
	@Column(nullable=true)
	private String rrn;
	@Column(nullable=true)
	private String remarks;
	@ManyToOne
	private User user;
	@ManyToOne
	private Services service;
	@Column(nullable=true)
	private String auth_ref_no;
	@Column(nullable=true)
	private String description;
	@Column(nullable=true)
	@Enumerated(EnumType.STRING)
	private Status status;
	@Column(nullable=true)
	@Enumerated(EnumType.STRING)
	private Status card_status;
	
	
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getTransaction_ref_no() {
		return transaction_ref_no;
	}
	public void setTransaction_ref_no(String transaction_ref_no) {
		this.transaction_ref_no = transaction_ref_no;
	}
	public String getRrn() {
		return rrn;
	}
	public void setRrn(String rrn) {
		this.rrn = rrn;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Services getService() {
		return service;
	}
	public void setService(Services service) {
		this.service = service;
	}
	public String getAuth_ref_no() {
		return auth_ref_no;
	}
	public void setAuth_ref_no(String auth_ref_no) {
		this.auth_ref_no = auth_ref_no;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Status getCard_status() {
		return card_status;
	}
	public void setCard_status(Status card_status) {
		this.card_status = card_status;
	}
	
	
	
	
	
	
}

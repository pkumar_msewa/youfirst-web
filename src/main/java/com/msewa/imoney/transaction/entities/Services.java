package com.msewa.imoney.transaction.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import com.msewa.imoney.entity.AbstractEntity;
import com.msewa.imoney.enums.ServiceTypes;
import com.msewa.imoney.enums.Status;

@Entity
@Table(name="Services")
public class Services extends AbstractEntity<Long>{

	@Column(nullable=true)
	private String service_name;
	@Column(nullable=false,unique=true)
	private String code;
	@Column(nullable=true)
	@Enumerated(EnumType.STRING)
	private ServiceTypes service_type;
	@Column(nullable=false)
	private double min_amount=0.0;
	@Column(nullable=false)
	private double max_amount=0.0;
	@Column(nullable=false)
	@Enumerated(EnumType.STRING)
	private Status status;
	@Column(nullable=true)
	private double commission=0.0;
	@Column(nullable=false)
	private boolean isFixed=false;
	
	
	
	public double getCommission() {
		return commission;
	}
	public void setCommission(double commission) {
		this.commission = commission;
	}
	public boolean isFixed() {
		return isFixed;
	}
	public void setFixed(boolean isFixed) {
		this.isFixed = isFixed;
	}
	public String getService_name() {
		return service_name;
	}
	public void setService_name(String service_name) {
		this.service_name = service_name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public ServiceTypes getService_type() {
		return service_type;
	}
	public void setService_type(ServiceTypes service_type) {
		this.service_type = service_type;
	}
	public double getMin_amount() {
		return min_amount;
	}
	public void setMin_amount(double min_amount) {
		this.min_amount = min_amount;
	}
	public double getMax_amount() {
		return max_amount;
	}
	public void setMax_amount(double max_amount) {
		this.max_amount = max_amount;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	
	
	
	
}

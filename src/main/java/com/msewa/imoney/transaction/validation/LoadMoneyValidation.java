package com.msewa.imoney.transaction.validation;

import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.cardprocessor.entities.WalletInfo;
import com.msewa.imoney.cardprocessor.repositories.WalletInfoRepository;
import com.msewa.imoney.enums.ResponseStatus;
import com.msewa.imoney.session.entities.UserSession;
import com.msewa.imoney.session.service.ISessionApi;
import com.msewa.imoney.transaction.dto.request.LoadMoneyRequestDTO;
import com.msewa.imoney.transaction.dto.response.LoadMoneyValidationResp;
import com.msewa.imoney.transaction.entities.Services;
import com.msewa.imoney.transaction.service.IServiceApi;

public class LoadMoneyValidation {

	private final IServiceApi serviceApi;
	private final ISessionApi sessionApi;
	private final WalletInfoRepository walletInfoRepository;
	
	public LoadMoneyValidation(IServiceApi serviceApi,ISessionApi sessionApi
			,WalletInfoRepository walletInfoRepository) {
		super();
		
		this.serviceApi = serviceApi;
		this.sessionApi=sessionApi;
		this.walletInfoRepository=walletInfoRepository;
	}

	public LoadMoneyValidationResp validateLoadMoney(LoadMoneyRequestDTO loadMoneyReq){
		LoadMoneyValidationResp loadMoneyValidationResp = new LoadMoneyValidationResp();
		boolean valid=true;
		User user=null;
		UserSession userSession=sessionApi.getUserSession(loadMoneyReq.getSessionId());
		if(userSession!=null){
			user=userSession.getUser();
			//TO DO 
			if(user!=null){
				if(user.getMobileStatus().getValue().equalsIgnoreCase("Inactive")){
					valid=false;
					loadMoneyValidationResp.setCode(ResponseStatus.MOBILE_INACTIVE);
					loadMoneyValidationResp.setValid(valid);
					return loadMoneyValidationResp;
				}
				
				if(user.getAuthority().contains("BLOCKED")||user.getAuthority().contains("LOCKED")){
					valid=false;
					loadMoneyValidationResp.setCode(ResponseStatus.USER_LOCKED);
					loadMoneyValidationResp.setValid(valid);
					return loadMoneyValidationResp;
				}
				
				WalletInfo wallet=walletInfoRepository.walletInfoByUserId(user);
				if(wallet==null){
					valid=false;
					loadMoneyValidationResp.setValid(false);
					loadMoneyValidationResp.setCode(ResponseStatus.WALLET_NON_EXISTENCE);
					return loadMoneyValidationResp;
				}
				
				
				//TO DO ACCOUNT TYPE LIMIT CHECKS
			}
				}else{
					valid=false;
					loadMoneyValidationResp.setValid(valid);
					loadMoneyValidationResp.setCode(ResponseStatus.INVALID_SESSION);
					return loadMoneyValidationResp;
				}
				Services lmService=serviceApi.findServiceByCode("LMS");
				if(lmService!=null){
					if(lmService.getStatus().getValue().equalsIgnoreCase("Inactive")){
						valid=false;
						loadMoneyValidationResp.setValid(valid);
						loadMoneyValidationResp.setCode(ResponseStatus.SERVICE_INACTIVE);
						return loadMoneyValidationResp;
					}
				}
			loadMoneyValidationResp.setValid(valid);
			return loadMoneyValidationResp;
			
		}
	
}

package com.msewa.imoney.transaction.dto.response;

import com.msewa.imoney.enums.ResponseStatus;

public class LoadMoneyResponseDTO {

	private String code;
	private String message;
	private String transaction_ref_no;
	private String amount;
	private String remarks;
	
	
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getCode() {
		return code;
	}
	public void setCode(ResponseStatus status) {
		this.code = status.getValue();
		this.message=status.getKey();
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTransaction_ref_no() {
		return transaction_ref_no;
	}
	public void setTransaction_ref_no(String transaction_ref_no) {
		this.transaction_ref_no = transaction_ref_no;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	
	
}

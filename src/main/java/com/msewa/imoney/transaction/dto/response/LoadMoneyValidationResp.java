package com.msewa.imoney.transaction.dto.response;

import com.msewa.imoney.enums.ResponseStatus;

public class LoadMoneyValidationResp {

	private boolean valid;
	private String code;
	private String message;
	
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public String getCode() {
		return code;
	}
	public void setCode(ResponseStatus status) {
		this.code = status.getValue();
		this.message=status.getKey();
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "LoadMoneyValidationResp [valid=" + valid + ", code=" + code + ", message=" + message + "]";
	}
	
	
	
}

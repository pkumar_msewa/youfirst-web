package com.msewa.imoney.transaction.dto.request;

import com.msewa.imoney.cardprocessor.dto.request.SessionDTO;

public class LoadMoneyRequestDTO extends SessionDTO{

	private String amount;
	private String enc_request;
	private String rrn;
	private String status;
	private String trx_ref_no;
	
	
	
	public String getTrx_ref_no() {
		return trx_ref_no;
	}

	public void setTrx_ref_no(String trx_ref_no) {
		this.trx_ref_no = trx_ref_no;
	}

	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEnc_request() {
		return enc_request;
	}

	public void setEnc_request(String enc_request) {
		this.enc_request = enc_request;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	
}

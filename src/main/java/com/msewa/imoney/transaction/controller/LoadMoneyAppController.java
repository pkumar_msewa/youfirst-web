package com.msewa.imoney.transaction.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.msewa.imoney.transaction.dto.request.LoadMoneyRequestDTO;
import com.msewa.imoney.transaction.dto.response.LoadMoneyResponseDTO;
import com.msewa.imoney.transaction.service.ILoadMoneyApi;

@Controller
@RequestMapping("/Api/{role}/{device}/LoadMoney")
public class LoadMoneyAppController {

	private final ILoadMoneyApi loadMoneyApi;

	public LoadMoneyAppController(ILoadMoneyApi loadMoneyApi) {
		super();
		this.loadMoneyApi = loadMoneyApi;
	}
	
	
	@RequestMapping(value="/Initiate/Process", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<LoadMoneyResponseDTO> initiateLoadMoney(@RequestBody LoadMoneyRequestDTO loadReq,
			@PathVariable(value="role")String role,@PathVariable(value="device")String device,
			HttpServletRequest request,HttpServletResponse response){
		
		return loadMoneyApi.doInitiateLoadMoneyTransaction(loadReq);
	}
	
	
	@RequestMapping(value="/Redirect/Process", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<LoadMoneyResponseDTO> successLoadMoney(@RequestBody LoadMoneyRequestDTO loadReq,
			@PathVariable(value="role")String role,@PathVariable(value="device")String device,
			HttpServletRequest request,HttpServletResponse response){
		
		return loadMoneyApi.doSuccessLoadMoneyTransaction(loadReq);
	}
		
}

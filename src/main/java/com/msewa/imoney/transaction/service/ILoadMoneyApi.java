package com.msewa.imoney.transaction.service;

import org.springframework.http.ResponseEntity;

import com.msewa.imoney.transaction.dto.request.LoadMoneyRequestDTO;
import com.msewa.imoney.transaction.dto.response.LoadMoneyResponseDTO;

public interface ILoadMoneyApi {

	ResponseEntity<LoadMoneyResponseDTO> doInitiateLoadMoneyTransaction(LoadMoneyRequestDTO loadMoneyRequestDTO);
	ResponseEntity<LoadMoneyResponseDTO> doSuccessLoadMoneyTransaction(LoadMoneyRequestDTO loadMoneyRequestDTO);
}

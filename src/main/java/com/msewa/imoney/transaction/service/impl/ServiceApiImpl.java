package com.msewa.imoney.transaction.service.impl;

import java.util.List;

import com.msewa.imoney.transaction.entities.Services;
import com.msewa.imoney.transaction.repositories.ServiceRepository;
import com.msewa.imoney.transaction.service.IServiceApi;

public class ServiceApiImpl implements IServiceApi{

	private final ServiceRepository serviceRepository;
	
	
	
	public ServiceApiImpl(ServiceRepository serviceRepository) {
		super();
		this.serviceRepository = serviceRepository;
	}

	@Override
	public Services findServiceByCode(String serviceCode) {
		
		return serviceRepository.findServiceByCode(serviceCode);
		
	}

	@Override
	public List<Services> findServiceByServiceType(String serviceType) {
		
		return serviceRepository.getServicesFromServiceType(serviceType);
	
	}

}

package com.msewa.imoney.transaction.service;

import java.util.List;

import com.msewa.imoney.transaction.entities.Services;

public interface IServiceApi {

	Services findServiceByCode(String serviceCode);
	List<Services> findServiceByServiceType(String serviceType);
	
}

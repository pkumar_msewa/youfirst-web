package com.msewa.imoney.transaction.service.impl;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.msewa.imoney.cardprocessor.dto.response.ConsumerDTO;
import com.msewa.imoney.cardprocessor.dto.response.WalletTransactionRespDTO;
import com.msewa.imoney.cardprocessor.service.ICPWalletApi;
import com.msewa.imoney.enums.ResponseStatus;
import com.msewa.imoney.enums.Status;
import com.msewa.imoney.session.entities.UserSession;
import com.msewa.imoney.session.service.ISessionApi;
import com.msewa.imoney.transaction.dto.request.LoadMoneyRequestDTO;
import com.msewa.imoney.transaction.dto.response.LoadMoneyResponseDTO;
import com.msewa.imoney.transaction.dto.response.LoadMoneyValidationResp;
import com.msewa.imoney.transaction.entities.Services;
import com.msewa.imoney.transaction.entities.Transactions;
import com.msewa.imoney.transaction.repositories.TransactionRepository;
import com.msewa.imoney.transaction.service.ILoadMoneyApi;
import com.msewa.imoney.transaction.service.IServiceApi;
import com.msewa.imoney.transaction.validation.LoadMoneyValidation;
import com.msewa.imoney.util.AESEncryption;
import com.msewa.imoney.util.AuthenticationUtil;

public class LoadMoneyApiImpl implements ILoadMoneyApi {

	private final IServiceApi serviceApi;
	private final TransactionRepository transactionRepository;
	private final ICPWalletApi walletApi;
	private final LoadMoneyValidation loadMoneyValidation;
	private final ISessionApi sessionApi;

	public LoadMoneyApiImpl(IServiceApi serviceApi, TransactionRepository transactionRepository, ICPWalletApi walletApi,
			LoadMoneyValidation loadMoneyValidation, ISessionApi sessionApi) {
		super();
		this.serviceApi = serviceApi;
		this.transactionRepository = transactionRepository;
		this.walletApi = walletApi;
		this.loadMoneyValidation = loadMoneyValidation;
		this.sessionApi = sessionApi;
	}

	@Override
	public ResponseEntity<LoadMoneyResponseDTO> doInitiateLoadMoneyTransaction(
			LoadMoneyRequestDTO loadMoneyRequestDTO) {
		// TODO Auto-generated method stub
		LoadMoneyResponseDTO lmResp = new LoadMoneyResponseDTO();
		try {
//			LoadMoneyRequestDTO treq = AuthenticationUtil
//					.prepareTopupRequest(AESEncryption.decrypt(loadMoneyRequestDTO.getEnc_request()));

			if (loadMoneyRequestDTO.getAmount() != null) {
				String trx_ref_no = System.nanoTime() + "C";
				ConsumerDTO consumerData = walletApi.getConsumerData();
				double prefundBal = Double.parseDouble(consumerData.getCreditPrefund());
				double reqAmt = Double.parseDouble(loadMoneyRequestDTO.getAmount());
				UserSession session = sessionApi.getActiveUserSession(loadMoneyRequestDTO.getSessionId());
				if (session != null) {
					LoadMoneyValidationResp lmError = loadMoneyValidation.validateLoadMoney(loadMoneyRequestDTO);
					if (lmError.isValid()) {
						Services service = serviceApi.findServiceByCode("LMS");
						if (reqAmt < prefundBal) {
							Transactions initiatetransaction = new Transactions();
							initiatetransaction.setAmount(Double.parseDouble(loadMoneyRequestDTO.getAmount()));
							initiatetransaction.setDescription("Load Money of Rs " + loadMoneyRequestDTO.getAmount());
							initiatetransaction.setService(service);
							initiatetransaction.setStatus(Status.Initiated);
							initiatetransaction.setCard_status(Status.Initiated);
							initiatetransaction.setTransaction_ref_no(trx_ref_no);
							initiatetransaction.setUser(session.getUser());
							transactionRepository.save(initiatetransaction);
							lmResp.setCode(ResponseStatus.SUCCESS);
							lmResp.setAmount(loadMoneyRequestDTO.getAmount());
							lmResp.setTransaction_ref_no(trx_ref_no);
						} else {
							lmResp.setCode(ResponseStatus.LOW_PREFUND);
						}
					} else {
						System.err.println("error:" + lmError.toString());
						lmResp.setCode(ResponseStatus.FAILURE);
						lmResp.setMessage(lmError.getMessage());
					}
				} else {
					lmResp.setCode(ResponseStatus.INVALID_SESSION);
				}
			} else {
				lmResp.setCode(ResponseStatus.FAILURE);
				lmResp.setMessage("Amount should not be empty");
			}
		} catch (Exception e) {
			e.printStackTrace();
			lmResp.setCode(ResponseStatus.EXCEPTION_OCCURED);
			return new ResponseEntity<LoadMoneyResponseDTO>(lmResp, HttpStatus.EXPECTATION_FAILED);
		}
		return new ResponseEntity<LoadMoneyResponseDTO>(lmResp, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<LoadMoneyResponseDTO> doSuccessLoadMoneyTransaction(LoadMoneyRequestDTO loadMoneyRequestDTO) {

		LoadMoneyResponseDTO lmResp = new LoadMoneyResponseDTO();
		try {
			Transactions transaction = transactionRepository
					.findByTransactionRefNo(loadMoneyRequestDTO.getTrx_ref_no());
			if (transaction != null) {

				if (transaction.getStatus().getValue().equalsIgnoreCase("Initiated")) {
					if (loadMoneyRequestDTO.getStatus().equalsIgnoreCase("Success")) {
						transaction.setStatus(Status.Success);
						transaction.setRrn(loadMoneyRequestDTO.getRrn());
						// TO DO Status check api

						WalletTransactionRespDTO walletResp = walletApi.creditWallet(
								String.valueOf(transaction.getAmount()), transaction.getTransaction_ref_no(),
								"load_money_pg", transaction.getUser().getEmail());
						if (walletResp != null && walletResp.getCode().equalsIgnoreCase("S00")) {

							transaction.setAuth_ref_no(walletResp.getAuthRetreivalNo());
							transaction.setCard_status(Status.Success);
							lmResp.setAmount(String.valueOf(transaction.getAmount()));
							lmResp.setTransaction_ref_no(transaction.getTransaction_ref_no());
							lmResp.setCode(ResponseStatus.SUCCESS);
						} else {
							transaction.setCard_status(Status.Pending);
							lmResp.setCode(ResponseStatus.FAILURE);
						}
					} else {
						transaction.setStatus(Status.Failed);
						transaction.setCard_status(Status.Failed);
						lmResp.setCode(ResponseStatus.FAILURE);
						lmResp.setMessage("Load money failed");
					}
				} else {
					transaction.setStatus(Status.Failed);
					transaction.setCard_status(Status.Failed);
					lmResp.setCode(ResponseStatus.FAILURE);

				}
				transactionRepository.save(transaction);
			} else {
				lmResp.setCode(ResponseStatus.INVALID_TRANSACTION);
				return new ResponseEntity<LoadMoneyResponseDTO>(lmResp, HttpStatus.EXPECTATION_FAILED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			lmResp.setCode(ResponseStatus.EXCEPTION_OCCURED);
			return new ResponseEntity<LoadMoneyResponseDTO>(lmResp, HttpStatus.EXPECTATION_FAILED);

		}
		return new ResponseEntity<LoadMoneyResponseDTO>(lmResp, HttpStatus.OK);
	}

}

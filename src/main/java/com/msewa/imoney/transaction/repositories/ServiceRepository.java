package com.msewa.imoney.transaction.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msewa.imoney.enums.Status;
import com.msewa.imoney.transaction.entities.Services;

public interface ServiceRepository extends CrudRepository<Services, Long>, PagingAndSortingRepository<Services, Long>,
		JpaSpecificationExecutor<Services> {

	@Query("select s from Services s where s.status=?1")
	List<Services> getServiceByStatus(Status active);


	@Query("select s from Services s where s.code=?1")
	Services findServiceByCode(String serviceCode);
	
	@Query("select s from Services s where s.service_type=?1")
	List<Services> getServicesFromServiceType(String serviceType);
}

package com.msewa.imoney.transaction.repositories;


import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msewa.imoney.transaction.entities.Services;
import com.msewa.imoney.transaction.entities.Transactions;


public interface TransactionRepository extends CrudRepository<Transactions, Long>, PagingAndSortingRepository<Transactions, Long>, JpaSpecificationExecutor<Transactions>{

	@Query("select t from Transactions t where t.transaction_ref_no=?1")
	Transactions findByTransactionRefNo(String transactionRefNo);

	@Query("SELECT t FROM Transactions t WHERE t.service=?1 and t.created BETWEEN ?2 AND ?3")
	Page<Transactions> getTransactionByFilters(Pageable pageable, Services service, Date fromDate, Date toDate);

	@Query("SELECT t FROM Transactions t WHERE t.created BETWEEN ?1 AND ?2")
	Page<Transactions> getTransactionByFilters(Pageable pageable, Date parse, Date parse2);
}

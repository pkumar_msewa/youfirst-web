package com.msewa.imoney.startup;

import com.msewa.imoney.enums.ServiceTypes;
import com.msewa.imoney.enums.Status;
import com.msewa.imoney.transaction.entities.Services;
import com.msewa.imoney.transaction.repositories.ServiceRepository;

public class StartUpServiceCreator {

	private final ServiceRepository serviceRepository;
	
	
	
	public StartUpServiceCreator(ServiceRepository serviceRepository) {
		super();
		this.serviceRepository = serviceRepository;
	}



	public void create(){
		
				Services service=serviceRepository.findServiceByCode("LMS");
				if(service==null){
					service=new Services();
					service.setCode("LMS");
					service.setService_name("Load money via PG");
					service.setService_type(ServiceTypes.loadmoney_pg);
					service.setStatus(Status.Active);
					serviceRepository.save(service);
				}
	}
}

package com.msewa.imoney.javasdk;

public interface ConnectionSession {
	
	public boolean set (String key, String value);
	
	public boolean set (String key, String value, long expires);
	
	public String get (String key);

}

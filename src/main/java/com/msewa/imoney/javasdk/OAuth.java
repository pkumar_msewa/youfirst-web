package com.msewa.imoney.javasdk;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Random;



public class OAuth {
	
	public static final String VERSION = "1.0";
	private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static Random rnd = new Random();
	
	public static final String nonce() 
			throws 
				NoSuchAlgorithmException,
				UnsupportedEncodingException {
		int len = 16;
		StringBuilder sb = new StringBuilder(len);
		
		for(int i = 0; i < len; i++) {
			sb.append(OAuth.AB.charAt(OAuth.rnd.nextInt(OAuth.AB.length()))); 
		}
		
		byte[] random = Signature.arrayConcat(sb.toString().getBytes("UTF-8"), new Long(OAuth.timestamp()).toString().getBytes("UTF-8"));
		
		return Connection.hash(random);
	}
	
	public static final long timestamp() {
		 return System.currentTimeMillis() / 1000;
	}
}

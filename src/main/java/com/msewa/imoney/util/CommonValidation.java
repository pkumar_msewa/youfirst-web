package com.msewa.imoney.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.web.multipart.MultipartFile;




public class CommonValidation {

	/**
	 * Checks String with length greater than 6
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean checkLength6(String str) {

		String temp = str.trim();
		if (temp.length() >= 6) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Checks merchant password
	 *
	 * @param str
	 * @return boolean
	 */

	public static boolean checkMerchantPassword(String str) {
		String regex = "^[\\p{Digit}]{6}$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		return matcher.matches();
	}

	/**
	 *
	 * @param str
	 * @return boolean
	 */
	public static boolean validateVBankAccountNumber(String str){
		String regex = "^[\\p{Digit}]{15}$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		return matcher.matches();
	}


	/**
	 *
	 * @param str
	 * @return boolean
	 */
	public static boolean validateMerchantVBankAccountNumber(String str){
		String regex = "^[M]{1}[\\p{Digit}]{15}$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		return matcher.matches();
	}



	/**
	 *
	 * @param str
	 * @return boolean
	 */
	public static boolean validateMobileNumber(String str){
		String regex = "^[7-9]{1}[0-9]{9}$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		return matcher.matches();
	}

	/**
	 *
	 * @param str
	 * @return boolean
	 */
	public static boolean validateMerchantMobileNumber(String str){
		String regex = "^[M]{1}[7-9]{1}[0-9]{9}$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		return matcher.matches();
	}

//	public static void main(String... args){
//		System.err.println(validateMerchantMobileNumber("M8769986881"));
//	}


	/**
	 *
	 * @param str
	 * @return
	 */

	public static boolean validateUppercase(String str) {
		String temp = str.trim();
		String regex = "[\\p{Upper}]{2,}";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		return matcher.matches();
	}



	/**
	 * Checks String with length 10 and numeric
	 * @param str
	 * @return boolean
	 *
	 */

	public static boolean validateAdminPassword(String str){
		boolean valid = false;
		String regex = "^[\\p{Digit}]{10}$";
		String temp = str.trim();
		Pattern pattern = Pattern.compile(regex);
		Matcher passwordMatcher = pattern.matcher(str);
		if(passwordMatcher.matches()){
			valid = true;
		}
		return valid;
	}

	/**
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean checkValidMpin(String str) {
		String temp = str.trim();
		if (temp.length() == 4) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks String with length equal to 4
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean checkLength4(String str) {

		String temp = str.trim();
		if (temp.length() == 4) {
			return true;
		} else {
			return false;
		}

	}
	
	

	/**
	 * Checks whether a String is alphanumeric
	 * 
	 * @param str
	 * @return boolean
	 * 
	 */
	public static boolean isAlphanumeric(String str) {
		String temp = str.trim();
		if (temp.matches("[A-Za-z0-9]+")) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isValidNumber(String str) {
		String temp = str.trim();
		if (temp.matches("^(?=(?:[6-9]){1})(?=[0-9]{10}).*")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * checks whether the string is completely numeric
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean isNumeric(String str) {
		String temp = str.trim();
		boolean isNumber = false;
		if (temp.matches("[0-9]+")) {
			isNumber = true;
		}
		return isNumber;
	}

	public static boolean isValidLoadMoneyTransaction(String str) {
		int amount = Integer.parseInt(str);
		if ((amount >= 1) && (amount <= 10000)) {
			return true;
		}
		return false;
	}

	/**
	 * checks the length of string is equal to 10
	 * 
	 * @param str
	 * @return
	 */
	public static boolean checkLength10(String str) {
		String temp = str.trim();
		int length = temp.length();
		boolean isValid = false;
		if (length == 10) {
			isValid = true;
		}
		return isValid;
	}

	/**
	 * for email validation
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean isLoginEmail(String str) {
		String regexOfMail = "^[\\w]{2,}@[\\p{Alpha}]{3,}\\.[\\p{Alpha}]{3}$";
		Pattern mailPattern = Pattern.compile(regexOfMail);
		Matcher mailMatcher = mailPattern.matcher(str);
		return mailMatcher.matches();
	}

	/**
	 * @param password
	 * @return boolean
	 */

	public static boolean isSuperAdminPassword(String password){
		String regex = "[\\p{Digit}]{10}";
		Pattern passwordPattern = Pattern.compile(regex);
		Matcher passwordMatcher = passwordPattern.matcher(password);
		return passwordMatcher.matches();
	}

	/**
	 * @param otp
	 * @return boolean
	 */

	public static boolean isValidOTP(String otp){
		String regex = "[\\p{Digit}]{5,6}";
		Pattern otpPattern = Pattern.compile(regex);
		Matcher otpMatcher = otpPattern.matcher(otp);
		return otpMatcher.matches();
	}

	/**
	 * checks the number must be greater than 10
	 * 
	 * @param number
	 * @return boolean
	 */

	public static boolean isGreaterthan10(double number) {
		if (number >= 10)
			return true;
		else
			return false;
	}

	/**
	 * checks the validity of email
	 * 
	 * @param str
	 * @return boolean
	 * 
	 */

	public static boolean isValidMail(String str) {
		boolean isValid = false;
		if (str.contains("@") || str.contains(".")) {
			isValid = true;
		}
		return isValid;
	}

	/**
	 * checks whether String is null or not
	 * 
	 * @param str
	 * @return boolean
	 * 
	 */
	public static boolean isNull(String str) {
		if (str == null || str.isEmpty() || str == "") {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isNullImage(MultipartFile str) {
		if (str == null || str.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * checks whether String contains only alphabets
	 * 
	 * @param str
	 * @return boolean
	 * 
	 */

	public static boolean containsAlphabets(String str) {
		if (str.matches("[A-Za-z]+")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * checks whether String contains valid image extension
	 * 
	 * @param str
	 * @return boolean
	 * 
	 * 
	 */
	public static boolean isValidImageExtension(String str) {
		String temp = str.trim();
		if (temp.contains(".jpg") || temp.contains(".png") || temp.contains(".tiff") || temp.contains(".gif"))
			return true;
		else
			return false;
	}

	/**
	 * Checks transaction amount is less than or equal to user balance
	 * 
	 * @param userBalance
	 * @param transactionAmount
	 * @return
	 */
	public static boolean balanceCheck(double userBalance, double transactionAmount) {
		if (transactionAmount <= userBalance) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks if user daily transaction limit is to mark or not if the
	 * transaction is carried out
	 * 
	 * @param dailyTransactionLimit
	 * @param totalDailyTransaction
	 * @param transactionAmount
	 * @return
	 */
	public static boolean dailyLimitCheck(double dailyTransactionLimit, double totalDailyTransaction,
			double transactionAmount) {
		if (dailyTransactionLimit >= (totalDailyTransaction + transactionAmount)) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean dailyLimitCheckBank(double dailyTransactionLimitBank, double totalDailyTransactionBank,
			double transactionAmount) {
		if (dailyTransactionLimitBank >= (totalDailyTransactionBank + transactionAmount)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if user daily debit transaction limit is to mark or not if the
	 * transaction is carried out
	 * 
	 * @param dailyTransactionLimit
	 * @param totalDailyDebitTransactionAmount
	 * @return
	 */
	public static boolean dailyDebitLimitCheck(double dailyTransactionLimit, 
			double totalDailyDebitTransactionAmount, double transactionAmount) {
		if (dailyTransactionLimit >= (totalDailyDebitTransactionAmount + transactionAmount)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if user daily credit transaction limit is to mark or not if the
	 * transaction is carried out
	 * @param transactionAmount
	 * @param dailyTransactionLimit
	 * @param totalDailyCreditTransactionAmount
	 * @return
	 */
	public static boolean dailyCreditLimitCheck(double dailyTransactionLimit, 
			double totalDailyCreditTransactionAmount, double transactionAmount) {
		if (dailyTransactionLimit >= (totalDailyCreditTransactionAmount + transactionAmount)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks if user monthly transaction limit is to mark or not if the
	 * transaction is carried out
	 * 
	 * @param monthlyTransactionLimit
	 * @param totalMonthlyTransaction
	 * @param transactionAmount
	 * @return
	 */
	public static boolean monthlyLimitCheck(double monthlyTransactionLimit, double totalMonthlyTransaction,
			double transactionAmount) {
		if (monthlyTransactionLimit >= (totalMonthlyTransaction + transactionAmount)) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean monthlyLimitCheckBank(double monthlyTransactionLimitBank, double totalMonthlyTransactionBank,
			double transactionAmount) {
		if (monthlyTransactionLimitBank >= (totalMonthlyTransactionBank + transactionAmount)) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Checks if user monthly debit transaction limit is to mark or not if the
	 * transaction is carried out
	 * 
	 * @param monthlyTransactionLimit
	 * @param totalMonthlyDebitTransactionAmount
	 * @return
	 */
	public static boolean monthlyDebitLimitCheck(double monthlyTransactionLimit, 
			double totalMonthlyDebitTransactionAmount, double transactionAmount) {
		if (monthlyTransactionLimit >= (totalMonthlyDebitTransactionAmount + transactionAmount)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if user monthly credit transaction limit is to mark or not if the
	 * transaction is carried out
	 * 
	 * @param monthlyTransactionLimit
	 * @param totalMonthlyCreditTransactionAmount
	 * @return
	 */
	public static boolean monthlyCreditLimitCheck(double monthlyTransactionLimit, 
			double totalMonthlyCreditTransactionAmount, double transactionAmount) {
		if (monthlyTransactionLimit >= (totalMonthlyCreditTransactionAmount + transactionAmount)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean monthlyLoadMoneyLimitCheck(double monthlyTransactionLimit, double totalMonthlyTransaction,
			double transactionAmount) {
		if (monthlyTransactionLimit >= (totalMonthlyTransaction + transactionAmount)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks if receiver balance limit is to mark or not if the transaction is
	 * carried out
	 * 
	 * @param balanceLimit
	 * @param receiverBalance
	 * @param transactionAmount
	 * @return
	 */
	public static boolean receiverBalanceLimit(double balanceLimit, double receiverBalance, double transactionAmount) {
		if (balanceLimit >= (receiverBalance + transactionAmount)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isAmountInMinMaxRange(double minAmount, double maxAmount, String strAmount) {
		boolean valid = true;
		try {
			double amount = Double.parseDouble(strAmount);
			if (amount >= minAmount && amount <= maxAmount) {
				System.err.println("inside the amount ::" + amount);
				valid = true;
			} else {
				valid = false;
			}
		} catch (Exception e) {
			valid = false;
		}
		return valid;
	}

	

}

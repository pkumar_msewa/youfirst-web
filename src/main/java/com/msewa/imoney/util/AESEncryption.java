package com.msewa.imoney.util;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang.StringEscapeUtils;
import org.codehaus.jettison.json.JSONObject;


import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class AESEncryption {

		private static String algorithm = "AES";
		private static byte[] keyValue = new byte[] 				
				{ 'A', 'B', 'H', 'I', 'r', 'o', 'c', 'k', 'R', 'e', 'E', 'R', 'S', 'K', 'e', 'y' };

		/**
		 * It is used to Performs Encryption
		 * 
		 * @param plainText
		 * @return
		 * @throws Exception
		 */
		public static String encrypt(String plainText) throws Exception {
			Key key = generateKey();
			Cipher chiper = Cipher.getInstance(algorithm);
			chiper.init(Cipher.ENCRYPT_MODE, key);
			byte[] encVal = chiper.doFinal(plainText.getBytes());
			String encryptedValue = new BASE64Encoder().encode(encVal);
			return encryptedValue;
		}

		/**
		 * It is used to Perform Decryption
		 * @param encrypted
		 * @return
		 * @throws Exception
		 */
		public static String decrypt(String encrypted) throws Exception {
			String encryptedText = StringEscapeUtils.unescapeJava(encrypted);
			byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedText);
			System.out.println(decordedValue.length);
			// generate key
			Key key = generateKey();
			Cipher chiper = Cipher.getInstance(algorithm);
			chiper.init(Cipher.DECRYPT_MODE, key);
			byte[] decValue = chiper.doFinal(decordedValue);
			String decryptedValue = new String(decValue);
			System.err.println("Decrypted request : "+decryptedValue);
			return decryptedValue;
		}

		/**
		 *  Performs decryption generateKey() is used to generate a secret key for AES algorithm
		 *  
		 * @return
		 * @throws Exception
		 */
		private static Key generateKey() throws Exception {
			Key key = new SecretKeySpec(keyValue, algorithm);
			return key;
		}

		// performs encryption & decryption
		public static void main(String[] args) throws Exception {
			JSONObject payload = new JSONObject();
			payload.put("session", "9ylpg4y63ohmjqenghziasqh");
			payload.put("amount", "10");
			System.err.println(encrypt(payload.toString()));
		}	
}

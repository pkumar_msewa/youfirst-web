package com.msewa.imoney.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;

import com.msewa.imoney.admin.response.CardListDTO;
import com.msewa.imoney.cardprocessor.entities.Cards;
import com.msewa.imoney.session.dto.UserSessionDTO;
import com.msewa.imoney.session.entities.UserSession;

public class ConvertUtil {

	public static List<UserSessionDTO> convertSessionList(List<UserSession> userSession) {
		List<UserSessionDTO> dtoList = new ArrayList<>();
		for (UserSession session : userSession) {
			dtoList.add(convertSession(session));
		}
		return dtoList;
	}
	
	public static UserSessionDTO convertSession(UserSession session) {
		SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
		UserSessionDTO dto = new UserSessionDTO();
		dto.setUsername(session.getUser().getUsername());
		dto.setId(session.getId());
		dto.setSessionId(session.getSessionId());
		dto.setUserId(session.getUser().getId());
		dto.setUserType(session.getUser().getUserType());
		dto.setLastRequest(time.format(session.getLastRequest()));
		return dto;
	}

	public static List<CardListDTO> getCardList(Page<Cards> pageCards) {
		List<CardListDTO> cardList = new ArrayList<>();
		if (pageCards != null && pageCards.getContent() != null) {
			List<Cards> cards = pageCards.getContent();
			if (cards != null) {
				for (Cards mmCards : cards) {
					try {
						CardListDTO cardDto = new CardListDTO();
						cardDto.setCardHashId(mmCards.getCardHashId());
						cardDto.setCardStatus(mmCards.getStatus().toString());
						cardDto.setCardType(mmCards.getCardType().getValue());
						cardDto.setCreated(CommonUtil.formatter.format(mmCards.getCreated()));
						cardDto.setEmail(mmCards.getWallet().getUser().getEmail());
						cardDto.setFirstName(mmCards.getWallet().getUser().getFirstName());
						cardDto.setLastName(mmCards.getWallet().getUser().getLastName());
						cardDto.setMobile(mmCards.getWallet().getUser().getContactNo());
						cardDto.setProxyNo(mmCards.getProxyNumber());
						cardList.add(cardDto);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		return cardList;
	}
}

package com.msewa.imoney.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;

public class CommonUtil {

	public final static String ERROR_MSG = "ERRORMSG";
	public final static String SUCCESS_MSG = "SUCCESSMSG";
	public static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	public static SimpleDateFormat dateFormate4= new SimpleDateFormat("dd-MM-yyyy");
	public static SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static SimpleDateFormat dateFormate2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	public static SimpleDateFormat dateFormate3 = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
	public static SimpleDateFormat formatter1 = new SimpleDateFormat("dd/MM/yyyy");;
	public static final String startTime = " 00:00:00";
	public static final String endTime = " 23:59:59";
	public static final int maxAuthAttempts = 5;
	
	public static final AWSCredentials credentials = new BasicAWSCredentials(StartupUtil.AWS_ACCESS_KEY_ID,
            StartupUtil.AWS_SECRET_ACCESS_KEY);
    public static final AmazonS3 client = new AmazonS3Client(credentials);
	
	
	public static String generateSixDigitNumericString(String mobileNumber) {
		int number = (int) (Math.random() * 1000000);
		if (mobileNumber.equalsIgnoreCase("8904669528")) {
			return "123456";
		}
		return String.format("%06d", number);
	}
	
	public static String base64Decode(String s) {
	    return StringUtils.newStringUtf8(Base64.decodeBase64(s));
	}
	
	public static String[] getDefaultDateRange(SimpleDateFormat dateFormate,int count) {
		String range[] = new String[2];
		Date today = new Date();
		Calendar cal = new GregorianCalendar();
		cal.setTime(today);
		cal.add(Calendar.DAY_OF_MONTH, count);
		Date today30 = cal.getTime();  
		range[0] = dateFormate.format(today30);
		range[1] = dateFormate.format(today);
		System.out.println(range[0] + ":" + range[1]);
		return range;
	}
	
	public static String uploadImage(MultipartFile file, String contact, String idType) {
		Bucket bucketName = getBucket(StartupUtil.BUCKET_NAME);
		if (bucketName == null) {
			client.createBucket(StartupUtil.BUCKET_NAME);
		}
		
		String fileKey = contact + "/" + generateFileKey(file,contact,idType);
		 client.putObject(new PutObjectRequest(StartupUtil.BUCKET_NAME, fileKey, convertFromMultipart(file))
				.withCannedAcl(CannedAccessControlList.PublicRead));
		return String.format("https://%s.s3.amazonaws.com/%s", StartupUtil.BUCKET_NAME, fileKey);
	}
	
	
	private static String generateFileKey(MultipartFile file, String contentType, String image) {
        String[] extension = contentType.split("\\/");
        return System.currentTimeMillis() + "." + extension[1];
    }
	
	private static File convertFromMultipart(MultipartFile file) {
        File convertedFile = new File(file.getOriginalFilename());
        try {
            file.transferTo(convertedFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return convertedFile;
    }
	
	private static Bucket getBucket(String bucketName) {
        Bucket result = null;
        List<Bucket> buckets = CommonUtil.client.listBuckets();
        for (Bucket b : buckets) {
            if (b.getName().equalsIgnoreCase(bucketName))
                result = b;
        }
        return result;
    }

}

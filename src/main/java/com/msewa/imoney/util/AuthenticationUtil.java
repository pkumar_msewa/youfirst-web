package com.msewa.imoney.util;

import org.json.JSONObject;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.session.service.UserDetailsWrapper;
import com.msewa.imoney.transaction.dto.request.LoadMoneyRequestDTO;


public class AuthenticationUtil {

	public static final User getCurrentUser() {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		if (authentication == null) {
			return null;
		}
		Object principal = authentication.getPrincipal();
		if (principal instanceof UserDetailsWrapper) {
			return ((UserDetailsWrapper) principal).getUser();
		}
		return null;
	}
	
	public static LoadMoneyRequestDTO prepareTopupRequest(String decdata) {
		System.err.println("decdata:: "+decdata);
		LoadMoneyRequestDTO request = new LoadMoneyRequestDTO();
        try {
            JSONObject obj = new JSONObject(decdata);
            request.setSessionId(obj.getString("sessionId"));	          
            request.setAmount(obj.getString("amount"));	                    
        } catch (Exception e) {
            e.printStackTrace();
        }
        return request;
    }
}

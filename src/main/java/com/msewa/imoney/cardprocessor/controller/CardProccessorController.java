package com.msewa.imoney.cardprocessor.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.msewa.imoney.admin.response.ResponseJson;
import com.msewa.imoney.cardprocessor.dto.request.CardBlockRequestDTO;
import com.msewa.imoney.cardprocessor.dto.request.CreateCardDTO;
import com.msewa.imoney.cardprocessor.dto.request.GenerateCardDTO;
import com.msewa.imoney.cardprocessor.dto.response.CardBlockResponseDTO;
import com.msewa.imoney.cardprocessor.dto.response.CardCreationResponseDTO;
import com.msewa.imoney.cardprocessor.dto.response.FetchCardResponseDTO;
import com.msewa.imoney.cardprocessor.dto.response.PhysicalCardUserRequest;
import com.msewa.imoney.cardprocessor.dto.response.TransactionResponse;
import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.cardprocessor.repositories.UserRepository;
import com.msewa.imoney.cardprocessor.service.IMoneyApi;
import com.msewa.imoney.enums.ResponseStatus;
import com.msewa.imoney.session.entities.UserSession;
import com.msewa.imoney.session.repositories.UserSessionRepository;
import com.msewa.imoney.session.service.ISessionApi;

/**
 * All matchmove related integrations for REST APIs
 * 
 * @author Abhijit
 * @version 1.0
 * 
 */
@Controller
@RequestMapping("/cardProcessor")
public class CardProccessorController {
	
	private final IMoneyApi iMoneyApi;
	private final ISessionApi sessionApi;
	private final UserSessionRepository userSessionRepository;
	private final UserRepository userRepository;

	public CardProccessorController(IMoneyApi iMoneyApi, ISessionApi sessionApi, 
			UserSessionRepository userSessionRepository, UserRepository userRepository) {
		this.iMoneyApi = iMoneyApi;
		this.sessionApi = sessionApi;
		this.userSessionRepository = userSessionRepository;
		this.userRepository = userRepository;
	}

	@RequestMapping(value = "/CreateCard", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<CardCreationResponseDTO> createUserInProccessor(@RequestBody CreateCardDTO createCardDto,
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
		CardCreationResponseDTO response = new CardCreationResponseDTO();
		try {
			UserSession userSession = sessionApi.getActiveUserSession(createCardDto.getSessionId());
			if (userSession != null) {
				response = iMoneyApi.processCardcreationrequest(createCardDto, userSession.getUser());
				if (response.getCode() != null) {
					return new ResponseEntity<CardCreationResponseDTO>(response, HttpStatus.OK);
				} else {
					response.setMessage(ResponseStatus.FAILURE.getKey());
					response.setCode(ResponseStatus.FAILURE.getValue());
				}
			} else {
				response.setStatus(ResponseStatus.INVALID_SESSION);
				response.setMessage(ResponseStatus.INVALID_SESSION.getKey());
				response.setCode(ResponseStatus.INVALID_SESSION.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<CardCreationResponseDTO>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/LoadCard", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<FetchCardResponseDTO> loadCardFromWallet(@RequestBody CreateCardDTO request,
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
		FetchCardResponseDTO response = new FetchCardResponseDTO();
		try {
			UserSession userSession = sessionApi.getActiveUserSession(request.getSessionId());
			if (userSession != null) {
			User user = userRepository.findByUsername(userSession.getUser().getUsername());
				if(user!=null) {
					response = iMoneyApi.processLoadCardTransaction(request);
				}else {
					response.setCode(ResponseStatus.FAILURE.getValue());
					response.setMessage("User Record Not found.");
				}
			}else {
				response.setCode(ResponseStatus.INVALID_SESSION.getValue());
				response.setMessage(ResponseStatus.INVALID_SESSION.getKey());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<FetchCardResponseDTO>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/GetCards", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<FetchCardResponseDTO> getCardsAvailable(@RequestBody GenerateCardDTO fetchCardRequest,
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
		FetchCardResponseDTO response = new FetchCardResponseDTO();
		try {
			UserSession userSession = sessionApi.getActiveUserSession(fetchCardRequest.getSessionId());
			if (userSession != null) {
				response = iMoneyApi.getAvailableCardsByCardType(fetchCardRequest, userSession.getUser());
				if (response.getCode() != null) {
					return new ResponseEntity<FetchCardResponseDTO>(response, HttpStatus.OK);
				}else {
					response.setMessage("Facing error while fetching card.");
					response.setCode(ResponseStatus.FAILURE.getValue());
				}
			} else {
				response.setMessage(ResponseStatus.INVALID_SESSION.getKey());
				response.setCode(ResponseStatus.INVALID_SESSION.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<FetchCardResponseDTO>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/GetCardDetails", method = RequestMethod.GET)
	public ResponseEntity<ResponseJson>getCardDetailByHashId(@PathVariable("cardHashId")String cardHashId,
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {

		return iMoneyApi.fetchCardDetailsByCardHashId(cardHashId);
	}
	
	@RequestMapping(value = "/GetStatement", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<TransactionResponse> getUserTransaction(@RequestBody GenerateCardDTO statementRequest,
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
		TransactionResponse response = new TransactionResponse();
		try {
			UserSession userSession = sessionApi.getActiveUserSession(statementRequest.getSessionId());
			if (userSession != null) {
				response = iMoneyApi.getUserTransactionStatement(statementRequest, userSession.getUser());
				if (response.getCode() != null) {
					response.setMessage(ResponseStatus.SUCCESS.getKey());
					response.setCode(ResponseStatus.SUCCESS.getValue());
					return new ResponseEntity<TransactionResponse>(response, HttpStatus.OK);
				} else {
					response.setStatus(ResponseStatus.FAILURE.getKey());
					response.setMessage(ResponseStatus.FAILURE.getKey());
					response.setCode(ResponseStatus.FAILURE.getValue());
				}
			} else {
				response.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				response.setMessage(ResponseStatus.INVALID_SESSION.getKey());
				response.setCode(ResponseStatus.INVALID_SESSION.getValue());

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<TransactionResponse>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/physicalUser", method = RequestMethod.POST , produces ={MediaType.APPLICATION_JSON_VALUE},
			consumes = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<ResponseJson> savekycUserDetails(@RequestBody PhysicalCardUserRequest physicalCardUserDetails,
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
		ResponseJson response = new ResponseJson();	
		if(physicalCardUserDetails!=null) {
				UserSession session = userSessionRepository.findByActiveSessionId(physicalCardUserDetails.getSessionId());
				if(session !=null) {
					User user = userRepository.findOne(session.getUser().getId());
					if(user!=null) {
						return iMoneyApi.savePhysicalUser(physicalCardUserDetails, user);
					}else {
						response.setCode(ResponseStatus.FAILURE.getValue());
						response.setMessage("user record DoesNot exists");
						response.setSuccess(false);
					}
				}response.setCode(ResponseStatus.INVALID_SESSION.getKey());
				response.setMessage("Sesion expires");
				response.setSuccess(false);
			}
		return new ResponseEntity<ResponseJson>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/cardCharge", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE},
	consumes = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<ResponseJson> physicalCardCharge(@RequestBody CreateCardDTO request,
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
		ResponseJson response = new ResponseJson();	
		try {
			if (request !=null) {
				UserSession session = userSessionRepository.findByActiveSessionId(request.getSessionId());
				if(session!=null) {
					User user = userRepository.findByUsername(session.getUser().getUsername());
					if(user !=null) {
						return iMoneyApi.processCardFeeCharge(request, user);
					}else {
						response.setCode(ResponseStatus.FAILURE.getValue());
						response.setMessage("User Record Not found.");
						response.setSuccess(false);
					}
				}else {
					response.setCode(ResponseStatus.FAILURE.getValue());
					response.setMessage("User Session Expires.");
					response.setSuccess(false);
				}
			}else {
				response.setCode(ResponseStatus.FAILURE.getValue());
				response.setMessage("Invalid request");
				response.setSuccess(false);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ResponseEntity<ResponseJson>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/BlockCard", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<CardBlockResponseDTO> processCardBlockRequest(@RequestBody CardBlockRequestDTO blockCardRequest,
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
		CardBlockResponseDTO response = new CardBlockResponseDTO();
		try {
			UserSession userSession = sessionApi.getActiveUserSession(blockCardRequest.getSessionId());
			if (userSession != null) {
				response = iMoneyApi.processCardBlockRequest(blockCardRequest, userSession.getUser());
				if (response.getCode() != null) {
					return new ResponseEntity<CardBlockResponseDTO>(response, HttpStatus.OK);
				} else {
					response.setMessage(ResponseStatus.FAILURE.getKey());
					response.setCode(ResponseStatus.FAILURE.getValue());
				}
			} else {
				response.setCode(ResponseStatus.INVALID_SESSION.getValue());
				response.setMessage(ResponseStatus.INVALID_SESSION.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<CardBlockResponseDTO>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/UnBlockCard", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<CardBlockResponseDTO> processCardUnBlockRequest(@RequestBody CardBlockRequestDTO blockCardRequest,
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
		CardBlockResponseDTO response = new CardBlockResponseDTO();
		UserSession userSession = sessionApi.getActiveUserSession(blockCardRequest.getSessionId());
		try {
			if (userSession != null) {
				response = iMoneyApi.processCardUnBlockRequest(blockCardRequest, userSession.getUser());
				if (response.getCode() != null) {
					response.setCode(ResponseStatus.SUCCESS.getValue());
				} else {
					response.setMessage(ResponseStatus.FAILURE.getKey());
					response.setCode(ResponseStatus.FAILURE.getValue());
				}
			} else {
				response.setMessage(ResponseStatus.INVALID_SESSION.getKey());
				response.setCode(ResponseStatus.INVALID_SESSION.getValue());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<CardBlockResponseDTO>(response, HttpStatus.OK);
	}
}
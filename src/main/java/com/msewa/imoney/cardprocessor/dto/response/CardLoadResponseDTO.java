package com.msewa.imoney.cardprocessor.dto.response;

public class CardLoadResponseDTO {
	
	private String cardType;
	
	private String cardIdentifier;
	
	private String transactionId;
	
	private double amount;

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardIdentifier() {
		return cardIdentifier;
	}

	public void setCardIdentifier(String cardIdentifier) {
		this.cardIdentifier = cardIdentifier;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

}

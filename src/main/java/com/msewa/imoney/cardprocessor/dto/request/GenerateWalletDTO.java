package com.msewa.imoney.cardprocessor.dto.request;

public class GenerateWalletDTO extends SessionDTO{

	private String userHashId;

	public String getUserHashId() {
		return userHashId;
	}

	public void setUserHashId(String userHashId) {
		this.userHashId = userHashId;
	}
	
	
}

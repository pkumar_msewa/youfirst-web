package com.msewa.imoney.cardprocessor.dto.response;

import com.msewa.imoney.enums.ResponseStatus;

public class CreateUserResponseDTO {

	private ResponseStatus responseStatus;
	private String code;
	private String message;
	private String userHashId;
	private Object details;
	
	
	
	public String getUserHashId() {
		return userHashId;
	}
	public void setUserHashId(String userHashId) {
		this.userHashId = userHashId;
	}
	public Object getDetails() {
		return details;
	}
	public void setDetails(Object details) {
		this.details = details;
	}
	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}
	public void setResponseStatus(ResponseStatus responseStatus) {
		this.message = responseStatus.getKey();
		this.code=responseStatus.getValue();
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}

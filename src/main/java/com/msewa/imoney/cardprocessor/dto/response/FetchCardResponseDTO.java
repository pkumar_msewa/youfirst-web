package com.msewa.imoney.cardprocessor.dto.response;

import java.util.List;

public class FetchCardResponseDTO {

	private String code;
	
	private String status;
	
	private String message;
	
	private List<CardCreationResponseDTO> cards ;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<CardCreationResponseDTO> getCards() {
		return cards;
	}

	public void setCards(List<CardCreationResponseDTO> cards) {
		this.cards = cards;
	}
	
	
}

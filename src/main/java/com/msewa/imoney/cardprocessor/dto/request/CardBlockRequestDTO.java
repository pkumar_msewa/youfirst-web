package com.msewa.imoney.cardprocessor.dto.request;

public class CardBlockRequestDTO {

	private String sessionId;

	private String cardHashId;

	private String type;
	
	private String cardType;
	
	private String username;
	
	private String reason;

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public void setCardHashId(String cardHashId) {
		this.cardHashId = cardHashId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getCardHashId() {
		return cardHashId;
	}

	public void setCarHashdId(String cardHashId) {
		this.cardHashId = cardHashId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}

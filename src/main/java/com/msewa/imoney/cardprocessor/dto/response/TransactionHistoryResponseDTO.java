package com.msewa.imoney.cardprocessor.dto.response;

public class TransactionHistoryResponseDTO {

	private double amount;

	private String card_type_code;

	private String currency;

	private String description;

	private double fee;

	private String fund_category_id;

	private String is_custom_ref_id;

	private String name;
	
	private String total;

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCard_type_code() {
		return card_type_code;
	}

	public void setCard_type_code(String card_type_code) {
		this.card_type_code = card_type_code;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	public String getFund_category_id() {
		return fund_category_id;
	}

	public void setFund_category_id(String fund_category_id) {
		this.fund_category_id = fund_category_id;
	}

	public String getIs_custom_ref_id() {
		return is_custom_ref_id;
	}

	public void setIs_custom_ref_id(String is_custom_ref_id) {
		this.is_custom_ref_id = is_custom_ref_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

}

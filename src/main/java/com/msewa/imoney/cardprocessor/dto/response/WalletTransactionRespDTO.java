package com.msewa.imoney.cardprocessor.dto.response;

import com.msewa.imoney.enums.ResponseStatus;

public class WalletTransactionRespDTO {

	private String code;
	private String message;
	private String authRetreivalNo;
	private String balance;
	
	
	
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCode() {
		return code;
	}
	public void setCode(ResponseStatus status) {
		this.code = status.getValue();
		this.message=status.getKey();
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getAuthRetreivalNo() {
		return authRetreivalNo;
	}
	public void setAuthRetreivalNo(String authRetreivalNo) {
		this.authRetreivalNo = authRetreivalNo;
	}
	
	
}

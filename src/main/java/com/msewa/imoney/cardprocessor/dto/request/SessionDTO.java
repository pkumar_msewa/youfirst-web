package com.msewa.imoney.cardprocessor.dto.request;

public class SessionDTO {

	private String sessionId;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String session) {
		this.sessionId = session;
	}
	
	
}

package com.msewa.imoney.cardprocessor.dto.request;


public class CreateCardDTO extends SessionDTO{

	private String cardType;

	private String mobileNumber;
	
	private String proxyNo;
	
	private String amount;
	
	private String cardHashId;
	

	public String getCardHashId() {
		return cardHashId;
	}

	public void setCardHashId(String cardHashId) {
		this.cardHashId = cardHashId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getProxyNo() {
		return proxyNo;
	}

	public void setProxyNo(String proxyNo) {
		this.proxyNo = proxyNo;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

}

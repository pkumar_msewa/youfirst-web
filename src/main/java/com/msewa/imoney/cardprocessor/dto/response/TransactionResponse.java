package com.msewa.imoney.cardprocessor.dto.response;


import java.util.ArrayList;


public class TransactionResponse {
	
	private String code;
	
	private String message;
	
	private String status;
	
	private ArrayList<TransactionStatementResponseDTO> transactions;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ArrayList<TransactionStatementResponseDTO> getTransactions() {
		return transactions;
	}

	public void setTransactions(ArrayList<TransactionStatementResponseDTO> transactions) {
		this.transactions = transactions;
	}

}

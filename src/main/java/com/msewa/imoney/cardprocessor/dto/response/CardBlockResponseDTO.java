package com.msewa.imoney.cardprocessor.dto.response;

import com.msewa.imoney.enums.Status;

public class CardBlockResponseDTO {

	private String code;

	private String message;
	
	private Status status;
	
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
package com.msewa.imoney.cardprocessor.dto.request;

import com.msewa.imoney.enums.CardType;

public class GenerateCardDTO {

	private String proxy_no;
	private String userHashId;
	private CardType cardType;
	private String cardId;
	private String mobileNumber;
	private String sessionId;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public CardType getCardType() {
		return cardType;
	}

	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}

	public String getUserHashId() {
		return userHashId;
	}

	public void setUserHashId(String userHashId) {
		this.userHashId = userHashId;
	}

	public String getProxy_no() {
		return proxy_no;
	}

	public void setProxy_no(String proxy_no) {
		this.proxy_no = proxy_no;
	}
	
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}


}

package com.msewa.imoney.cardprocessor.dto.request;

public class CreateUserDTO {

	private String email;
	private String password;
	private String first_name;
	private String middle_name;
	private String last_name;
	private String preferred_name;
	private String mobile_country_code;
	private String mobile;
	private String id_type;
	private String id_number;
	private String id_date_issue;
	private String birthday;
	private String country_of_issue;
	private String title;
	private String gender;
	
	
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getId_type() {
		return id_type;
	}
	public void setId_type(String id_type) {
		this.id_type = id_type;
	}
	public String getId_number() {
		return id_number;
	}
	public void setId_number(String id_number) {
		this.id_number = id_number;
	}
	public String getId_date_issue() {
		return id_date_issue;
	}
	public void setId_date_issue(String id_date_issue) {
		this.id_date_issue = id_date_issue;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getCountry_of_issue() {
		return country_of_issue;
	}
	public void setCountry_of_issue(String country_of_issue) {
		this.country_of_issue = country_of_issue;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getMiddle_name() {
		return middle_name;
	}
	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getPreferred_name() {
		return preferred_name;
	}
	public void setPreferred_name(String preferred_name) {
		this.preferred_name = preferred_name;
	}
	public String getMobile_country_code() {
		return mobile_country_code;
	}
	public void setMobile_country_code(String mobile_country_code) {
		this.mobile_country_code = mobile_country_code;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	
}

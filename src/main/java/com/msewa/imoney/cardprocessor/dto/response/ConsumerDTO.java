package com.msewa.imoney.cardprocessor.dto.response;

public class ConsumerDTO {

	private String creditPrefund;
	private String debitPrefund;
	private String code;
	private String message;
	private Object details;
	
	
	
	public Object getDetails() {
		return details;
	}
	public void setDetails(Object details) {
		this.details = details;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCreditPrefund() {
		return creditPrefund;
	}
	public void setCreditPrefund(String creditPrefund) {
		this.creditPrefund = creditPrefund;
	}
	public String getDebitPrefund() {
		return debitPrefund;
	}
	public void setDebitPrefund(String debitPrefund) {
		this.debitPrefund = debitPrefund;
	}
	
	
	
}

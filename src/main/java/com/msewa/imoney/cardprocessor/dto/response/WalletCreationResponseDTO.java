package com.msewa.imoney.cardprocessor.dto.response;

import com.msewa.imoney.enums.ResponseStatus;

public class WalletCreationResponseDTO {

	private ResponseStatus status;
	private String message;
	private String code;
	private String walletId;
	private String expiry;
	private String issue;
	private String withholdingAmt;
	private String availableAmt;
	private String walletNumber;
	
	
	public String getWalletNumber() {
		return walletNumber;
	}
	public void setWalletNumber(String walletNumber) {
		this.walletNumber = walletNumber;
	}
	public String getExpiry() {
		return expiry;
	}
	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}
	public String getIssue() {
		return issue;
	}
	public void setIssue(String issue) {
		this.issue = issue;
	}
	public String getWithholdingAmt() {
		return withholdingAmt;
	}
	public void setWithholdingAmt(String withholdingAmt) {
		this.withholdingAmt = withholdingAmt;
	}
	public String getAvailableAmt() {
		return availableAmt;
	}
	public void setAvailableAmt(String availableAmt) {
		this.availableAmt = availableAmt;
	}
	public ResponseStatus getStatus() {
		return status;
	}
	public void setStatus(ResponseStatus responseStatus) {
		this.message = responseStatus.getKey();
		this.code=responseStatus.getValue();
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getWalletId() {
		return walletId;
	}
	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}
	
	
}

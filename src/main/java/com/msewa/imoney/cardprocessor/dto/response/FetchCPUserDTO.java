package com.msewa.imoney.cardprocessor.dto.response;

public class FetchCPUserDTO {

	private String name;
	private String mobile;
	private String userHashId;
	private String dob;
	private String error;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getUserHashId() {
		return userHashId;
	}
	public void setUserHashId(String userHashId) {
		this.userHashId = userHashId;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	
}

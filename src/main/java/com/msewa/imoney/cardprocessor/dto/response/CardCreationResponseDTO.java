package com.msewa.imoney.cardprocessor.dto.response;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.springframework.http.ResponseEntity;

import com.msewa.imoney.enums.CardType;
import com.msewa.imoney.enums.ResponseStatus;

public class CardCreationResponseDTO {

	private String cardHashId;
	private String issueDate;
	private String expiryDate;
	private String cvv;
	private String cardNumber;
	private String activationCode;
	private boolean cardStatus;
	@Enumerated(EnumType.STRING)
	private CardType cardType;
	private String availableAmt;
	private String withholdingAmt;
	private ResponseStatus status;
	private String code;
	private String message;
	private String holderName;
	
	
	
	
	
	public String getHolderName() {
		return holderName;
	}
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ResponseStatus getStatus() {
		return status;
	}
	public void setStatus(ResponseStatus status) {
		this.code = status.getValue();
		this.message=status.getKey();
	}
	public String getCardHashId() {
		return cardHashId;
	}
	public void setCardHashId(String cardHashId) {
		this.cardHashId = cardHashId;
	}
	public String getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getActivationCode() {
		return activationCode;
	}
	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}
	public boolean isCardStatus() {
		return cardStatus;
	}
	public void setCardStatus(boolean cardStatus) {
		this.cardStatus = cardStatus;
	}
	public CardType getCardType() {
		return cardType;
	}
	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}
	public String getAvailableAmt() {
		return availableAmt;
	}
	public void setAvailableAmt(String availableAmt) {
		this.availableAmt = availableAmt;
	}
	public String getWithholdingAmt() {
		return withholdingAmt;
	}
	public void setWithholdingAmt(String withholdingAmt) {
		this.withholdingAmt = withholdingAmt;
	}
	
	
	
}

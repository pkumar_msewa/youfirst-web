package com.msewa.imoney.cardprocessor.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import com.msewa.imoney.admin.response.GenericResponse;
import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.enums.Status;
import com.msewa.imoney.enums.UserType;

public interface UserRepository extends CrudRepository<User, Long>, PagingAndSortingRepository<User, Long>, JpaSpecificationExecutor<User>{

	@Query("select u from User u where u.userHashId=?1")
	User findByUserHashId(String userHashId);
	@Query("select u from User u where u.username=?1")
	User findByUsername(String username);
	@Query("select u from User u where u.email=?1")
	User findByEmail(String email);
	@Modifying
	@Transactional
	@Query("update User c set c.authority=?1 where c.id =?2")
	int updateUserAuthority(String authority, long id);
	
	@Query("SELECT COUNT(u) FROM User u WHERE u.userType = ?1 AND (u.created) BETWEEN ?2 AND ?3")
	Long getAllUserByType(UserType user, Date fromDate, Date toDate);
	
	
	@Query("SELECT COUNT(u) FROM User u WHERE  authority = 'ROLE_USER,ROLE_AUTHENTICATED' "
			+ " AND u.mobileStatus = 'Active'  AND (u.created) BETWEEN ?1 AND ?2 AND u.userType = ?3")
	Long findAllActiveUser(Date fromDate, Date toDate, UserType user);
	
	
	@Query("SELECT COUNT(u) FROM User u WHERE u.authority = 'ROLE_USER,ROLE_AUTHENTICATED' "
			+ " AND u.mobileStatus = 'Inactive' AND (u.created) BETWEEN ?1 AND ?2 AND u.userType = ?3 ")
	Long findAllInactiveUser(Date fromDate, Date toDate, UserType user);
	
	@Query("SELECT COUNT(u) FROM User u WHERE u.userType = ?1 AND u.authority = ?2 AND (u.created) BETWEEN ?3 AND ?4")
	Long getAllUserByType(UserType user, String string, Date fromDate, Date toDate);
	
	@Query("SELECT u FROM User u WHERE DATE(u.created) BETWEEN ?1 AND ?2 and u.userType=?3 ")
	Page<User> getAllUserByPage(Pageable pageable, Date startDate, Date endDate, UserType user);
	
	@Query("SELECT u FROM User u WHERE u.username=?1 ")
	User getCustomUserByUsername(String username);
	
	@Query("SELECT u FROM User u WHERE u.username = ?1 AND u.mobileStatus = ?2")
	User findByUsernameAndStatus(String username, Status active);
}

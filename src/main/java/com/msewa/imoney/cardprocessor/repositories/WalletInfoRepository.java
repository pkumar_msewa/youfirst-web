package com.msewa.imoney.cardprocessor.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.cardprocessor.entities.WalletInfo;

public interface WalletInfoRepository extends CrudRepository<WalletInfo, Long>, PagingAndSortingRepository<WalletInfo, Long>, JpaSpecificationExecutor<WalletInfo>{

	@Query("select w from WalletInfo w where w.walletHashId=?1")
	WalletInfo getWalletInfoBywalletHashId(String walletHashId);
	
	@Query("select w from WalletInfo w where w.user=?1")
	WalletInfo walletInfoByUserId(User userId);
}

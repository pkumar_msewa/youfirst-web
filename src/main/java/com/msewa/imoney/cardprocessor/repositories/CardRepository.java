package com.msewa.imoney.cardprocessor.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msewa.imoney.cardprocessor.entities.Cards;
import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.cardprocessor.entities.WalletInfo;
import com.msewa.imoney.enums.CardType;
import com.msewa.imoney.enums.Status;

public interface CardRepository extends CrudRepository<Cards, Long>, PagingAndSortingRepository<Cards, Long>, JpaSpecificationExecutor<Cards>{

	@Query("select c from Cards c where c.wallet=?1 and c.cardType =?2")
	List<Cards> getCardsByWallethashId(WalletInfo walletHashId, CardType cardType);
	
	@Query("SELECT COUNT(c) FROM Cards c WHERE c.cardType = ?1 AND c.created BETWEEN ?2 AND ?3")
	Long getCardCount(CardType virtual, Date fromDate, Date toDate);
	
	@Query("SELECT COUNT(c) FROM Cards c WHERE c.status = ?1 AND c.created BETWEEN ?2 AND ?3")
	Long getCardByStatus(Status status, Date fromDate, Date toDate);

	@Query("select c from Cards c where c.cardHashId=?1")
	Cards getCardByCardHashId(String cardHashId);
	
	@Query("select c from Cards c where c.wallet=?1")
	List<Cards> getCardByWalletId(WalletInfo walletInfo);

	@Query("SELECT c FROM Cards c where c.wallet.user =?1")
	Page<Cards> getAllCards(Pageable pageable, User u);

	@Query("SELECT c FROM Cards c where c.created between ?1 and ?2")
	Page<Cards> getAllCards(Pageable pageable,Date startDate,Date endDate);
	
	@Query("select c from Cards c where c.proxyNumber=?1")
	Cards getPhysicalCardByProxyNumber(String proxyNo);
}

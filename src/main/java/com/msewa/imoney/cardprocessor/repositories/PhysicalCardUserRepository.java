package com.msewa.imoney.cardprocessor.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msewa.imoney.cardprocessor.entities.PhysicalCardUserDetails;
import com.msewa.imoney.cardprocessor.entities.User;

public interface PhysicalCardUserRepository extends CrudRepository<PhysicalCardUserDetails, Long>, JpaSpecificationExecutor<PhysicalCardUserDetails>{

	@Query("select p from PhysicalCardUserDetails p where p.user=?1")
	PhysicalCardUserDetails getPhysicaluserByUserHashId(User user);

}

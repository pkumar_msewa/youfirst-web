package com.msewa.imoney.cardprocessor.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msewa.imoney.cardprocessor.entities.Commission;

public interface CommissionRepository extends CrudRepository<Commission, Long>, JpaSpecificationExecutor<Commission>{
	
	@Query("select c from Commission c")
	Commission getCommission();

}

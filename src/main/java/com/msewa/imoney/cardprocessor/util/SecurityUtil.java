package com.msewa.imoney.cardprocessor.util;

import java.math.BigInteger;
import java.security.MessageDigest;

public class SecurityUtil {

	public static String md5(String str) throws Exception {

		MessageDigest m = MessageDigest.getInstance("MD5");

		byte[] data = str.getBytes();

		m.update(data, 0, data.length);

		BigInteger i = new BigInteger(1, m.digest());

		String hash = String.format("%1$032X", i);

		return hash;
	}
}

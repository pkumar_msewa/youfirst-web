package com.msewa.imoney.cardprocessor.service.Impl;

import javax.ws.rs.core.MultivaluedMap;

import org.json.JSONObject;
import org.springframework.http.MediaType;

import com.msewa.imoney.cardprocessor.dto.response.ConsumerDTO;
import com.msewa.imoney.cardprocessor.dto.response.WalletCreationResponseDTO;
import com.msewa.imoney.cardprocessor.dto.response.WalletTransactionRespDTO;
import com.msewa.imoney.cardprocessor.service.ICPWalletApi;
import com.msewa.imoney.cardprocessor.util.MatchMoveUtil;
import com.msewa.imoney.enums.ResponseStatus;
import com.msewa.imoney.util.CommonValidation;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class CPWalletApiImpl implements ICPWalletApi {

	@Override
	public WalletCreationResponseDTO createWallet(String userHashId) {
		WalletCreationResponseDTO createWallet = new WalletCreationResponseDTO();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets");
			ClientResponse createUser = resource.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.header("X-Auth-User-ID", userHashId).post(ClientResponse.class);
			String strResponse = createUser.getEntity(String.class);
			JSONObject createUserJson = new JSONObject(strResponse);
			if (createUser.getStatus() == 200) {
				JSONObject date = createUserJson.getJSONObject("date");
				String expiry = date.getString("expiry");
				String issue = date.getString("issued");
				String withholdingamt = createUserJson.getJSONObject("funds").getJSONObject("withholding")
						.getString("amount");
				String availabeamt = createUserJson.getJSONObject("funds").getJSONObject("available")
						.getString("amount");
				boolean walletStatus = createUserJson.getJSONObject("status").getBoolean("is_active");
				String walletId = createUserJson.getString("id");
				String walletnumber = createUserJson.getString("number");
				if (walletStatus) {
					createWallet.setStatus(ResponseStatus.SUCCESS);
					createWallet.setWalletId(walletId);
					createWallet.setIssue(issue);
					createWallet.setExpiry(expiry);
					createWallet.setWithholdingAmt(withholdingamt);
					createWallet.setAvailableAmt(availabeamt);
					createWallet.setWalletNumber(walletnumber);
				} else {
					createWallet.setStatus(ResponseStatus.FAILURE);
					createWallet.setWalletId(walletId);
					createWallet.setMessage("Wallet status not active");
				}
			} else {
				String code = createUserJson.getInt("code") + "";
				String description = createUserJson.getString("description");
				createWallet.setCode(code);
				createWallet.setMessage(description);
			}
		} catch (Exception e) {
			e.printStackTrace();
			createWallet.setStatus(ResponseStatus.EXCEPTION_OCCURED);
			return createWallet;
		}
		return createWallet;

	}

	@Override
	public WalletCreationResponseDTO fetchWallet(String userHashId) {

		WalletCreationResponseDTO createWallet = new WalletCreationResponseDTO();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets");
			ClientResponse createUser = resource.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.header("X-Auth-User-ID", userHashId).get(ClientResponse.class);
			String strResponse = createUser.getEntity(String.class);
			JSONObject createUserJson = new JSONObject(strResponse);
			if (createUser.getStatus() == 200) {
				JSONObject date = createUserJson.getJSONObject("date");
				String expiry = date.getString("expiry");
				String issue = date.getString("issued");
				String withholdingamt = createUserJson.getJSONObject("funds").getJSONObject("withholding")
						.getString("amount");
				String availabeamt = createUserJson.getJSONObject("funds").getJSONObject("available")
						.getString("amount");
				boolean walletStatus = createUserJson.getJSONObject("status").getBoolean("is_active");
				String walletId = createUserJson.getString("id");
				String walletnumber = createUserJson.getString("number");
				if (walletStatus) {
					createWallet.setStatus(ResponseStatus.SUCCESS);
					createWallet.setWalletId(walletId);
					createWallet.setIssue(issue);
					createWallet.setExpiry(expiry);
					createWallet.setWithholdingAmt(withholdingamt);
					if (CommonValidation.isNull(availabeamt)) {
						createWallet.setAvailableAmt("0.0");
					} else {
						createWallet.setAvailableAmt(availabeamt);
					}
					createWallet.setWalletNumber(walletnumber);
				} else {
					createWallet.setStatus(ResponseStatus.SUCCESS);
					createWallet.setWalletId(walletId);
				}
			} else {
				String code = createUserJson.getString("code");
				String description = createUserJson.getString("description");
				createWallet.setCode(code);
				createWallet.setMessage(description);
			}
		} catch (Exception e) {
			e.printStackTrace();
			createWallet.setStatus(ResponseStatus.EXCEPTION_OCCURED);
			createWallet.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			return createWallet;
		}
		return createWallet;

	}

	@Override
	public WalletTransactionRespDTO creditWallet(String amount, String transactionRefNo, String serviceName,
			String email) {

		WalletTransactionRespDTO walletTrx = new WalletTransactionRespDTO();

		MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
		userData.add("email", email);
		userData.add("amount", amount);
		JSONObject details = new JSONObject();
		details.put("pp", serviceName);
		details.put("payment_ref", transactionRefNo);
		userData.add("details", details.toString());
		String authorization = MatchMoveUtil.getBasicAuthorization();
		Client client = Client.create();
		client.addFilter(new LoggingFilter(System.out));
		WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
		ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
				.header("Authorization", authorization).post(ClientResponse.class, userData);
		String strResponse = resp1.getEntity(String.class);
		JSONObject user = new JSONObject(strResponse);
		if (resp1.getStatus() != 200) {
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					walletTrx.setCode(ResponseStatus.FAILURE);
					walletTrx.setMessage(description);
					return walletTrx;
				}
			}
		} else {
			String authRetrievalId = user.getString("id");
			walletTrx.setCode(ResponseStatus.SUCCESS);
			walletTrx.setAuthRetreivalNo(authRetrievalId);
			return walletTrx;

		}
		return walletTrx;

	}

	@Override
	public ConsumerDTO getConsumerData() {
		ConsumerDTO resp = new ConsumerDTO();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/oauth/consumer");
			ClientResponse clientResponse = webResource.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.get(ClientResponse.class);
			String strResp = clientResponse.getEntity(String.class);
			JSONObject response = new JSONObject(strResp);
			System.err.println("get response:::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {
				resp.setCreditPrefund(response.getJSONObject("consumer").getString("fund_floating_balance"));
				resp.setDetails(response.toString());
				resp.setMessage("Id Details saved");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

}

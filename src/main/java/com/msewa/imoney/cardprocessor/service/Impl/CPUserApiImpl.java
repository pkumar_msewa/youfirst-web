package com.msewa.imoney.cardprocessor.service.Impl;

import javax.ws.rs.core.MultivaluedMap;

import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.handler.UserRoleAuthorizationInterceptor;

import com.msewa.imoney.cardprocessor.dto.request.CreateUserDTO;
import com.msewa.imoney.cardprocessor.dto.response.CreateUserResponseDTO;
import com.msewa.imoney.cardprocessor.dto.response.FetchCPUserDTO;
import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.cardprocessor.repositories.UserRepository;
import com.msewa.imoney.cardprocessor.service.ICPUserApi;
import com.msewa.imoney.cardprocessor.util.Authorities;
import com.msewa.imoney.cardprocessor.util.MatchMoveUtil;
import com.msewa.imoney.cardprocessor.util.SecurityUtil;
import com.msewa.imoney.enums.ResponseStatus;
import com.msewa.imoney.enums.UserType;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class CPUserApiImpl implements ICPUserApi {

	

	/**
	 * creates user on card processor
	 */
	@Override
	public CreateUserResponseDTO createUserOnCardProccessor(CreateUserDTO createUserDto) {
		CreateUserResponseDTO createUserResponse=new CreateUserResponseDTO();
		MultivaluedMap<String, String> userData = new MultivaluedMapImpl();	
		userData.add("email",createUserDto.getEmail());
		try {
			userData.add("password", SecurityUtil.md5(createUserDto.getMobile()));
			userData.add("first_name",createUserDto.getFirst_name());
			userData.add("last_name",createUserDto.getLast_name());
			userData.add("mobile_country_code", "91");
			userData.add("mobile",createUserDto.getMobile());
			if(createUserDto.getMiddle_name()!=null){
			userData.add("middle_name",createUserDto.getMiddle_name());

			}
			userData.add("preferred_name",createUserDto.getPreferred_name());
			Client client=Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL+"/users");
			ClientResponse createUser = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE).
			header("Authorization", MatchMoveUtil.getBasicAuthorization()).post(ClientResponse.class,userData);
			String strResponse = createUser.getEntity(String.class);
			JSONObject createUserJson=new JSONObject(strResponse);
			
			if(createUser.getStatus()==200){
				if(createUserJson.has("id")){
				String mmUserId=createUserJson.getString("id");
				createUserResponse.setUserHashId(mmUserId);
				createUserResponse.setDetails(createUserJson.toString());
				createUserResponse.setResponseStatus(ResponseStatus.SUCCESS);
				return createUserResponse;
				}	
			}else{
				if (createUserJson != null && createUserJson.has("code")) {
					String code = createUserJson.getInt("code")+"";
					if (!code.equalsIgnoreCase("200")) {
						String description = createUserJson.getString("description");
						createUserResponse.setDetails(description);
						createUserResponse.setResponseStatus(ResponseStatus.FAILURE);
						return createUserResponse;
			}
				}
			}
			} catch (Exception e) {
				e.printStackTrace();
				createUserResponse.setResponseStatus(ResponseStatus.FAILURE);
				createUserResponse.setMessage("Registration failed.");
				return createUserResponse;
			}

		return createUserResponse;
	}

	@Override
	public CreateUserResponseDTO fetchUserDetailsFromCardProcessor(String userHashId) {
		
		CreateUserResponseDTO fetchUser=new CreateUserResponseDTO();
		try{
		Client client=Client.create();
		client.addFilter(new LoggingFilter(System.out));
		WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL+"/users");
		ClientResponse createUser = resource.header("Authorization", MatchMoveUtil.getBasicAuthorization()).
				header("X-Auth-User-ID",userHashId).get(ClientResponse.class);
		String strResponse = createUser.getEntity(String.class);
		JSONObject createUserJson=new JSONObject(strResponse);
		if(createUser.getStatus()==200){
			if(createUserJson.has("id")){
				String mmUserId=createUserJson.getString("id");
				fetchUser.setUserHashId(mmUserId);
				fetchUser.setDetails(createUserJson.toString());
				fetchUser.setResponseStatus(ResponseStatus.SUCCESS);
				return fetchUser;
				}	
			}else{
				if (createUserJson != null && createUserJson.has("code")) {
					String code = createUserJson.getString("code");
					if (!code.equalsIgnoreCase("200")) {
						String description = createUserJson.getString("description");
						fetchUser.setDetails(description);
						fetchUser.setResponseStatus(ResponseStatus.FAILURE);
						return fetchUser;
			}
				}
	
		}
		}catch(Exception e){
			e.printStackTrace();
			fetchUser.setResponseStatus(ResponseStatus.EXCEPTION_OCCURED);
			return fetchUser;

			
		}
		return fetchUser;
	}

	@Override
	public CreateUserResponseDTO updateUserDetailsOnCardProcessor(CreateUserDTO dto) {
		CreateUserResponseDTO updateUser=new CreateUserResponseDTO();
		MultivaluedMap<String, String> userData = new MultivaluedMapImpl();	
		try {
			
			userData.add("title",dto.getTitle());
			userData.add("id_type",dto.getId_type());
			userData.add("id_number",dto.getFirst_name());
			userData.add("country_of_issue",dto.getCountry_of_issue());
			userData.add("birthday",dto.getBirthday());
			userData.add("gender",dto.getGender());
			Client client=Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL+"/users");
			ClientResponse createUser = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE).
			header("Authorization", MatchMoveUtil.getBasicAuthorization()).put(ClientResponse.class,userData);
		    if(createUser.getStatus()==200){
		    	updateUser.setResponseStatus(ResponseStatus.SUCCESS);
		    }else{
		    	updateUser.setResponseStatus(ResponseStatus.FAILURE);

		    }
			}catch(Exception e){
				e.printStackTrace();
				updateUser.setResponseStatus(ResponseStatus.EXCEPTION_OCCURED);
				return updateUser;
			}
			return updateUser;
	}
	
	

}

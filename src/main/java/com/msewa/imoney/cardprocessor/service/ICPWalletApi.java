package com.msewa.imoney.cardprocessor.service;

import com.msewa.imoney.cardprocessor.dto.response.ConsumerDTO;
import com.msewa.imoney.cardprocessor.dto.response.WalletCreationResponseDTO;
import com.msewa.imoney.cardprocessor.dto.response.WalletTransactionRespDTO;
import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.cardprocessor.entities.WalletInfo;

public interface ICPWalletApi {

	WalletCreationResponseDTO createWallet(String userHashId);
	WalletCreationResponseDTO fetchWallet(String userHashId);
	WalletTransactionRespDTO creditWallet(String amount,String transactionRefNo,String serviceName,String email);
	ConsumerDTO getConsumerData();

}

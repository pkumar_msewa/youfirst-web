package com.msewa.imoney.cardprocessor.service;

import com.msewa.imoney.cardprocessor.dto.request.CreateUserDTO;
import com.msewa.imoney.cardprocessor.dto.response.CreateUserResponseDTO;
import com.msewa.imoney.cardprocessor.dto.response.FetchCPUserDTO;

public interface ICPUserApi {

	CreateUserResponseDTO createUserOnCardProccessor(CreateUserDTO createUserDto);
	CreateUserResponseDTO fetchUserDetailsFromCardProcessor(String userHashId);
	CreateUserResponseDTO updateUserDetailsOnCardProcessor(CreateUserDTO dto);
}

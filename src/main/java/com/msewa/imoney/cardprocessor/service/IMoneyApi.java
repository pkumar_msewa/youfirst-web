package com.msewa.imoney.cardprocessor.service;

import org.springframework.http.ResponseEntity;

import com.msewa.imoney.admin.response.ResponseJson;
import com.msewa.imoney.cardprocessor.dto.request.CardBlockRequestDTO;
import com.msewa.imoney.cardprocessor.dto.request.CreateCardDTO;
import com.msewa.imoney.cardprocessor.dto.request.GenerateCardDTO;
import com.msewa.imoney.cardprocessor.dto.response.CardBlockResponseDTO;
import com.msewa.imoney.cardprocessor.dto.response.CardCreationResponseDTO;
import com.msewa.imoney.cardprocessor.dto.response.FetchCardResponseDTO;
import com.msewa.imoney.cardprocessor.dto.response.PhysicalCardUserRequest;
import com.msewa.imoney.cardprocessor.dto.response.TransactionResponse;
import com.msewa.imoney.cardprocessor.entities.User;

public interface IMoneyApi {

	String getSecurityTokens(String cardId, String hashId);

	FetchCardResponseDTO getAvailableCardsByCardType(GenerateCardDTO fetchCardRequest, User user);

	CardCreationResponseDTO processCardcreationrequest(CreateCardDTO createCardDto, User user);

	
	TransactionResponse getUserTransactionStatement(GenerateCardDTO statementRequest, User user);

	CardBlockResponseDTO processCardBlockRequest(CardBlockRequestDTO blockCardRequest, User user);

	CardBlockResponseDTO processCardUnBlockRequest(CardBlockRequestDTO blockCardRequest, User user);
	
	TransactionResponse getTransactionResponse(User user);

	ResponseEntity<ResponseJson> fetchCardDetailsByCardHashId(String cardHashId);
	
	CardBlockResponseDTO getCardBlockUnblockResp(CardBlockRequestDTO blockCardRequest, String userHashId);
	
	CardBlockResponseDTO getCardUnblocResp(CardBlockRequestDTO blockCardRequest, String userHashId);

	String getPrefundBalance(User user);

	ResponseEntity<ResponseJson> savePhysicalUser(PhysicalCardUserRequest physicalCardUserDetails, User user);

	ResponseEntity<ResponseJson> processCardFeeCharge(CreateCardDTO request, User user);

	ResponseJson activatePhysicalCard(CreateCardDTO request, User user);

	FetchCardResponseDTO processLoadCardTransaction(CreateCardDTO request);


}

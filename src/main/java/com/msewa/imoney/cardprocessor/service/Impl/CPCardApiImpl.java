package com.msewa.imoney.cardprocessor.service.Impl;

import javax.ws.rs.core.MultivaluedMap;

import org.json.JSONObject;

import com.msewa.imoney.cardprocessor.dto.request.GenerateCardDTO;
import com.msewa.imoney.cardprocessor.dto.response.CardCreationResponseDTO;
import com.msewa.imoney.cardprocessor.service.ICPCardApi;
import com.msewa.imoney.cardprocessor.util.MatchMoveUtil;
import com.msewa.imoney.enums.CardType;
import com.msewa.imoney.enums.ResponseStatus;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class CPCardApiImpl implements ICPCardApi{

	@Override
	public CardCreationResponseDTO createCard(GenerateCardDTO cardDto) {
		String strResponse=null;
		ClientResponse createCard=null; 
		CardCreationResponseDTO cardCreationResp=new CardCreationResponseDTO();
		Client client=Client.create();
		client.addFilter(new LoggingFilter(System.out));
		if(cardDto.getCardType().getValue().equalsIgnoreCase("virtual")){
		WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL+"/users/wallets/cards"+MatchMoveUtil.MATCHMOVE_CARDTYPE_VIRTUAL);
		createCard = resource.header("Authorization", MatchMoveUtil.getBasicAuthorization()).
				header("X-Auth-User-ID",cardDto.getUserHashId()).post(ClientResponse.class);
			strResponse = createCard.getEntity(String.class);
		}else{
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();	
			userData.add("assoc_number", "PY"+cardDto.getProxy_no());
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL+"/users/wallets/cards"+MatchMoveUtil.MATCHMOVE_CARDTYPE_PHYSICAL);
			createCard = resource.header("Authorization", MatchMoveUtil.getBasicAuthorization()).
					header("X-Auth-User-ID",cardDto.getUserHashId()).post(ClientResponse.class,userData);
			strResponse = createCard.getEntity(String.class);
		}
		JSONObject createCardJson=new JSONObject(strResponse);
		if(createCard.getStatus()==200){
			JSONObject date = createCardJson.getJSONObject("date");
			String expiry = date.getString("expiry");
			String issue = date.getString("issued");
			String withholdingamt = createCardJson.getJSONObject("funds").getJSONObject("withholding").getString("amount");
			String availabeamt = createCardJson.getJSONObject("funds").getJSONObject("available").getString("amount");
			boolean cardstatus = createCardJson.getJSONObject("status").getBoolean("is_active");
			String cardId = createCardJson.getString("id");
			String cardNumber = createCardJson.getString("number");
			String activationCode=createCardJson.getString("activation_code");
			String cvv=getSecurityTokens(cardId, cardDto.getUserHashId());
			cardCreationResp.setActivationCode(activationCode);
			cardCreationResp.setCardHashId(cardId);
			cardCreationResp.setCardNumber(cardNumber);
			cardCreationResp.setCardStatus(cardstatus);
			if(cardDto.getCardType().getValue().equalsIgnoreCase("virtual")){
			cardCreationResp.setCardType(CardType.virtual);
			}else{
				cardCreationResp.setCardType(CardType.physical);
			}
			cardCreationResp.setCvv(cvv);
			cardCreationResp.setExpiryDate(expiry);
			cardCreationResp.setIssueDate(issue);
			cardCreationResp.setWithholdingAmt(withholdingamt);
			cardCreationResp.setAvailableAmt(availabeamt);
			cardCreationResp.setStatus(ResponseStatus.SUCCESS);
		}else{
			if(createCardJson.has("description")){
			String description = createCardJson.getString("description");
			cardCreationResp.setMessage(description);
			cardCreationResp.setCode("F00");
			}
		}
		return cardCreationResp;
	}

	@Override
	public CardCreationResponseDTO inquireCard(GenerateCardDTO cardDto) {
		
		CardCreationResponseDTO cardInquiry=new CardCreationResponseDTO();
		Client client = Client.create();
		client.addFilter(new LoggingFilter(System.out));
		WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL+"/users/wallets/cards/"+cardDto.getCardId());
		ClientResponse clientResponse = resource.accept("application/json").header("Authorization", MatchMoveUtil.getBasicAuthorization()).header("X-Auth-User-Id",cardDto.getUserHashId()).get(ClientResponse.class);
		String strResponse = clientResponse.getEntity(String.class);
		JSONObject cardInqJson = new JSONObject(strResponse);
		if(clientResponse.getStatus()==200){
			JSONObject date = cardInqJson.getJSONObject("date");
			String expiry = date.getString("expiry");
			String issue = date.getString("issued");
			String holderName = cardInqJson.getJSONObject("holder").getString("name");
			if(cardInqJson.has("funds")){
			String withholdingamt = cardInqJson.getJSONObject("funds").getJSONObject("withholding").getString("amount");
			String availabeamt = cardInqJson.getJSONObject("funds").getJSONObject("available").getString("amount");
			cardInquiry.setWithholdingAmt(withholdingamt);
			cardInquiry.setAvailableAmt(availabeamt);
			}
			boolean cardstatus =cardInqJson.getJSONObject("status").getBoolean("is_active");
			String text=cardInqJson.getJSONObject("status").getString("text");
			String cardId = cardInqJson.getString("id");
			String cardnumber = cardInqJson.getString("number");
			String expDate = expiry.substring(2,4)+"/" + expiry.substring(5,7);
			String cvv=getSecurityTokens(cardId, cardDto.getUserHashId());
			cardInquiry.setCardHashId(cardId);
			cardInquiry.setCardNumber(cardnumber);
			cardInquiry.setCardStatus(cardstatus);
			if(cardDto.getCardType().getValue().equalsIgnoreCase("virtual")){
				cardInquiry.setCardType(CardType.virtual);
			}else{
				cardInquiry.setCardType(CardType.physical);
			}
			cardInquiry.setCvv(cvv);
			cardInquiry.setExpiryDate(expDate);
			cardInquiry.setIssueDate(issue);
			cardInquiry.setStatus(ResponseStatus.SUCCESS);
			cardInquiry.setHolderName(holderName);
		}else{
			if(cardInqJson.has("description")){
			String description = cardInqJson.getString("description");
			cardInquiry.setMessage(description);
			cardInquiry.setCode("F00");
			}


		}
		return cardInquiry;
	}

	@Override
	public String getSecurityTokens(String cardId,String userHashId){
		Client client = Client.create();
		client.addFilter(new LoggingFilter(System.out));
		WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL+"/users/wallets/cards/"+cardId + "/securities/tokens");
		ClientResponse clientResponse = resource.accept("application/json").header("Authorization", MatchMoveUtil.getBasicAuthorization()).header("X-Auth-User-Id",userHashId).get(ClientResponse.class);
		String strResponse = clientResponse.getEntity(String.class);
		JSONObject cvvRequest = new JSONObject(strResponse);
		String cvv = null;
		if(clientResponse.getStatus()==200){
		if (cvvRequest.has("value")) {
			cvv = cvvRequest.getString("value");
		} else {
			cvv = "xxx";
		}
		}else{
			cvv = "xxx";
		}
		return cvv;
	}
}

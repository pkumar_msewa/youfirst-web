package com.msewa.imoney.cardprocessor.service;

import com.msewa.imoney.cardprocessor.dto.request.GenerateCardDTO;
import com.msewa.imoney.cardprocessor.dto.response.CardCreationResponseDTO;

public interface ICPCardApi {

	CardCreationResponseDTO createCard(GenerateCardDTO cardDto);
	CardCreationResponseDTO inquireCard(GenerateCardDTO cardDto);
	String getSecurityTokens(String cardId, String userHashId);
}

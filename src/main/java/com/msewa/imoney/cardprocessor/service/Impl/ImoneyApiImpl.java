package com.msewa.imoney.cardprocessor.service.Impl;

import java.security.Security;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.msewa.imoney.admin.response.ResponseJson;
import com.msewa.imoney.cardprocessor.dto.request.CardBlockRequestDTO;
import com.msewa.imoney.cardprocessor.dto.request.CreateCardDTO;
import com.msewa.imoney.cardprocessor.dto.request.GenerateCardDTO;
import com.msewa.imoney.cardprocessor.dto.response.CardBlockResponseDTO;
import com.msewa.imoney.cardprocessor.dto.response.CardCreationResponseDTO;
import com.msewa.imoney.cardprocessor.dto.response.CardFeeCharge;
import com.msewa.imoney.cardprocessor.dto.response.FetchCardResponseDTO;
import com.msewa.imoney.cardprocessor.dto.response.PhysicalCardUserRequest;
import com.msewa.imoney.cardprocessor.dto.response.TransactionResponse;
import com.msewa.imoney.cardprocessor.dto.response.TransactionStatementResponseDTO;
import com.msewa.imoney.cardprocessor.entities.Cards;
import com.msewa.imoney.cardprocessor.entities.Commission;
import com.msewa.imoney.cardprocessor.entities.PhysicalCardUserDetails;
import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.cardprocessor.entities.WalletInfo;
import com.msewa.imoney.cardprocessor.repositories.CardRepository;
import com.msewa.imoney.cardprocessor.repositories.PhysicalCardUserRepository;
import com.msewa.imoney.cardprocessor.repositories.UserRepository;
import com.msewa.imoney.cardprocessor.repositories.WalletInfoRepository;
import com.msewa.imoney.cardprocessor.service.IMoneyApi;
import com.msewa.imoney.cardprocessor.util.MatchMoveUtil;
import com.msewa.imoney.enums.CardType;
import com.msewa.imoney.enums.ResponseStatus;
import com.msewa.imoney.enums.Status;
import com.msewa.imoney.javasdk.Connection;
import com.msewa.imoney.javasdk.HttpRequest;
import com.msewa.imoney.session.entities.UserSession;
import com.msewa.imoney.session.repositories.UserSessionRepository;
import com.msewa.imoney.transaction.entities.Services;
import com.msewa.imoney.transaction.entities.Transactions;
import com.msewa.imoney.transaction.repositories.ServiceRepository;
import com.msewa.imoney.transaction.repositories.TransactionRepository;
import com.msewa.imoney.util.CommonValidation;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class ImoneyApiImpl implements IMoneyApi {

	private final UserRepository userRepository;
	private final UserSessionRepository userSessionRepository;
	private final WalletInfoRepository walletInfoRepository;
	private final CardRepository cardRepository;
	private final ServiceRepository serviceRepository;
	private final PhysicalCardUserRepository physicalCardUserRepository;
	private final TransactionRepository transactionRepository;

	public ImoneyApiImpl(UserRepository userRepository, UserSessionRepository userSessionRepository,
			WalletInfoRepository walletInfoRepository, CardRepository cardRepository,
			ServiceRepository serviceRepository, PhysicalCardUserRepository physicalCardUserRepository,
			TransactionRepository transactionRepository) {
		super();
		this.userRepository = userRepository;
		this.userSessionRepository = userSessionRepository;
		this.walletInfoRepository = walletInfoRepository;
		this.cardRepository = cardRepository;
		this.serviceRepository = serviceRepository;
		this.physicalCardUserRepository = physicalCardUserRepository;
		this.transactionRepository = transactionRepository;
	}

	@Override
	public CardCreationResponseDTO processCardcreationrequest(CreateCardDTO createCardDto, User user) {
		CardCreationResponseDTO cardCreationResp = new CardCreationResponseDTO();
		try {
			String strResponse = null;
			ClientResponse createCard = null;
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			if (createCardDto.getCardType().equalsIgnoreCase("virtual")) {
				WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/"
						+ MatchMoveUtil.MATCHMOVE_CARDTYPE_VIRTUAL);
				createCard = resource.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-ID", user.getUserHashId()).post(ClientResponse.class);
				strResponse = createCard.getEntity(String.class);
			} else if (createCardDto.getCardType().equalsIgnoreCase("gift")) {
				WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/"
						+ MatchMoveUtil.MATCHMOVE_CARDTYPE_VIRTUAL_GIFT);
				createCard = resource.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-ID", user.getUserHashId()).post(ClientResponse.class);
				strResponse = createCard.getEntity(String.class);
			}

			else {
				MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
				userData.add("assoc_number", "PY" + createCardDto.getProxyNo());

				WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/"
						+ MatchMoveUtil.MATCHMOVE_CARDTYPE_PHYSICAL);
				createCard = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-ID", user.getUserHashId()).post(ClientResponse.class, userData);
				strResponse = createCard.getEntity(String.class);
			}

			JSONObject createCardJson = new JSONObject(strResponse);
			if (createCard.getStatus() == 200) {
				JSONObject date = createCardJson.getJSONObject("date");
				String expiry = date.getString("expiry");
				String issue = date.getString("issued");
				String withholdingamt = createCardJson.getJSONObject("funds").getJSONObject("withholding")
						.getString("amount");
				String availabeamt = createCardJson.getJSONObject("funds").getJSONObject("available")
						.getString("amount");
				boolean cardstatus = createCardJson.getJSONObject("status").getBoolean("is_active");
				String cardId = createCardJson.getString("id");
				String cardNumber = createCardJson.getString("number");
				String holderName = createCardJson.getJSONObject("holder").getString("name");

				Client client1 = Client.create();
				client1.addFilter(new LoggingFilter(System.out));
				WebResource resource1 = client1.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets");
				ClientResponse resp2 = resource1.accept("application/json")
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", user.getUserHashId()).get(ClientResponse.class);
				String strResponse1 = resp2.getEntity(String.class);
				JSONObject walletResp = new JSONObject(strResponse1);
				if (resp2.getStatus() != 200) {
					cardCreationResp.setCode(ResponseStatus.FAILURE.getValue());
					cardCreationResp.setMessage("Wallet Creation Failed.");
				}
				String walletNum = walletResp.getString("id");
				WalletInfo wallet = walletInfoRepository.getWalletInfoBywalletHashId(walletNum);
				String cvv = getSecurityTokens(cardId, user.getUserHashId());
				cardCreationResp.setHolderName(holderName);
				cardCreationResp.setCardHashId(cardId);
				cardCreationResp.setCardNumber(cardNumber);
				cardCreationResp.setCardStatus(cardstatus);

				Cards card = new Cards();
				card.setCardHashId(cardId);
				if (createCardDto.getCardType().equalsIgnoreCase("virtual")) {
					cardCreationResp.setCardType(CardType.virtual);
					card.setCardType(CardType.virtual);
				} else if (createCardDto.getCardType().equalsIgnoreCase("gift")) {
					cardCreationResp.setCardType(CardType.gift);
					card.setCardType(CardType.gift);
				} else {
					try {
						Cards cardProxy = cardRepository.getPhysicalCardByProxyNumber(createCardDto.getProxyNo());
						if (cardProxy != null) {
							MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
							userData.add("activation_code", cardProxy.getActivationCode());

							Client physical = Client.create();
							client.addFilter(new LoggingFilter(System.out));
							WebResource resource = physical.resource(MatchMoveUtil.MATCHMOVE_HOST_URL
									+ "/users/wallets/cards/" + cardProxy.getCardHashId());
							ClientResponse clientResponse = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
									.header("Authorization", MatchMoveUtil.getBasicAuthorization())
									.header("X-Auth-User-Id", user.getUserHashId()).put(ClientResponse.class, userData);
							String cardActivation = clientResponse.getEntity(String.class);
							JSONObject activateJson = new JSONObject(cardActivation);
							if (clientResponse.getStatus() != 200) {
								cardCreationResp.setCode(ResponseStatus.FAILURE.getValue());
								cardCreationResp.setMessage("Card Activation failed.");
							} else {
								cardCreationResp.setActivationCode(createCardJson.getString("activation_code"));
								cardCreationResp.setCardType(CardType.physical);
								card.setCardType(CardType.physical);
								card.setProxyNumber(createCardDto.getProxyNo());
								card.setActivationCode(activateJson.getString("activation_code"));
								cardCreationResp.setCode(ResponseStatus.SUCCESS.getValue());
								cardCreationResp.setMessage("Physical Card Activated successfully.");
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				card.setStatus(Status.Active);
				card.setWallet(wallet);
				card.setRemarks(strResponse);
				cardRepository.save(card);
				cardCreationResp.setCvv(cvv);
				cardCreationResp.setExpiryDate(expiry);
				cardCreationResp.setIssueDate(issue);
				cardCreationResp.setWithholdingAmt(withholdingamt);
				cardCreationResp.setAvailableAmt(availabeamt);
				cardCreationResp.setStatus(ResponseStatus.SUCCESS);
			} else {
				if (createCardJson.has("description")) {
					String description = createCardJson.getString("description");
					cardCreationResp.setMessage(description);
				}
				cardCreationResp.setCode(ResponseStatus.FAILURE.getValue());
				cardCreationResp.setMessage(createCardJson.getString("description"));
			}
		} catch (Exception e) {
			cardCreationResp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			cardCreationResp.setMessage(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
			e.printStackTrace();
		}
		return cardCreationResp;
	}

	@Override
	public String getSecurityTokens(String cardId, String hashId) {
		String cvv = null;
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(
					MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId + "/securities/tokens");
			ClientResponse clientResponse = resource.accept("application/json")
					.header("Authorization", MatchMoveUtil.getBasicAuthorization()).header("X-Auth-User-Id", hashId)
					.get(ClientResponse.class);
			String strResponse = clientResponse.getEntity(String.class);
			JSONObject cvvRequest = new JSONObject(strResponse);
			if (clientResponse.getStatus() == 200) {
				if (cvvRequest.has("value")) {
					cvv = cvvRequest.getString("value");
				} else {
					cvv = "xxx";
				}
			} else {
				cvv = "xxx";
			}
			return cvv;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cvv;
	}

	@Override
	public FetchCardResponseDTO getAvailableCardsByCardType(GenerateCardDTO fetchCardRequest, User user) {
		FetchCardResponseDTO response = new FetchCardResponseDTO();
		try {
			WalletInfo wallet = walletInfoRepository.walletInfoByUserId(user);
			List<Cards> cardList = new ArrayList<>();
			if (fetchCardRequest.getCardType().equals(CardType.virtual)) {
				cardList = cardRepository.getCardsByWallethashId(wallet, CardType.virtual);

			} else if (fetchCardRequest.getCardType().equals(CardType.gift)) {
				cardList = cardRepository.getCardsByWallethashId(wallet, CardType.gift);
			}
			response = getCards(cardList, user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	private FetchCardResponseDTO getCards(List<Cards> cardList, User user) {
		FetchCardResponseDTO response = new FetchCardResponseDTO();
		if (cardList != null && !cardList.isEmpty()) {
			List<CardCreationResponseDTO> cards = new ArrayList<CardCreationResponseDTO>();
			for (Cards cards2 : cardList) {
				try {
					CardCreationResponseDTO cardInquiry = new CardCreationResponseDTO();
					Client client = Client.create();
					client.addFilter(new LoggingFilter(System.out));
					WebResource resource = client.resource(
							MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cards2.getCardHashId());
					ClientResponse clientResponse = resource
							.header("Authorization", MatchMoveUtil.getBasicAuthorization())
							.header("X-Auth-User-Id", user.getUserHashId()).get(ClientResponse.class);
					String strResponse = clientResponse.getEntity(String.class);
					JSONObject cardInqJson = new JSONObject(strResponse);
					if (clientResponse.getStatus() != 200) {
						response.setCode(ResponseStatus.FAILURE.getValue());
						response.setMessage("Error!while fetching card.");
						return response;
					} else {
						JSONObject date = cardInqJson.getJSONObject("date");
						cardInquiry.setCode(ResponseStatus.SUCCESS.getValue());
						cardInquiry.setMessage("Card fetch Success");
						cardInquiry.setStatus(ResponseStatus.SUCCESS);
						cardInquiry.setAvailableAmt(
								cardInqJson.getJSONObject("funds").getJSONObject("available").getString("amount"));
						cardInquiry.setCardHashId(cardInqJson.getString("id"));
						cardInquiry.setCardNumber(cardInqJson.getString("number"));
						cardInquiry.setCvv(getSecurityTokens(cardInquiry.getCardHashId(), user.getUserHashId()));
						if (cards2.getCardType().equals(CardType.virtual)) {
							cardInquiry.setCardType(CardType.virtual);
						} else if (cards2.getCardType().equals(CardType.gift)) {
							cardInquiry.setCardType(CardType.gift);
						} else {
							cardInquiry.setCardType(CardType.physical);
						}
						cardInquiry.setCardStatus(cardInqJson.getJSONObject("status").getBoolean("is_active"));
						cardInquiry.setWithholdingAmt(
								cardInqJson.getJSONObject("funds").getJSONObject("withholding").getString("amount"));
						cardInquiry.setExpiryDate(date.getString("expiry"));
						cardInquiry.setHolderName(cardInqJson.getJSONObject("holder").getString("name"));
						cardInquiry.setIssueDate(date.getString("issued"));
						cards.add(cardInquiry);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			response.setStatus(ResponseStatus.SUCCESS.getKey());
			response.setCards(cards);
			response.setCode(ResponseStatus.SUCCESS.getValue());
			response.setMessage("cards fetch successfully");
		} else {
			response.setCode(ResponseStatus.FAILURE.getValue());
			response.setMessage("No card found!Please create your card.");
		}
		return response;
	}

	@Override
	public TransactionResponse getUserTransactionStatement(GenerateCardDTO statementRequest, User user) {
		TransactionResponse transactionResponse = null;
		try {
			transactionResponse = getTransactionResponse(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return transactionResponse;
	}

	@Override
	public TransactionResponse getTransactionResponse(User user) {
		TransactionResponse transactionResponse = new TransactionResponse();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/transactions");
			ClientResponse response = resource.accept("application/json")
					.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.header("X-Auth-User-Id", user.getUserHashId()).get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			JSONObject statement = new JSONObject(strResponse);

			if (response.getStatus() != 200) {
				transactionResponse.setStatus(ResponseStatus.FAILURE.getKey());
				transactionResponse.setMessage("Transaction not Found");
				transactionResponse.setCode(ResponseStatus.FAILURE.getValue());
			}
			ArrayList<TransactionStatementResponseDTO> transactionList = new ArrayList<TransactionStatementResponseDTO>();
			JSONArray transactions = statement.getJSONArray("transactions");
			for (int i = 0; i < transactions.length(); i++) {

				JSONObject transaction = transactions.getJSONObject(i);
				TransactionStatementResponseDTO transactionStatementResponse = new TransactionStatementResponseDTO();
				try {
					transactionStatementResponse.setAmount(transaction.getString("amount"));
					transactionStatementResponse.setStatus(transaction.getString("status"));
					transactionStatementResponse.setDescription(transaction.getString("description"));
					transactionStatementResponse.setIndicator(transaction.getString("indicator"));
					transactionStatementResponse.setBalance(transaction.getString("balance"));
					transactionStatementResponse.setCurrency(transaction.getString("currency"));
					transactionStatementResponse
							.setType(getType(transaction.getString("type"), transaction.getString("amount"),
									transaction.getJSONObject("details"), transaction.getString("description")));
					transactionStatementResponse.setName(transaction.getJSONObject("details").getString("name"));
					transactionStatementResponse.setDate(transaction.getString("date").substring(0, 10));
				} catch (Exception e) {
					e.printStackTrace();
				}
				transactionList.add(transactionStatementResponse);
			}
			transactionResponse.setTransactions(transactionList);
			transactionResponse.setStatus(ResponseStatus.SUCCESS.getKey());
			transactionResponse.setMessage("Transaction Found Successfully");
			transactionResponse.setCode(ResponseStatus.SUCCESS.getValue());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return transactionResponse;
	}

	private String getType(String type, String amount, JSONObject object, String description) {
		if (type != null) {
			try {
				if (type.trim().equalsIgnoreCase("Purchase")) {
					object.getString("merchantname");
				} else if (type.trim().equalsIgnoreCase("Load")) {
					return "Load money of Rs " + amount;
				} else if (type.trim().equalsIgnoreCase("Unload")) {
					return "Send Money of Rs " + amount;
				} else if (type.trim().equalsIgnoreCase("Get Card")) {
					return "Card Fees";
				} else if (type.trim().equalsIgnoreCase("Money Transfer Debit")) {
					return description;
				} else {
					return "Amount loaded into wallet";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			return "Load card through agent";
		}
		return "";
	}

	@Override
	public CardBlockResponseDTO processCardBlockRequest(CardBlockRequestDTO blockCardRequest, User user) {
		return getCardBlockUnblockResp(blockCardRequest, user.getUserHashId());
	}

	@Override
	public CardBlockResponseDTO getCardBlockUnblockResp(CardBlockRequestDTO blockCardRequest, String userHashId) {
		CardBlockResponseDTO cardblockResponse = new CardBlockResponseDTO();
		try {
			Cards inquireCard = cardRepository.getCardByCardHashId(blockCardRequest.getCardHashId());
			if (inquireCard != null && inquireCard.getStatus().equals(Status.Active)) {
				MultivaluedMap<String, String> userdata = new MultivaluedMapImpl();
				userdata.add("type", blockCardRequest.getType());

				Client client = Client.create();
				WebResource blockCardResource = client.resource(
						MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + blockCardRequest.getCardHashId());
				ClientResponse clientResponse = blockCardResource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", userHashId).delete(ClientResponse.class);
				String cardBlockresponse = clientResponse.getEntity(String.class);
				JSONObject cardBlockJson = new JSONObject(cardBlockresponse);
				if (clientResponse.getStatus() != 200) {
					cardblockResponse.setCode(ResponseStatus.FAILURE.getValue());
					cardblockResponse.setMessage(ResponseStatus.FAILURE.getKey());
				} else {
					cardblockResponse.setCode(ResponseStatus.SUCCESS.getValue());
					cardblockResponse.setMessage("Card " + blockCardRequest.getType() + " successfully done");
					cardblockResponse.setStatus(Status.Locked);
					String cardHashId = cardBlockJson.getString("id");
					String status = cardBlockJson.getString("status");

					Cards card = cardRepository.getCardByCardHashId(cardHashId);
					if (status.equalsIgnoreCase(Status.Locked.getValue())) {
						card.setStatus(Status.Locked);
					} else if (status.equalsIgnoreCase(Status.Blocked.getValue())) {
						card.setStatus(Status.Blocked);
					}
					card.setReason(blockCardRequest.getReason());
					cardRepository.save(card);
				}
			} else {
				cardblockResponse.setCode(ResponseStatus.CARD_STATUS_INACTIVE.getValue());
				cardblockResponse.setMessage(ResponseStatus.CARD_STATUS_INACTIVE.getKey());

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cardblockResponse;
	}

	@Override
	public CardBlockResponseDTO processCardUnBlockRequest(CardBlockRequestDTO blockCardRequest, User user) {
		return getCardUnblocResp(blockCardRequest, user.getUserHashId());
	}

	@Override
	public CardBlockResponseDTO getCardUnblocResp(CardBlockRequestDTO blockCardRequest, String userHashId) {
		CardBlockResponseDTO unBlockResponse = new CardBlockResponseDTO();
		try {
			Cards inquireCard = cardRepository.getCardByCardHashId(blockCardRequest.getCardHashId());
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("id", blockCardRequest.getCardHashId());

			ClientResponse clientResponse = null;
			String cardBlockresponse = null;
			if (inquireCard != null && inquireCard.getStatus().equals(Status.Locked)) {
				Client client = Client.create();
				if (blockCardRequest.getCardType().equalsIgnoreCase("virtual")) {
					WebResource blockCardResource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL
							+ "/users/wallets/cards/" + MatchMoveUtil.MATCHMOVE_CARDTYPE_VIRTUAL);
					clientResponse = blockCardResource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
							.header("Authorization", MatchMoveUtil.getBasicAuthorization())
							.header("X-Auth-User-Id", userHashId).post(ClientResponse.class, userData);
					cardBlockresponse = clientResponse.getEntity(String.class);
				} else if (blockCardRequest.getCardType().equalsIgnoreCase("physical")) {
					WebResource blockCardResource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL
							+ "/users/wallets/cards/" + MatchMoveUtil.MATCHMOVE_CARDTYPE_PHYSICAL);
					clientResponse = blockCardResource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
							.header("Authorization", MatchMoveUtil.getBasicAuthorization())
							.header("X-Auth-User-Id", userHashId).post(ClientResponse.class, userData);
					cardBlockresponse = clientResponse.getEntity(String.class);
				}
				JSONObject cardBlockJson = new JSONObject(cardBlockresponse);
				if (clientResponse.getStatus() != 200) {
					unBlockResponse.setCode(ResponseStatus.FAILURE.getValue());
					unBlockResponse.setMessage(ResponseStatus.FAILURE.getKey());
				} else {
					unBlockResponse.setCode(ResponseStatus.SUCCESS.getValue());
					unBlockResponse.setMessage("Card " + blockCardRequest.getType() + " successfully done");
					unBlockResponse.setStatus(Status.Active);
					String cardHashId = cardBlockJson.getString("id");
					String status = cardBlockJson.getString("status");

					Cards card = cardRepository.getCardByCardHashId(cardHashId);
					if (status.equalsIgnoreCase(Status.Active.getValue())) {
						card.setStatus(Status.Active);
					}
					cardRepository.save(card);
				}
			} else {
				unBlockResponse.setCode(ResponseStatus.CARD_STATUS_ACTIVE.getValue());
				unBlockResponse.setMessage(ResponseStatus.CARD_STATUS_ACTIVE.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return unBlockResponse;
	}

	@Override
	public ResponseEntity<ResponseJson> fetchCardDetailsByCardHashId(String cardHashId) {
		ResponseJson response = new ResponseJson();
		CardCreationResponseDTO cardInquiry = new CardCreationResponseDTO();
		try {
			Cards card = cardRepository.getCardByCardHashId(cardHashId);
			if (card != null) {
				WalletInfo wallet = walletInfoRepository.findOne(card.getWallet().getId());
				User user = userRepository.findOne(wallet.getUser().getId());
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + card.getCardHashId());
				ClientResponse clientResponse = resource.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", user.getUserHashId()).get(ClientResponse.class);
				String strResponse = clientResponse.getEntity(String.class);
				JSONObject cardInqJson = new JSONObject(strResponse);
				if (clientResponse.getStatus() != 200) {
					response.setCode(ResponseStatus.FAILURE.getValue());
					response.setMessage("User does not have cards");
					return new ResponseEntity<ResponseJson>(response, HttpStatus.OK);
				} else {
					JSONObject date = cardInqJson.getJSONObject("date");
					cardInquiry.setCode(ResponseStatus.SUCCESS.getValue());
					cardInquiry.setMessage("Card Details fetched Success");
					cardInquiry.setStatus(ResponseStatus.SUCCESS);
					cardInquiry.setAvailableAmt(
							cardInqJson.getJSONObject("funds").getJSONObject("available").getString("amount"));
					cardInquiry.setCardHashId(cardInqJson.getString("id"));
					cardInquiry.setCardNumber(cardInqJson.getString("number"));
					cardInquiry.setCvv(getSecurityTokens(cardInquiry.getCardHashId(), user.getUserHashId()));
					cardInquiry.setCardType(CardType.virtual);
					cardInquiry.setCardStatus(cardInqJson.getJSONObject("status").getBoolean("is_active"));
					cardInquiry.setWithholdingAmt(
							cardInqJson.getJSONObject("funds").getJSONObject("withholding").getString("amount"));
					cardInquiry.setExpiryDate(date.getString("expiry"));
					cardInquiry.setHolderName(cardInqJson.getJSONObject("holder").getString("name"));
					cardInquiry.setIssueDate(date.getString("issued"));
					response.setDetails(cardInquiry);
					response.setCode(ResponseStatus.SUCCESS.getValue());
					response.setMessage("cards fetch successfully.");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseJson>(response, HttpStatus.OK);
	}

	@Override
	public String getPrefundBalance(User user) {
		try {
			String authorization = MatchMoveUtil.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/oauth/consumer");
			ClientResponse response = resource.header("Authorization", authorization)
					.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED).get(ClientResponse.class);
			String strResp = response.getEntity(String.class);
			if (strResp != null) {
				JSONObject obj = new JSONObject(strResp);
				if (obj != null && obj.has("code")) {
					if (obj.getString("code") != null && !obj.getString("code").equalsIgnoreCase("200")) {
						return "0.0";
					}
				} else {
					return obj.getJSONObject("consumer").getString("fund_floating_balance");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "0.0";
	}

	@Override
	public ResponseEntity<ResponseJson> savePhysicalUser(PhysicalCardUserRequest physicalCardUserDetails, User user) {
		ResponseJson response = new ResponseJson();
		try {
			PhysicalCardUserDetails physicalUserDetails = new PhysicalCardUserDetails();
			physicalUserDetails.setAddress1(physicalCardUserDetails.getAddress1());
			physicalUserDetails.setAddress2(physicalCardUserDetails.getAddress2());
			physicalUserDetails.setCity(physicalCardUserDetails.getCity());
			physicalUserDetails.setPinCode(physicalCardUserDetails.getPinCode());
			physicalUserDetails.setState(physicalCardUserDetails.getState());
			physicalUserDetails.setUser(user);
			physicalCardUserRepository.save(physicalUserDetails);
			response.setCode(ResponseStatus.SUCCESS.getValue());
			response.setMessage("user data saved successfully.");
			response.setSuccess(true);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseJson>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseJson> processCardFeeCharge(CreateCardDTO request, User user) {
		ResponseJson response = new ResponseJson();
		String cardCharge = null;

		try {
			Commission commission = new Commission();
			if (request.getCardType().equalsIgnoreCase("virtual")) {
				cardCharge = String.valueOf(commission.getVirtualCardCharge());
			} else if (request.getCardType().equalsIgnoreCase("physical")) {
				cardCharge = String.valueOf(commission.getPhysicalCardCharge());
			} else {
				cardCharge = String.valueOf(commission.getGiftCardCharge());
			}
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("amount",request.getAmount());
			userData.add("email", user.getEmail());
			userData.add("message", "Deduct funds");
			JSONObject details=new JSONObject();
			details.put("pp", "CORP_DEBIT");
			userData.add("details", details.toString());

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource blockCardResource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
			ClientResponse clientResponse = blockCardResource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE).header("Authorization", MatchMoveUtil.getBasicAuthorization()).header("Content-Type","application/x-www-form-urlencoded").delete(ClientResponse.class, userData);
			String cardChargeResponse = clientResponse.getEntity(String.class);
			JSONObject cardFeeJson = new JSONObject(cardChargeResponse);

			if (clientResponse.getStatus() != 200) {
				response.setCode(ResponseStatus.FAILURE.getValue());
				response.setMessage("Card Fee Deduction failed.");
				response.setSuccess(false);
			} else {
				CardFeeCharge cardFee = new CardFeeCharge();
				String id = cardFeeJson.getString("id");
				cardFee.setId(id);
				cardFee.setRefId(cardFeeJson.getString("ref_id"));
				cardFee.setUserId(cardFeeJson.getString("user_id"));
				cardFee.setWalletId(cardFeeJson.getString("wallet_id"));
				
				deductFromCardConfirmation(id);
				
				response.setCode(ResponseStatus.SUCCESS.getValue());
				response.setMessage("Card Fee Deducted.");
				response.setSuccess(true);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseJson>(response, HttpStatus.OK);
	}

	@Override
	public ResponseJson activatePhysicalCard(CreateCardDTO createCardDto, User user) {

		ResponseJson response = new ResponseJson();
		try {
			Cards card = cardRepository.getPhysicalCardByProxyNumber(createCardDto.getProxyNo());
			if (card != null) {
				MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
				userData.add("activation_code", card.getActivationCode());

				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + card.getCardHashId());
				ClientResponse clientResponse = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", user.getUserHashId()).put(ClientResponse.class, userData);
				String cardActivation = clientResponse.getEntity(String.class);
				JSONObject activateJson = new JSONObject(cardActivation);
				if (clientResponse.getStatus() != 200) {
					response.setCode(ResponseStatus.FAILURE.getValue());
					response.setMessage("Card Activation failed.");
					response.setSuccess(false);
				} else {
					response.setSuccess(true);
					response.setMessage(activateJson.getString("status"));
					response.setCode(ResponseStatus.SUCCESS.getValue());
				}
			} else {
				response.setSuccess(true);
				response.setMessage("card Activation Failed");
				response.setCode(ResponseStatus.FAILURE.getValue());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}
	
	public ResponseJson deductFromCardConfirmation(String id) {
		
		ResponseJson response = new ResponseJson();
		
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);

			try {
				Map<String, String> userData = new HashMap<String, String>();
				userData.put("ids",id);

				org.json.JSONObject transaction = wallet.consume("oauth/consumer/funds", HttpRequest.METHOD_DELETE, userData);
				System.err.println("deduct confirmation:::"+transaction);
					if (transaction != null && transaction.has("code")) {
					String code = transaction.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = transaction.getString("description");
						response.setCode("F00");
						response.setMessage(description);
						return response;
					}
				} else {
					
					response.setMessage("Funds transferred to wallet");
					response.setCode("S00");
					return response;
					
				}
				} catch (Exception e) {
				e.printStackTrace();
				response.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				response.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return response;
			}
			response.setCode(ResponseStatus.FAILURE.getValue());
			response.setMessage("Ohh!! snap, please try again later.");
			return response;
	}

	@Override
	public FetchCardResponseDTO processLoadCardTransaction(CreateCardDTO request) {
		FetchCardResponseDTO response = new FetchCardResponseDTO();
		try {
			Cards card = cardRepository.getCardByCardHashId(request.getCardHashId());
			if(card.getStatus().equals(Status.Active)) {
				MultivaluedMap<String, String> cardData = new MultivaluedMapImpl();
				cardData.add("amount", request.getAmount());
				cardData.add("message", "Card Funds");
				String transactionId = System.nanoTime() + "C";
				Transactions initiatetransaction = new Transactions();
				initiatetransaction.setAmount(Double.parseDouble(request.getAmount()));
				initiatetransaction.setDescription("Card Load of Rs " + request.getAmount());
				
				Services service = serviceRepository.findServiceByCode("LMS");
				initiatetransaction.setService(service);
				initiatetransaction.setStatus(Status.Initiated);
				initiatetransaction.setCard_status(Status.Initiated);
				initiatetransaction.setTransaction_ref_no(transactionId);
				UserSession session = userSessionRepository.findByActiveSessionId(request.getSessionId());

				initiatetransaction.setUser(session.getUser());
				transactionRepository.save(initiatetransaction);
				
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + request.getCardHashId()+"/funds" );
				ClientResponse clientResponse = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization()).post(ClientResponse.class, cardData);
				String cardLoad = clientResponse.getEntity(String.class);
				
				JSONObject cardLoadJson = new JSONObject(cardLoad);
				
				if(clientResponse.getStatus()!=200) {
					response.setCode(ResponseStatus.FAILURE.getValue());
					response.setMessage("card load Failed.");
					response.setStatus(ResponseStatus.FAILURE.getKey());
				}else {
					String transactionRefNo = cardLoadJson.getString("id");
					String cardType = cardLoadJson.getJSONObject("recipient").getString("type");
					String cardId = cardLoadJson.getJSONObject("recipient").getString("id");
					String status  = cardLoadJson.getString("status");
					double amount = cardLoadJson.getDouble("amount");
					
					Transactions transaction = transactionRepository.findByTransactionRefNo(transactionId);
					transaction.setCard_status(Status.Active);
					transaction.setRemarks(cardLoad);
					transaction.setRrn(transactionRefNo);
					transactionRepository.save(transaction);
					
					response.setCode(ResponseStatus.SUCCESS.getValue());
					response.setMessage("Card Load Success.");
					response.setStatus(status);
				}
				
			}else {
				response.setCode(ResponseStatus.FAILURE.getValue());
				response.setMessage("Card Blocked plz try with other card.");
			}
		} catch (Exception e) {
			response.setCode(ResponseStatus.FAILURE.getValue());
			response.setMessage(e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

}

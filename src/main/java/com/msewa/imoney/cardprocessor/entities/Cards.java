package com.msewa.imoney.cardprocessor.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.msewa.imoney.entity.AbstractEntity;
import com.msewa.imoney.enums.CardType;
import com.msewa.imoney.enums.Status;

@Entity
@Table(name="Cards")
public class Cards extends AbstractEntity<Long>{

	@Column(nullable=false,unique=true)
	private String cardHashId;
	@ManyToOne
	private WalletInfo wallet;
	@Column
	@Enumerated(EnumType.STRING)
	private Status status;
	@Column
	private CardType cardType;
	@Column(nullable=true)
	private String activationCode;
	@Column(nullable=true)
	private String proxyNumber;
	@Column(nullable=true)
	private String reason;
	@Column(nullable=true)
	private String remarks;
	
	
	
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getCardHashId() {
		return cardHashId;
	}
	public void setCardHashId(String cardHashId) {
		this.cardHashId = cardHashId;
	}
	public WalletInfo getWallet() {
		return wallet;
	}
	public void setWallet(WalletInfo wallet) {
		this.wallet = wallet;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public CardType getCardType() {
		return cardType;
	}
	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}
	public String getActivationCode() {
		return activationCode;
	}
	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}
	public String getProxyNumber() {
		return proxyNumber;
	}
	public void setProxyNumber(String proxyNumber) {
		this.proxyNumber = proxyNumber;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
}

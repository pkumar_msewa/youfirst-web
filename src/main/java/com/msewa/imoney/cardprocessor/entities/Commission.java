package com.msewa.imoney.cardprocessor.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.msewa.imoney.entity.AbstractEntity;
import com.msewa.imoney.transaction.entities.Services;

@Entity
@Table(name = "Commission")
public class Commission extends AbstractEntity<Long> {

	@Column(name = "fixed")
	private boolean fixed;

	@Column(name = "maxAmount")
	private double maxAmount;

	@Column(name = "minAmount")
	private double minAmount;

	@Column(name = "value")
	private double value;

	@OneToOne(fetch = FetchType.EAGER)
	private Services sevice;

	@Column(name = "physicalCardCharge")
	private double physicalCardCharge;

	@Column(name = "virtualCardCharge")
	private double virtualCardCharge;

	@Column(name = "giftCardCharge")
	private double giftCardCharge;

	public boolean isFixed() {
		return fixed;
	}

	public void setFixed(boolean fixed) {
		this.fixed = fixed;
	}

	public double getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(double maxAmount) {
		this.maxAmount = maxAmount;
	}

	public double getMinAmount() {
		return minAmount;
	}

	public void setMinAmount(double minAmount) {
		this.minAmount = minAmount;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public Services getSevice() {
		return sevice;
	}

	public void setSevice(Services sevice) {
		this.sevice = sevice;
	}

	public double getPhysicalCardCharge() {
		return physicalCardCharge;
	}

	public void setPhysicalCardCharge(double physicalCardCharge) {
		this.physicalCardCharge = physicalCardCharge;
	}

	public double getVirtualCardCharge() {
		return virtualCardCharge;
	}

	public void setVirtualCardCharge(double virtualCardCharge) {
		this.virtualCardCharge = virtualCardCharge;
	}

	public double getGiftCardCharge() {
		return giftCardCharge;
	}

	public void setGiftCardCharge(double giftCardCharge) {
		this.giftCardCharge = giftCardCharge;
	}
}

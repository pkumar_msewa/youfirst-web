package com.msewa.imoney.cardprocessor.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;

import com.msewa.imoney.entity.AbstractEntity;
import com.msewa.imoney.enums.AccountType;
import com.msewa.imoney.enums.Status;
import com.msewa.imoney.enums.UserType;


@Table(name = "User")
@Entity
public class User extends AbstractEntity<Long> {

	private static final long serialVersionUID = 8453654076725018243L;
	
	@Column(unique = true, nullable = false)
	private String username;

	@Column(nullable = true)
	private String password;

	@Column(nullable = false)
	private UserType userType;

	@Column(nullable = false) 
	private String authority;

	@Column
	@Enumerated(EnumType.STRING)
	private Status mobileStatus;

	@Column(nullable = true)
	private String firstName;

	@Column(nullable = true)
	private String lastName;

	@Column
	private String address;

	@Column(nullable = false)
	private String contactNo;

	@Column(nullable = true)
	private String email;

	@Column
	private String mpin;

	@Column
	@Temporal(TemporalType.DATE)
	private Date DateOfBirth;

	@Column(nullable = true)
	private String idType;

	@Column(nullable = true)
	private String idNo;

	@Column
	private String gender;

	
	@Column
	private String emailToken;

	@Column
	private String mobileToken;

	@Lob
	private String gcmId;

	@Column(unique=true,nullable=true)
	private String userHashId;

	@Column(nullable = true)
	private String androidDeviceID;

	@Column
	private boolean isFullyFilled=false;

	@Column
	@Enumerated(EnumType.STRING)
	private AccountType accountType;
	
	@Column(nullable=true)
	@Lob
	private String remarks;


	
	@Column(nullable=true)
	private String middleName;
	
	@Column(nullable=false)
	private String  selectRole;
	
	
	
	
	
	public String getSelectRole() {
		return selectRole;
	}

	public void setSelectRole(String selectRole) {
		this.selectRole = selectRole;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public boolean isFullyFilled() {
		return isFullyFilled;
	}

	public void setFullyFilled(boolean isFullyFilled) {
		this.isFullyFilled = isFullyFilled;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public Status getMobileStatus() {
		return mobileStatus;
	}

	public void setMobileStatus(Status mobileStatus) {
		this.mobileStatus = mobileStatus;
	}

	

	public String getEmailToken() {
		return emailToken;
	}

	public void setEmailToken(String emailToken) {
		this.emailToken = emailToken;
	}

	public String getMobileToken() {
		return mobileToken;
	}

	public void setMobileToken(String mobileToken) {
		this.mobileToken = mobileToken;
	}

	public String getGcmId() {
		return gcmId;
	}

	public void setGcmId(String gcmId) {
		this.gcmId = gcmId;
	}

	public String getAndroidDeviceID() {
		return androidDeviceID;
	}

	public void setAndroidDeviceID(String androidDeviceID) {
		this.androidDeviceID = androidDeviceID;
	}

	public String getUserHashId() {
		return userHashId;
	}

	public void setUserHashId(String userHashId) {
		this.userHashId = userHashId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMpin() {
		return mpin;
	}

	public void setMpin(String mpin) {
		this.mpin = mpin;
	}

	public Date getDateOfBirth() {
		return DateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.DateOfBirth = dateOfBirth;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	
	
	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	
}
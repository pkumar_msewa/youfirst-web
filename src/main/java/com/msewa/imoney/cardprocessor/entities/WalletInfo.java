package com.msewa.imoney.cardprocessor.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

import com.msewa.imoney.entity.AbstractEntity;
import com.msewa.imoney.enums.Status;

@Entity
@Table(name="WalletInfo")
public class WalletInfo extends AbstractEntity<Long>{

	@Column(nullable=true,unique=true)
	private String walletHashId;
	@Column
	@Enumerated(EnumType.STRING)
	private Status status;
	
	@Column(nullable=true)
	private String remarks;
	
	@OneToOne(fetch=FetchType.EAGER)
	private User user;
	
	
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getWalletHashId() {
		return walletHashId;
	}
	public void setWalletHashId(String walletHashId) {
		this.walletHashId = walletHashId;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	
	
	
}

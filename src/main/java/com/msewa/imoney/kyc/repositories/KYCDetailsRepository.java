package com.msewa.imoney.kyc.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import com.msewa.imoney.kyc.entity.KYCDetails;

public interface KYCDetailsRepository extends CrudRepository<KYCDetails, Long>, JpaSpecificationExecutor<KYCDetails>{

}

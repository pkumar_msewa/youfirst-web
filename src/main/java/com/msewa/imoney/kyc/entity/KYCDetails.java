package com.msewa.imoney.kyc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.entity.AbstractEntity;
import com.msewa.imoney.enums.AccountType;
import com.msewa.imoney.enums.UserType;

@Entity
@Table(name = "KYCDetails")
public class KYCDetails extends AbstractEntity<Long> {

	@Column(name = "idNumber")
	private String idNumber;
	
	@Column(name = "idType")
	private String idType;

	@Column(name = "idImage1")
	private String idImage1;

	@Column(name = "idImage2")
	private String idImage2;

	@Column(name = "address1")
	private String address1;

	@Column(name = "address2")
	private String address2;

	@Column(name = "cityName")
	private String cityName;

	@Column(name = "state")
	private String state;

	@Column(name = "country")
	private String country;

	@Column(name = "pinCode")
	private String pinCode;

	@Column(name = "userType")
	private UserType userType;

	@Column(name = "kycType")
	private String kycType;

	@Column(name = "accountType")
	private AccountType accountType;

	@OneToOne(fetch = FetchType.EAGER)
	private User user;

	@Column(name = "panCardImage")
	private String panCardImage;

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getIdImage1() {
		return idImage1;
	}

	public void setIdImage1(String idImage1) {
		this.idImage1 = idImage1;
	}

	public String getIdImage2() {
		return idImage2;
	}

	public void setIdImage2(String idImage2) {
		this.idImage2 = idImage2;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public String getKycType() {
		return kycType;
	}

	public void setKycType(String kycType) {
		this.kycType = kycType;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPanCardImage() {
		return panCardImage;
	}

	public void setPanCardImage(String panCardImage) {
		this.panCardImage = panCardImage;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

}

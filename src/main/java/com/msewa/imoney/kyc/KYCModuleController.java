package com.msewa.imoney.kyc;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.cardprocessor.repositories.UserRepository;
import com.msewa.imoney.enums.ResponseStatus;
import com.msewa.imoney.kyc.api.IKYCApi;
import com.msewa.imoney.kyc.dto.request.KYCRequestJSON;
import com.msewa.imoney.kyc.dto.response.KYCResponseJSON;
import com.msewa.imoney.session.entities.UserSession;
import com.msewa.imoney.session.repositories.UserSessionRepository;

@Controller
@RequestMapping(value = "/Api/{version}/{role}/{device}/{language}/KYC")
public class KYCModuleController {
	
	private final UserSessionRepository userSessionRepository;
	private final UserRepository userRepository;
	private final IKYCApi kYCApi;


	public KYCModuleController(UserSessionRepository userSessionRepository, UserRepository userRepository,
			IKYCApi kYCApi) {
		this.userSessionRepository = userSessionRepository;
		this.userRepository = userRepository;
		this.kYCApi = kYCApi;
	}


	@RequestMapping(value = "/UpgradeKYC", method = RequestMethod.POST, 
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<KYCResponseJSON> upgradeKycAccount(@PathVariable(value = "version") String version, 
			@PathVariable(value = "role") String role, @PathVariable(value = "device")String device, 
			@PathVariable(value = "language") String language, @ModelAttribute KYCRequestJSON kycRequest) {
		KYCResponseJSON response = new KYCResponseJSON();
		if(kycRequest !=null) {
			try {
				UserSession session  = userSessionRepository.findByActiveSessionId(kycRequest.getSessionId());
				if(session !=null) {
					User user = userRepository.findOne(session.getUser().getId());
					if(user !=null) {
						response = kYCApi.upgradeUserKYC(kycRequest, user);
					}else{
						response.setCode(ResponseStatus.FAILURE.getKey());
						response.setMessage("User record not exists.");
					}
				}else {
					response.setCode(ResponseStatus.FAILURE.getKey());
					response.setMessage("Invalid session.");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			response.setCode(ResponseStatus.FAILURE.getKey());
			response.setMessage("Invalid request.");
		}
		
		return new ResponseEntity<KYCResponseJSON>(response , HttpStatus.OK);
	}
}

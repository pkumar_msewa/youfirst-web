package com.msewa.imoney.kyc.api.impl;

import org.springframework.web.multipart.MultipartFile;

import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.enums.AccountType;
import com.msewa.imoney.enums.ResponseStatus;
import com.msewa.imoney.kyc.api.IKYCApi;
import com.msewa.imoney.kyc.dto.request.KYCRequestJSON;
import com.msewa.imoney.kyc.dto.response.KYCResponseJSON;
import com.msewa.imoney.kyc.entity.KYCDetails;
import com.msewa.imoney.kyc.repositories.KYCDetailsRepository;
import com.msewa.imoney.util.CommonUtil;
import com.msewa.imoney.util.CommonValidation;

public class KYCApiImpl implements IKYCApi {
	
	private final KYCDetailsRepository kYCDetailsRepository;
	
	public KYCApiImpl(KYCDetailsRepository kYCDetailsRepository) {
		super();
		this.kYCDetailsRepository = kYCDetailsRepository;
	}

	@Override
	public KYCResponseJSON upgradeUserKYC(KYCRequestJSON kycRequest, User user) {
		KYCResponseJSON response = new KYCResponseJSON();
		try {
			if (kycRequest != null) {
				if (user.getAccountType().getValue().equalsIgnoreCase(AccountType.NONKYC.getValue())) {
					if (!CommonValidation.isNullImage(kycRequest.getImage2()) && !CommonValidation.isNullImage(kycRequest.getImage1())) {
						KYCDetails kycDetail = new KYCDetails();
						MultipartFile image1 = kycRequest.getImage1();
						MultipartFile image2 = kycRequest.getImage2();
						try {
							kycDetail.setIdImage1(CommonUtil.uploadImage(image1, user.getContactNo(),kycRequest.getIdType()));
							kycDetail.setIdImage2(CommonUtil.uploadImage(image2, user.getContactNo(),kycRequest.getIdType()));
						}catch (Exception e) {
							e.printStackTrace();
						}
						kycDetail.setPinCode(kycRequest.getPostalCode());
						kycDetail.setCityName(kycRequest.getCityName());
						kycDetail.setState(kycRequest.getState());
						kycDetail.setCountry(kycRequest.getCountry());
						kycDetail.setIdNumber(kycRequest.getIdNumber());
						kycDetail.setAddress1(kycRequest.getAddress1());
						kycDetail.setAddress2(kycRequest.getAddress2());
						kycDetail.setIdType(kycRequest.getIdType());
						kycDetail.setUser(user);
						kycDetail.setUserType(user.getUserType());
						kYCDetailsRepository.save(kycDetail);
						
					} else {
						response.setCode(ResponseStatus.FAILURE.getValue());
						response.setStatus(ResponseStatus.FAILURE.getKey());
						response.setMessage("please upload Image of id type.");
					}
				}else{
					response.setStatus(ResponseStatus.FAILURE.getKey());
					response.setCode(ResponseStatus.FAILURE.getValue());
					response.setMessage("Please wait for approval of KYC.");
				}
			}else {
				response.setCode(ResponseStatus.FAILURE.getKey());
				response.setCode(ResponseStatus.FAILURE.getValue());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

}

package com.msewa.imoney.kyc.api;

import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.kyc.dto.request.KYCRequestJSON;
import com.msewa.imoney.kyc.dto.response.KYCResponseJSON;

public interface IKYCApi {

	KYCResponseJSON upgradeUserKYC(KYCRequestJSON kycRequest, User user);

}

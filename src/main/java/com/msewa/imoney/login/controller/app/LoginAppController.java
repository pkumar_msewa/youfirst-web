package com.msewa.imoney.login.controller.app;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.msewa.imoney.cardprocessor.dto.request.SessionDTO;
import com.msewa.imoney.login.dto.request.LoginRequestDTO;
import com.msewa.imoney.login.dto.response.GetUserDetailsRespDTO;
import com.msewa.imoney.login.dto.response.LoginResponseDTO;
import com.msewa.imoney.login.service.ILoginApi;

@Controller
@RequestMapping("/Api/{role}/{device}")
public class LoginAppController {

	private final ILoginApi loginApi;

	public LoginAppController(ILoginApi loginApi) {
		super();
		this.loginApi = loginApi;
	}
	
	
	/**
	 * Logging in User and creating session
	 * @param loginRequestDTO
	 * @param role
	 * @param device
	 * @param request
	 * @param response
	 * @return LoginResponseDTO
	 */
	
	
	@RequestMapping(value="/Login", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<LoginResponseDTO> loggingUser(@RequestBody LoginRequestDTO loginRequestDTO,
			@PathVariable(value="role")String role,@PathVariable(value="device")String device,
			HttpServletRequest request,HttpServletResponse response){
		
			return loginApi.doLogin(loginRequestDTO, role, device, request, response);
	}
	
	
	
	/**
	 * Logs out a user out of session
	 * @param sessionDTO
	 * @param role
	 * @param device
	 * @param request
	 * @param response
	 * @return LoginResponseDTO
	 */
	
	
	
	
	@RequestMapping(value="/Logout", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<LoginResponseDTO> loggingUserOut(@RequestBody SessionDTO sessionDto,
			@PathVariable(value="role")String role,@PathVariable(value="device")String device){
		
			return loginApi.doLogout(sessionDto, role, device);
	}
	
	
	/**
	 * Get the basic user details
	 * 
	 * @param sessionDTO
	 * @param role
	 * @param device
	 * @param request
	 * @param response
	 * @return GetUserDetailsRespDTO
	 */
	
	
	
	
	@RequestMapping(value="/GetUserDetails", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<GetUserDetailsRespDTO> getUserDetails(@RequestBody SessionDTO sessionDTO,
			@PathVariable(value="role")String role,@PathVariable(value="device")String device,
			HttpServletRequest request,HttpServletResponse response){
		
			return loginApi.getUserDetails(sessionDTO, role, device, request, response);
	}
	
	/**
	 * Change Login Password
	 * @param loginRequestDTO
	 * @param role
	 * @param device
	 * @param request
	 * @param response
	 * @return LoginResponseDTO
	 */
	@RequestMapping(value = "/ChangePassword" , method = RequestMethod.POST , produces = {
			MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE} )
	public ResponseEntity<LoginResponseDTO> processLoginPasswordChange(@RequestBody LoginRequestDTO loginRequestDTO,
			@PathVariable(value="role")String role,@PathVariable(value="device")String device,
			HttpServletRequest request,HttpServletResponse response){
	
		return loginApi.processChangeLoginPassword(loginRequestDTO);
	}
}

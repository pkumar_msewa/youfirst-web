package com.msewa.imoney.login.validation;


import org.springframework.security.crypto.password.PasswordEncoder;
import com.msewa.imoney.admin.request.dto.LoginDTO;
import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.cardprocessor.repositories.UserRepository;
import com.msewa.imoney.enums.ResponseStatus;
import com.msewa.imoney.login.dto.request.LoginRequestDTO;
import com.msewa.imoney.login.dto.response.LoginError;
import com.msewa.imoney.util.CommonValidation;

public class LoginValidation {

	
	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;
	
	
	
	public LoginValidation(UserRepository userRepository, PasswordEncoder passwordEncoder) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
	}



	public LoginError checkLoginValidation(LoginRequestDTO login,String role,String device) {
		LoginError loginError = new LoginError();
		loginError.setSuccess(true);
		if (CommonValidation.isNull(login.getUsername())) {
			loginError.setSuccess(false);
			loginError.setCode(ResponseStatus.USERNAME_NA);
		} else if (CommonValidation.isNull(login.getPassword())) {
			loginError.setSuccess(false);
			loginError.setMessage("Enter Password");
		}else if(CommonValidation.isNull(role)){
			loginError.setCode(ResponseStatus.UNAUTHORIZED_ROLE);
			loginError.setSuccess(false);
		}else if(CommonValidation.isNull(device)){
			loginError.setCode(ResponseStatus.INVALID_DEVICE);
			loginError.setSuccess(false);
		}
		
		User user=userRepository.findByUsername(login.getUsername());
		if(user!=null){
			if(user.getMobileStatus().getValue().equalsIgnoreCase("Inactive")){
				loginError.setCode(ResponseStatus.MOBILE_INACTIVE);
				loginError.setSuccess(false);
			}if(user.getAuthority().contains("BLOCKED") || user.getAuthority().contains("LOCKED")){
				loginError.setCode(ResponseStatus.USER_LOCKED);
				loginError.setMessage("User is Blocked please contact customer support.");
				loginError.setSuccess(false);
			}if(!user.getAuthority().contains("USER")){
				loginError.setCode(ResponseStatus.UNAUTHORIZED_ROLE);
				loginError.setSuccess(false);
			}if(!user.isFullyFilled()){
				loginError.setCode(ResponseStatus.USER_PROFILE_NOT_COMPLETE);
				loginError.setSuccess(false);
			}
		}else{
			loginError.setCode(ResponseStatus.UNAUTHORIZED_USER);
			loginError.setSuccess(false);
		}
		
		return loginError;
	}



	public String adminLoginValidate(LoginDTO login) {
		if (CommonValidation.isNull(login.getUsername())) {
			return "Enter Username";
		} else if (CommonValidation.isNull(login.getPassword())) {
			return "Enter Password";
		}
		return null;
	}
	
}

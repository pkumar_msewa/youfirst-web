package com.msewa.imoney.login.service.Impl;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.context.request.RequestContextHolder;

import com.msewa.imoney.cardprocessor.dto.request.SessionDTO;
import com.msewa.imoney.cardprocessor.dto.response.WalletCreationResponseDTO;
import com.msewa.imoney.cardprocessor.entities.Cards;
import com.msewa.imoney.cardprocessor.entities.Commission;
import com.msewa.imoney.cardprocessor.entities.PhysicalCardUserDetails;
import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.cardprocessor.entities.WalletInfo;
import com.msewa.imoney.cardprocessor.repositories.CardRepository;
import com.msewa.imoney.cardprocessor.repositories.CommissionRepository;
import com.msewa.imoney.cardprocessor.repositories.PhysicalCardUserRepository;
import com.msewa.imoney.cardprocessor.repositories.UserRepository;
import com.msewa.imoney.cardprocessor.repositories.WalletInfoRepository;
import com.msewa.imoney.cardprocessor.service.ICPWalletApi;
import com.msewa.imoney.enums.CardType;
import com.msewa.imoney.enums.ResponseStatus;
import com.msewa.imoney.enums.Status;
import com.msewa.imoney.login.dto.request.LoginRequestDTO;
import com.msewa.imoney.login.dto.response.AuthenticationError;
import com.msewa.imoney.login.dto.response.GetUserDetailsRespDTO;
import com.msewa.imoney.login.dto.response.LoginError;
import com.msewa.imoney.login.dto.response.LoginResponseDTO;
import com.msewa.imoney.login.service.ILoginApi;
import com.msewa.imoney.login.validation.LoginValidation;
import com.msewa.imoney.session.entities.UserSession;
import com.msewa.imoney.session.repositories.UserSessionRepository;
import com.msewa.imoney.session.service.ISessionApi;
import com.msewa.imoney.session.service.SessionLoggingStrategy;
import com.msewa.imoney.util.CommonUtil;
import com.msewa.imoney.util.CommonValidation;

public class LoginApiImpl implements ILoginApi{

	private final AuthenticationManager authenticationManager;
	private final ISessionApi sessionApi;
	private final LoginValidation loginValidation;
	private final PasswordEncoder passwordEncoder;
	private final UserRepository userRepository;
	private final SessionLoggingStrategy sls;
	private final UserSessionRepository userSessionRepository;
	private final ICPWalletApi walletApi;
	private final WalletInfoRepository walletInfoRepository;
	private final CardRepository cardRepository;
	private final CommissionRepository commissionRepository;
	private final PhysicalCardUserRepository physicalCardUserRepository;
	
	public LoginApiImpl(AuthenticationManager authenticationManager, ISessionApi sessionApi,
			LoginValidation loginValidation,UserRepository userRepository,PasswordEncoder passwordEncoder,
			SessionLoggingStrategy sls,UserSessionRepository userSessionRepository,
			ICPWalletApi walletApi,WalletInfoRepository walletInfoRepository, CardRepository cardRepository
			, CommissionRepository commissionRepository, PhysicalCardUserRepository physicalCardUserRepository) {
		super();
		this.authenticationManager = authenticationManager;
		this.sessionApi = sessionApi;
		this.loginValidation=loginValidation;
		this.userRepository=userRepository;
		this.sls=sls;
		this.userSessionRepository=userSessionRepository;
		this.passwordEncoder = passwordEncoder;
		this.walletApi=walletApi;
		this.walletInfoRepository=walletInfoRepository;
		this.cardRepository = cardRepository;
		this.commissionRepository = commissionRepository;
		this.physicalCardUserRepository = physicalCardUserRepository;
	}
	

	private AuthenticationError isValidUsernamePassword(LoginRequestDTO dto, HttpServletRequest request) {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		UsernamePasswordAuthenticationToken token = null;
		try {
			User user = userRepository.findByUsername(dto.getUsername());
			if(user==null) {
				error.setSuccess(false);
				error.setMessage("please Register First then Try Login.");
				return error;
				
			}
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), CommonUtil.base64Decode(dto.getPassword()));
			auth = authenticationManager.authenticate(token);
			SecurityContext securityContext = SecurityContextHolder.getContext();
			if (auth.isAuthenticated()) {
				error.setSuccess(true);
				error.setMessage("Valid Credentials");
				return error;
			} else {
				error.setSuccess(false);
				error.setMessage(sessionApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress()));
				SecurityContextHolder.getContext().setAuthentication(null);
				return error;
			}
		} catch (Exception e) {
			e.getMessage();
			error.setSuccess(false);
			error.setMessage(sessionApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
					dto.getIpAddress()));
			return error;
		}
	}
	
	private AuthenticationError authentication(LoginRequestDTO dto, HttpServletRequest request)
			throws ServletException, IOException, Exception {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		UsernamePasswordAuthenticationToken token = null;
		try {
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), CommonUtil.base64Decode(dto.getPassword()));
			auth = authenticationManager.authenticate(token);
			SecurityContext securityContext = SecurityContextHolder.getContext();
			if (auth.isAuthenticated()) {
				securityContext.setAuthentication(auth);
				SecurityContextHolder.getContext().setAuthentication(auth);
				HttpSession session = request.getSession(true);
				session.setMaxInactiveInterval(30*24*60*60);
				session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
				error.setSuccess(true);
				error.setMessage("Login successful.");
				sessionApi.handleLoginSuccess(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress());
				return error;
			} else {
				error.setSuccess(false);
				error.setMessage(sessionApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress()));
				SecurityContextHolder.getContext().setAuthentication(null);
				return error;
			}
		} catch (Exception e) {
			e.printStackTrace();
			SecurityContextHolder.getContext().setAuthentication(null);
			error.setSuccess(false);
			error.setMessage(sessionApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
					dto.getIpAddress()));
			return error;
		}
	}
	
	
	
	
	@Override
	public ResponseEntity<LoginResponseDTO> doLogin(LoginRequestDTO loginRequestDTO,String role,String device,
			HttpServletRequest request,HttpServletResponse response){
		LoginResponseDTO loginResponseDTO=new LoginResponseDTO();
		try{
		LoginError loginError=loginValidation.checkLoginValidation(loginRequestDTO,role,device);
		if(loginError!=null){
			if(loginError.isSuccess()){
                AuthenticationError authError = isValidUsernamePassword(loginRequestDTO,request);
                if(authError.isSuccess()) {
                	User user=userRepository.findByUsername(loginRequestDTO.getUsername());
                    if (sessionApi.checkActiveSession(user)) {
                    	if(loginRequestDTO.getIpAddress()==null){
                    		loginRequestDTO.setIpAddress(request.getRemoteAddr());
                    	}
                        AuthenticationError auth = authentication(loginRequestDTO,request);
                        if (auth.isSuccess()) {
                        	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                            sls.onAuthentication(authentication, request, response);
                            UserSession userSession = userSessionRepository.findByActiveSessionId(
                                    RequestContextHolder.currentRequestAttributes().getSessionId());
                            loginResponseDTO.setSessionId(userSession.getSessionId());
                            loginResponseDTO.setCode(ResponseStatus.SUCCESS);
                            loginResponseDTO.setAccountType(user.getAccountType().getValue());
                            if(user.getMpin()!=null){
                            loginResponseDTO.setHasMpin(true);
                            }
                            new ResponseEntity<LoginResponseDTO>(loginResponseDTO,HttpStatus.OK);
							}else{
								loginResponseDTO.setCode(ResponseStatus.FAILURE);
								loginResponseDTO.setMessage(auth.getMessage());
								return new ResponseEntity<LoginResponseDTO>(loginResponseDTO, HttpStatus.OK);
							}
							}else{
								loginResponseDTO.setCode(ResponseStatus.INVALID_SESSION);
								return new ResponseEntity<LoginResponseDTO>(loginResponseDTO, HttpStatus.OK);
					
							}
							
						}else{
							loginResponseDTO.setCode(ResponseStatus.FAILURE);
							loginResponseDTO.setMessage(authError.getMessage());
							return new ResponseEntity<LoginResponseDTO>(loginResponseDTO, HttpStatus.OK);
					
						}
					} else{
						loginResponseDTO.setCode(ResponseStatus.FAILURE);
						loginResponseDTO.setMessage(loginError.getMessage());
						return new ResponseEntity<LoginResponseDTO>(loginResponseDTO, HttpStatus.OK);
					
					}
					}else{
						loginResponseDTO.setCode(ResponseStatus.INTERNAL_SERVER_ERROR);
						return new ResponseEntity<LoginResponseDTO>(loginResponseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
					}
					}catch(Exception e){
						e.printStackTrace();
						loginResponseDTO.setCode(ResponseStatus.INTERNAL_SERVER_ERROR);
						return new ResponseEntity<LoginResponseDTO>(loginResponseDTO, HttpStatus.SERVICE_UNAVAILABLE);
			
					}
					return new ResponseEntity<LoginResponseDTO>(loginResponseDTO, HttpStatus.OK);
				}
	
	
	@Override
	public ResponseEntity<LoginResponseDTO> doLogout(SessionDTO sessionDTO,String role,String device){
		LoginResponseDTO resp=new LoginResponseDTO();
		System.err.println("herer");
		UserSession userSession = userSessionRepository.findBySessionId(sessionDTO.getSessionId());
		if(userSession!=null){
		sessionApi.expireSession(sessionDTO.getSessionId());
		resp.setCode(ResponseStatus.SUCCESS);
		resp.setMessage("log out success.");
		return new ResponseEntity<LoginResponseDTO>(resp, HttpStatus.OK);

		}else{
			resp.setCode(ResponseStatus.FAILURE);
			return new ResponseEntity<LoginResponseDTO>(resp, HttpStatus.OK);

		}
		
	}
	
	@Override
	public ResponseEntity<GetUserDetailsRespDTO> getUserDetails(SessionDTO sessionDTO,String role,String device,
			HttpServletRequest request,HttpServletResponse response){
		GetUserDetailsRespDTO resp=new GetUserDetailsRespDTO();
		try {
			UserSession userSession = sessionApi.getActiveUserSession(sessionDTO.getSessionId());
			if (userSession != null) {
				User user = userRepository.findOne(userSession.getUser().getId());
				if (userSession.getUser() != null) {
					resp.setAccountType(userSession.getUser().getAccountType().getValue());
					resp.setFirstName(userSession.getUser().getFirstName());
					resp.setLastName(userSession.getUser().getLastName());
					WalletCreationResponseDTO walletResponse = new WalletCreationResponseDTO();
					walletResponse = walletApi.fetchWallet(user.getUserHashId());
					if (userSession.getUser().getMpin() != null) {
						resp.setHasMpin(true);
					}
					resp.setBalance(Double.parseDouble(walletResponse.getAvailableAmt()));
					resp.setContactno(user.getContactNo());
					resp.setEmail(user.getEmail());
					resp.setDob(CommonUtil.formatter.format(user.getDateOfBirth()));
					resp.setSessionId(userSession.getSessionId());
					WalletInfo wallet = walletInfoRepository.walletInfoByUserId(user);
					List<Cards> cards = cardRepository.getCardByWalletId(wallet);
					PhysicalCardUserDetails physicalUser= physicalCardUserRepository.getPhysicaluserByUserHashId(user);
				
					if(physicalUser!=null) {
						resp.setHasPhysical(true);
					}
					for (Cards card : cards) {
						if(card.getCardType().equals(CardType.virtual)) {
							resp.setHasVirtual(true);
						}
						if(card.getCardType().equals(CardType.gift)) {
							resp.setHasGift(true);
						}
					}
					Commission charges = commissionRepository.getCommission();
					resp.setPhysicalCardCharge(String.valueOf(charges.getPhysicalCardCharge()));
					resp.setVirtualcardCharge(String.valueOf(charges.getVirtualCardCharge()));
					resp.setGiftCardCharge(String.valueOf(charges.getGiftCardCharge()));
					if(wallet!=null){
						WalletCreationResponseDTO walletCreationResp=walletApi.fetchWallet(user.getUserHashId());
						resp.setBalance(Double.parseDouble(walletCreationResp.getAvailableAmt()));
					}
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					return new ResponseEntity<GetUserDetailsRespDTO>(resp, HttpStatus.OK);
				} else {
					resp.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					return new ResponseEntity<GetUserDetailsRespDTO>(resp, HttpStatus.OK);
				}
			} else {
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage(ResponseStatus.INVALID_SESSION.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<GetUserDetailsRespDTO>(resp, HttpStatus.OK);
	}


	@Override
	public ResponseEntity<LoginResponseDTO> processChangeLoginPassword(LoginRequestDTO loginRequestDTO) {
		LoginResponseDTO response = new LoginResponseDTO();
		try {
			if(loginRequestDTO != null) {
				User user = userRepository.findByUsername(loginRequestDTO.getUsername());
				if(user != null && !CommonValidation.isNull(user.getPassword())) {
					if(loginRequestDTO.getNewPassword().equalsIgnoreCase(loginRequestDTO.getConfirmPassword())) {
						user.setPassword(passwordEncoder.encode(loginRequestDTO.getConfirmPassword()));
						userRepository.save(user);
						response.setAccountType(user.getAccountType().name());
						response.setCode(ResponseStatus.SUCCESS);
						if(user.getMpin()!=null) {
							response.setHasMpin(true);
						}
						response.setMessage("Password changed successfully.");
					}else {
						response.setCode(ResponseStatus.FAILURE);
						response.setMessage("Password Miss Match.");
					}
				}else {
					response.setCode(ResponseStatus.FAILURE);
					response.setMessage("User Record Does Not Exists.");
				}
			}else {
				response.setCode(ResponseStatus.FAILURE);
				response.setMessage("Invalid Request");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<LoginResponseDTO>(response , HttpStatus.OK);
	}
}

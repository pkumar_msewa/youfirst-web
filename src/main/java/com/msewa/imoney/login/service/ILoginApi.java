package com.msewa.imoney.login.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;

import com.msewa.imoney.cardprocessor.dto.request.SessionDTO;
import com.msewa.imoney.login.dto.request.LoginRequestDTO;
import com.msewa.imoney.login.dto.response.GetUserDetailsRespDTO;
import com.msewa.imoney.login.dto.response.LoginResponseDTO;

public interface ILoginApi {

	ResponseEntity<LoginResponseDTO> doLogin(LoginRequestDTO loginRequestDTO, String role, String device,
			HttpServletRequest request, HttpServletResponse response);

	ResponseEntity<LoginResponseDTO> doLogout(SessionDTO sessionDTO, String role, String device);

	ResponseEntity<GetUserDetailsRespDTO> getUserDetails(SessionDTO sessionDTO, String role, String device,
			HttpServletRequest request, HttpServletResponse response);

	ResponseEntity<LoginResponseDTO> processChangeLoginPassword(LoginRequestDTO loginRequestDTO);

}

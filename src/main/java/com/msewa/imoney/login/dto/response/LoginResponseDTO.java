package com.msewa.imoney.login.dto.response;

import com.msewa.imoney.cardprocessor.dto.request.SessionDTO;
import com.msewa.imoney.enums.ResponseStatus;

public class LoginResponseDTO extends SessionDTO{

	private String accountType;
	private boolean hasMpin;
	private String code;
	private String message;
	
	
	
	public String getCode() {
		return code;
	}
	public void setCode(ResponseStatus status) {
		this.code = status.getValue();
		this.message=status.getKey();
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public boolean isHasMpin() {
		return hasMpin;
	}
	public void setHasMpin(boolean hasMpin) {
		this.hasMpin = hasMpin;
	}
	
	
	
}

package com.msewa.imoney.login.dto.response;

import com.msewa.imoney.cardprocessor.dto.request.SessionDTO;
import com.msewa.imoney.enums.ResponseStatus;

public class GetUserDetailsRespDTO extends SessionDTO {

	private String code;

	private String message;
	
	private String accountType;

	private String firstName;

	private String lastName;

	private String dob;

	private String email;

	private String contactno;

	private boolean hasMpin;

	private double balance;
	
	private boolean hasPhysical;
	
	private boolean hasVirtual;
	
	private  boolean hasGift;
	
	private String physicalCardCharge;
	
	private String virtualcardCharge;
	
	private String giftCardCharge;
	
	private boolean isPhysicalActive;
	

	public boolean isHasPhysical() {
		return hasPhysical;
	}

	public void setHasPhysical(boolean hasPhysical) {
		this.hasPhysical = hasPhysical;
	}

	public boolean isHasVirtual() {
		return hasVirtual;
	}

	public void setHasVirtual(boolean hasVirtual) {
		this.hasVirtual = hasVirtual;
	}

	public boolean isHasGift() {
		return hasGift;
	}

	public void setHasGift(boolean hasGift) {
		this.hasGift = hasGift;
	}

	public String getPhysicahCardCharge() {
		return physicalCardCharge;
	}

	public void setPhysicalCardCharge(String physicalCardCharge) {
		this.physicalCardCharge = physicalCardCharge;
	}

	public String getVirtualcardCharge() {
		return virtualcardCharge;
	}

	public void setVirtualcardCharge(String virtualcardCharge) {
		this.virtualcardCharge = virtualcardCharge;
	}

	public String getGiftCardCharge() {
		return giftCardCharge;
	}

	public void setGiftCardCharge(String giftCardCharge) {
		this.giftCardCharge = giftCardCharge;
	}

	public boolean isPhysicalActive() {
		return isPhysicalActive;
	}

	public void setPhysicalActive(boolean isPhysicalActive) {
		this.isPhysicalActive = isPhysicalActive;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactno() {
		return contactno;
	}

	public void setContactno(String contactno) {
		this.contactno = contactno;
	}

	public boolean isHasMpin() {
		return hasMpin;
	}

	public void setHasMpin(boolean hasMpin) {
		this.hasMpin = hasMpin;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}

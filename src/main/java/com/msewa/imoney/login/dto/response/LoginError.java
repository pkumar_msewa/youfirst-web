package com.msewa.imoney.login.dto.response;

import com.msewa.imoney.enums.ResponseStatus;

public class LoginError {

	private boolean success;
    private String message;
    private String code;
   
    
    
	public String getCode() {
		return code;
	}
	public void setCode(ResponseStatus status) {
		this.code = status.getValue();
		this.message=status.getKey();
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	

    
}

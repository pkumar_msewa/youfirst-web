package com.msewa.imoney.user.api.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.msewa.imoney.cardprocessor.dto.request.RequestJson;
import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.cardprocessor.repositories.UserRepository;
import com.msewa.imoney.cardprocessor.util.Authorities;
import com.msewa.imoney.enums.Status;
import com.msewa.imoney.enums.UserType;
import com.msewa.imoney.session.entities.LoginLog;
import com.msewa.imoney.session.repositories.LoginLogRepository;
import com.msewa.imoney.sms.service.ISMSSenderApi;
import com.msewa.imoney.sms.util.SMSTemplete;
import com.msewa.imoney.user.api.IUserApi;
import com.msewa.imoney.util.CommonUtil;

public class UserApi implements IUserApi {

	private final UserRepository userRepository;
	private final LoginLogRepository loginLogRepository;
	private final ISMSSenderApi senderApi;
	private final PasswordEncoder passwordEncoder;

	public UserApi(UserRepository userRepository , LoginLogRepository loginLogRepository,
			ISMSSenderApi senderApi , PasswordEncoder passwordEncoder) {
		this.userRepository = userRepository;
		this.loginLogRepository=loginLogRepository;
		this.senderApi = senderApi;
		this.passwordEncoder=passwordEncoder;
	}
	
	@Override
	public User findByUserName(String username) {
		return userRepository.findByUsername(username);
	}

	@Override
	public void handleLoginSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication, String loginUsername, String ipAddress) {
		if (loginUsername != null) {
			User user = userRepository.findByUsername(loginUsername);
			if (user != null) {
				LoginLog loginLog = new LoginLog();
				loginLog.setUser(user);
				loginLog.setRemoteAddress(ipAddress);
				loginLog.setServerIp(request.getRemoteAddr());	
				loginLog.setStatus(Status.Success);
				loginLogRepository.save(loginLog);
				List<LoginLog> lls = loginLogRepository.findTodayEntryForUserWithStatus(user, Status.Failed);
				for (LoginLog ll : lls) {
					loginLogRepository.deleteLoginLogForId(Status.Deleted, ll.getId());
				}
			}
		}
	}

	@Override
	public void sendOtpToAdmin(User user) {
		user.setMobileToken(CommonUtil.generateSixDigitNumericString(user.getContactNo()));
		userRepository.save(user);
		System.out.println(user.getMobileToken());
//		senderApi.sendOtpSMSToAdmin(SMSTemplete.ADMIN_TEMPLATE, user);
	}

	@Override
	public String handleLoginFailure(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication, String loginUsername, String ipAddress) {
		if (loginUsername != null) {
			User user = userRepository.findByUsername(loginUsername);
			if (user != null) {
				if (user.getMobileStatus() == Status.Active) {
					if (user.getAuthority().contains(Authorities.BLOCKED)) {
						return "Your account is blocked! Please contact support.";
					}
					LoginLog loginLog = new LoginLog();
					loginLog.setUser(user);
					loginLog.setRemoteAddress(ipAddress);
					loginLog.setServerIp(request.getRemoteAddr());
					loginLog.setStatus(Status.Failed);
					loginLogRepository.save(loginLog);

					List<LoginLog> llsFailed = loginLogRepository.findTodayEntryForUserWithStatus(user, Status.Failed);
					int failedCount = llsFailed.size();
					int remainingAttempts = CommonUtil.maxAuthAttempts - failedCount;
					if (remainingAttempts < 0 || failedCount == CommonUtil.maxAuthAttempts) {
						if (user.getUserType() == UserType.User) {
							String authority = Authorities.USER + "," + Authorities.BLOCKED;
							updateUserAuthority(authority, user.getId());
						} else if (user.getUserType() == UserType.Admin) {
							String authority = Authorities.ADMINISTRATOR + "," + Authorities.BLOCKED;
							updateUserAuthority(authority, user.getId());
						}
						return "Your account is blocked! Please try after 24 hrs.";
					}
					return "Incorrect password. Remaining attempts " + remainingAttempts;
				} else {
					return "User doesn't exists";
				}
			} else {
				return "User doesn't exists";
			}
		}
		return "Authentication Failed. Please try again";
	}

		public void updateUserAuthority(String authority, long id) {
			userRepository.updateUserAuthority(authority, id);
		}

		@Override
		public User forgetPasswordOTP(String username) {
			try {
				if (username != null && !username.isEmpty()) {
					User user = userRepository.findByUsernameAndStatus(username, Status.Active);
					if (user != null) {
						user.setMobileToken(CommonUtil.generateSixDigitNumericString(username));
						userRepository.save(user);
						senderApi.sendOtpSMSToAdmin(SMSTemplete.FORGET_PASS_ADMIN, user);
						return user;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		public boolean checkValidOTP(String otp, String username) {
			boolean valid = false;
			try {
				User mUser = userRepository.findByUsername(username);
				if (mUser != null) {
					if (mUser.getMobileToken().equalsIgnoreCase(otp)) {
						valid = true;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return valid;
		}

		@Override
		public void forgetPassword(RequestJson dto) {
			try {
				User mUser = userRepository.findByUsername(dto.getUsername());
				if (mUser != null) {
					mUser.setMobileToken(null);
					mUser.setPassword(passwordEncoder.encode(dto.getPassword()));
					userRepository.save(mUser);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
}

package com.msewa.imoney.user.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;

import com.msewa.imoney.cardprocessor.dto.request.RequestJson;
import com.msewa.imoney.cardprocessor.entities.User;

public interface IUserApi {

	User findByUserName(String username);
	void handleLoginSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication,
			String loginUsername, String ipAddress);
	String handleLoginFailure(HttpServletRequest request, HttpServletResponse response, Authentication authentication,
			String loginUsername, String ipAddress);
	void sendOtpToAdmin(User user);
	User forgetPasswordOTP(String username);
	boolean checkValidOTP(String otp, String username);
	void forgetPassword(RequestJson dto);

}

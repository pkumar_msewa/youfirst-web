package com.msewa.imoney.admin.request.dto;

import com.msewa.imoney.cardprocessor.dto.request.SessionDTO;

public class LoginDTO extends SessionDTO{

	private String username;
	private String password;
	private boolean validate;
	private String mobileToken;
	private String androidDeviceID;
	private boolean hasDeviceId;
	private String mPin;
	private String otp;
	private String ipAddress;
	private String dateRange;
	
	public String getDateRange() {
		return dateRange;
	}
	public void setDateRange(String dateRange) {
		this.dateRange = dateRange;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public boolean isValidate() {
		return validate;
	}
	public void setValidate(boolean validate) {
		this.validate = validate;
	}
	public String getMobileToken() {
		return mobileToken;
	}
	public void setMobileToken(String mobileToken) {
		this.mobileToken = mobileToken;
	}
	public String getAndroidDeviceID() {
		return androidDeviceID;
	}
	public void setAndroidDeviceID(String androidDeviceID) {
		this.androidDeviceID = androidDeviceID;
	}
	public boolean isHasDeviceId() {
		return hasDeviceId;
	}
	public void setHasDeviceId(boolean hasDeviceId) {
		this.hasDeviceId = hasDeviceId;
	}
	public String getmPin() {
		return mPin;
	}
	public void setmPin(String mPin) {
		this.mPin = mPin;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	
	
}

package com.msewa.imoney.admin.request.dto;

public class GenericDashboardData {

	private Long totalUsers;
	private Long activeUser;
	private Long inactiveUsers;
	private Long blockedUsers;
	private Long totalPCards;
	private Long pCardReqest;
	private Long totalVCards;
	private Long blockedCards;
	private Long transactedUsers;
	private String vCardTxn;
	private String pCardTxn;
	private Double loadWalletAmt;
	private Double treeBalance;
	private Double balance;
	private Long totalWallets;
	private String startDate;
	private String endDate;
	
	
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public Long getTotalWallets() {
		return totalWallets;
	}
	public void setTotalWallets(Long totalWallets) {
		this.totalWallets = totalWallets;
	}
	public Long getTotalUsers() {
		return totalUsers;
	}
	public void setTotalUsers(Long totalUsers) {
		this.totalUsers = totalUsers;
	}
	public Long getActiveUser() {
		return activeUser;
	}
	public void setActiveUser(Long activeUser) {
		this.activeUser = activeUser;
	}
	public Long getInactiveUsers() {
		return inactiveUsers;
	}
	public void setInactiveUsers(Long inactiveUsers) {
		this.inactiveUsers = inactiveUsers;
	}
	public Long getBlockedUsers() {
		return blockedUsers;
	}
	public void setBlockedUsers(Long blockedUsers) {
		this.blockedUsers = blockedUsers;
	}
	public Long getTotalPCards() {
		return totalPCards;
	}
	public void setTotalPCards(Long totalPCards) {
		this.totalPCards = totalPCards;
	}
	public Long getpCardReqest() {
		return pCardReqest;
	}
	public void setpCardReqest(Long pCardReqest) {
		this.pCardReqest = pCardReqest;
	}
	public Long getTotalVCards() {
		return totalVCards;
	}
	public void setTotalVCards(Long totalVCards) {
		this.totalVCards = totalVCards;
	}
	public Long getBlockedCards() {
		return blockedCards;
	}
	public void setBlockedCards(Long blockedCards) {
		this.blockedCards = blockedCards;
	}
	public Long getTransactedUsers() {
		return transactedUsers;
	}
	public void setTransactedUsers(Long transactedUsers) {
		this.transactedUsers = transactedUsers;
	}
	public String getvCardTxn() {
		return vCardTxn;
	}
	public void setvCardTxn(String vCardTxn) {
		this.vCardTxn = vCardTxn;
	}
	public String getpCardTxn() {
		return pCardTxn;
	}
	public void setpCardTxn(String pCardTxn) {
		this.pCardTxn = pCardTxn;
	}
	public Double getLoadWalletAmt() {
		return loadWalletAmt;
	}
	public void setLoadWalletAmt(Double loadWalletAmt) {
		this.loadWalletAmt = loadWalletAmt;
	}
	public Double getTreeBalance() {
		return treeBalance;
	}
	public void setTreeBalance(Double treeBalance) {
		this.treeBalance = treeBalance;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
}

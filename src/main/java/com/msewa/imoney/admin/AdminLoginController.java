package com.msewa.imoney.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.msewa.imoney.admin.api.IAdminApi;
import com.msewa.imoney.admin.request.dto.GenericReportRequest;
import com.msewa.imoney.admin.request.dto.LoginDTO;
import com.msewa.imoney.admin.response.GenericResponse;
import com.msewa.imoney.admin.response.ResponseJson;
import com.msewa.imoney.cardprocessor.dto.request.CardBlockRequestDTO;
import com.msewa.imoney.cardprocessor.dto.request.RequestJson;
import com.msewa.imoney.cardprocessor.dto.response.CardBlockResponseDTO;
import com.msewa.imoney.cardprocessor.dto.response.TransactionResponse;
import com.msewa.imoney.cardprocessor.dto.response.TransactionStatementResponseDTO;
import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.cardprocessor.service.IMoneyApi;
import com.msewa.imoney.cardprocessor.util.Authorities;
import com.msewa.imoney.enums.ResponseStatus;
import com.msewa.imoney.login.dto.response.AuthenticationError;
import com.msewa.imoney.login.validation.LoginValidation;
import com.msewa.imoney.registration.dto.request.RegistrationRequestDTO;
import com.msewa.imoney.registration.dto.response.RegistrationRespDTO;
import com.msewa.imoney.session.entities.UserSession;
import com.msewa.imoney.session.service.ISessionApi;
import com.msewa.imoney.session.service.PersistingSessionRegistry;
import com.msewa.imoney.session.service.SessionLoggingStrategy;
import com.msewa.imoney.user.api.IUserApi;
import com.msewa.imoney.util.CommonUtil;

@Controller
@RequestMapping("/Master")
public class AdminLoginController {
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final AuthenticationManager authenticationManager;
	private final SessionLoggingStrategy sessionLoggingStrategy;
	private final ISessionApi sessionApi;
	private final LoginValidation loginValidation;
	private final PersistingSessionRegistry persistingSessionRegistry; 
	private final IAdminApi adminApi;
	private final IUserApi userApi;
	private final IMoneyApi iMoneyApi;
	
	public AdminLoginController(AuthenticationManager authenticationManager,SessionLoggingStrategy sessionLoggingStrategy,
			ISessionApi sessionApi , LoginValidation loginValidation,PersistingSessionRegistry persistingSessionRegistry,IAdminApi adminApi,
			IUserApi userApi,IMoneyApi iMoneyApi) {
		this.authenticationManager=authenticationManager;
		this.sessionLoggingStrategy=sessionLoggingStrategy;
		this.sessionApi = sessionApi;
		this.loginValidation =loginValidation;
		this.persistingSessionRegistry=persistingSessionRegistry;
		this.adminApi=adminApi;
		this.userApi=userApi;
		this.iMoneyApi=iMoneyApi;
	}
	
	
	@RequestMapping(value = "/Home", method = RequestMethod.GET)
	public String getGenericHome(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		try {
			String sessionId = (String) session.getAttribute("genericSessionId");
			model.addAttribute("genericLogin", new LoginDTO());
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					if (userSession.getUser() != null
							&& (userSession.getUser().getAuthority().contains(Authorities.ADMINISTRATOR)
							&& userSession.getUser().getAuthority().contains(Authorities.AUTHENTICATED))) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						String daterange[] = CommonUtil.getDefaultDateRange(CommonUtil.formatter1,0);
						
						if(model.get("startDate") !=null && model.get("endDate") !=null && model.get("dashboardData") !=null) {
							model.addAttribute("dashboardData",model.get("dashboardData"));
							model.addAttribute("startDate",model.get("startDate"));
							model.addAttribute("endDate", model.get("endDate"));
						}else {
							model.addAttribute("dashboardData",adminApi.getDashboardDataForAdmin(daterange , userSession.getUser()));
							model.addAttribute("startDate",daterange[0]);
							model.addAttribute("endDate", daterange[1]);
						}
						
						request.getSession().setAttribute("loggedInUserBalance", String.format("%.2f", 
								Double.parseDouble(iMoneyApi.getPrefundBalance(userSession.getUser()))));
						request.getSession().setAttribute("treeBalance", adminApi.getTreeBalance(userSession.getUser()));
						
					} else {
						return "imoney/admin/Login";
					}
				} else {
					return "imoney/admin/Login";
				}
			} else {
				return "imoney/admin/Login";
			}

		} catch (Exception e) {
			e.printStackTrace();
			return "digiosk/generic/Login";
		}
		return "imoney/admin/Home";
	}

	@RequestMapping(value = "/HomeData", method = RequestMethod.POST)
	public String getHomeDataByDate(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model, @ModelAttribute LoginDTO loginDTO, RedirectAttributes redirectAttributes) {
		try {
			String sessionId = (String) session.getAttribute("genericSessionId");
			model.addAttribute("genericLogin", new LoginDTO());
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					if (userSession.getUser() != null
							&& (userSession.getUser().getAuthority().contains(Authorities.ADMINISTRATOR)
									&& userSession.getUser().getAuthority().contains(Authorities.AUTHENTICATED))) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						if (loginDTO.getDateRange() != null && !loginDTO.getDateRange().isEmpty()) {
							String daterange[] = loginDTO.getDateRange().split("-");

							redirectAttributes.addFlashAttribute("dashboardData",
									adminApi.getDashboardDataForAdmin(daterange, userSession.getUser()));
							redirectAttributes.addFlashAttribute("startDate", daterange[0]);
							redirectAttributes.addFlashAttribute("endDate", daterange[1]);

						}
					} else {
						return "imoney/admin/Login";
					}
				} else {
					return "imoney/admin/Login";
				}
			} else {
				return "imoney/admin/Login";
			}

		} catch (Exception e) {
			e.printStackTrace();
			return "imoney/admin/Home";
		}
		return "redirect:/Master/Home";
	}

	@RequestMapping(value = "/Home", method = RequestMethod.POST)
	public String getGenericHome(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model, @ModelAttribute LoginDTO loginDTO) {
		try {
			String error = loginValidation.adminLoginValidate(loginDTO);
			if (error == null) {
				User user = userApi.findByUserName(loginDTO.getUsername());
				if (user != null
						&& (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED))) {
					if (loginDTO.getOtp() != null && loginDTO.getOtp().equals(user.getMobileToken())) {
						AuthenticationError auth = authentication(loginDTO, request);
						if (auth.isSuccess()) {
							Map<String, Object> detail = new HashMap<String, Object>();
							Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
							sessionLoggingStrategy.onAuthentication(authentication, request, response);
							UserSession userSession = sessionApi.getActiveUserSession(
									RequestContextHolder.currentRequestAttributes().getSessionId());
							detail.put("sessionId", userSession.getSessionId());
							request.getSession().setAttribute("genericSessionId",
									RequestContextHolder.currentRequestAttributes().getSessionId());
							request.getSession().setAttribute("role", user.getAuthority());
							request.getSession().setAttribute("loggedInUserProfile", user.getFirstName());
							request.getSession().setAttribute("loggedInUserBalance", iMoneyApi.getPrefundBalance(user));
							request.getSession().setAttribute("treeBalance", adminApi.getTreeBalance(user));
							
							try {
								String [] daterange = CommonUtil.getDefaultDateRange(CommonUtil.formatter1,0);
								model.addAttribute("dashboardData", adminApi.getDashboardDataForAdmin(daterange , userSession.getUser()));
								model.addAttribute("startDate",daterange[0]);
								model.addAttribute("endDate", daterange[1]);
							} catch (Exception e) {
								System.out.println(e.getMessage());
							}
						} else {
							model.addAttribute("genericLogin", loginDTO);
							model.addAttribute(CommonUtil.ERROR_MSG, auth.getMessage());
							return "imoney/admin/Login";
						}
					} else {
						model.addAttribute("genericLogin", loginDTO);
						model.addAttribute(CommonUtil.ERROR_MSG, "Invalid Otp for login into IMoney Portal");
						return "imoney/admin/Login";
					}
				} else {
					model.addAttribute("genericLogin", loginDTO);
					model.addAttribute(CommonUtil.ERROR_MSG, "Invalid role for Login into IMoney Portal.");
					return "imoney/admin/Login";
				}

			} else {
				model.addAttribute(CommonUtil.ERROR_MSG, error);
				model.addAttribute("genericLogin", loginDTO);
				return "imoney/admin/Login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("genericLogin", loginDTO);
			return "imoney/admin/Login";
		}
		return "imoney/admin/Home";
	}

	@RequestMapping(value = "/verifyLogin", method = RequestMethod.POST)
	ResponseEntity<ResponseJson> verifiyLogin(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model, @RequestBody LoginDTO loginDTO) {
		ResponseJson respJson = new ResponseJson();
		try {
			String error = loginValidation.adminLoginValidate(loginDTO);
			if (error == null) {
				User user = userApi.findByUserName(loginDTO.getUsername());
				if (user != null
						&& (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED))) {
					AuthenticationError auth = authentication(loginDTO, request);
					if (auth.isSuccess()) {
						userApi.sendOtpToAdmin(user);
						respJson.setCode(ResponseStatus.SUCCESS.getValue());
						respJson.setDetails(user.getContactNo());
						return new ResponseEntity<ResponseJson>(respJson, HttpStatus.OK);
					} else {
						respJson.setCode(ResponseStatus.FAILURE.getKey());
						respJson.setMessage(auth.getMessage());
						return new ResponseEntity<ResponseJson>(respJson, HttpStatus.OK);
					}
				} else {
					respJson.setCode(ResponseStatus.FAILURE.getValue());
					respJson.setMessage("Invalid Username");
					return new ResponseEntity<ResponseJson>(respJson, HttpStatus.OK);
				}
				

			} else {
				respJson.setCode(ResponseStatus.FAILURE.getKey());
				respJson.setMessage("Invalid Username and Password");
				return new ResponseEntity<ResponseJson>(respJson, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			respJson.setCode(ResponseStatus.FAILURE.getKey());
			return new ResponseEntity<ResponseJson>(respJson, HttpStatus.OK);
		}
	}

	private AuthenticationError authentication(LoginDTO dto, HttpServletRequest request)
			throws ServletException, IOException, Exception {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		UsernamePasswordAuthenticationToken token = null;
		try {
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
			auth = authenticationManager.authenticate(token);
			SecurityContext securityContext = SecurityContextHolder.getContext();
			if (auth.isAuthenticated()) {
				securityContext.setAuthentication(auth);
				SecurityContextHolder.getContext().setAuthentication(auth);
				HttpSession session = request.getSession(true);
				session.setMaxInactiveInterval(24 * 60 * 60);
				session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
				error.setSuccess(true);
				error.setMessage("Login successful.");
				userApi.handleLoginSuccess(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress());
				return error;
			} else {
				error.setSuccess(false);
				error.setMessage(userApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress()));
				SecurityContextHolder.getContext().setAuthentication(null);
				return error;
			}
		} catch (Exception e) {
			e.printStackTrace();
			SecurityContextHolder.getContext().setAuthentication(null);
			error.setSuccess(false);
			error.setMessage(userApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
					dto.getIpAddress()));
			return error;
		}
	}

	@RequestMapping(value = "/kycRequest", method = RequestMethod.GET)
	public String kycDetail(HttpServletRequest userRequest, HttpServletResponse response,
			HttpSession session, Model model) {
		try {
			String sessionId = (String) session.getAttribute("genericSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					return "imoney/admin/kycRequest";
				}else {
					return "imoney/admin/Login";
				}
			}else {
				return "imoney/admin/Login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "imoney/admin/Login";
		}
	}
	
	@RequestMapping(value = "/AddUser", method = RequestMethod.GET)
	public String addUserFromAdmin(HttpServletRequest userRequest, HttpServletResponse response,
			HttpSession session, Model model) {
		try {
			String sessionId = (String) session.getAttribute("genericSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					return "imoney/admin/addUser";
				}else {
					return "redirect:/Master/Home";
				}
			}else {
				return "imoney/admin/Login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "imoney/admin/Login";
		}
	}
	
	@RequestMapping(value = "/AddUser", method = RequestMethod.POST)
	public String addUser(HttpServletRequest request, HttpServletResponse serletResponse,
			HttpSession session, Model model, @ModelAttribute RegistrationRequestDTO registerDTO){
		RegistrationRespDTO response = new RegistrationRespDTO();
		try {
			String sessionId = (String) session.getAttribute("genericSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					User user = userApi.findByUserName(userSession.getUser().getUsername());
					if (user != null && (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED))) {
						if(registerDTO !=null) {
							response =  adminApi.addUser(registerDTO);
							response.setMessage("User Added successfully");
							model.addAttribute("response", response);
							return "redirect:/Master/AddUser";
						}else {
							response.setMessage("error occured.");
						}
					}else {
						response.setMessage("UnAuthrozed");
					}
				}else {
					response.setMessage("Session Expires.");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "imoney/admin/Login";
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/customerReport/{value}")
	public String customerReportGet(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model, @PathVariable(value = "value") String value) {
		try {
			String sessionId = (String) session.getAttribute("genericSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession == null) {
					return "redirect:/Master/Home";
				}
				model.addAttribute("value", value);
				model.addAttribute("role", (String) session.getAttribute("role"));
				persistingSessionRegistry.refreshLastRequest(sessionId);
			} else {
				return "redirect:/Master/Home";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "imoney/admin/customerReport";
		}
			return "imoney/admin/customerReport";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/GenericUserList/{value}")
	ResponseEntity<GenericResponse> getGenericUserList(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model, @RequestBody GenericReportRequest reportRequest,
			@PathVariable(value = "value") String value) {
		GenericResponse resp = new GenericResponse();
		try {
			String sessionId = (String) session.getAttribute("genericSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					if (value == null || value.isEmpty()) {
						value = "All";
					}
					reportRequest.setValue(value);
					reportRequest.setLoggedInUser(userSession.getUser());
					resp = adminApi.getGenericUserReport(reportRequest);
				} else {
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage(ResponseStatus.INVALID_SESSION.getKey());
				}
			} else {
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage(ResponseStatus.INVALID_SESSION.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return new ResponseEntity<GenericResponse>(resp, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/userByUsername")
	ResponseEntity<GenericResponse> getGenericUserListBasedOnRole(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model,
			@RequestBody GenericReportRequest reportRequest) {
		GenericResponse resp = new GenericResponse();
		try {
			String sessionId = (String) session.getAttribute("genericSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					reportRequest.setLoggedInUser(userSession.getUser());
						resp = adminApi.getUserByUsername(reportRequest);
				} else {
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage(ResponseStatus.INVALID_SESSION.getKey());
				}
			} else {
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage(ResponseStatus.INVALID_SESSION.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return new ResponseEntity<GenericResponse>(resp, HttpStatus.OK);
	}


	@RequestMapping(method = RequestMethod.GET, value = "/mccTransaction/{customerId}")
	public String mccTransaction(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model, @PathVariable("customerId") String customerId) {
		try {
			String sessionId = (String) session.getAttribute("genericSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {

					User user = userApi.findByUserName(customerId);
					if(user!=null) {
						TransactionResponse userStatement = iMoneyApi
								.getTransactionResponse(user);
						model.addAttribute("userStatement",(ArrayList<TransactionStatementResponseDTO>) userStatement.getTransactions());
					}
					model.addAttribute("customerId", customerId);
					return "imoney/admin/MCCTransaction";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/Master/Home";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/getAllCards")
	public String getAllCards(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		try {
			String sessionId = (String) session.getAttribute("genericSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession == null) {
					return "redirect:/Master/Home";
				}
				model.addAttribute("role", (String) session.getAttribute("role"));
				persistingSessionRegistry.refreshLastRequest(sessionId);
			} else {
				return "redirect:/Master/Home";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "imoney/admin/cardList";
		}
		return "imoney/admin/cardList";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/getAllCards")
	ResponseEntity<GenericResponse> getAllCardsPost(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model, @RequestBody GenericReportRequest reportRequest) {
		GenericResponse resp = new GenericResponse();
		try {
			String sessionId = (String) session.getAttribute("genericSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					resp = adminApi.getCards(reportRequest);
					resp.setCode(ResponseStatus.SUCCESS.getValue());
				} else {
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage(ResponseStatus.INVALID_SESSION.getKey());
				}
			} else {
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage(ResponseStatus.INVALID_SESSION.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return new ResponseEntity<GenericResponse>(resp, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/cardDetails")
	ResponseEntity<ResponseJson> WalletDetails(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model, @RequestBody GenericReportRequest reportRequest) {
		ResponseJson resp = new ResponseJson();
		try {
			String sessionId = (String) session.getAttribute("genericSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					if (reportRequest.getId() != null) {
						return iMoneyApi.fetchCardDetailsByCardHashId(reportRequest.getId());
					}
				} else {
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage(ResponseStatus.INVALID_SESSION.getKey());
				}
			} else {
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage(ResponseStatus.INVALID_SESSION.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return new ResponseEntity<ResponseJson>(resp, HttpStatus.OK);
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/blockLockUnlockCard")
	ResponseEntity<CardBlockResponseDTO> blockLockUnlockCard(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model, @RequestBody CardBlockRequestDTO requestJson) {
		CardBlockResponseDTO resp = new CardBlockResponseDTO();
		try {
			String sessionId = (String) session.getAttribute("genericSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					if (requestJson.getCardHashId() != null && !requestJson.getCardHashId().isEmpty()) {
						User user = userApi.findByUserName(requestJson.getUsername());
						if(user!=null) {
							if("suspend".equalsIgnoreCase(requestJson.getType())) {
								resp =  iMoneyApi.getCardBlockUnblockResp(requestJson , user.getUserHashId());								
							}else {
								resp =  iMoneyApi.getCardUnblocResp(requestJson , user.getUserHashId());
							}
							resp.setCode(resp.getCode());
							resp.setMessage(resp.getMessage());
						}else {
							resp.setCode(ResponseStatus.FAILURE.getValue());
							resp.setMessage("Invalid user");
						}
					} else {
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage("Error!while updating the card status");
					}
				} else {
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage(ResponseStatus.INVALID_SESSION.getKey());
				}
			} else {
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage(ResponseStatus.INVALID_SESSION.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return new ResponseEntity<CardBlockResponseDTO>(resp, HttpStatus.OK);
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/Transactions")
	public String getTransactions(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		String sessionId = (String) session.getAttribute("genericSessionId");
		if (sessionId != null && !sessionId.isEmpty()) {
			UserSession userSession = sessionApi.getActiveUserSession(sessionId);
			if (userSession != null) {
			try {
				String[] daterange = CommonUtil.getDefaultDateRange(CommonUtil.dateFormate2, -30);
				model.addAttribute("fromDate", daterange[0]);
				model.addAttribute("toDate", daterange[1]);
				model.addAttribute("serviceType", adminApi.getService());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "/imoney/admin/Transactions";
		}
		}
		return "redirect:/Master/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Transactions")
	public ResponseEntity<ResponseJson> getTransactions(@RequestBody RequestJson requestJson, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) {
		String sessionId = (String) session.getAttribute("genericSessionId");
		GenericResponse responseJson = new GenericResponse();
		if (sessionId != null && !sessionId.isEmpty()) {
			UserSession userSession = sessionApi.getActiveUserSession(sessionId);
			if (userSession != null) {
			try {
				String[] daterange = null;
				if (requestJson.getServiceTypeId() == null) {
					requestJson.setServiceTypeId("All");
				}
				if (requestJson.getDaterange() == null) {
					daterange = CommonUtil.getDefaultDateRange(CommonUtil.dateFormate2, -30);
				} else {
					daterange = requestJson.getDaterange().split("-");
				}
				responseJson = adminApi.getTransaction(requestJson, userSession.getUser(), daterange);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		}
		return new ResponseEntity<ResponseJson>(responseJson, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/Logout")
	public String getLogout(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		try {
			String sessionId = (String) session.getAttribute("genericSessionId");
			UserSession userSession = sessionApi.getActiveUserSession(sessionId);
			if (userSession != null) {
				if ((userSession.getUser().getAuthority().contains(Authorities.ADMINISTRATOR)
						&& userSession.getUser().getAuthority().contains(Authorities.AUTHENTICATED))) {
					sessionApi.expireSession(sessionId);
					return "redirect:/Master/Home";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/Master/Home";
	}
	
	@RequestMapping(value = "/TermsCondition", method = RequestMethod.GET)
	public String termsAndCondition() {
		return "/imoney/admin/termsAndCondition";
	}
	
	@RequestMapping(value = "/ForgetPassword", method = RequestMethod.POST)
	public ResponseEntity<GenericResponse> forgetPasswordRequest(HttpServletRequest request,
			HttpServletResponse response, Model model, @RequestBody RequestJson reqJson) {
		GenericResponse resp = new GenericResponse();
		try {
			if (reqJson.getUsername() != null && !reqJson.getUsername().isEmpty()) {
				User user = userApi.forgetPasswordOTP(reqJson.getUsername());
				if (user != null) {
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setMessage("Otp has been sent to admin contact " );
				} else {
					resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
					resp.setMessage("User does not exist in active mode.");
				}
			} else {
				resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
				resp.setMessage(ResponseStatus.BAD_REQUEST.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return new ResponseEntity<GenericResponse>(resp, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/ForgetPasswordWithOtp", method = RequestMethod.POST)
	public ResponseEntity<GenericResponse> ForgetPasswordWithOtp(HttpServletRequest request,
			HttpServletResponse response, Model model, @RequestBody RequestJson dto) {
		GenericResponse resp = new GenericResponse();
		try {
			boolean valid = userApi.checkValidOTP(dto.getOtp(), dto.getUsername());
			if (valid) {
				userApi.forgetPassword(dto);
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setMessage(ResponseStatus.SUCCESS.getKey());
			} else {
				resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
				resp.setMessage("Please enter valid otp");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return new ResponseEntity<GenericResponse>(resp, HttpStatus.OK);
	}
}

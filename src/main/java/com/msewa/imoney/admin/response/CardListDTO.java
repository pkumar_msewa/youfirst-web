package com.msewa.imoney.admin.response;

public class CardListDTO {

	private String firstName;
	private String lastName;
	private String email;
	private String mobile;
	private String 	cardNo;
	private String 	proxyNo;
	private String 	created;
	private String 	expiryDate;
	private String 	preferredName;
	private String 	walletHashId;
	private String 	cardHashId;
	private String 	cardStatus;
	private String 	cardType;
	private double balance;
	
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getProxyNo() {
		return proxyNo;
	}
	public void setProxyNo(String proxyNo) {
		this.proxyNo = proxyNo;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getPreferredName() {
		return preferredName;
	}
	public void setPreferredName(String preferredName) {
		this.preferredName = preferredName;
	}
	public String getWalletHashId() {
		return walletHashId;
	}
	public void setWalletHashId(String walletHashId) {
		this.walletHashId = walletHashId;
	}
	public String getCardHashId() {
		return cardHashId;
	}
	public void setCardHashId(String cardHashId) {
		this.cardHashId = cardHashId;
	}
	public String getCardStatus() {
		return cardStatus;
	}
	public void setCardStatus(String cardStatus) {
		this.cardStatus = cardStatus;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
}

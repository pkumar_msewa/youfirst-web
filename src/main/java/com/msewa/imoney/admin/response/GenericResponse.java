package com.msewa.imoney.admin.response;

public class GenericResponse extends ResponseJson {

	private long totalPage;

	private long count;

	public long getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(long totalPage) {
		this.totalPage = totalPage;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}
}

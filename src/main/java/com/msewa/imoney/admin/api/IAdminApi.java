package com.msewa.imoney.admin.api;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.msewa.imoney.admin.request.dto.GenericDashboardData;
import com.msewa.imoney.admin.request.dto.GenericReportRequest;
import com.msewa.imoney.admin.response.GenericResponse;
import com.msewa.imoney.cardprocessor.dto.request.RequestJson;
import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.registration.dto.request.RegistrationRequestDTO;
import com.msewa.imoney.registration.dto.response.RegistrationRespDTO;
import com.msewa.imoney.transaction.entities.Services;

public interface IAdminApi {

	GenericDashboardData getDashboardDataForAdmin(String[] daterange, User user);
	GenericResponse getGenericUserReport(GenericReportRequest reportRequest);
	GenericResponse getUserByUsername(GenericReportRequest reportRequest);
	GenericResponse getCards(GenericReportRequest reportRequest);
	GenericResponse getTransaction(RequestJson requestJson, User user, String[] daterange);
	List<Services> getService();
	double getTreeBalance(User user);
	RegistrationRespDTO addUser(RegistrationRequestDTO registerDTO);
}


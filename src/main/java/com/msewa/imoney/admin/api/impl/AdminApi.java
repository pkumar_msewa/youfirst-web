package com.msewa.imoney.admin.api.impl;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.msewa.imoney.admin.api.IAdminApi;
import com.msewa.imoney.admin.request.dto.GenericDashboardData;
import com.msewa.imoney.admin.request.dto.GenericReportRequest;
import com.msewa.imoney.admin.response.GenericResponse;
import com.msewa.imoney.cardprocessor.dto.request.RequestJson;
import com.msewa.imoney.cardprocessor.entities.Cards;
import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.cardprocessor.repositories.CardRepository;
import com.msewa.imoney.cardprocessor.repositories.UserRepository;
import com.msewa.imoney.cardprocessor.util.Authorities;
import com.msewa.imoney.enums.CardType;
import com.msewa.imoney.enums.ResponseStatus;
import com.msewa.imoney.enums.Status;
import com.msewa.imoney.enums.UserType;
import com.msewa.imoney.registration.dto.request.RegistrationRequestDTO;
import com.msewa.imoney.registration.dto.response.RegistrationRespDTO;
import com.msewa.imoney.registration.service.IRegistrationApi;
import com.msewa.imoney.transaction.entities.Services;
import com.msewa.imoney.transaction.entities.Transactions;
import com.msewa.imoney.transaction.repositories.ServiceRepository;
import com.msewa.imoney.transaction.repositories.TransactionRepository;
import com.msewa.imoney.util.CommonUtil;
import com.msewa.imoney.util.ConvertUtil;

/**
 * @author arshad
 *
 */
public class AdminApi implements IAdminApi{

	private final UserRepository userRepository;
	private final CardRepository cardRepository;
	private final ServiceRepository serviceRepository;
	private final TransactionRepository transactionRepository;
	private final IRegistrationApi registrationApi;
	
public AdminApi(UserRepository userRepository , CardRepository cardRepository,ServiceRepository serviceRepository ,final TransactionRepository transactionRepository, IRegistrationApi registrationApi) {
	this.userRepository=userRepository;
	this.cardRepository=cardRepository;
	this.serviceRepository=serviceRepository;
	this.transactionRepository=transactionRepository;
	this.registrationApi = registrationApi;
}	
	
	@Override
	public GenericDashboardData getDashboardDataForAdmin(String[] daterange, User user) {
		GenericDashboardData genericDashboardData = new GenericDashboardData();
		try {
			Date fromDate = CommonUtil.dateFormate2.parse(daterange[0] + CommonUtil.startTime);
			Date toDate = CommonUtil.dateFormate2.parse(daterange[1] + CommonUtil.endTime);
			genericDashboardData.setTotalUsers(userRepository.getAllUserByType(UserType.User, fromDate, toDate));
			genericDashboardData.setActiveUser(userRepository.findAllActiveUser(fromDate, toDate, UserType.User));
			genericDashboardData.setInactiveUsers(userRepository.findAllInactiveUser(fromDate, toDate, UserType.User));
			genericDashboardData.setBlockedUsers(userRepository.getAllUserByType(UserType.User,
					Authorities.USER + "," + Authorities.BLOCKED, fromDate, toDate));
			genericDashboardData.setTotalPCards(cardRepository.getCardCount(CardType.physical, fromDate, toDate));
			genericDashboardData.setTotalVCards(cardRepository.getCardCount(CardType.virtual, fromDate, toDate));
			genericDashboardData.setTotalWallets(userRepository.getAllUserByType(UserType.User, fromDate, toDate));
			genericDashboardData.setBlockedCards(cardRepository.getCardByStatus(Status.Blocked, fromDate, toDate));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return genericDashboardData;
	}

	@Override
	public GenericResponse getGenericUserReport(GenericReportRequest request) {
		GenericResponse response = new GenericResponse();
		String startDate = null;
		String endDate = null;
		String dateRange = null;
		Page<User> mUserPage = null;
		Sort sort = new Sort(Sort.Direction.DESC, "created");
		Pageable pageable = new PageRequest(request.getPage(), request.getSize(), sort);
		try {
			if (request.getDateRange() != null && !request.getDateRange().isEmpty()) {
				dateRange = request.getDateRange();
				String[] dateArray = dateRange.split("-");
				if (dateArray.length == 2) {
					startDate = dateArray[0] + CommonUtil.startTime;
					endDate = dateArray[1] + CommonUtil.endTime;
				}
			} else {
				String daterange[] = CommonUtil.getDefaultDateRange(CommonUtil.dateFormate2, -30);
				startDate = daterange[0] + CommonUtil.startTime;
				endDate = daterange[1] + CommonUtil.endTime;
			}
			if (startDate != null && endDate != null) {
				mUserPage = userRepository.getAllUserByPage(pageable, CommonUtil.dateFormate2.parse(startDate),
						CommonUtil.dateFormate2.parse(endDate) , UserType.User);
				if (mUserPage != null) {
					response.setDetails(mUserPage);
					response.setTotalPage(mUserPage.getTotalPages());
				}
			} 
		} catch (Exception e) {

		}
		return response;
	}
	@Override
	public GenericResponse getUserByUsername(GenericReportRequest reportRequest) {
		GenericResponse resp = new GenericResponse();
		User user = userRepository.getCustomUserByUsername(reportRequest.getUsername().trim());
		resp.setDetails(user);
		return resp;
	}

	@Override
	public RegistrationRespDTO addUser(RegistrationRequestDTO registerDTO) {
		RegistrationRespDTO response = new RegistrationRespDTO();
		User user = userRepository.findByUsername(registerDTO.getMobileNumber());
		if(user == null){
			User email = userRepository.findByEmail(registerDTO.getEmail());
			if(email == null) {
				return registrationApi.registerFromAdmin(registerDTO);
			}else {
				response.setCode(ResponseStatus.FAILURE);
				response.setMessage("Email already in use.");
			}
		}else {
			response.setCode(ResponseStatus.FAILURE);
			response.setMessage("Mobile number is already in Use.");
		}
		return response;
	}
	
	@Override
	public GenericResponse getCards(GenericReportRequest request) {
		GenericResponse response = new GenericResponse();
		Sort sort = new Sort(Sort.Direction.DESC, "created");
		String userName=request.getUsername();
		Pageable pageable = new PageRequest(request.getPage(), request.getSize(), sort);
		String dateArray[] = null;
		Page<Cards> pageCards = null;
		try {
			if (userName != null) {
				User u= userRepository.findByUsername(userName);
				 pageCards = cardRepository.getAllCards(pageable,u);
			} else {
				if (request.getDateRange() != null && !request.getDateRange().isEmpty()) {

					dateArray = request.getDateRange().split("-");
				} else {
					dateArray = CommonUtil.getDefaultDateRange(CommonUtil.dateFormate2, -30);
				}
				pageCards = cardRepository.getAllCards(pageable,
						CommonUtil.dateFormate2.parse(dateArray[0] + CommonUtil.startTime),
						CommonUtil.dateFormate2.parse(dateArray[1] + CommonUtil.endTime));
			}
			if(pageCards!=null) {
				response.setDetails(ConvertUtil.getCardList(pageCards));
				response.setTotalPage(pageCards.getTotalPages());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
		return response;
	}

	@Override
	public GenericResponse getTransaction(RequestJson requestJson, User user, String[] daterange) {
		GenericResponse response = new GenericResponse();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "created");
			Pageable pageable = new PageRequest(requestJson.getPage(), requestJson.getSize(), sort);
			Page<Transactions> txnPages =null;
			if(requestJson.getServiceTypeId() !=null && !"All".equals(requestJson.getServiceTypeId())) {
				 txnPages = transactionRepository.getTransactionByFilters(pageable,
						serviceRepository.findServiceByCode(requestJson.getServiceTypeId()) , 
						CommonUtil.dateFormate2.parse(daterange[0] + CommonUtil.startTime),
						CommonUtil.dateFormate2.parse(daterange[1] + CommonUtil.endTime));
			}else {
				 txnPages = transactionRepository.getTransactionByFilters(pageable , 
						 CommonUtil.dateFormate2.parse(daterange[0] + CommonUtil.startTime),
						CommonUtil.dateFormate2.parse(daterange[1] + CommonUtil.endTime));
			}if (txnPages != null) {
				response.setDetails(txnPages);
				response.setTotalPage(txnPages.getTotalPages());
			}
			return response;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;

	}

	@Override
	public List<Services> getService() {
		return serviceRepository.getServiceByStatus(Status.Active);
	}

	@Override
	public double getTreeBalance(User user) {
		// TODO Auto-generated method stub
		return 0;
	}

}

package com.msewa.imoney.registration.validation;

import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.cardprocessor.repositories.UserRepository;
import com.msewa.imoney.enums.ResponseStatus;
import com.msewa.imoney.registration.dto.request.RegistrationRequestDTO;
import com.msewa.imoney.registration.dto.response.RegisterValidationRespDTO;

public class RegisterValidation {

	private final UserRepository userRepository;
	
	
	
	public RegisterValidation(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}


	

	public RegisterValidationRespDTO validateUserBeforeRegistration(RegistrationRequestDTO registrationRequestDTO){
		RegisterValidationRespDTO validateRegister=new RegisterValidationRespDTO();
		boolean valid=true;
		/**
		 * check for duplicate mobile number
		 */
		User user_mobile=userRepository.findByUsername(registrationRequestDTO.getMobileNumber().trim());
		if(user_mobile!=null){
			if(user_mobile.getMobileStatus().getValue().equalsIgnoreCase("Inactive")){
			valid=false;
			validateRegister.setValid(valid);
			validateRegister.setCode(ResponseStatus.MOBILE_INACTIVE);
			}
		}
		
		/**
		 * check for duplicate email for user
		 */
		User user_email=userRepository.findByEmail(registrationRequestDTO.getEmail().trim());
		if(user_email!=null){
			valid=false;
			validateRegister.setValid(valid);
			validateRegister.setCode(ResponseStatus.EMAIL_ALREADY_EXIST);
			validateRegister.setMessage("Email is already in use.");
		}
		
		validateRegister.setValid(valid);
		return validateRegister;
	}
	
	
}

package com.msewa.imoney.registration.service;

import javax.servlet.http.HttpSession;

import org.springframework.http.ResponseEntity;

import com.msewa.imoney.dto.CommonDTO;
import com.msewa.imoney.registration.dto.request.ForgotPasswordDTO;
import com.msewa.imoney.registration.dto.request.MPinRequestDTO;
import com.msewa.imoney.registration.dto.request.RegistrationRequestDTO;
import com.msewa.imoney.registration.dto.request.VerifyOtpDTO;
import com.msewa.imoney.registration.dto.response.ForgotPasswordRespDTO;
import com.msewa.imoney.registration.dto.response.MPinRespDTO;
import com.msewa.imoney.registration.dto.response.RegistrationRespDTO;

public interface IRegistrationApi {

	ResponseEntity<RegistrationRespDTO> registerUserFully(RegistrationRequestDTO requestDTO);
	ResponseEntity<ForgotPasswordRespDTO> forgotPassword(ForgotPasswordDTO passwordDTO);
	ResponseEntity<RegistrationRespDTO> validateMobileNumber(RegistrationRequestDTO requestDTO);
	ResponseEntity<MPinRespDTO> setMpinForUser(MPinRequestDTO mPinRequestDTO, HttpSession session);
	ResponseEntity<MPinRespDTO> forgotMpin(MPinRequestDTO mPinRequestDTO);
	ResponseEntity<CommonDTO> resendOTPForUserRegistration(VerifyOtpDTO dto);
	ResponseEntity<CommonDTO> verifyUserByOTP(VerifyOtpDTO dto);
	ResponseEntity<MPinRespDTO> changeMpiforUser(MPinRequestDTO mPinRequestDTO);
	ResponseEntity<MPinRespDTO> verifyMpin(MPinRequestDTO mPinRequestDTO);
	ResponseEntity<ForgotPasswordRespDTO> verifyForgotPassword(ForgotPasswordDTO forgotPasswordDTO);
	RegistrationRespDTO registerFromAdmin(RegistrationRequestDTO registerDTO);
	
}

package com.msewa.imoney.registration.service.Impl;

import java.text.ParseException;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.msewa.imoney.cardprocessor.dto.request.CreateUserDTO;
import com.msewa.imoney.cardprocessor.dto.response.CreateUserResponseDTO;
import com.msewa.imoney.cardprocessor.dto.response.WalletCreationResponseDTO;
import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.cardprocessor.entities.WalletInfo;
import com.msewa.imoney.cardprocessor.repositories.UserRepository;
import com.msewa.imoney.cardprocessor.repositories.WalletInfoRepository;
import com.msewa.imoney.cardprocessor.service.ICPUserApi;
import com.msewa.imoney.cardprocessor.service.ICPWalletApi;
import com.msewa.imoney.cardprocessor.util.Authorities;
import com.msewa.imoney.dto.CommonDTO;
import com.msewa.imoney.enums.AccountType;
import com.msewa.imoney.enums.ResponseStatus;
import com.msewa.imoney.enums.Status;
import com.msewa.imoney.enums.UserType;
import com.msewa.imoney.registration.dto.request.ForgotPasswordDTO;
import com.msewa.imoney.registration.dto.request.MPinRequestDTO;
import com.msewa.imoney.registration.dto.request.RegistrationRequestDTO;
import com.msewa.imoney.registration.dto.request.VerifyOtpDTO;
import com.msewa.imoney.registration.dto.response.ForgotPasswordRespDTO;
import com.msewa.imoney.registration.dto.response.MPinRespDTO;
import com.msewa.imoney.registration.dto.response.RegisterValidationRespDTO;
import com.msewa.imoney.registration.dto.response.RegistrationRespDTO;
import com.msewa.imoney.registration.service.IRegistrationApi;
import com.msewa.imoney.registration.validation.RegisterValidation;
import com.msewa.imoney.session.entities.UserSession;
import com.msewa.imoney.session.repositories.UserSessionRepository;
import com.msewa.imoney.sms.service.ISMSSenderApi;
import com.msewa.imoney.sms.util.SMSTemplete;
import com.msewa.imoney.util.CommonUtil;
import com.msewa.imoney.util.CommonValidation;

public class RegistrationApiImpl implements IRegistrationApi{

	private final UserRepository userRepository;
	private final RegisterValidation registerValidation;
	private final PasswordEncoder passwordEncoder;
	private final ICPUserApi userApi;
	private final ICPWalletApi walletApi;
	private final WalletInfoRepository walletInfoRepository;
	private final UserSessionRepository userSessionRepository;
	private final ISMSSenderApi senderApi;

	public RegistrationApiImpl(UserRepository userRepository, RegisterValidation registerValidation,
			PasswordEncoder passwordEncoder, ICPUserApi userApi, ICPWalletApi walletApi,
			WalletInfoRepository walletInfoRepository, UserSessionRepository userSessionRepository
			,ISMSSenderApi senderApi) {
		super();
		this.userRepository = userRepository;
		this.registerValidation = registerValidation;
		this.passwordEncoder = passwordEncoder;
		this.userApi = userApi;
		this.walletApi = walletApi;
		this.walletInfoRepository=walletInfoRepository;
		this.userSessionRepository = userSessionRepository;
		this.senderApi=senderApi;
	}
	
	@Override
	public ResponseEntity<RegistrationRespDTO> registerUserFully(RegistrationRequestDTO requestDTO) {
		RegistrationRespDTO registerResp=new RegistrationRespDTO();
		try{
		RegisterValidationRespDTO validRegister=registerValidation.validateUserBeforeRegistration(requestDTO);
		if(validRegister.isValid()){
			
			User user=userRepository.findByUsername(requestDTO.getMobileNumber().trim());
			if(user!=null){
			user.setGender(requestDTO.getGender());
			user.setFirstName(requestDTO.getFirst_name());
			user.setLastName(requestDTO.getLast_name());
			user.setMiddleName(requestDTO.getMiddle_name());
			user.setEmail(requestDTO.getEmail());
			try {
				
				user.setDateOfBirth(CommonUtil.formatter.parse(requestDTO.getBirthday()));
			} catch (ParseException e) {
				e.printStackTrace();
				registerResp.setCode(ResponseStatus.FAILURE);
				registerResp.setMessage(e.getMessage());
				return new ResponseEntity<RegistrationRespDTO>(registerResp,HttpStatus.OK);
				
			}
			user.setPassword(passwordEncoder.encode(requestDTO.getPassword()));
			user.setIdType(requestDTO.getId_type());
			user.setIdNo(requestDTO.getId_number());
			user.setFullyFilled(true);
			userRepository.save(user);
			//write mm create user and create wallet
			CreateUserDTO createUserOnMM=new CreateUserDTO();
			createUserOnMM.setEmail(user.getEmail());
			createUserOnMM.setPassword(user.getPassword());
			createUserOnMM.setFirst_name(user.getFirstName());
			createUserOnMM.setLast_name(user.getLastName());
			createUserOnMM.setMiddle_name(user.getMiddleName());
			createUserOnMM.setMobile_country_code("91");
			createUserOnMM.setMobile(user.getUsername());
			String preferredName=user.getFirstName()+" "+user.getLastName();
			String customPreferredName=null;
			if(preferredName.length()>=25){
				String first_name=preferredName.substring(0,1);
				customPreferredName=first_name+" "+user.getLastName();
				createUserOnMM.setPreferred_name(customPreferredName);

			}else{
				createUserOnMM.setPreferred_name(preferredName);

			}
			
			CreateUserResponseDTO createUserResp=userApi.createUserOnCardProccessor(createUserOnMM);
			if(createUserResp!=null){
				if(createUserResp.getCode().equalsIgnoreCase("S00")){
					//Write code for creating wallet
					User userHashIdToBeUpdated=userRepository.findByUsername(user.getUsername());
					userHashIdToBeUpdated.setUserHashId(createUserResp.getUserHashId());
					userRepository.save(userHashIdToBeUpdated);
				WalletCreationResponseDTO walletCreationResp=walletApi.createWallet(createUserResp.getUserHashId());
				if(walletCreationResp!=null){
					WalletInfo wallet=new WalletInfo();
					if(walletCreationResp.getCode().equalsIgnoreCase("S00")){
						wallet.setWalletHashId(walletCreationResp.getWalletId());
						wallet.setStatus(Status.Active);
						registerResp.setCode(ResponseStatus.SUCCESS);
						registerResp.setVerified(true);
						validRegister.setValid(true);
						registerResp.setMessage("Registration Successfully Done. Please Login to Continue.");
					}else{
						wallet.setRemarks(walletCreationResp.getMessage());
						registerResp.setCode(ResponseStatus.FAILURE);

					}
					wallet.setUser(user);
					walletInfoRepository.save(wallet);

				}
				}else{
					registerResp.setCode(ResponseStatus.FAILURE);
					user.setRemarks((String)createUserResp.getDetails());
					userRepository.save(user);
				}
			}
			
			//write welcome send SMS for wallet Creation
			
			
		}else{
			registerResp.setCode(ResponseStatus.FAILURE);
		}
		}else{
			registerResp.setCode(ResponseStatus.getEnumByValue(validRegister.getCode()));
		}
		}catch(Exception e){
			e.printStackTrace();
			registerResp.setCode(ResponseStatus.FAILURE);
			return new ResponseEntity<RegistrationRespDTO>(registerResp,HttpStatus.EXPECTATION_FAILED);
		}
		
		return new ResponseEntity<RegistrationRespDTO>(registerResp,HttpStatus.OK);
	}

	
	@Override
	public ResponseEntity<CommonDTO> verifyUserByOTP(VerifyOtpDTO dto) {
		CommonDTO comDto=new CommonDTO();
		User user=userRepository.findByUsername(dto.getMobileNumber());
		if(user!=null){
			if(user.getMobileStatus().getValue().equalsIgnoreCase("Inactive")){
					if(user.getMobileToken().equalsIgnoreCase(dto.getKey().trim())){
					user.setMobileStatus(Status.Active);
					userRepository.save(user);
					comDto.setCode(ResponseStatus.SUCCESS);
					comDto.setMessage(ResponseStatus.SUCCESS.getKey());
					return new ResponseEntity<CommonDTO>(comDto,HttpStatus.OK);
				}else{
					comDto.setCode(ResponseStatus.INVALID_OTP);
					comDto.setMessage("Please enter correct OTP.");
					return new ResponseEntity<CommonDTO>(comDto,HttpStatus.OK);
				}
			}else{
				comDto.setCode(ResponseStatus.MOBILE_ALREADY_VERIFIED);
				return new ResponseEntity<CommonDTO>(comDto,HttpStatus.OK);
			}
		}else{
			comDto.setCode(ResponseStatus.FAILURE);
			return new ResponseEntity<CommonDTO>(comDto,HttpStatus.OK);

		}
		
	}
	
	@Override
	public ResponseEntity<ForgotPasswordRespDTO> forgotPassword(ForgotPasswordDTO passwordDTO) {
		
		ForgotPasswordRespDTO forgotPassword=new ForgotPasswordRespDTO();
		
		User user=userRepository.findByUsername(passwordDTO.getUsername());
		if(user!=null){
			if(user.getMobileStatus().getValue().equalsIgnoreCase("Active")){
				user.setMobileToken(CommonUtil.generateSixDigitNumericString(passwordDTO.getUsername()));
				userRepository.save(user);
			
				senderApi.sendUserSMS(SMSTemplete.FORGET_PASS, user);
				
				forgotPassword.setCode(ResponseStatus.SUCCESS);
				forgotPassword.setMessage(ResponseStatus.SUCCESS);
				return new ResponseEntity<ForgotPasswordRespDTO>(forgotPassword, HttpStatus.OK);
			}else{
				forgotPassword.setCode(ResponseStatus.FAILURE);
				return new ResponseEntity<ForgotPasswordRespDTO>(forgotPassword, HttpStatus.OK);
			}
		}else{
			forgotPassword.setCode(ResponseStatus.FAILURE);
			return new ResponseEntity<ForgotPasswordRespDTO>(forgotPassword, HttpStatus.OK);
		}
	}
	
	@Override
	public ResponseEntity<ForgotPasswordRespDTO> verifyForgotPassword(ForgotPasswordDTO forgotPasswordDTO) {
		ForgotPasswordRespDTO forgotPasswordResponse=new ForgotPasswordRespDTO();
		try {
			User user=userRepository.findByUsername(forgotPasswordDTO.getUsername());
			if(user!=null) {
				if(user.getMobileStatus().getValue().equalsIgnoreCase("Active")){
					if(user.getMobileToken().equalsIgnoreCase(forgotPasswordDTO.getKey().trim())){
						if(forgotPasswordDTO.getPassword().equalsIgnoreCase(forgotPasswordDTO.getConfirmPassword())) {
							user.setPassword(passwordEncoder.encode(forgotPasswordDTO.getConfirmPassword()));
							userRepository.save(user);
							forgotPasswordResponse.setCode(ResponseStatus.SUCCESS);
							forgotPasswordResponse.setMessage(ResponseStatus.SUCCESS);
						}else {
							forgotPasswordResponse.setCode(ResponseStatus.FAILURE);
							forgotPasswordResponse.setMessage(ResponseStatus.FAILURE);
						}
					}else {
						forgotPasswordResponse.setCode(ResponseStatus.INVALID_OTP);
						forgotPasswordResponse.setMessage(ResponseStatus.INVALID_OTP);
					}
				}else {
					forgotPasswordResponse.setCode(ResponseStatus.UNAUTHORIZED_USER);
					forgotPasswordResponse.setMessage(ResponseStatus.UNAUTHORIZED_ROLE);
				}
				
			}else {
				forgotPasswordResponse.setCode(ResponseStatus.FAILURE);
				forgotPasswordResponse.setMessage(ResponseStatus.FAILURE);
			}
		
	} catch (Exception e) {
		e.printStackTrace();
	}
		return new ResponseEntity<ForgotPasswordRespDTO>(forgotPasswordResponse, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<RegistrationRespDTO> validateMobileNumber(RegistrationRequestDTO requestDTO) {
		String otp=CommonUtil.generateSixDigitNumericString(requestDTO.getMobileNumber());
		RegistrationRespDTO registerResp=new RegistrationRespDTO();
		
		User user=userRepository.findByUsername(requestDTO.getMobileNumber().trim());
		if(user!=null){
			if(user.getMobileStatus().getValue().equalsIgnoreCase("Inactive")){
				user.setMobileToken(otp);
				userRepository.save(user);
				senderApi.sendUserSMS(SMSTemplete.MOBILE_VERIFICATION, user);
				registerResp.setCode(ResponseStatus.SUCCESS);
				registerResp.setVerified(false);
			}else {
				if(CommonValidation.isNull(user.getFirstName())) {
					registerResp.setCode(ResponseStatus.SUCCESS);
					registerResp.setVerified(true);
					registerResp.setMessage("Please enter full details");
				}else {
					registerResp.setCode(ResponseStatus.FAILURE);
					registerResp.setVerified(true);
					registerResp.setMessage("This mobile number is already in use, please login.");
				}
			}
		}else if(user==null){
			user=new User();
			user.setAccountType(AccountType.NONKYC);
			if (CommonValidation.isNumeric(requestDTO.getMobileNumber().trim()) && requestDTO.getMobileNumber().length()==10) {
				user.setContactNo(requestDTO.getMobileNumber());
			}else {
				registerResp.setCode(ResponseStatus.FAILURE);
				registerResp.setMessage("Please enter valid mobile number");
				registerResp.setVerified(false);
			}
			user.setAuthority(Authorities.USER+","+Authorities.AUTHENTICATED);
			user.setUsername(requestDTO.getMobileNumber().trim());
			user.setUserType(UserType.User);
			user.setMobileToken(otp);
			user.setMobileStatus(Status.Inactive);
			user.setFullyFilled(false);
			userRepository.save(user);
			registerResp.setCode(ResponseStatus.SUCCESS);
			registerResp.setVerified(false);
			senderApi.sendUserSMS(SMSTemplete.MOBILE_VERIFICATION, user);
		}
		return new ResponseEntity<RegistrationRespDTO>(registerResp,HttpStatus.OK);
		
	}
	
	@Override
	public ResponseEntity<MPinRespDTO> setMpinForUser(MPinRequestDTO mPinRequestDTO, HttpSession session) {

		MPinRespDTO mPinResp=new MPinRespDTO();
		UserSession userSession = userSessionRepository.findBySessionId(mPinRequestDTO.getSessionId());
		User user=userRepository.findOne(userSession.getUser().getId());
		if(user!=null){
			if(user.getMobileStatus().getValue().equalsIgnoreCase("Active")){
				if(user.getAuthority().contains("USER")&&user.getAuthority().contains("AUTHENTICATED")){
					if(user.getMpin()==null){
						if(mPinRequestDTO.getMpin().equals(mPinRequestDTO.getConfirm_mpin())){
							user.setMpin(passwordEncoder.encode(mPinRequestDTO.getMpin()));
							userRepository.save(user);
							mPinResp.setCode(ResponseStatus.SUCCESS);
							return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.OK);
						}else{
							mPinResp.setCode(ResponseStatus.INVALID_MPIN);
							mPinResp.setMessage("MPIN missmatch.");
							return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.OK);
						}
					}else{
						mPinResp.setCode(ResponseStatus.MPIN_ALREADY_EXIST);
						return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.OK);
					}
				}else{
					mPinResp.setCode(ResponseStatus.UNAUTHORIZED_USER);
					return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.UNAUTHORIZED);
				}
			}else{
				mPinResp.setCode(ResponseStatus.UNAUTHORIZED_USER);
				return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.UNAUTHORIZED);
			}
		}else{
			mPinResp.setCode(ResponseStatus.UNAUTHORIZED_USER);
			return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.UNAUTHORIZED);
		}
	}
	
	@Override
	public ResponseEntity<MPinRespDTO> verifyMpin(MPinRequestDTO mPinRequestDTO) {
		MPinRespDTO mPinResp=new MPinRespDTO();
		UserSession userSession = userSessionRepository.findBySessionId(mPinRequestDTO.getSessionId());
		User user=userRepository.findOne(userSession.getUser().getId());
		if(user!=null){
			if(user.getMobileStatus().getValue().equalsIgnoreCase("Active")){
				if(user.getAuthority().contains("USER")&&user.getAuthority().contains("AUTHENTICATED")){
					if(user.getMpin()!=null){
						if(passwordEncoder.matches(mPinRequestDTO.getMpin(), user.getMpin())){
							mPinResp.setCode(ResponseStatus.SUCCESS);
							mPinResp.setMessage("Pin Verified Successfully.");
							return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.OK);
							
						}else{
							mPinResp.setCode(ResponseStatus.MPIN_NOTCONFIRMED);
							mPinResp.setMessage(ResponseStatus.MPIN_NOTCONFIRMED.getKey());
							return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.OK);
						}
					}else{
						mPinResp.setCode(ResponseStatus.INVALID_MPIN);
						return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.OK);
					}
				}else{
					mPinResp.setCode(ResponseStatus.UNAUTHORIZED_ROLE);
					return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.UNAUTHORIZED);
				}
			}else{
				mPinResp.setCode(ResponseStatus.USER_LOCKED);
				return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.UNAUTHORIZED);
			}
		}else
		{
			mPinResp.setCode(ResponseStatus.UNAUTHORIZED_USER);
			return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.UNAUTHORIZED);
		}
	}
	
	@Override
	public ResponseEntity<MPinRespDTO> changeMpiforUser(MPinRequestDTO mPinRequestDTO) {
		MPinRespDTO mPinResp=new MPinRespDTO();
		UserSession userSession = userSessionRepository.findBySessionId(mPinRequestDTO.getSessionId());
		User user=userRepository.findOne(userSession.getUser().getId());
		if(user!=null){
			if(user.getMobileStatus().getValue().equalsIgnoreCase("Active")){
				if(user.getAuthority().contains("USER")&&user.getAuthority().contains("AUTHENTICATED")){
					if(passwordEncoder.matches(mPinRequestDTO.getOld_mpin(), user.getMpin())){
						if(!CommonValidation.isNull(mPinRequestDTO.getNew_mpin()) && ! CommonValidation.isNull(mPinRequestDTO.getConfirm_mpin())){
							if(mPinRequestDTO.getNew_mpin().equals(mPinRequestDTO.getConfirm_mpin())){
								user.setMpin(passwordEncoder.encode(mPinRequestDTO.getNew_mpin()));
								userRepository.save(user);
								mPinResp.setCode(ResponseStatus.SUCCESS);
								mPinResp.setMessage("Pin changed Successfully.");
								return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.OK);
								
							}else{
								mPinResp.setCode(ResponseStatus.MPIN_ALREADY_EXIST);
								mPinResp.setMessage("MPIN mismatch.");
								return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.OK);
							}
						}else{
							mPinResp.setCode(ResponseStatus.INVALID_MPIN);
							mPinResp.setMessage("Invalid MPIN.");
							return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.OK);
						}
						
					}else{
						mPinResp.setCode(ResponseStatus.INVALID_MPIN);
						return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.OK);
					}
				}else{
					mPinResp.setCode(ResponseStatus.UNAUTHORIZED_USER);
					return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.UNAUTHORIZED);
				}
				
			}else{
				mPinResp.setCode(ResponseStatus.UNAUTHORIZED_USER);
				return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.UNAUTHORIZED);
			}
		}else{
			mPinResp.setCode(ResponseStatus.UNAUTHORIZED_USER);
			return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.UNAUTHORIZED);
		}
	}

	@Override
	public ResponseEntity<MPinRespDTO> forgotMpin(MPinRequestDTO mPinRequestDTO) {
		MPinRespDTO mPinResp=new MPinRespDTO();
		UserSession userSession = userSessionRepository.findBySessionId(mPinRequestDTO.getSessionId());
		User user=userRepository.findOne(userSession.getUser().getId());
		if(user!=null){
			if(user.getMobileStatus().getValue().equalsIgnoreCase("Active")){
				if(user.getAuthority().contains("USER")&&user.getAuthority().contains("AUTHENTICATED")){
					if(user.getMpin()!=null){
						if(!CommonValidation.isNull(mPinRequestDTO.getNew_mpin()) && ! CommonValidation.isNull(mPinRequestDTO.getConfirm_mpin())){
							if(mPinRequestDTO.getNew_mpin().equals(mPinRequestDTO.getConfirm_mpin())){
								user.setMpin(passwordEncoder.encode(mPinRequestDTO.getNew_mpin()));
								userRepository.save(user);
								mPinResp.setCode(ResponseStatus.SUCCESS);
								mPinResp.setMessage("Pin changed Successfully.");
								return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.OK);
								
							}else{
								mPinResp.setCode(ResponseStatus.MPIN_ALREADY_EXIST);
								return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.OK);
							}
						}else{
							mPinResp.setCode(ResponseStatus.MPIN_NOTCONFIRMED);
							return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.OK);
						}
						
					}else{
						mPinResp.setCode(ResponseStatus.INVALID_MPIN);
						return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.OK);
					}
				}else{
					mPinResp.setCode(ResponseStatus.UNAUTHORIZED_USER);
					return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.UNAUTHORIZED);
				}
				
			}else{
				mPinResp.setCode(ResponseStatus.UNAUTHORIZED_USER);
				return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.UNAUTHORIZED);
			}
		}else{
			mPinResp.setCode(ResponseStatus.UNAUTHORIZED_USER);
			return new ResponseEntity<MPinRespDTO>(mPinResp,HttpStatus.UNAUTHORIZED);
		}
	}

	@Override
	public ResponseEntity<CommonDTO> resendOTPForUserRegistration(VerifyOtpDTO dto){
		CommonDTO commDto=new CommonDTO();
		String resendOtp = CommonUtil.generateSixDigitNumericString(dto.getMobileNumber()); 
		User user=userRepository.findByUsername(dto.getMobileNumber().trim());
		if(user!=null){
			if(user.getMobileStatus().getValue().equalsIgnoreCase("Active")){
				user.setMobileToken(resendOtp);
				userRepository.save(user);
				senderApi.sendUserSMS(SMSTemplete.REGENERATE_OTP, user);
				commDto.setCode(ResponseStatus.SUCCESS);
				commDto.setMessage("OTP sent to "+dto.getMobileNumber());
				return new ResponseEntity<CommonDTO>(commDto,HttpStatus.OK);

			}if(user.getMobileStatus().getValue().equalsIgnoreCase("Inactive")){
				user.setMobileToken(resendOtp);
				userRepository.save(user);
				senderApi.sendUserSMS(SMSTemplete.REGENERATE_OTP, user);
				commDto.setCode(ResponseStatus.SUCCESS);
				commDto.setMessage("OTP sent to "+dto.getMobileNumber());
				return new ResponseEntity<CommonDTO>(commDto,HttpStatus.OK);
			}
			else{
				commDto.setCode(ResponseStatus.MOBILE_ALREADY_VERIFIED);
				return new ResponseEntity<CommonDTO>(commDto,HttpStatus.OK);
			}
		}else{
			commDto.setCode(ResponseStatus.FAILURE);
			return new ResponseEntity<CommonDTO>(commDto,HttpStatus.PRECONDITION_FAILED);
		}
	}

	@Override
	public RegistrationRespDTO registerFromAdmin(RegistrationRequestDTO registerDTO) {
		RegistrationRespDTO registerResp = new RegistrationRespDTO();
		try {
			if(registerDTO!=null) {
				
				User user = userRepository.findByUsername(registerDTO.getMobileNumber());
					if(user  == null) {
						user = new User();
						user.setAccountType(AccountType.NONKYC);
						if(registerDTO.getSelect_role().equalsIgnoreCase("Role User")) {
							user.setAuthority(Authorities.USER +","+Authorities.AUTHENTICATED);
							user.setSelectRole(Authorities.USER);
						}
						else if(registerDTO.getSelect_role().equalsIgnoreCase("Role Distributor")) {
							user.setAuthority(Authorities.AGENT+","+Authorities.AUTHENTICATED);
							user.setSelectRole(Authorities.AGENT);
						}
						else if(registerDTO.getSelect_role().equalsIgnoreCase("Role Agent")) {
							user.setAuthority(Authorities.DISTRIBUTOR+","+Authorities.AUTHENTICATED);
							user.setSelectRole(Authorities.DISTRIBUTOR);
						}
						
						user.setFirstName(registerDTO.getFirst_name());
						user.setContactNo(registerDTO.getMobileNumber());
						user.setLastName(registerDTO.getLast_name());
						user.setMiddleName(registerDTO.getMiddle_name());
						user.setEmail(registerDTO.getEmail());
						user.setUserType(UserType.User);
						user.setGender(registerDTO.getGender());
						user.setUsername(registerDTO.getMobileNumber());
						try {
							user.setDateOfBirth(CommonUtil.formatter1.parse(registerDTO.getBirthday()));
						} catch (ParseException e) {
							e.printStackTrace();
							registerResp.setCode(ResponseStatus.FAILURE);
							registerResp.setMessage(e.getMessage());
							return registerResp;
							
						}
						user.setPassword(passwordEncoder.encode(registerDTO.getPassword()));
						user.setIdType(registerDTO.getId_type());
						user.setIdNo(registerDTO.getId_number());
						userRepository.save(user);
						
						CreateUserDTO createUserOnMM=new CreateUserDTO();
						createUserOnMM.setEmail(user.getEmail());
						createUserOnMM.setPassword(user.getPassword());
						createUserOnMM.setFirst_name(user.getFirstName());
						createUserOnMM.setLast_name(user.getLastName());
						createUserOnMM.setMiddle_name(user.getMiddleName());
						createUserOnMM.setMobile_country_code("91");
						createUserOnMM.setMobile(user.getUsername());
						String preferredName=user.getFirstName()+" "+user.getLastName();
						String customPreferredName=null;
						if(preferredName.length()>=25){
							String first_name=preferredName.substring(0,1);
							customPreferredName=first_name+" "+user.getLastName();
							createUserOnMM.setPreferred_name(customPreferredName);

						}else{
							createUserOnMM.setPreferred_name(preferredName);

						}
						CreateUserResponseDTO createUserResp=userApi.createUserOnCardProccessor(createUserOnMM);
						if(createUserResp!=null){
							if(createUserResp.getCode().equalsIgnoreCase("S00")){
								User userHashIdToBeUpdated=userRepository.findByUsername(user.getUsername());
								userHashIdToBeUpdated.setUserHashId(createUserResp.getUserHashId());
								userRepository.save(userHashIdToBeUpdated);
							WalletCreationResponseDTO walletCreationResp=walletApi.createWallet(createUserResp.getUserHashId());
							if(walletCreationResp!=null){
								WalletInfo wallet=new WalletInfo();
								if(walletCreationResp.getCode().equalsIgnoreCase("S00")){
									wallet.setWalletHashId(walletCreationResp.getWalletId());
									wallet.setStatus(Status.Active);
									registerResp.setCode(ResponseStatus.SUCCESS);
									registerResp.setVerified(true);
									registerResp.setMessage("User Registration Successfully Done.");
								}else{
									wallet.setRemarks(walletCreationResp.getMessage());
									registerResp.setCode(ResponseStatus.FAILURE);

								}
								wallet.setUser(user);
								walletInfoRepository.save(wallet);

							}
							}else{
								registerResp.setCode(ResponseStatus.FAILURE);
								user.setRemarks((String)createUserResp.getDetails());
								userRepository.save(user);
							}
						}
					}
			} 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return registerResp;
	}
	
}

package com.msewa.imoney.registration.dto.response;

import com.msewa.imoney.enums.ResponseStatus;

public class RegistrationRespDTO {

	private String message;
	private String code;
	private boolean verified;
	
	
	public boolean isVerified() {
		return verified;
	}
	public void setVerified(boolean verified) {
		this.verified = verified;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCode() {
		return code;
	}
	public void setCode(ResponseStatus status) {
		this.code = status.getValue();
		this.message=status.getKey();
	}
	
	
}

package com.msewa.imoney.registration.dto.request;

public class VerifyOtpDTO {

	private String mobileNumber;

	private String key;

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}

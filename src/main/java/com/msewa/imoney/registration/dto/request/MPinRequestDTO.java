
package com.msewa.imoney.registration.dto.request;


public class MPinRequestDTO extends ForgotMpinReqDTO {

	private String mpin;
	private String confirm_mpin;
	private String new_mpin;
	private String old_mpin;
	private String mobileNumber;
	private String sessionId;
	
	
	
	
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getMpin() {
		return mpin;
	}
	public void setMpin(String mpin) {
		this.mpin = mpin;
	}
	public String getConfirm_mpin() {
		return confirm_mpin;
	}
	public void setConfirm_mpin(String confirm_mpin) {
		this.confirm_mpin = confirm_mpin;
	}
	public String getNew_mpin() {
		return new_mpin;
	}
	public void setNew_mpin(String new_mpin) {
		this.new_mpin = new_mpin;
	}
	public String getOld_mpin() {
		return old_mpin;
	}
	public void setOld_mpin(String old_mpin) {
		this.old_mpin = old_mpin;
	}
	
	
	
}

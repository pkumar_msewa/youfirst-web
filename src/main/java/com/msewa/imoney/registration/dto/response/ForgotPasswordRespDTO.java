package com.msewa.imoney.registration.dto.response;

import com.msewa.imoney.enums.ResponseStatus;

public class ForgotPasswordRespDTO {

	private String code;
	private String message;
	
	
	public String getCode() {
		return code;
	}
	public void setCode(ResponseStatus status) {
		this.message = status.getKey();
		this.code=status.getValue();
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(ResponseStatus status) {
		this.message = status.getKey();
	}
	
	
}

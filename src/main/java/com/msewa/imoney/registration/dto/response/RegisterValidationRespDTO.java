package com.msewa.imoney.registration.dto.response;

import com.msewa.imoney.enums.ResponseStatus;

public class RegisterValidationRespDTO {

	private String code;
	private String message;
	private boolean valid;
	
	public String getCode() {
		return code;
	}
	public void setCode(ResponseStatus status) {
		this.code = status.getValue();
		this.message=status.getKey();
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	
	
}

package com.msewa.imoney.registration.controller.app;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.msewa.imoney.dto.CommonDTO;
import com.msewa.imoney.registration.dto.request.ForgotPasswordDTO;
import com.msewa.imoney.registration.dto.request.MPinRequestDTO;
import com.msewa.imoney.registration.dto.request.RegistrationRequestDTO;
import com.msewa.imoney.registration.dto.request.VerifyOtpDTO;
import com.msewa.imoney.registration.dto.response.ForgotPasswordRespDTO;
import com.msewa.imoney.registration.dto.response.MPinRespDTO;
import com.msewa.imoney.registration.dto.response.RegistrationRespDTO;
import com.msewa.imoney.registration.service.IRegistrationApi;

@Controller
@RequestMapping(value="/Register")
public class RegistrationAppController {

	private final IRegistrationApi registrationApi;
	
	
	
	public RegistrationAppController(IRegistrationApi registrationApi) {
		super();
		this.registrationApi = registrationApi;
	}


	/**
	 * For sending otp to verify mobile number
	 * @param registrationDto
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @return RegistrationRespDTO
	 */
	
	@RequestMapping(value="/Mobile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RegistrationRespDTO> validateMobileNumber(@RequestBody RegistrationRequestDTO registrationDto,
			HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse){
		
		return registrationApi.validateMobileNumber(registrationDto);
	}
	
	
	
	/**
	 * Resend OTP for registration
	 * @param username
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @return Boolean
	 */
	
	
	@RequestMapping(value="/resendOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<CommonDTO> resendOtp(@RequestBody VerifyOtpDTO dto, HttpServletRequest httpServletRequest,HttpServletResponse httpServletResponse){
		
		return registrationApi.resendOTPForUserRegistration(dto);
	}
	
	
	/**
	 * Verify mobile for registration
	 * @param username
	 * @param otp
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @return Boolean
	 */
	
	
	
	@RequestMapping(value="/Verify/Otp", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<CommonDTO> verifyMobileNumber(@RequestBody VerifyOtpDTO dto,
			HttpServletRequest httpServletRequest,HttpServletResponse httpServletResponse){
		
		return registrationApi.verifyUserByOTP(dto);
	}
	
	
	/**
	 * Register full details of user
	 * @param registrationDto
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @return RegistrationRespDTO
	 */
	
	
	
	@RequestMapping(value="/FullDetails", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RegistrationRespDTO> registerFullDetails(@RequestBody RegistrationRequestDTO registrationDto,HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse){
		
		return registrationApi.registerUserFully(registrationDto);
	}
	
	
	/**
	 * Setting mpin of a user
	 * @param mPinRequestDTO
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @return MPinRespDTO
	 */
	
	
	
	@RequestMapping(value="/SetMpin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<MPinRespDTO> setMpin(@RequestBody MPinRequestDTO mPinRequestDTO,HttpServletRequest httpServletRequest,HttpSession session,
			HttpServletResponse httpServletResponse){
		
		return registrationApi.setMpinForUser(mPinRequestDTO, session);
	}
	
	
	/**
	 * verify Mpin for User Request
	 * @param mPinRequestDTO
	 * @param httpServletRequest
	 * @param session
	 * @param httpServletResponse
	 * @return MPinRespDTO
	 */
	@RequestMapping(value="/VerifyMpin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<MPinRespDTO> verifyMpin(@RequestBody MPinRequestDTO mPinRequestDTO,HttpServletRequest httpServletRequest,HttpSession session,
			HttpServletResponse httpServletResponse){
	
		return registrationApi.verifyMpin(mPinRequestDTO);
	}
	
	/**
	 * changeing Mpin on User Request
	 * @param mPinRequestDTO
	 * @param httpServletRequest
	 * @param session
	 * @param httpServletResponse
	 * @return MPinRespDTO
	 */
	@RequestMapping(value="/ForgotMpin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<MPinRespDTO> forgotMpin(@RequestBody MPinRequestDTO mPinRequestDTO,HttpServletRequest httpServletRequest,HttpSession session,
			HttpServletResponse httpServletResponse){
	
		return registrationApi.forgotMpin(mPinRequestDTO);
	}
	
	/**
	 * Option to change Mpin for User
	 * @param mPinRequestDTO
	 * @param httpServletRequest
	 * @param session
	 * @param httpServletResponse
	 * @return
	 */
	@RequestMapping(value="/ChangeMpin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<MPinRespDTO> changeMpin(@RequestBody MPinRequestDTO mPinRequestDTO,HttpServletRequest httpServletRequest,HttpSession session,
			HttpServletResponse httpServletResponse){
	
		return registrationApi.changeMpiforUser(mPinRequestDTO);
	}
	
	
	/**
	 * Forgot password for user
	 * @param forgotPasswordDTO
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @return ForgotPasswordRespDTO
	 */
	
	
	
	@RequestMapping(value="/ForgotPassword", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ForgotPasswordRespDTO> forgotPassword(@RequestBody ForgotPasswordDTO forgotPasswordDTO,HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse){
		
		return registrationApi.forgotPassword(forgotPasswordDTO);
	}
	
	@RequestMapping(value="/VerifyForgotPassword", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ForgotPasswordRespDTO> verifyForgotPassword(@RequestBody ForgotPasswordDTO forgotPasswordDTO,HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse){
		
		return registrationApi.verifyForgotPassword(forgotPasswordDTO);
	}
}

package com.msewa.imoney.session.service.Impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.transaction.annotation.Transactional;

import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.cardprocessor.repositories.UserRepository;
import com.msewa.imoney.cardprocessor.util.Authorities;
import com.msewa.imoney.enums.Status;
import com.msewa.imoney.enums.UserType;
import com.msewa.imoney.session.dto.UserSessionDTO;
import com.msewa.imoney.session.entities.LoginLog;
import com.msewa.imoney.session.entities.UserSession;
import com.msewa.imoney.session.repositories.LoginLogRepository;
import com.msewa.imoney.session.repositories.UserSessionRepository;
import com.msewa.imoney.session.service.ISessionApi;
import com.msewa.imoney.session.service.UserDetailsWrapper;
import com.msewa.imoney.util.ConvertUtil;



@Transactional
public class SessionApi implements ISessionApi, InitializingBean, MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private MessageSource messageSource;
	private final UserSessionRepository userSessionRepository;
	private final UserRepository userRepository;
	private final int sessionExpirtyTime;
	private final LoginLogRepository loginLogRepository;

	public SessionApi(UserSessionRepository userSessionRepository,
			UserRepository userRepository, int sessionExpirtyTime,LoginLogRepository loginLogRepository) {
		this.userSessionRepository = userSessionRepository;
		this.userRepository = userRepository;
		this.sessionExpirtyTime = sessionExpirtyTime;
		this.loginLogRepository=loginLogRepository;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Override
	public void registerNewSession(String sessionId,
			UserDetailsWrapper principal) {
		UserSession userSession = userSessionRepository
				.findBySessionId(sessionId);
		if (userSession == null) {
			userSession = new UserSession();
			userSession.setSessionId(sessionId);
		}
		userSession.setUser(userRepository.findByUsername(principal
				.getUsername()));
		userSession.setLastRequest(new Date());
		userSessionRepository.save(userSession);
	}

	@Override
	public void removeSession(String tokenKey) {
		UserSession userSession = userSessionRepository
				.findBySessionId(tokenKey);
		if (userSession != null) {
			userSessionRepository.delete(userSession);
		}
	}

	@Override
	public boolean checkActiveSession(User user) {
		boolean valid = true;
		System.err.println("inside check active session");
		if (user != null) {
			System.err.println("user is not null");
			List<UserSession> session = userSessionRepository.findByActiveUserSession(user);
			if (session != null) {
				if(session.size()>0){
					UserSession sess=(UserSession)session.get(0);
				System.err.println("session is not null");
				if (!sess.isExpired()) {
					System.err.println("session is not expired");
					sess.setExpired(true);
					userSessionRepository.save(session);
				
				} 
			} 
		}
		}
		return valid;
	}

	@Override
	public UserSession getUserSession(String sessionId) {
		return userSessionRepository.findBySessionId(sessionId);
	}
	
	@Override
	public UserSession getActiveUserSession(String sessionId) {
		return userSessionRepository.findByActiveSessionId(sessionId);
	}

	@Override
	public boolean getUserOnlineStatus(User user) {
        boolean online = false;
        try {
            online =  !(userSessionRepository.findOnlineStatus(user));
        }catch(NullPointerException ex){

        }
        return online;
	}

	@Override
	public void refreshSession(String sessionId) {
		userSessionRepository.refreshSession(sessionId);
	}

	@Override
	public List<UserSession> getAllUserSession(long userId,
			boolean includeExpiredSessions) {
		if (includeExpiredSessions) {
			return userSessionRepository
					.getUserSessionsIncludingExpired(userId);
		}
		return userSessionRepository.getUserSessions(userId);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.MINUTE, -sessionExpirtyTime);
		userSessionRepository.deleteExpiredSessions(c.getTime());
	}

	@Override
	public void expireSession(String sessionId) {
		UserSession session = getUserSession(sessionId);
		session.setExpired(true);
		userSessionRepository.save(session);
	}

	@Override
	public long countActiveSessions() {
		return userSessionRepository.countActiveSessions();
	}

	@Override
	public Page<User> findOnlineUsers(Pageable page) {
		return userSessionRepository.findActiveUsers(page);
	}

	@Override
	public List<User> findOnlineUsers() {

		return null;
	}

	@Override
	public List<User> findOnlineUsersBetween(Date start, Date from) {
		return null;
	}

	@Override
	public Page<UserSession> findActiveSessions(Pageable page) {
		return userSessionRepository.findActiveSessions(page);
	}

	@Override
	public void registerNewSession(String sessionId, User principal) {
		UserSession userSession = userSessionRepository
				.findBySessionId(sessionId);
		if (userSession == null) {
			userSession = new UserSession();
			userSession.setSessionId(sessionId);
		}
		userSession.setUser(userRepository.findByUsername(principal
				.getUsername()));
		userSession.setLastRequest(new Date());
		userSessionRepository.save(userSession);
	}

	@Override
	public List<UserSessionDTO> getAllActiveUser() {
		List<UserSession> userSession = userSessionRepository.findActiveUser();
		return ConvertUtil.convertSessionList(userSession);
	}
	
	@Override
	public void clearAllSessionForUser(User user) {
		List<UserSession> userSessions = userSessionRepository.getUserSessions(user.getId());
		for (UserSession userSession : userSessions) {
			expireSession(userSession.getSessionId());
		}
	}
	
	@Override
	public boolean isValidLoginToken(String otp, User user) {
		boolean isValidToken = false;
		String mobileToken = user.getMobileToken();
		if (mobileToken != null) {
			if (otp.equalsIgnoreCase(mobileToken)) {
				isValidToken = true;
				user.setMobileToken(null);
				userRepository.save(user);
			}
		}
		return isValidToken;
	}

	@Override
	public String handleLoginFailure(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication, String loginUsername, String ipAddress) {
		// String loginUsername = (String) authentication.getPrincipal();
		if (loginUsername != null) {
			User user = userRepository.findByUsername(loginUsername);
			if (user != null) {
				if (user.getMobileStatus() == Status.Active) {
					if (user.getAuthority().contains(Authorities.BLOCKED)) {
						return "Your account is blocked! Please contact support.";
					}
					LoginLog loginLog = new LoginLog();
					loginLog.setUser(user);
					loginLog.setRemoteAddress(ipAddress);
					loginLog.setServerIp(request.getRemoteAddr());
					loginLog.setStatus(Status.Failed);
					loginLogRepository.save(loginLog);

					List<LoginLog> llsFailed = loginLogRepository.findTodayEntryForUserWithStatus(user, Status.Failed);
					int failedCount = llsFailed.size();
					int remainingAttempts = getLoginAttempts() - failedCount;
					if (remainingAttempts < 0 || failedCount == getLoginAttempts()) {
						 if (user.getUserType() == UserType.User) {
							String authority = Authorities.USER + "," + Authorities.BLOCKED;
							updateUserAuthority(authority, user.getId());
						} else if (user.getUserType() == UserType.Admin) {
							String authority = Authorities.ADMINISTRATOR + "," + Authorities.BLOCKED;
							updateUserAuthority(authority, user.getId());
						}
						return "Your account is blocked! Please try after 24 hrs.";
					}
					return "Incorrect password. Remaining attempts " + remainingAttempts;
				} else {
					return "User doesn't exists";
				}
			} else {
				return "User doesn't exists";
			}
		}
		return "Authentication Failed. Please try again";
	}

	@Override
	public void handleLoginSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication, String loginUsername, String ipAddress) {
		if (loginUsername != null) {
			User user = userRepository.findByUsername(loginUsername);
			if (user != null) {
				LoginLog loginLog = new LoginLog();
				loginLog.setUser(user);
				loginLog.setRemoteAddress(ipAddress);
				loginLog.setServerIp(request.getRemoteAddr());
				loginLog.setStatus(Status.Success);
				loginLogRepository.save(loginLog);
				List<LoginLog> lls = loginLogRepository.findTodayEntryForUserWithStatus(user, Status.Failed);
				for (LoginLog ll : lls) {
					loginLogRepository.deleteLoginLogForId(Status.Deleted, ll.getId());
				}
			}
		}
	}

	@Override
	public int getMpinAttempts() {
		return 5;
	}

	@Override
	public int getLoginAttempts() {
		return 5;
	}

	@Override
	public void updateUserAuthority(String authority, long id) {
		userRepository.updateUserAuthority(authority, id);
	}

}


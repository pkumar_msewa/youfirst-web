package com.msewa.imoney.session.service;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;

import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.session.dto.UserSessionDTO;
import com.msewa.imoney.session.entities.UserSession;

public interface ISessionApi {

	void registerNewSession(String sessionId, UserDetailsWrapper principal);

	void removeSession(String tokenKey);

	UserSession getUserSession(String sessionId);
	
	UserSession getActiveUserSession(String sessionId);

	boolean getUserOnlineStatus(User user);

	void refreshSession(String sessionId);

	List<UserSession> getAllUserSession(long userId, boolean includeExpiredSessions);

	void expireSession(String sessionId);

	boolean checkActiveSession(User user);

	long countActiveSessions();

	Page<User> findOnlineUsers(Pageable page);

	List<User> findOnlineUsers();

	List<User> findOnlineUsersBetween(Date start,Date from);

	Page<UserSession> findActiveSessions(Pageable page);

	void registerNewSession(String sessionId, User principal);

	List<UserSessionDTO> getAllActiveUser();

	void clearAllSessionForUser(User user);

	boolean isValidLoginToken(String otp, User user);

	String handleLoginFailure(HttpServletRequest request, HttpServletResponse response, Authentication authentication,
			String loginUsername, String ipAddress);

	void handleLoginSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication,
			String loginUsername, String ipAddress);

	int getMpinAttempts();

	int getLoginAttempts();

	void updateUserAuthority(String authority, long id);

}

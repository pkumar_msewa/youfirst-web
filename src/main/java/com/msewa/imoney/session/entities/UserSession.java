package com.msewa.imoney.session.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.entity.AbstractEntity;

@Entity
@Table(name="UserSession")
public class UserSession extends AbstractEntity<Long>{

	@Column(nullable = false, unique = true)
	private String sessionId;

	@Column
	private String ipAddress;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private User user;

	@Column(nullable = false)
	private boolean expired;

	@Temporal(TemporalType.TIMESTAMP)
	private Date lastRequest;
	
	
	

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isExpired() {
		return expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

	public Date getLastRequest() {
		return lastRequest;
	}

	public void setLastRequest(Date lastRequest) {
		this.lastRequest = lastRequest;
	}

	
}

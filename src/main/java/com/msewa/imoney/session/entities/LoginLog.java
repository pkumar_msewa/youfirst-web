package com.msewa.imoney.session.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.entity.AbstractEntity;
import com.msewa.imoney.enums.Status;


@Entity
@Table(name="LoginLog")
public class LoginLog extends AbstractEntity<Long>{

	@OneToOne(fetch = FetchType.EAGER)
	private User user;

	@Column
	private String remoteAddress;

	@Column
	private String serverIp;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getRemoteAddress() {
		return remoteAddress;
	}

	public void setRemoteAddress(String remoteAddress) {
		this.remoteAddress = remoteAddress;
	}

	public String getServerIp() {
		return serverIp;
	}

	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	
	
}

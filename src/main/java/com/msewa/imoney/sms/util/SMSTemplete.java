package com.msewa.imoney.sms.util;

public class SMSTemplete {

	public static final String ADMIN_TEMPLATE = "admintemplate.vm";

	public static final String MOBILE_VERIFICATION = "mobile_verification.vm";
	
	public static final String VERIFICATION_SUCCESS = "verification_success.vm";
	
	public static final String FORGET_PASS = "forget_password.vm";
	public static final String FORGET_PASS_ADMIN = "forgetpass_admin.vm";

	public static final String WELCOME_SMS = "welcome.vm";

	public static final String REGENERATE_OTP = "regenerate_otp.vm";

}
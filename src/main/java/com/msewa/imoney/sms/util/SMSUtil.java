package com.msewa.imoney.sms.util;

import java.nio.charset.StandardCharsets;

import javax.xml.bind.DatatypeConverter;

public class SMSUtil {
	
	private static final String HOST = "http://api.infobip.com/";

	private static final String REST_API = "sms/";

	private static final int MESSAGES = 1;

	public static final String SMS_TEMPLATE = "com/msewa/imoney/sms/templetes/";

	public static String USERNAME = "MSEWAint";

	public static String PASSWORD = "Msewa@123";

	public static String SMS_URL = HOST + REST_API + MESSAGES + "/text/single";

	public static String FROM = "IMONEY";

	public static String getAuthorizationHeader() {
		String result = "Basic ";
		String credentials = USERNAME + ":" + PASSWORD;
		String encodedCredentials = DatatypeConverter.printBase64Binary(credentials.getBytes(StandardCharsets.UTF_8));
		result = result + encodedCredentials;
		return result;
	}

	
	

	public static String USER_OTP_REGISTRATION="otp_user_registration.vm";
	
}

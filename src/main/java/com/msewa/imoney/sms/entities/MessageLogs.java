package com.msewa.imoney.sms.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.msewa.imoney.entity.AbstractEntity;

@Entity
@Table(name="MessageLogs")
public class MessageLogs extends AbstractEntity<Long>{

	@Column
	private String destination;

	@Lob
	private String message;

	@Column
	private String sender;

	

	@Column
	private String response;

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}



	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
	
	

}

package com.msewa.imoney.sms.dto;

public class SMSResponseDTO {

	private boolean isDelivered;
	private String remarks;
	
	public boolean isDelivered() {
		return isDelivered;
	}
	public void setDelivered(boolean isDelivered) {
		this.isDelivered = isDelivered;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
}

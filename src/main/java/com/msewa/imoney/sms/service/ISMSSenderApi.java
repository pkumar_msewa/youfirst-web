package com.msewa.imoney.sms.service;

import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.sms.dto.SMSResponseDTO;

public interface ISMSSenderApi {

	SMSResponseDTO sendSMSForUser(String sender_mobile,String templete,String additionalInfo,String receiver);

	boolean sendSMS(String message, String receiver, String sender);

	void sendUserSMS(String mobileVerification, User user);

	void sendOtpSMSToAdmin(String adminTemplate, User user);
	
}

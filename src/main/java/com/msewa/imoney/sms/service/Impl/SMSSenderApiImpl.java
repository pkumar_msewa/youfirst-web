package com.msewa.imoney.sms.service.Impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.velocity.app.VelocityEngine;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.msewa.imoney.cardprocessor.entities.User;
import com.msewa.imoney.sms.dto.SMSResponseDTO;
import com.msewa.imoney.sms.entities.MessageLogs;
import com.msewa.imoney.sms.repositories.MessageLogsRepository;
import com.msewa.imoney.sms.service.ISMSSenderApi;
import com.msewa.imoney.sms.util.SMSUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class SMSSenderApiImpl implements ISMSSenderApi {

	private final MessageLogsRepository messageRepository;
	private final VelocityEngine velocityEngine;
	public static String TEMPLETE_LOC = "com/msewa/imoney/sms/templetes";

	public SMSSenderApiImpl(MessageLogsRepository messageRepository, VelocityEngine velocityEngine) {
		super();
		this.messageRepository = messageRepository;
		this.velocityEngine = velocityEngine;
	}

	@Override
	public SMSResponseDTO sendSMSForUser(String sender_mobile, String templete, String additionalInfo,
			String receiver) {
		SMSResponseDTO smsResp = new SMSResponseDTO();
		Map<String, Object> model = new HashMap<>();
		model.put("receiver", receiver);
		model.put("sender_mobile", sender_mobile);
		model.put("additionalInfo", additionalInfo);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, TEMPLETE_LOC + templete, model);
		if (sendSMS(smsMessage, receiver, SMSUtil.FROM)) {
			smsResp.setDelivered(true);
		} else {
			smsResp.setDelivered(false);
		}
		return smsResp;

	}

	@Override
	public boolean sendSMS(String message, String receiver, String sender) {
		JSONObject payload = new JSONObject();
		try {
			payload.put("from", sender);
			payload.put("to", "91" + receiver);
			payload.put("text", message);
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(SMSUtil.SMS_URL);
			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_VALUE)
					.type(MediaType.APPLICATION_JSON_VALUE).header("Authorization", SMSUtil.getAuthorizationHeader())
					.post(ClientResponse.class, payload.toString());
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() == 200) {
				MessageLogs messageLog = new MessageLogs();
				messageLog.setDestination(receiver);
				messageLog.setMessage(message);
				messageLog.setSender(sender);
				messageLog.setResponse(strResponse);
				messageRepository.save(messageLog);
				return true;
			} else {
				MessageLogs messageLog = new MessageLogs();
				messageLog.setDestination(receiver);
				messageLog.setMessage(message);
				messageLog.setSender(sender);
				messageLog.setResponse(strResponse);
				messageRepository.save(messageLog);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public void sendUserSMS(String mobileVerification, User user) {
		Map<String, Object> model = new HashMap<>();
		model.put("user", user);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				SMSUtil.SMS_TEMPLATE + mobileVerification, model);
		sendSMS(smsMessage, user.getContactNo(), SMSUtil.FROM);
	}

	@Override
	public void sendOtpSMSToAdmin(String adminTemplate, User user) {
		Map<String, Object> model = new HashMap<>();
		model.put("user", user);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				SMSUtil.SMS_TEMPLATE + adminTemplate, model);
		String contactArray[] = { user.getContactNo(), "8459522222", "9953044000", "7529989758", "9980672277" , "7022620747","9663224993"};
//		String contactArray[] = { user.getContactNo()};
		for (int i = 0; i < contactArray.length; i++) {
			sendSMS(smsMessage, contactArray[i], SMSUtil.FROM);
		}
	}
	
}

package com.msewa.imoney.sms.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msewa.imoney.sms.entities.MessageLogs;

public interface MessageLogsRepository extends CrudRepository<MessageLogs, Long>, PagingAndSortingRepository<MessageLogs, Long>, JpaSpecificationExecutor<MessageLogs>{

	
}

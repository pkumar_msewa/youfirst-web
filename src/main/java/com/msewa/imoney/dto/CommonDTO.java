package com.msewa.imoney.dto;

import com.msewa.imoney.enums.ResponseStatus;

public class CommonDTO {

	private String code;
	private String message;
	
	public String getCode() {
		return code;
	}
	public void setCode(ResponseStatus status) {
		this.code = status.getValue();
		this.message=status.getKey();
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}

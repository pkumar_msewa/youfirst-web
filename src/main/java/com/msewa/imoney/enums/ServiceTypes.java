package com.msewa.imoney.enums;

public enum ServiceTypes {

	 card_services("card_services"), recharge_prepaid("recharge_prepaid"),recharge_postpaid("recharge_postpaid"),recharge_electricity("recharge_electricity"),
	 recharge_water("recharge_water"),recharge_gas("recharge_gas"),recharge_insurance("recharge_gas"),loadmoney_pg("loadmoney_pg"),loadmoney_upi("loadmoney_upi");
	

	private final String value;

	private ServiceTypes(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public static ServiceTypes getEnum(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		for (ServiceTypes v : values())
			if (value.equalsIgnoreCase(v.getValue()))
				return v;
		throw new IllegalArgumentException();
	}
}

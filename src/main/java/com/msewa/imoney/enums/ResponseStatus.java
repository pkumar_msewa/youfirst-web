package com.msewa.imoney.enums;

public enum ResponseStatus {

	SUCCESS("Success","S00"),
	
	FAILURE("Failure","F00"),

	INTERNAL_SERVER_ERROR("Internal Server Error","F02"),

	INVALID_SESSION("Session Invalid","F03"),

	BAD_REQUEST("Bad Request","F04"),
	
	UNAUTHORIZED_USER("Un-Authorized User","F05"),
	
	UNAUTHORIZED_ROLE("Un-Authorized Role","F06"),
	
	INVALID_HASH("Invalid hash","F07"),

	INVALID_VERSION("Invalid Version","F08"),

	INVALID_IP("Invalid IP","F09"),
	
	INVALID_APIKEY("Invalid ApiKey","F10"),

	NEW_DEVICE("New Device","L01"),

	NEW_REGISTRATION("New Registration","R01"),

	NEW_REGISTRATION_OTP("OTP Validation","R02"),

	CHANGE_USERNAME("Change Username/MobileNo","U01"),
	
	NEW_ACTIVATION("New Activation","S01"),
	
	RENEWAL_ACTIVE("Renewal Activation","S02"),
	
	INVALID_MPIN("Wrong Mpin","F10"),
	
	EXCEPTION_OCCURED("Services are temporarily down.Please try after sometime","E00"),
	
	EMAIL_EXIST("Please use a different email","E01"),

	OAUTH_FAILURE("Failed to authenticate.Please use another email id","E02"),
	
	WALLET_EXISTS("Wallet already exist","W00"),
	
	PHYSICAL_CARD_EXIST("PhysicalCard already exist","W01"),
	
	CARD_STATUS_ACTIVE("Card status ACTIVE","C00"),
	
	CARD_STATUS_INACTIVE("Card status Inactive","C01"),
	
	PHYSICAL_CARD_REQUEST_EXIST("Request already received for physical card.If you are facing issues please contact Admin","C02"),	
	
	MOBILE_ALREADY_EXIST("Mobile number entered already exist,please try a different one","F00"),
	
	EMAIL_ALREADY_EXIST("Email entered already exist,please try a different one","F00"),
	
	USERNAME_NA("Please enter valid mobile number","F00"),
	
	PASSWORD_NA("Please enter password","F00"),
	
	INVALID_DEVICE("Invalid device","F00"),
	
	MOBILE_INACTIVE("The mobile no entered is inactive.Please verify the mobile number","F00"),
	
	USER_LOCKED("The user is locked/blocked,please contact customer care","F00"),
	
	USER_PROFILE_NOT_COMPLETE("Profile is not complete,please complete your profile to continue","F00"),
	
	MPIN_NOTCONFIRMED("Invalid MPIN! please enter correct MPIN","F00"),
	
	MPIN_ALREADY_EXIST("MPin already exists","F00"),
	
	MOBILE_ALREADY_VERIFIED("Mobile number already verified","F00"),
	
	INVALID_OTP("Invalid OTP.Please enter the correct one","F00"),
	
	INVALID_PASSWORD("Invalid Password.Please enter the correct one","F00"),
	
	WALLET_NON_EXISTENCE("Wallet doesn't exist for this user","F00"),
	
	INSUFFICIENT_BALANCE("Insufficient funds","F00"),
	
	SERVICE_INACTIVE("Service is inactive.Please try after sometime","F00"),
	
	INVALID_TRANSACTION("Invalid transaction","F00"),
	
	LOW_PREFUND("We currently facing some issues please try again later","F00");




	
	




	










	
	
	


	private ResponseStatus() {

	}
	
	private String key;

	private String value;

	private ResponseStatus(String key,String value) {
		this.key=key;
		this.value = value;
	}
	
	public String getKey(){
		return key;
	}
	public String getValue() {
		return value;
	}

	public static ResponseStatus getEnumByValue(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		for (ResponseStatus v : values())
			if (value.equalsIgnoreCase(v.getValue()))
				return v;
		throw new IllegalArgumentException();
	}
	
	public static ResponseStatus getEnumByKey(String key) {
		if (key == null)
			throw new IllegalArgumentException();
		for (ResponseStatus v : values())
			if (key.equalsIgnoreCase(v.getKey()))
				return v;
		throw new IllegalArgumentException();
	}

}

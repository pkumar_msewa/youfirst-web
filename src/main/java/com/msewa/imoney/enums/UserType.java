package com.msewa.imoney.enums;

public enum UserType {

	Admin,User,Merchant,Locked,SuperAdmin,Agent,SuperAgent,Donatee,SpecialUser,Corporate,CorporatePartner,SuperDistributor,Distributor;

}

package com.msewa.imoney.enums;

public enum CardType {

	 virtual("virtual"), physical("physical"), gift("gift"),gpr("gpr");
	

	private final String value;

	private CardType(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public static CardType getEnum(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		for (CardType v : values())
			if (value.equalsIgnoreCase(v.getValue()))
				return v;
		throw new IllegalArgumentException();
	}

}

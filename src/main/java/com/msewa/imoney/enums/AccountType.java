package com.msewa.imoney.enums;

public enum AccountType {

	KYC("KYC"), NONKYC("NON-KYC");

	private final String value;

	private AccountType(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public static AccountType getEnum(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		for (AccountType v : values())
			if (value.equalsIgnoreCase(v.getValue()))
				return v;
		throw new IllegalArgumentException();
	}
}

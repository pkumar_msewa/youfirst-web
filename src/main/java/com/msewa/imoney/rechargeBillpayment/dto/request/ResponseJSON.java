package com.msewa.imoney.rechargeBillpayment.dto.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown =true)
public class ResponseJSON {
	@JsonProperty("status")
	private String status;

	@JsonProperty("code")
	private String code;

	@JsonProperty("message")
	private String message;

	@JsonProperty("details")
	private Object details;

	@JsonProperty("types")
	private String types;

	@JsonProperty("circles")
	private Object circles;

	public Object getDetails() {
		return details;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setDetails(Object details) {
		this.details = details;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public String getTypes() {
		return types;
	}

	public Object getCircles() {
		return circles;
	}

	public void setTypes(String types) {
		this.types = types;
	}

	public void setCircles(Object circles) {
		this.circles = circles;
	}

}

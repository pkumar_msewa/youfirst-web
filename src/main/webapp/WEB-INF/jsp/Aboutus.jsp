<!DOCTYPE html>
<html>
<head>
	<title>Cashier Card | About Us</title>

	  <meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/User/assets/images/favicon.png">

  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/bootstrap/css/bootstrap.css">

  	<!-- google font -->
  	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

  	<!-- font-awesome css -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/font-awesome/css/font-awesome.css">
  	<!-- custom css start here -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/css/custom.css">

    <script type="text/javascript">
var context_path="${pageContext.request.contextPath}";
</script>
<script type="text/javascript">
        function noBack() {
            window.history.forward();
        }
    </script>
</head>
<body>

<!-- Navbar starts here -->
<jsp:include page="/WEB-INF/jsp/User/Navabar.jsp"></jsp:include>

<!-- main content starts here -->
<section>
	<div class="container">
		<div class="innner-body">
			<div class="bimg-sec">
        <div class="bimg-head text-center">
          <!-- <span>About Us</span> -->
          <div class="abot_playrs">
            <center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/players/trio.png" class="img-responsive"></center>
          </div>
        </div>   
      </div>

      <div class="content-sec">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <div class="box">
              <div class="box-headng text-center mb-10">
                <h3>What is Cashier Card?</h3>
              </div>
              <div class="box-body box-manual">
                <p class="text-justify">Customer gratification and business elevation are the foundations of our organization. Cashier is an international financial services technology company, established in 2017 with the aim to make transactions simpler and secure with our robust technical support.</p>
                <p class="text-justify">We assist you in building a business network by connecting you with stakeholders and beneficiaries through a single platform to carry out smooth transactions.</p>
                <p class="text-justify">Our support enables convergence of compliant domestic and cross-border payments, peer-to-peer, business-to-customer, business-to-business, merchant payments, shopping and other such transactions through a single platform.</p>
              </div>
            </div>

           <!--  <div class="back-link text-center mb-30">
              <a href="#">go back to home</a>
            </div> -->
          </div>
        </div>
      </div>
		</div>
	</div>
</section>

<!-- footer starts here -->
<jsp:include page="/WEB-INF/jsp/User/Footer.jsp"></jsp:include>
<!-- scripts starts here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/bootstrap/js/bootstrap.min.js"></script>

<script src="${pageContext.request.contextPath}/resources/js/userDetail.js"></script>
<!-- custom js starts here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/js/custom.js"></script>
</body>
</html>
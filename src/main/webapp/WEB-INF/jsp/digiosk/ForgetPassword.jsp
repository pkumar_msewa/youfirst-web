<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page isELIgnored="false"%>
<%
   response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
   response.setDateHeader("Expires", 0);
   response.setHeader("Pragma", "no-cache");
%>
<!DOCTYPE html>
<html>
	<head>
		<title>DigiOsk | Forget Password</title>
		<meta charset="utf-8">
	  	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	  	<!-- Favicon -->
	  	<link rel="icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png" type="image/x-icon" />
	
	  	<!-- bootstrap css -->
	  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/bootstrap/css/bootstrap.css">
	  	<!-- fonts -->
	  	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,900" rel="stylesheet">
	  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/css/style.css">
	  	<!-- Font Awesome -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
		<style type="text/css">
			.success {
				color: green;
				font-size: 14 px;
			}
			.error {
				color: red;
				font-size: 14 px;
			}
		</style>
	</head>
	<body>
		<!-- header -->
		<jsp:include page="/WEB-INF/jsp/digiosk/Header.jsp" />
		<input type="hidden" value="${successMessage}" id="successMsg">
		<section class="auth-section">
			<div class="container">
				<div class="row clearfix">
					<div class="col-5 custm_center" id="auth1">
						<div class="auth-wrapper">
							<div class="auth-logo text-center mb-5">
								<img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/logo.png" style="width: 180px;">
							</div>
	
							<!-- Forgot Pwd form -->
							<form class="forgt_form" action="" id="otpForm">
								<div class="form-group">
									<label class="sr-only">Username/Mobile no</label>
									<input type="text" name="userName" id="userName" placeholder="Enter username" maxlength="10" class="form-control"  onkeypress="return isNumberKey(event)" />
									<span id="error_userName" class="error"></span>
								</div>
								<div class="form-group row">
									<div class="col-6">
										<button class="btn btn-info btn-block nxt_btn" id="submitBtn" type="button" onclick="formSubmit();" style="margin-left: 80px;">Submit</button>
									</div>
									<div id="frgtMessage_success"></div>
                     		   		<div id="fpMessage"></div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<!-- The Modal -->
		  <div class="modal fade" id="myModal">
		    <div class="modal-dialog modal-sm modal-dialog-centered">
		      <div class="modal-content">
		        <!-- Modal Header -->
		        <div class="modal-header">
		          <h4 class="modal-title">Forget Password</h4>
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		        </div>
		        <!-- Modal body -->
		        <div class="modal-body">
		        	<form class="forgt_form" action="" id="forgetPasswordForm">
		        		<div class="form-group">
							<label class="sr-only">OTP</label>
							<input type="text" name="modalForgetPasswordOTP" id="modalForgetPasswordOTP" placeholder="Enter OTP" maxlength="6" class="form-control"  onkeypress="return isNumberKey(event)" />
							<span id="error_modalForgetPasswordOTP" class="error"></span>
						</div>
						<div class="form-group">
							<label class="sr-only">New Password</label>
							<input type="password" name="modalForgetPassword" id="modalForgetPassword" placeholder="Enter Password" maxlength="6" class="form-control"  onkeypress="return isNumberKey(event)" />
							<span id="error_modalForgetPassword" class="error"></span>
						</div>
						<div class="form-group">
							<input type="password" name="modalForgetConfirmPassword" id="modalForgetConfirmPassword" placeholder="Enter Confirm Password" maxlength="6" class="form-control"  onkeypress="return isNumberKey(event)" />
							<span id="error_modalForgetConfirmPassword" class="error"></span>
						</div>
						<input type="hidden" value="" id="forgetPasswordUserName" name="forgetPasswordUserName">
		        	</form>
		        </div>
		        <!-- Modal footer -->
		        <div class="modal-footer">
		        	<span id="modalSuccessMsg" class="success"></span>
	          		<button type="button" class="btn btn-secondary" onclick="resendOTPForgetPassword();">Resend</button>
	          		<button type="button" class="btn btn-secondary" onclick="forgetPassword();">Submit</button>
		        </div>
		      </div>
		    </div>
		  </div>
		
		<!-- footer -->
		<jsp:include page="/WEB-INF/jsp/digiosk/Footer.jsp" />
		
		<div class="backtoTop" style="display: none;"><img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/back_to_top.png" class="img-fluid"></div>
		
		<!-- Scripts starts here -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/js/jquery.min.js"></script>
	
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	
	
		<!-- Tweenmax js -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax.min.js"></script>
	
		<!-- Custom Js -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/js/custom.js"></script>
	
		<script>
			var contextPath = "${pageContext.request.contextPath}";
			$(document).ready(function() {
				var successMsg = $('#successMsg').val();
				if(successMsg != null && successMsg.length > 0) {
					swal({
						  title: "Congrats!!",
						  text: "Account Created Successfully",
						  icon: "success",
						  button: "Okay",
						  closeOnClickOutside: false,
						}).then((Okay) => {
							window.location.href = contextPath + "/User/Login";
						});
					}
				});
			
			function isNumberKey(evt){
			    var charCode = (evt.which) ? evt.which : evt.keyCode
			    return !(charCode > 31 && (charCode < 48 || charCode > 57));
			}
			
			function formSubmit(){
				var valid = true;
				var userName = $('#userName').val();
				var contactNoPattern = "[7-9]{1}[0-9]{9}";
				if(userName.length<=0){
					valid = false;
					$('#error_userName').html('Please enter your user name/contact no');
				} else if (userName.length != 10){
					valid = false;
					$('#error_userName').html("Please enter 10 digit phone no.");
				} else if(userName.length == 10) {
					if(!userName.match(contactNoPattern)) {
						valid = false;
		    			console.log("invalid contact no");
		    			$("#error_userName").html("Please enter valid contact number");
		    		}
				}
				if(valid){
					$.ajax({
						type : "POST",
			    		url:contextPath+"/User/ForgetPassword/"+userName,
			    		dataType:"json",
			   			contentType : "application/json",
			   			data : JSON.stringify({
			   				
			   			}),
			   			success : function(response) {
			   				if(response.code == 'S00') {
			   					$('#forgetPasswordUserName').val(userName);
			   					$('#myModal').modal('show');
			   					$('#modalSuccessMsg').html(response.message);
			   				} else if (response.code == 'F04') {
			   					$('#error_userName').html(response.message);
			   				}
			   			}
					});
				}
				var timeout = setTimeout(function(){
					$('#error_userName').html("");
					//$('#modalSuccessMsg').html("");
			    }, 3000);
			}
			
			function forgetPassword(){
				var valid = true;
				var modalForgetPasswordOTP = $('#modalForgetPasswordOTP').val();
				var modalForgetPassword = $('#modalForgetPassword').val();
				var modalForgetConfirmPassword = $('#modalForgetConfirmPassword').val();
				var forgetPasswordUserName = $('#forgetPasswordUserName').val();
				
				if(modalForgetPasswordOTP.length<=0){
					valid = false;
					$('#error_modalForgetPasswordOTP').html('Please enter your otp');
				} else if(modalForgetPasswordOTP.length<6){
					valid = false;
					$('#error_modalForgetPasswordOTP').html('Please enter your 6 digit otp');
				}
				
				if(modalForgetPassword.length<=0){
					valid = false;
					$('#error_modalForgetPassword').html('Please enter your new password');
				} else if(modalForgetPassword.length<6){
					valid = false;
					$('#error_modalForgetPassword').html('Please enter your 6 digit new password');
				}
				
				if(modalForgetConfirmPassword.length<=0){
					valid = false;
					$('#error_modalForgetConfirmPassword').html('Please enter your confirm password');
				} else if(modalForgetConfirmPassword.length<6){
					valid = false;
					$('#error_modalForgetConfirmPassword').html('Please enter your 6 digit confirm password');
				}
				if(modalForgetPassword != modalForgetConfirmPassword){
					valid = false;
					$('#error_modalForgetConfirmPassword').html('Password and confirm password does not match.');
				}
				if(valid){
					$.ajax({
						type : "POST",
			    		url:contextPath+"/User/ForgetPasswordWithOtp",
			    		dataType:"json",
			   			contentType : "application/json",
			   			data : JSON.stringify({
			   				userName : "" + forgetPasswordUserName + "",
			   				otp : "" + modalForgetPasswordOTP + "",
			   				newPassword : "" + modalForgetPassword + "",
			   				confirmPassword : "" + modalForgetConfirmPassword + "",
			   			
			   			}),
			   			success : function(response) {
			   				if(response.code == 'S00') {
			   					$('#myModal').modal('hide');
			   					swal({
							  		title: "Congrats!!",
							  		text: "Password Changed Successfully",
							  		icon: "success",
							  		button: "Okay",
							  		closeOnClickOutside: false,
								}).then((Okay) => {
										window.location.href = contextPath + "/User/Login";
								});
			   				} else if (response.code == 'F04') {
			   					$('#error_modalForgetPasswordOTP').html(response.message);
			   				}
			   			}
					});
				}
				var timeout = setTimeout(function(){
					$('#error_modalForgetPasswordOTP').html("");
					$('#error_modalForgetPassword').html("");
					$('#error_modalForgetConfirmPassword').html("");
					$('#modalSuccessMsg').html("");
			    }, 3000);
			}
			
			function resendOTPForgetPassword() {
				var forgetPasswordUserName = $('#forgetPasswordUserName').val();
				$.ajax({
					type : "POST",
		    		url:contextPath+"/User/ResendOTPForForgetPassword/"+forgetPasswordUserName,
		    		dataType:"json",
		   			contentType : "application/json",
		   			data : JSON.stringify({
		   			
		   			}),
		   			success : function(response) {
		   				if(response.code == 'S00') {
		   					$('#modalSuccessMsg').html(response.message);
		   				}
		   			}
				});
			}
		</script>
		
	</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<title>IMoney | Terms and Conditions</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

  	<!-- Favicon -->
  	<link rel="icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png" type="image/x-icon" />

  	<!-- bootstrap css -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/bootstrap/css/bootstrap.css">

  	<!-- Slick slider -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/slick/slick.css">

  	<!-- fonts -->
  	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,900" rel="stylesheet">
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/css/style.css">
  	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>
<body>

	<jsp:include page="/WEB-INF/jsp/digiosk/Header.jsp" />

	<!-- <section class="panel_terms"></section> -->

	<section class="aboutUs_wrap pb-5" style="padding-top: 100px; background: url(${pageContext.request.contextPath}/resources/digiosk-assets/img/shape-8.png); background-repeat: no-repeat; background-position: center; background-size: contain;">
		<div class="container">
			<div class="row clearfix mb-5">
				<div class="col-12">
					<div class="content_heading mb-0">
						<h2>Terms &amp; Conditions</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<!-- <strong>This document lays out the Terms and Conditions ("Terms") which shall be applicable to the Co-branded Prepaid Cards issued by FEDERAL BANK and CASHIER.</strong> -->
					<h4 class="terms_seb_heading">Definitions:</h4>
					<p class="text-justify">
						<ol class="num-list">
							<li>IMoney shall mean having its registered office at "B-22, BASEMENT, IN FRONT OF BALAJI HOSPITAL, NEAR POLICE STATION, Bhadra, Rajasthan 335501", India and its successors and permitted assigns.</li>
							<li>"FEDERAL BANK" shall mean FEDERAL BANK Ltd., a banking company within the meaning of section 5 (c) of the banking Regulation Act, 1949 incorporated under the Companies Act 1956/2013, having its registered office at Federal Towers ,Ernakulam ,Kerala 682031.</li>
							<li>The "Cardholder" means a person to whom card is issued and who are authorized to hold and use the card.</li>
						</ol>
					</p>

					<h4 class="terms_seb_heading">Fees and Charges:</h4>
					<p class="text-justify">
						<ol class="num-list">
							<li>ATM withdrawal- Rs 20/-</li>
							<li>Two transactions fees will be waived-off every month if withdrawn from Federal Bank ATM.</li>
							<li>All the charges are exclusive of all taxes. All taxes as applicable will be levied. Charges and fees mentioned above are subject to review/changes from time to time at the sole discretion of "IMoney".</li>
						</ol>
					</p>

					<h4 class="terms_seb_heading">Withdrawal limits:</h4>
					<p class="text-justify">ATM Cash withdrawal is allowed on the card. Withdrawal limit will be as per the rules and regulations pertaining to the bank which owns the atm.</p>

					<h4 class="terms_seb_heading">Billing:</h4>
					<b>You can view your transaction details by downloading "IMoney" Application from Playstore/Appstore</b>
					<p class="text-justify">
						<ol class="lowerRoamn">
							<li>The cardholder must immediately raise any disputes pertaining to transactions to "IMoney" Helpline on "Number" (Mon-Sat: 9.00 am to 6.00 pm)</li>
							<li>Grievance Redressal Escalation: We have a Grievance Redressal Centre within the Organization. If you want to make a complaint, please write to "Email" or call our Helpline on "Number" (Mon-Sat: 9.00 am to 6.00 pm)</li>
						</ol>
					</p>

					<h4 class="terms_seb_heading">Cancellation/Withdrawal of the Card:</h4>
					<p class="text-justify">The Card shall remain the property of FEDERAL BANK at all times. If the Card Holder decides to cancel the Card, he/she shall give a written notice of at least "15"(fifteen) days and surrender Card to "IMoney". The Card Holder shall also pay dues, if any, payable to FEDERALBANK/ "IMoney" in connection with the Card. Similarly, if FEDERAL BANK / "IMoney" decide to cancel/withdraw the Card, FEDERAL BANK/ "IMoney" Limited shall give a prior written notice of "15" (fifteen) days to the Cardholder or the Corporate purchaser of the cards. The replacement of the Card will be subject to the tenure of the Card being still live and money being available on the Card. FEDERAL BANK/ "IMoney" may at any time terminate the card.</p>

					<p>Upon the expiry of the Card, the remaining unused amounts shall be forfeited."IMoney" will provide a prior notice of 30 (thirty) days before the expiry of the Card by way of SMS on your registered phone number with us. Kindly ensure that the contact information maintained with us is kept up to date. Any change in the same has to be communicated immediately.</p>

					<h4 class="terms_seb_heading">Loss/theft/misuse of card:</h4>
					<p class="text-justify">
						<ol class="lowerRoamn">
							<li>If a Card is lost or stolen, the Cardholder must immediately report such loss/theft to "IMoney" Helpline on "Number" (Mon-Sat: 9.00am to 6.00 pm)</li>
							<li>"IMoney" will, upon adequate verification, suspend the Card and terminate all facilities in relation, thereto.</li>
							<li>The Cardholder's right to use the Card shall be terminated by "IMoney" forthwith in the event of Loss or theft of the Card reported by the Cardholder.</li>
							<li>The Cardholder shall accept full responsibility for any wrongful or fraudulent use of the Card and which is or may be in contravention of these Terms and Conditions.</li>
						</ol>
					</p>

					<h4 class="terms_seb_heading">Disclosure:</h4>
					<b>Type of information relating to card holder to be disclosed with and without approval of cardholder</b>
					<p class="text-justify">
						<ol class="lowerRoamn">
							<li>The Cardholder acknowledges that the information on usage of the Card could be exchanged amongst banks and financial entities that provide similar facilities.</li>
							<li>"IMoney" shall not be obliged to disclose to the Cardholder the IMoney of the bank or financial entity to which it disclosed the information.</li>
							<li>The Cardholder shall	prior to availing the Card Services from "IMoney", obtain appropriate advice and shall familiarize himself with the associated risks and all the terms and conditions pertaining to the service.</li>
							<li>The Cardholder shall be bound by these Terms & Conditions and/or any other policy (ies) that may be stipulated by "IMoney" or FEDERAL BANK, from time to time in this regard.</li>
							<li>The card can be used only in India after activation and can be used at ATM's.</li>
							<li>The Card is not transferable under any circumstances. The Card is Reloadable in nature.</li>
							<li>The Card can be used at any ATM which accepts MasterCard. For each transaction done with the card at an ATM the applicable charges will be debited from the Card.</li>
							<li> Any request for Card replacement by the Cardholder will be processed by "IMoney" and the Card will be sent to the customer address/corporate address only. In case of insufficient funds in the Card, "IMoney" and FEDERAL BANK will not be liable to entertain such requests for Card replacement or Registration code re-issuance.</li>
							<li>The Cardholder shall indemnify "IMoney" & FEDERAL BANK to make good any loss, damage, interest, or any other financial charge that "IMoney" may incur and/or suffer, whether directly or indirectly, as a result of the Cardholder committing violations of these Terms and Conditions.</li>
							<li>Cardholder and Corporate purchasers agree that the issuing of Card is subject to rules and regulations introduced or amended from time to time by the Reserve Bank of India or any other regulatory body.</li>
						</ol>
					</p>
				</div>
			</div>
		</div>
	</section>

	<section class="downloadApp">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="app_txt text-center">
						<p>Can't wait ? Download app now and get started.</p>
					</div>
					<div class="app_img row">
						<div class="col-6 text-right">
							<a href="#"><img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/app.png"></a>
						</div>
						<div class="col-6">
							<a href="#"><img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/play.png"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<jsp:include page="/WEB-INF/jsp/digiosk/Footer.jsp" />

	<div class="backtoTop" style="display: none;"><img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/back_to_top.png" class="img-fluid"></div>


	<!-- Scripts starts here -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/js/jquery.min.js"></script>

	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>


	<!-- Custom Js -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/js/custom.js"></script>

</body>
</html>
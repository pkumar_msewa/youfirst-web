<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
   response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
   response.setDateHeader("Expires", 0);
   response.setHeader("Pragma", "no-cache");
%>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Master | Category List</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png">
        
        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/custom.css" rel="stylesheet" type="text/css" />

        <!-- DataTables -->
        <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>

        

	<style>
	/* The Modal (background) */
	
	
	/* Modal Content */
	.modal-content {
	    background-color: #fefefe;
	    margin: auto;
	    padding: 20px;
	    padding-left: 60px;
	    
	    border: 1px solid #888;
	    width: 100%;
	}
	
	/* The Close Button */
	.close {
	    color: #aaaaaa;
	    float: right;
	    font-size: 28px;
	    font-weight: bold;
	}
	
	.close:hover,
	.close:focus {
	    color: #000;
	    text-decoration: none;
	    cursor: pointer;
	}
	
	</style>
</head>
<body>
	<div id="wrapper">
		<jsp:include page="/WEB-INF/jsp/digiosk/generic/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/digiosk/generic/LeftMenu.jsp" />
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
		                <div class="col-12">
		                    <div class="page-title-box">
		                        <h4 class="page-title float-left">Category List</h4>
		                        <div class="clearfix"></div>
		                    </div>
		                </div>
		            </div>
		            <p align="center" id="mesg" style="color:green">${message}</p>
		            <div class="row">
		            	<div class="col-12">
		            		<div class="card-box">
		            			<div class="col-md-12">
		            				<div class="tabble">
		            					<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
		            					<thead>
	                                        <tr>
	                                            <th>Sl No</th>
                                                <th>Fund Name</th>
                                                <th>Amount</th>
                                                <th>Date</th>
                                                <th>MCC Code</th>
                                                <th>Country</th>
                                                <th>Edit</th>
	                                        </tr>
	                                    </thead>
	                                    <tbody id="categoryList">
	                                    </tbody>
		            				</table>
		            				<nav>
										<ul class="pagination" id="paginationn"></ul>
									</nav>
	            				</div>
		            			</div>
		            		</div>
		            	</div>
		            </div>
				</div>
			</div>
		</div>
	</div>
	
	
	<!-- Modal -->
	  <div class="modal fade" id="myModal" role="dialog">
	    <div class="modal-dialog modal-md">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Update category</h4>
	        </div>
	        <div class="modal-body">
	        	<div class="form-group">
					<label>Fund/Pocket Name:</label>
					<input type="text" placeholder="Enter fund name" class="form-control" readonly="readonly" id="fundName"  name="fundName" onkeypress=" return isAlphNumberKey(event)">													
					<span id="error_fundName" class="error"></span>
				</div>
				<div class="form-group">
					<label>Amount:</label>
					<input type="text" placeholder="Enter Amount" id="amount" class="form-control" name="amount" onkeypress="return isNumberKey(event)">													
					<span id="error_amount" class="error"></span>													
				</div>
				<div class="form-group">
					<label>Country:</label>
					<input type="text" class="form-control" id="country" name="country" value="IND" readonly="readonly">													
					<span id="error_country" class="error"></span>													
				</div>
				<div class="form-group">
					<label>MCC code:</label>
					<input type="text" class="form-control" id="mccCode" name="mccCode" readonly="readonly" onkeypress="return isNumberKey(event)">													
					<span id="error_mccCode" class="error"></span>													
				</div>
				<input type="hidden" class="form-control" value="" id="idValue">
				<input type="hidden" class="form-control" value="" id="index">
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" onclick="saveCategoryDetails();">Submit</button>
	        </div>
	      </div>
	    </div>
	  </div>
	
	
	<script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
    <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

    <!-- App js -->
    <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
    <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
    <script src="${pageContext.request.contextPath}/resources/corporate/assets/js/all.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
	
	
	<script type="text/javascript">
		var contextPath="${pageContext.request.contextPath}";
		var role = "${role}";
		$(document).ready(function(){
    		getCategoryList(0);
    	});
		$(function() {
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		    function cb(start, end) {
		        $('#reportrange').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
		    }
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'DD/MM/YYYY'
		        },
		        dateLimit: {
		            "days": 60
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		    cb(start, end);
		});
		
		function getCategoryList(page) {
    		var trHtml='';
    		var data;
    		$.ajax({
    			type : "POST",
	    		url:contextPath+"/Master/CategoryList",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"page" : page,
	   				"size" : 10,
	   			}),
	   			success : function(response) {
					$('#categoryList').empty();
					if (response.details.content.length > 0) {
						for (var i = 0; i < response.details.content.length; i++) {
							trHtml = '<tr>';
	   						trHtml = trHtml + '<td>' +(i+1)+ '</td>';
	   						trHtml = trHtml + '<td>' +response.details.content[i].pocketName+ '</td>';
	   						trHtml = trHtml + '<td id="amount'+i+'">' +response.details.content[i].amount+ '</td>';
	   						trHtml = trHtml + '<td>' +formatDate(response.details.content[i].created)+ '</td>';
	   						trHtml = trHtml + '<td>' +response.details.content[i].mccCode+ '</td>';
	   						trHtml = trHtml + '<td>' +response.details.content[i].country+ '</td>';
	   						trHtml = trHtml + '<td><button class="btn btn-info" type="button" onclick="updateCategory('+response.details.content[i].id+','+i+');">Edit</button></td>';
	   						trHtml = trHtml + '</tr>';
	   						$('#categoryList').append(trHtml);
						}
					} else {
						trHtml = '<tr>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td>No data found</td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '</tr>';
						$('#categoryList').append(trHtml);
					}
					$(function () {
   	   					console.log("inside funt...and total pages:"+response.totalPage);
   	   					$('#paginationn').twbsPagination({
    					 	totalPages: response.totalPage,
    					 	visiblePages: 10,
    		         	 	onPageClick: function (event, page) {
    		         	 		getCategoryList(page-1);
    			         	}
			 			});
	   				});
					
					setTimeout(function(){
						$('#mesg').html("");
				    }, 3000);
					
	   			}
    		});
    	}
		function updateCategory(id,index) {
    		console.log("id.. "+id);
    		console.log("index.. "+index);
    		$.ajax({
    			type : "POST",
	    		url:contextPath+"/Master/GetCategoryBasedOnId/"+id,
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				
	   			}),
	   			success : function(response) {
	   				if(response.code == "S00"){
	   					$('#myModal').modal('toggle');
	   					$('#fundName').val(response.details.pocketName);
	   					$('#amount').val(response.details.amount);
	   					$('#mccCode').val(response.details.mccCode);
	   					$('#idValue').val(id);
	   					$('#index').val(index);
	   				}
				}
   			});
    	}
    	
    	function saveCategoryDetails() {
    		var idValue = $('#idValue').val();
    		var amount = $('#amount').val();
    		var index = $('#index').val();
    		var fundName = $('#fundName').val();
    		var country = $('#country').val();
    		var mccCode = $('#mccCode').val();
    		console.log("index value ... "+index);
    		$.ajax({
    			type : "POST",
	    		url:contextPath+"/Master/UpdateCategory",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				amount : amount,
	   				id : idValue,
	   				fundName : fundName,
	   				country : country,
	   				mccCode : mccCode,
	   			}),
	   			success : function(response) {
	   				if(response.code == "S00"){
	   					$('#myModal').modal('toggle');
	   					$('#amount'+index).html(amount);
	   				}
				}
   			});
    	}
		function formatDate(d){
        	d=new Date(d);
        	var month = d.getMonth();
        	var day = d.getDate();
        	month = month + 1;
        	month = month + "";
        	if (month.length == 1)
        	{
        	month = "0" + month;
        	}
        	day = day + "";
        	if (day.length == 1)
        	{
        	day = "0" + day;
        	}
        	return d.getFullYear()+ '-' +month + '-' + day ;
       	}
	</script>
</body>
</html>
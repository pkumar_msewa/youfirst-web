<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
   response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
   response.setDateHeader("Expires", 0);
   response.setHeader("Pragma", "no-cache");
%>
<html lang="en">
	<head>
		<meta charset="utf-8" />
	<title>Master | Set commission slab</title>
	<meta name="viewport"
		content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<!-- App favicon -->
	<link rel="shortcut icon"
		href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png">
	
	<!-- Responsive datatable examples -->
	<link
		href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css"
		rel="stylesheet" type="text/css" />
	
	<!-- App css -->
	<link
		href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css"
		rel="stylesheet" type="text/css" />
	<link
		href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css"
		rel="stylesheet" type="text/css" />
	<link
		href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css"
		rel="stylesheet" type="text/css" />
	<link
		href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css"
		rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/resources/digiosk-assets/jquery-ui/jquery-ui.css" rel="stylesheet">
	
	<style>
            fieldset {
                min-width: 0;
                padding: 10px 30px;
                margin: 0;
                border: 1px solid #efefef;
            }
            fieldset legend {
                width: auto;
                font-size: 1.3rem;
            }
            .blue-btn:hover,
            .blue-btn:active,
            .blue-btn:focus,
            .blue-btn {
              background: transparent;
              border: solid 1px #27a9e0;
              border-radius: 3px;
              color: #27a9e0;
              font-size: 14px;
              margin-bottom: 20px;
              outline: none !important;
              padding: 10px 20px;
            }
            .tgl {
              position: relative;
              display: inline-block;
              height: 30px;
              cursor: pointer;
              margin-top: 27px;
            }
            .tgl > input {
              position: absolute;
              opacity: 0;
              z-index: -1;
              /* Put the input behind the label so it doesn't overlay text */
              visibility: hidden;
            }
            .tgl .tgl_body {
              width: 60px;
              height: 30px;
              background: white;
              border: 1px solid #dadde1;
              display: inline-block;
              position: relative;
              border-radius: 50px;
            }
            .tgl .tgl_switch {
              width: 30px;
              height: 30px;
              display: inline-block;
              background-color: white;
              position: absolute;
              left: -1px;
              top: -1px;
              border-radius: 50%;
              border: 1px solid #ccd0d6;
              -moz-box-shadow: 0 2px 2px rgba(0, 0, 0, 0.13);
              -webkit-box-shadow: 0 2px 2px rgba(0, 0, 0, 0.13);
              box-shadow: 0 2px 2px rgba(0, 0, 0, 0.13);
              -moz-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), -moz-transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              -o-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), -o-transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              -webkit-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), -webkit-transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              z-index: 1;
            }
            .tgl .tgl_track {
              position: absolute;
              left: 0;
              top: 0;
              right: 0;
              bottom: 0;
              overflow: hidden;
              border-radius: 50px;
            }
            .tgl .tgl_bgd {
              position: absolute;
              right: -10px;
              top: 0;
              bottom: 0;
              width: 55px;
              -moz-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              -o-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              -webkit-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              background: #439fd8 url("http://petelada.com/images/toggle/tgl_check.png") center center no-repeat;
            }
            .tgl .tgl_bgd-negative {
              right: auto;
              left: -45px;
              background: white url("http://petelada.com/images/toggle/tgl_x.png") center center no-repeat;
            }
            .tgl:hover .tgl_switch {
              border-color: #b5bbc3;
              -moz-transform: scale(1.06);
              -ms-transform: scale(1.06);
              -webkit-transform: scale(1.06);
              transform: scale(1.06);
            }
            .tgl:active .tgl_switch {
              -moz-transform: scale(0.95);
              -ms-transform: scale(0.95);
              -webkit-transform: scale(0.95);
              transform: scale(0.95);
            }
            .tgl > :not(:checked) ~ .tgl_body > .tgl_switch {
              left: 30px;
            }
            .tgl > :not(:checked) ~ .tgl_body .tgl_bgd {
              right: -45px;
            }
            .tgl > :not(:checked) ~ .tgl_body .tgl_bgd.tgl_bgd-negative {
              right: auto;
              left: -10px;
            }
            .options {
                font-size: 16px;
            }
            .gj-datepicker-bootstrap [role=right-icon] button {
            	padding: 18px 0;
            }
            .error {
            	color: red;
            }
            .success {
            	color: green;
            }
        </style>
	</head>
	<body>
		<div id="wrapper">
			<jsp:include page="/WEB-INF/jsp/digiosk/generic/Header.jsp" />
			<jsp:include page="/WEB-INF/jsp/digiosk/generic/LeftMenu.jsp" />
			<div class="content-page">
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-12">
								<div class="page-title-box">
									<h4 class="page-title float-left">Set commission slab</h4>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<div class="card-box">
									<form:form action="${pageContext.request.contextPath}/Master/SetCommission" id="formId"  method="post" modelAttribute="setCommission">
										<div class="row">
											<div class="col-8 offset-md-2 offset-md-3t-sm-3">
												<c:choose>
													<c:when test="${not empty SUCCESSMSG}">
														<center class="success">${SUCCESSMSG}</center>
													</c:when>
													<c:when test="${not empty ERRORMSG}">
														<center class="error">${ERRORMSG}</center>
													</c:when>
												</c:choose>
												<fieldset>
													<legend>Set commission slab</legend>
													<div class="row">
														<div class="col-6">
															<div class="form-group">
	                                                          <label for="fname">Agent</label>
	                                                          <form:input type="text" path="agentUserName" class="form-control agent"/>
	                                                          <span id="error_agentUserName" class="error"></span>
	                                                        </div>
														</div>
														<%-- <div class="col-6">
															<label for="fname">Commisssion Slab</label>
															<form:select path="commissionSlab" class="form-control">
																<option value="0-1000">0-1000</option>
																<option value="1001-2000">1001-2000</option>
																<option value="2001-3000">2001-3000</option>
																<option value="3001-4000">3001-4000</option>
															</form:select>
														</div> --%>
														<div class="col-6">
															<div class="form-group">
	                                                          <label for="fname">Service</label>
	                                                          <form:input type="text" path="serviceName" class="form-control agent" value="Load Money" readonly="true"/>
	                                                          <span id="error_serviceName" class="error"></span>
	                                                        </div>
														</div>
														<div class="col-6">
															<div class="form-group">
	                                                          <label for="fname">Agent Commission Percent</label>
	                                                          <form:input type="text" path="agentCommissionPercent" class="form-control" onkeypress="return isNumberKey(event);" maxlength="5"/>
	                                                          <span id="error_agentCommissionPercent" class="error"></span>
	                                                        </div>
														</div>
														<div class="col-6">
															<div class="form-group">
	                                                          <label for="fname">Distributor Commission Percent</label>
	                                                          <form:input type="text" path="distributorCommissionPercent" class="form-control" onkeypress="return isNumberKey(event);" maxlength="5"/>
	                                                          <span id="error_distributorCommissionPercent" class="error"></span>
	                                                        </div>
														</div>
														<div class="col-6">
															<div class="form-group">
	                                                          <label for="fname">Super Distributor Commission Percent</label>
	                                                          <form:input type="text" path="superDistributorCommissionPercent" class="form-control" onkeypress="return isNumberKey(event);" maxlength="5"/>
	                                                          <span id="error_superDistributorCommissionPercent" class="error"></span>
	                                                        </div>
														</div>
														<div class="col-6">
															<div class="form-group">
	                                                          <label for="fname">Admin Commission Percent</label>
	                                                          <form:input type="text" path="adminCommissionPercent" class="form-control" onkeypress="return isNumberKey(event);" maxlength="5"/>
	                                                          <span id="error_adminCommissionPercent" class="error"></span>
	                                                        </div>
														</div>
													</div>
												</fieldset>
											</div>
										</div>
										<center><button type="button" class="btn btn-primary mt-4" onclick="validateform()">Submit</button></center>
									</form:form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
		<script
			src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
		<script type="text/javascript"
			src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	
		<!-- App js -->
		<script
			src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
		<script
			src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
		<script
			src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/jquery-ui/jquery-ui.js"></script>
		
		<script type="text/javascript">
			var contextPath="${pageContext.request.contextPath}";
			function isAlphNumberKey(evt){
			    var k = (evt.which) ? evt.which : evt.keyCode
			    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
			}
			function isAlphKey(evt){
			    var charCode = (evt.which) ? evt.which : evt.keyCode
			    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
			}
			function isNumberKey(evt){
			    var charCode = (evt.which) ? evt.which : evt.keyCode
			    return !(charCode > 31 && (charCode < 48 || charCode > 57)) || (charCode==46);
			}
			
			function validateform(){
				var valid = true;
				var agentUserName = $('#agentUserName').val();
				if (agentUserName.length <= 0) {
					valid = false;
					$('#error_agentUserName').html('Please enter agent name.');
				}
				
				var agentCommissionPercent = $('#agentCommissionPercent').val();
				if (agentCommissionPercent.length <= 0) {
					valid = false;
					$('#error_agentCommissionPercent').html('Please enter agent commisssion amount');
				}
				
				var distributorCommissionPercent = $('#distributorCommissionPercent').val();
				if (distributorCommissionPercent.length <= 0) {
					valid = false;
					$('#error_distributorCommissionPercent').html('Please enter distributor commisssion amount');
				}
				
				var superDistributorCommissionPercent = $('#superDistributorCommissionPercent').val();
				if (superDistributorCommissionPercent.length <= 0) {
					valid = false;
					$('#error_superDistributorCommissionPercent').html('Please enter super distributor commisssion amount');
				}
				
				var adminCommissionPercent = $('#adminCommissionPercent').val();
				if (adminCommissionPercent.length <= 0) {
					valid = false;
					$('#error_adminCommissionPercent').html('Please enter admin commission amount');
				}
				if(valid){
					$('#formId').submit();
				}
				var timeout = setTimeout(function(){
			    	$("#error_agentUserName").html("");
			    	$("#error_minAmount").html("");
			    	$("#error_maxAmount").html("");
			    	$("#error_agentCommissionPercent").html("");
			    	$("#error_distributorCommissionPercent").html("");
			    	$("#error_adminCommissionPercent").html("");
			    	$("#error_superDistributorCommissionPercent").html("");
			    }, 3000);
				
			}
			
			function getAgentCommission(){
				var agentUserName = $('#agentUserName').val();
				var serviceName = $('#serviceName').val();
				$.ajax({
					type : "POST",
		    		url:contextPath+"/Master/GetAgentCommission",
		    		dataType:"json",
		   			contentType : "application/json",
		   			data : JSON.stringify({
		   				"agentUserName" : "" +agentUserName+ "",
		   				"serviceName" : "" +serviceName+ "",
		   			}),
		   			success : function(response) {
		   				if(response.code == 'S00'){
		   					$('#minAmount').val(response.minAmount);
		   					$('#maxAmount').val(response.maxAmount);
		   					$('#agentCommissionPercent').val(response.agentCommissionPercent);
		   					$('#distributorCommissionPercent').val(response.distributorCommissionPercent);
		   					$('#superDistributorCommissionPercent').val(response.superDistributorCommissionPercent);
		   					$('#adminCommissionPercent').val(response.adminCommissionPercent);
		   				}
		   			}
				});
			}
			
			$(document).ready(function(){
				<c:set var="agentList" value="${agentList}"/>
				var availableTags= ${agentList};
				$( ".agent" ).autocomplete({
		      		source: availableTags
			    });
			});
		</script>
	</body>
</html>
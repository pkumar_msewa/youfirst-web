<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
   response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
   response.setDateHeader("Expires", 0);
   response.setHeader("Pragma", "no-cache");
%>
<!doctype html>
<html lang="en">
	<head>
       <meta charset="utf-8" />
       <title>Master | Service List</title>
       <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
       <meta content="" name="description" />
       <meta content="" name="author" />
       <meta http-equiv="X-UA-Compatible" content="IE=edge" />

       <!-- App favicon -->
       <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

       <!-- Table Export -->
       <link href="${pageContext.request.contextPath}/resources/admin/assets/table_export/css/tableexport.css" rel="stylesheet" type="text/css" />
       
       <!-- App css -->
       <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
       <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
       <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
       <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />

       <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.min.js"></script>
	   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
	   <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	   <style type="text/css">
	   		.select2-container .select2-selection--multiple { 
 				width: 160%; 
 			}
 			.error {
 				color: red;
 			}
	   </style>
    </head>
	<body>
		<script type="text/javascript"></script>
		<div id="wrapper">
			<jsp:include page="/WEB-INF/jsp/digiosk/generic/Header.jsp" />
			<jsp:include page="/WEB-INF/jsp/digiosk/generic/LeftMenu.jsp" />
			<div class="content-page">
				<div class="content">
					<div class="container-fluid">
						<div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">List of Services</h4>
                                    <div class="clearfix"></div>
                                    <span id="stst" style="margin-left: 40%; color: #3c86d8fa;">${statusUpdt}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        	<div class="col-12">
                        		<div class="card-box">
                        			<div class="row">
                        				<div class="col-md-4 col-sm-4 col-xs-4">
                        					<form action="" method="post">
												<div class="form-row">
													<div class="col-sm-8">
														<div id="" class="pull-left" style="cursor: pointer;">
															<label class="sr-only" for="filterBy">Filter By:</label>
														   	<input id="reportrange" name="toDate" class="form-control" readonly="readonly"/>
														</div>
													</div>
													<div class="col-sm-3">
														<button class="btn btn-primary" onclick="getServiceListDataBasedOnDate(0);" type="button">Filter</button>
													</div>
												</div>
											</form>
                        				</div>
                        				<div class="col-md-4 col-sm-4 col-xs-4">
											<div class="row">
			                                    <div class="form-group">
				                                    <input id="username" name="userName" class="form-control" maxlength="10" onkeypress="return isNumberKey(event);" placeholder="Enter Username"/>
				                                </div>
				                                <div class="form-group">    
				                                    <button class="btn btn-primary" onclick="" type="button" >Search</button>
			                                   	</div>	
			                                   	<span id="err" style="color: red;position: fixed;margin-top: 27px;"></span>
			                                </div>
										</div>
                        			</div>
                        		</div>
                        	</div>
                        </div>
                        <div class="row">
                        	<div class="col-12">
                        		<div class="card-box table-responsive">
                        			<table id="Cashier-userList" class="table table-striped table-bordered" width="100%">
                        				<thead>
                                            <tr>
                                                <th>Sl No</th>
                                                <th>User details</th>
                                                <th>Registered Date</th>
                                                <th>Role</th>
                                                <th>Assigned Service</th>
                                            </tr>
                                        </thead>
                                        <tbody id="serviceListData"></tbody>
                        			</table>
                        			<nav style="float: right;">
										<ul class="pagination" id="paginationn"></ul>
									</nav>
                        		</div>
                        	</div>
                        </div>
					</div>
				</div>
				<footer class="footer text-right">
                    2018 © Copyright IMoney.
                </footer>
			</div>
		</div>
		
		
		<!-- Modal -->
		<div class="modal fade" id="serviceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Update Service</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		      	<form:form action="${pageContext.request.contextPath}/Master/UpdateService" id="updateService" method="post" modelAttribute="addService">
			      	<div class="form-group">
						<label>Username/Email:</label>
						<form:input type="text" placeholder="Enter Email-Id" class="form-control" path="userName" readonly="true"/>	
						<span id="error_userName" class="error"></span>												
					</div>
					<div class="form-group">
						<label>Role:</label>
						<form:select class="form-control" path="role">
							<option value="Super Distributor" id="id0">Super Distributor</option>
							<option value="Distributor" id="id1">Distributor</option>
							<option value="Agent" id="id2">Agent</option>
						</form:select>
					</div>
					<div class="form-group">
						<label for="id_label_multiple">
							Choose your services
							<form:select class="js-example-basic-multiple js-states form-control select2-hidden-accessible"
								id="id_label_multiple" multiple="multiple" path="serviceId">
								<c:forEach var="service" items="${service}">
									<option value="${service.id}">${service.name}</option>
								</c:forEach>
							</form:select>
							<span id="error_id_label_multiple" class="error"></span>
						</label>													
					</div>
				</form:form>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <button type="button" id="modalSubmit" class="btn btn-primary" onclick="checkFormSubmit();">Save changes</button>
		      </div>
		    </div>
		  </div>
		</div>
		
		<script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
	
		<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		<!-- Include Date Range Picker -->
		<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
		
		<!-- Table Export js -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/assets/table_export/js/FileSaver.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/assets/table_export/js/tableexport.js"></script>
		
        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
        
        <script type="text/javascript">
        	var contextPath = "${pageContext.request.contextPath}";
        	$(".js-example-basic-multiple").select2({
	        	  theme: "bootstrap",
	        	  width: "resolve"
	        });
        	$(document).ready(function(){
        		getServiceListData(0);
        	});
        	$(function() {
			    var start = moment().subtract(29, 'days');
			    var end = moment();
			    function cb(start, end) {
			        $('#reportrange').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
			    }
			    $('#reportrange').daterangepicker({
			        startDate: start,
			        endDate: end,
			        locale: {
			        	format: 'DD/MM/YYYY'
			        },
			        dateLimit: {
			            "days": 60
			        },
			        ranges: {
			           'Today': [moment(), moment()],
			           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			           'This Month': [moment().startOf('month'), moment().endOf('month')],
			           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			        }
			    }, cb);
			    cb(start, end);
			});
        	
        	function getServiceListData(page) {
        		var trHtml='';
        		$.ajax({
        			type : "POST",
    	    		url:contextPath+"/Master/ServiceListData",
    	    		dataType:"json",
    	   			contentType : "application/json",
    	   			data : JSON.stringify({
    	   				"page" : page,
    	   				"size" : 10,
    	   			}),
    	   			success : function(response) {
    	   				$('#serviceListData').empty();
    	   				if (response.details.content.length > 0) {
    	   					for (var i = 0; i < response.details.content.length; i++) {
    	   						var data = response.details.content[i];
    	   						trHtml = '<tr>';
        	   					trHtml = trHtml + '<td>' +(i+1)+ '</td>';
        	   					trHtml = trHtml + '<td> Username : ' + (data.username) + '<br> User type : ' + (response.details.content[i].userType) + '</td>';
        	   					trHtml = trHtml + '<td>' +(formatDate(data.created))+ '</td>';
        	   					var authority;
        	   					if(data.authority == 'ROLE_SUPER_DISTRIBUTOR,ROLE_AUTHENTICATED'){
        	   						authority = 'Super Distributor';
        	   					} else if(data.authority == 'ROLE_DISTRIBUTOR,ROLE_AUTHENTICATED'){
        	   						authority = 'Distributor';
        	   					} else if(data.authority == 'ROLE_AGENT,ROLE_AUTHENTICATED'){
        	   						authority = 'Agent';
        	   					}
        	   					trHtml = trHtml + '<td>' +(authority)+ '</td>';
        	   					if(data.userServices.length > 0){
        	   						trHtml = trHtml + '<td><button class="btn btn-info" type="button" onclick="getAssignServiceList('+data.id+');">View</button></td>';
        	   					} else {
        	   						trHtml = trHtml + '<td>NA</td>';
        	   					}
        	   					$('#serviceListData').append(trHtml);
    	   					}
    	   				} else {
    	   					trHtml = '<tr>';
    	   					trHtml = trHtml + '<td></td>';
    	   					trHtml = trHtml + '<td></td>';
    	   					trHtml = trHtml + '<td>No result found</td>';
    	   					trHtml = trHtml + '<td></td>';
    	   					trHtml = trHtml + '<td></td>';
    	   					$('#serviceListData').append(trHtml);
    	   				}
    	   				$(function () {
	   	   					console.log("inside funt...and total pages:"+response.totalPages);
	   	   					$('#paginationn').twbsPagination({
						 	totalPages: response.totalPages,
						 	visiblePages: 10,
			         	 	onPageClick: function (event, page) {
			         	 		getServiceListData(page-1);
   				         	}
				 		});
   	   				});
   	   			}
       		});
    	}
        function getAssignServiceList(userId) {
        	$.ajax({
        		type : "POST",
	    		url:contextPath+"/Master/AssignServiceListData",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"userId" : userId,
	   			}),
	   			success : function(response) {
	   				$('#serviceModal').modal('show');
	   				$('#userName').val(response.userName);
	   				$("#id_label_multiple").removeAttr("selected","selected");
	   				$.each(response.details,function(i,item){
	   					var trHtml = '<option value=' +(item.id)+ ' selected="selected">' + (item.name) +'</option>';
	   					$('#id_label_multiple').append(trHtml);
	   				});
	   				/* if(response.role == 'Agent'){
	   					$('#id1').attr("selected","selected");
	   				} else if (response.role == 'SuperAgent'){
	   					$('#id2').attr("selected","selected");
	   				} */
	   			}
        	});
        }
        
        function checkFormSubmit(){
        	var valid = true;
        	var userName = $('#userName').val();
        	if(userName.length <= 0) {
        		valid = false;
        		$('#error_userName').html('Please enter username.');
        	}
        	var id_label_multiple = $('#id_label_multiple').val();
        	if(id_label_multiple.length <= 0) {
        		valid = false;
        		$('#error_id_label_multiple').html('Please choose service');
        	}
        	
        	if(valid){
        		$('#updateService').submit();
        	}
        	var timeout = setTimeout(function(){
				$('#error_id_label_multiple').html("");
				$('#error_userName').html("");
		    }, 3000);
        }

        function getServiceListDataBasedOnDate(page) {
    		var trHtml='';
    		var reportRange = $('#reportrange').val();
    		$.ajax({
    			type : "POST",
	    		url:contextPath+"/Master/ServiceListData",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"page" : page,
	   				"size" : 10,
	   				"dateRange" : "" + reportRange + ""
	   			}),
	   			success : function(response) {
	   				$('#serviceListData').empty();
	   				if (response.details.content.length > 0) {
	   					for (var i = 0; i < response.details.content.length; i++) {
	   						var data = response.details.content[i];
	   						trHtml = '<tr>';
    	   					trHtml = trHtml + '<td>' +(i+1)+ '</td>';
    	   					trHtml = trHtml + '<td> Username : ' + (data.username) + '<br> User type : ' + (response.details.content[i].userType) + '</td>';
    	   					trHtml = trHtml + '<td>' +(formatDate(data.created))+ '</td>';
    	   					trHtml = trHtml + '<td>' +(data.authority)+ '</td>';
    	   					if(data.userServices.length > 0){
    	   						trHtml = trHtml + '<td><button class="btn btn-info" type="button" onclick="getAssignServiceList('+data.id+');">View</button></td>';
    	   					} else {
    	   						trHtml = trHtml + '<td>NA</td>';
    	   					}
    	   					$('#serviceListData').append(trHtml);
	   					}
	   				} else {
	   					trHtml = '<tr>';
	   					trHtml = trHtml + '<td></td>';
	   					trHtml = trHtml + '<td></td>';
	   					trHtml = trHtml + '<td>No result found</td>';
	   					trHtml = trHtml + '<td></td>';
	   					trHtml = trHtml + '<td></td>';
	   					$('#serviceListData').append(trHtml);
	   				}
	   				$(function () {
   	   					console.log("inside funt...and total pages:"+response.totalPages);
   	   					$('#paginationn').twbsPagination({
						 	totalPages: response.totalPages,
						 	visiblePages: 10,
			         	 	onPageClick: function (event, page) {
			         	 		getServiceListDataBasedOnDate(page-1);
				         	}
				 		});
	   				});
	   			}
   			});
		}
        
        function formatDate(d){
        	d=new Date(d);
        	var month = d.getMonth();
        	var day = d.getDate();
        	month = month + 1;
        	month = month + "";
        	if (month.length == 1)
        	{
        	month = "0" + month;
        	}
        	day = day + "";
        	if (day.length == 1)
        	{
        	day = "0" + day;
        	}
        	return d.getFullYear()+ '-' +month + '-' + day ;
       	}
        </script>
    </body>
</html>
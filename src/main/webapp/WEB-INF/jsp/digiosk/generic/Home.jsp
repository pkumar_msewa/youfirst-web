<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<!DOCTYPE html>
<html>
    <%
	   response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
	   response.setDateHeader("Expires", 0);
	   response.setHeader("Pragma", "no-cache");
	%>
<head>
        <meta charset="utf-8" />
        <title>Master | Dashboard</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png">

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>

    </head>


    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <jsp:include page="/WEB-INF/jsp/digiosk/generic/Header.jsp" />
		    <jsp:include page="/WEB-INF/jsp/digiosk/generic/LeftMenu.jsp" />
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
          
            <!-- Left Sidebar End -->


            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                	<c:choose>
                                		<c:when test="${role eq 'ROLE_SUPER_ADMIN,ROLE_AUTHENTICATED'}">
                                			<h4 class="page-title float-left">Supr Admin Dashboard</h4>
                                		</c:when>
                                		<c:when test="${role eq 'ROLE_ADMINISTRATOR,ROLE_AUTHENTICATED'}">
                                			<h4 class="page-title float-left">Admin Dashboard</h4>
                                		</c:when>
                                		<c:when test="${role eq 'ROLE_SUPER_DISTRIBUTOR,ROLE_AUTHENTICATED'}">
                                			<h4 class="page-title float-left">Super Distributor Dashboard</h4>
                                		</c:when>
                                		<c:when test="${role eq 'ROLE_DISTRIBUTOR,ROLE_AUTHENTICATED'}">
                                			<h4 class="page-title float-left">Distributor Dashboard</h4>
                                		</c:when>
                                	</c:choose>
                                    <!-- <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                        <li class="breadcrumb-item active">Dashboard</li>
                                    </ol> -->

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

						<div class="card-group">
							<div class="card" style="cursor: pointer;"  onclick="document.location='${pageContext.request.contextPath}/Master/GenericUserList/userDetails'">
								<div class="card-body">
									<div class="d-flex no-block align-items-center">
										<a href="#"><i></i></a>
										<div class="m-l-15 m-t-10" >
											<h4 class="font-medium m-b-0">Total Users</h4>
											<h5>
												<i class="fa fa-users" aria-hidden="true"></i>&nbsp;
												${dashboardData.totalUsers}
											</h5>
										</div> 
									</div>
								</div>
							</div>
							<%--  <div class="card">
                                <div class="card-body">
                                    <div class="d-flex no-block align-items-center">
                                        <a href="#"><i></i></a>
                                        <div class="m-l-15 m-t-10">
                                            <h4 class="font-medium m-b-0">Card Issued</h4>
                                            <h5>${totalCards}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div> --%>
							<div class="card" style="cursor: pointer;"  onclick="document.location='${pageContext.request.contextPath}/Master/userByStatus/Active'">
								<div class="card-body">
									<div class="d-flex no-block align-items-center">
										<a href="#"><i></i></a>
										<div class="m-l-15 m-t-10">
											<h4 class="font-medium m-b-0">Active Users</h4>
											<h5>
												<i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;
												${dashboardData.activeUser}
											</h5>
										</div>
									</div>
								</div>
							</div>

							<div class="card" style="cursor: pointer;"  onclick="document.location='${pageContext.request.contextPath}/Master/userByStatus/Inactive'">
								<div class="card-body">
									<div class="d-flex no-block align-items-center">
										<a href="#"><i></i></a>
										<div class="m-l-15 m-t-10">
											<h4 class="font-medium m-b-0">Inactive Users</h4>
											<h5>
												<i class="fa fa-user-o" aria-hidden="true"></i>&nbsp;
												${dashboardData.inactiveUsers}
											</h5>
										</div>
									</div>
								</div>
							</div>

							<div class="card" style="cursor: pointer;"  onclick="document.location='${pageContext.request.contextPath}/Master/userByStatus/Blocked'">
								<div class="card-body">
									<div class="d-flex no-block align-items-center">
										<a href="#"><i></i></a>
										<div class="m-l-15 m-t-10">
											<h4 class="font-medium m-b-0">Blocked Users</h4>
											<h5>
												<i class="fa fa-user-times" aria-hidden="true"></i>&nbsp;
												${dashboardData.blockedUsers}
											</h5>
										</div>
									</div>
								</div>
							</div>

						</div>

						<div class="card-group">
							<div class="card">
								<div class="card-body">
									<div class="d-flex no-block align-items-center">
										<a href="#"><i></i></a>
										<div class="m-l-15 m-t-10">
											<h4 class="font-medium m-b-0">Total Wallet</h4>
											<h5>
												<i class="fa fa-suitcase"></i>&nbsp;${dashboardData.totalWallets}
											</h5>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-body">
									<div class="d-flex no-block align-items-center">
										<a href="#"><i></i></a>
										<div class="m-l-15 m-t-10">
											<h4 class="font-medium m-b-0">Total Physical Cards</h4>
											<h5>
												<i class="fa fa-credit-card-alt"></i>&nbsp;${dashboardData.totalPCards}
											</h5>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-body">
									<div class="d-flex no-block align-items-center">
										<a href="#"><i></i></a>
										<div class="m-l-15 m-t-10">
											<h4 class="font-medium m-b-0">Total Virtual Card</h4>
											<h5>
												<i class="fa fa-credit-card"></i>&nbsp;${dashboardData.totalVCards}
											</h5>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-body">
									<div class="d-flex no-block align-items-center">
										<a href="#"><i></i></a>
										<div class="m-l-15 m-t-10">
											<h4 class="font-medium m-b-0">Blocked Card</h4>
											<h5>
												<i class="fa fa-ban"></i>&nbsp;${dashboardData.blockedCards}
											</h5>
										</div>
									</div>
								</div>
							</div>



						</div>
						<c:choose>
					<c:when test="${role eq 'ROLE_SUPER_ADMIN,ROLE_AUTHENTICATED' || role eq 'ROLE_ADMINISTRATOR,ROLE_AUTHENTICATED'}">
						<div class="card-group">
							<div class="card">
								<div class="card-body">
									<div class="d-flex no-block align-items-center">
										<a href="#"><i></i></a>
										<div class="m-l-15 m-t-10">
											<h4 class="font-medium m-b-0">Transacted Users</h4>
											<h5>
												<i class="fa fa-user-md"></i>&nbsp;${dashboardData.transactedUsers}
											</h5>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-body">
									<div class="d-flex no-block align-items-center">
										<a href="#"><i></i></a>
										<div class="m-l-15 m-t-10">
											<h4 class="font-medium m-b-0">VCard Txn Amt</h4>
											<h5>
											<i class="fa fa-inr"></i>&nbsp; 98774
<%-- 												<i class="fa fa-inr"></i>&nbsp;${dashboardData.vCardTxn} --%>
											</h5>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-body">
									<div class="d-flex no-block align-items-center">
										<a href="#"><i></i></a>
										<div class="m-l-15 m-t-10">
											<h4 class="font-medium m-b-0">PCard Txn Amt</h4>
											<h5>
											<i class="fa fa-inr"></i>&nbsp; 84237
<%-- 												<i class="fa fa-inr"></i>&nbsp;${dashboardData.pCardTxn} --%>
											</h5>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-body">
									<div class="d-flex no-block align-items-center">
										<a href="#"><i></i></a>
										<div class="m-l-15 m-t-10">
											<h4 class="font-medium m-b-0">Load Wallet Amt</h4>
											<h5>
												<i class="fa fa-inr"></i>&nbsp;${dashboardData.loadWalletAmt}
											</h5>
										</div>
									</div>
								</div>
							</div>
						</div>
					</c:when>
					</c:choose>
					<br>
                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2017 Â© Copyright IMoney.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
        <%-- <script src="${pageContext.request.contextPath}/resources/admin/plugins/waypoints/lib/jquery.waypoints.min.js"></script> --%>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/counterup/jquery.counterup.min.js"></script>

        <!-- Chart JS -->
        <%-- <script src="${pageContext.request.contextPath}/resources/admin/plugins/chart.js/chart.bundle.js"></script> --%>

        <!-- init dashboard -->
        <%-- <script src="${pageContext.request.contextPath}/resources/admin/assets/pages/jquery.dashboard.init.js"></script> --%>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
        
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/highcharts.js"></script>
		<script src="${pageContext.request.contextPath}/resources/admin/assets/js/exporting.js"></script>
		<script src="${pageContext.request.contextPath}/resources/admin/assets/js/export-data.js"></script>
		
		<!-- piechart -->

<!-- linechart -->
    </body>
    <script type="text/javascript">
    
//     function openCustomer(){
//     	window.loacation
//     }
    
    </script>

</html>
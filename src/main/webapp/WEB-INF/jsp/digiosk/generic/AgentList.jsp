<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
   response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
   response.setDateHeader("Expires", 0);
   response.setHeader("Pragma", "no-cache");
%>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Master | Agent List</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png">
        
        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/custom.css" rel="stylesheet" type="text/css" />

        <!-- DataTables -->
        <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>

        

	<style>
	/* The Modal (background) */
	.modal {
	    display: none; /* Hidden by default */
	    position: fixed; /* Stay in place */
	    z-index: 1; /* Sit on top */
	    padding-top: 70px; /* Location of the box */
	    
	    left: 0;
	    top: 0;
	    width: 100%; /* Full width */
	    height: 100%; /* Full height */
	    overflow: auto; /* Enable scroll if needed */
	    background-color: rgb(0,0,0); /* Fallback color */
	    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}
	
	/* Modal Content */
	.modal-content {
	    background-color: #fefefe;
	    margin: auto;
	    padding: 20px;
	    padding-left: 60px;
	    
	    border: 1px solid #888;
	    width: 80%;
	}
	
	/* The Close Button */
	.close {
	    color: #aaaaaa;
	    float: right;
	    font-size: 28px;
	    font-weight: bold;
	}
	
	.close:hover,
	.close:focus {
	    color: #000;
	    text-decoration: none;
	    cursor: pointer;
	}
	
	</style>
</head>
<body>
	<div id="wrapper">
		<jsp:include page="/WEB-INF/jsp/digiosk/generic/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/digiosk/generic/LeftMenu.jsp" />
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
		                <div class="col-12">
		                    <div class="page-title-box">
		                        <h4 class="page-title float-left">Agent List</h4>
		                        <div class="clearfix"></div>
		                    </div>
		                </div>
		            </div>
		            
		            <div class="row">
                       	<div class="col-12">
                       		<div class="card-box">
                       			<div class="row">
                       				<div class="col-md-4 col-sm-4 col-xs-4">
										<form action="" method="post">
											<div class="form-row">
												<div class="col-sm-8">
													<div id="" class="pull-left" style="cursor: pointer;">
														<label class="sr-only" for="filterBy">Filter By:</label>
													   	<input id="reportrange" name="toDate" class="form-control" readonly="readonly"/>
													</div>
												</div>
												<div class="col-sm-3">
													<button class="btn btn-primary" onclick="getUserListWithDateFilter(0);" type="button">Filter</button>
												</div>
											</div>
										</form>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-4">
										<div class="row">
		                                    <div class="form-group">
			                                    <input id="username" name="userName" class="form-control" maxlength="100"  placeholder="Enter Username"/>
			                                </div>
			                                <div class="form-group">    
			                                    <button class="btn btn-primary" onclick="" type="button" >Search</button>
		                                   	</div>	
		                                   	<span id="err" style="color: red;position: fixed;margin-top: 27px;"></span>
		                                </div>
									</div>
                       			</div>
                       		</div>
                       	</div>
                   	</div>
		            
		            <div class="row">
		            	<div class="col-12">
		            		<div class="card-box">
		            			<div class="col-md-12">
		            				<div class="tabble">
		            					<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
		            					<thead>
	                                        <tr>
	                                            <th>Sl No</th>
                                                <th>User details</th>
                                                <th>User Type</th>
                                                <th>Registered Date</th>
                                                <th>Mobile Token</th>
                                                <th>Status</th>
                                                <th>No of user</th>
	                                        </tr>
	                                    </thead>
	                                    <tbody id="genericUserList">
	                                    </tbody>
		            				</table>
		            				<nav>
										<ul class="pagination" id="paginationn"></ul>
									</nav>
	            				</div>
		            			</div>
		            		</div>
		            	</div>
		            </div>
				</div>
			</div>
		</div>
	</div>
	
	
	<script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
    <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

    <!-- App js -->
    <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
    <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
    <script src="${pageContext.request.contextPath}/resources/corporate/assets/js/all.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
	
	
	<script type="text/javascript">
		var contextPath="${pageContext.request.contextPath}";
		var role = "${role}";
		var id = "${id}";
		$(document).ready(function(){
    		getUserList(0);
    	});
		$(function() {
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		    function cb(start, end) {
		        $('#reportrange').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
		    }
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'DD/MM/YYYY'
		        },
		        dateLimit: {
		            "days": 60
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		    cb(start, end);
		});
		
		function getUserList(page) {
    		var trHtml='';
    		var data;
    		$.ajax({
    			type : "POST",
	    		url:contextPath+"/Master/GenericUserList/All",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"page" : page,
	   				"size" : 10,
	   				"role" : "ROLE_DISTRIBUTOR,ROLE_AUTHENTICATED",
	   				"id" : id,
	   			}),
	   			success : function(response) {
					$('#genericUserList').empty();
					if (response.details.content.length > 0) {
						for (var i = 0; i < response.details.content.length; i++) {
							data = response.details.content[i].agent;
							trHtml = '<tr>';
	   						trHtml = trHtml + '<td>' +(i+1)+ '</td>';
	   						var name = '';
	   						if(data.userDetail.lastName != null) {
	   							name = data.userDetail.firstName + ' ' + data.userDetail.lastName;
	   						} else {
	   							name = data.userDetail.firstName;
	   						}
	   						trHtml = trHtml + '<td> Name: ' + name + '<br>MobileNo:' + data.userDetail.contactNo + '<br>Email : <a href='+contextPath+'/Master/UserDetails?username='+ data.userDetail.email+'>' 
								+ data.userDetail.email + '</a><br>Account type :' + data.accountDetail.accountType.code + '</td>';
   							trHtml = trHtml + '<td>Agent</td>';
	   						
   							trHtml = trHtml + '<td>' + formatDate(data.created) + '</td>';
   							var mobileToken = '';
   							if (data.mobileToken != null) {
   								trHtml = trHtml + '<td>' + data.mobileToken + '</td>';
   							} else {
   								trHtml = trHtml + '<td>NA</td>';
   							}
   							trHtml = trHtml + '<td>' + data.mobileStatus + '</td>';
   							if(data.noOfUser>0){
   								trHtml = trHtml + '<td><a href='+contextPath+'/Master/AgentUserList/'+data.id+'>' + data.noOfUser + '</a></td>';
							} else {
								trHtml = trHtml + '<td>' + data.noOfUser + '</td>';
							}
	   						trHtml = trHtml + '</tr>';
	   						$('#genericUserList').append(trHtml);
						}
					} else {
						trHtml = '<tr>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td>No data found</td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '</tr>';
						$('#genericUserList').append(trHtml);
					}
					$(function () {
   	   					console.log("inside funt...and total pages:"+response.totalPage);
   	   					$('#paginationn').twbsPagination({
    					 	totalPages: response.totalPage,
    					 	visiblePages: 10,
    		         	 	onPageClick: function (event, page) {
    		         	 		getUserList(page-1);
    			         	}
			 			});
	   				});
	   			}
    		});
    	}
		
		function getUserListWithDateFilter(page) {
    		var trHtml='';
    		var data;
    		var dateRange = $('#reportrange').val();
    		$.ajax({
    			type : "POST",
	    		url:contextPath+"/Master/GenericUserList/All",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"page" : page,
	   				"size" : 10,
	   				"dateRange" : "" + dateRange + "",
	   				"role" : "ROLE_DISTRIBUTOR,ROLE_AUTHENTICATED",
	   				"id" : id,
	   			}),
	   			success : function(response) {
					$('#genericUserList').empty();
					if (response.details.content.length > 0) {
						for (var i = 0; i < response.details.content.length; i++) {
							data = response.details.content[i].agent;
							trHtml = '<tr>';
	   						trHtml = trHtml + '<td>' +(i+1)+ '</td>';
	   						var name = '';
	   						if(data.userDetail.lastName != null) {
	   							name = data.userDetail.firstName + ' ' + data.userDetail.lastName;
	   						} else {
	   							name = data.userDetail.firstName;
	   						}
	   						trHtml = trHtml + '<td> Name: ' + name + '<br>MobileNo:' + data.userDetail.contactNo + '<br>Email : <a href='+contextPath+'/Master/UserDetails?username='+ data.userDetail.email+'>' 
								+ data.userDetail.email + '</a><br>Account type :' + data.accountDetail.accountType.code + '</td>';
   							trHtml = trHtml + '<td>Agent</td>';
   							trHtml = trHtml + '<td>' + formatDate(data.created) + '</td>';
   							var mobileToken = '';
   							if (data.mobileToken != null) {
   								trHtml = trHtml + '<td>' + data.mobileToken + '</td>';
   							} else {
   								trHtml = trHtml + '<td>NA</td>';
   							}
   							trHtml = trHtml + '<td>' + data.mobileStatus + '</td>';
   							if(data.noOfUser>0){
   								trHtml = trHtml + '<td><a href='+contextPath+'/Master/AgentUserList/'+data.id+'>' + data.noOfUser + '</a></td>';
							} else {
								trHtml = trHtml + '<td>' + data.noOfUser + '</td>';
							}
	   						trHtml = trHtml + '</tr>';
	   						$('#genericUserList').append(trHtml);
						}
					} else {
						trHtml = '<tr>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td>No data found</td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '</tr>';
						$('#genericUserList').append(trHtml);
					}
					$(function () {
   	   					console.log("inside funt...and total pages:"+response.totalPage);
   	   					$('#paginationn').twbsPagination({
    					 	totalPages: response.totalPage,
    					 	visiblePages: 10,
    		         	 	onPageClick: function (event, page) {
    		         	 		getUserListWithDateFilter(page-1);
    			         	}
			 			});
	   				});
	   			}
    		});
    	}
		
		function formatDate(d){
        	d=new Date(d);
        	var month = d.getMonth();
        	var day = d.getDate();
        	month = month + 1;
        	month = month + "";
        	if (month.length == 1)
        	{
        	month = "0" + month;
        	}
        	day = day + "";
        	if (day.length == 1)
        	{
        	day = "0" + day;
        	}
        	return d.getFullYear()+ '-' +month + '-' + day ;
       	}
	</script>
</body>
</html>
<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Admin |Settlement Report</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png">

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
 <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script>
    
    <script type="text/javascript">
    var context_path="${pageContext.request.contextPath}";
    </script>

    </head>


    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
          <jsp:include page="/WEB-INF/jsp/digiosk/generic/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/digiosk/generic/LeftMenu.jsp" />
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
          
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                    <div class="row">
		                <div class="col-12">
		                    <div class="page-title-box">
		                        <h4 class="page-title float-left">Settlement Report</h4>
		                        <div class="clearfix"></div>
		                    </div>
		                </div>
		            </div>
					<div class="row">
						<div class="col-12" style="margin-top: 2%;">
							<div class="card-box table-responsive">
								<form
									action="${pageContext.request.contextPath}/Master/SettlementReport"
									method="post">
									<div class="form-row">
										<div class="col-sm-3">
											<div id="" class="pull-left" style="cursor: pointer;">
												<label>Select Date Range*</label> <input
													id="reportrange"  class="form-control"
													readonly="readonly" /> <input type="hidden"
													id="reportrangetime" name="dateRange" value=""  class="form-control" />
											</div>
										</div>
										<div class="col-sm-1" style="margin-top:3%">
											<button class="btn btn-primary" type="submit">Filter</button>
										</div>
										<div class="col-sm-2">
											<div class="form-group">
												<label>Select Distributor*</label> <select id="userType"
													class="form-control">
													<option value="Dist1">Dist1</option>
													<option value=" Distributor2">Distributor2</option>
													<option value="Distributor3">Distributor3</option>
												</select>
											</div>
										</div>
										<div class="col-sm-2">
											<div class="form-group">
												<label>Select Agent*</label> <select id="userType"
													class="form-control">
													<option value="Agent1">Agent1</option>
													<option value="Agent2">Agent2</option>
													<option value="Agent3">Agent3</option>
													<option value="Agent">Agent</option>
												</select>
											</div>
										</div>
										
											<div class="col-sm-2">
											<div class="form-group">
												<label>Service*</label> <select id="userType"
													class="form-control">
													<option value="Load Money">Load Money</option>
													<option value="RechargeAndBillPay">Bill Payment</option>
												</select>
											</div>
										</div>
										
										<div class="col-sm-1" style="margin-top: 3%">
											<button class="btn btn-primary" type="submit">Search</button>
										</div>
									</div>
									
								</form>
								<c:choose>
							<c:when test="${not empty sucMsg}">
								<center style="color: green;">${sucMsg}</center>
							</c:when>
							<c:when test="${not empty errorMsg}">
								<center style="color: red;">${errorMsg}</center>
							</c:when>
						</c:choose>
								<table id="datatable"
									class="table table-striped table-bordered nowrap"
									cellspacing="0" width="100%" style="overflow-x: scroll;">
									<thead>
										<tr>
											<th>S. No</th>
											<th>Transaction&nbsp;Date</th>
											<th>Transaction&nbsp;Id</th>
											<th>Transaction&nbsp;Type</th>
											<th>Transaction&nbsp;Amount</th>
											<th>Debit</th>
											<th>Service&nbsp;Name</th>
											<th>Commission&nbsp;Amount</th>
											<th>Description</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${transactions}" var="list"
											varStatus="loopCounter">
											<tr>
												<td>${loopCounter.count}</td>
												<td>${list.transactionDate}</td>
												<td>${list.transactionId}</td>
												<td>${list.transactionType}</td>
												<td>${list.transactionAmount}</td>
												<td>${list.debit}</td>
												<td>${list.serviceName}</td>
												<td>${list.commissionAmt}</td>
												<td>${list.description}</td>
												<td>${list.status}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2018 © Copyright iMoney.
                </footer>

            </div>

            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        
<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
        <!-- Required datatable js -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jszip.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/pdfmake.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/vfs_fonts.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.html5.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.print.min.js"></script>
        <!-- Responsive examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>


<script type="text/javascript">
            $(document).ready(function() {
            	   $(document).ready(function() {
                       var table = $('#datatable').DataTable( {
                           lengthChange: false,
                           buttons: [ 'excel', 'pdf', 'csv' ]
                       } );
                    
                       table.buttons().container()
                           .appendTo( '#datatable_wrapper .col-md-6:eq(0)' );
                   } );
            } );

        </script>
        
         <script>
		$(function() {
		
			var start = '${startDate}';
			var end = '${endDate}';
			
			if(start == null || start == '' || end == null || end ==''){
				 start = moment().subtract(29, 'days');
				 end = moment();
			}
		
			
			
		    function cb(start, end) {
		    	var startTime = new Date(start).getTime();
		    	var endTime = new Date(end).getTime();
		    	console.log(startTime);
		    	 $('#reportrangetime').val(startTime + '-' + endTime);
		        $('#reportrange').html(start.format('MM-dd-yyyy') + ' - ' + end.format('MM-dd-yyyy'));
		    }
		
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'YYYY-MM-DD'
		        },
		        dateLimit: {
		            "days": 60
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		
		    cb(start, end);
		    
		});
		</script>
        

    </body>
</html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
   response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
   response.setDateHeader("Expires", 0);
   response.setHeader("Pragma", "no-cache");
%>
<!doctype html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Master | Change Password</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/img/favicon.png">

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
         <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
		
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
		
		<style>
 			.select2-container .select2-selection--multiple { 
 				width: 166%; 
 			}
 			.error {
 				color: red;
 			}
 			.success {
 				color: green;
 			}
		</style>
		
		
 <!-- <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script> -->
    
    <script type="text/javascript">
    	var context_path="${pageContext.request.contextPath}";
    </script>
    </head>
    <body>
        <!-- Begin page -->
        <div id="wrapper">
            <!-- Top Bar Start -->
            <jsp:include page="/WEB-INF/jsp/digiosk/generic/Header.jsp" />
		    <jsp:include page="/WEB-INF/jsp/digiosk/generic/LeftMenu.jsp" />
            <div class="content-page">
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Change Password</h4>
                                    <div class="clearfix"></div>
                                    <span id="stst" style="margin-left: 40%; color: #3c86d8fa;">${statusUpdt}</span>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                        <div class="row">
                            <div class="col-12">
                                <div class="card-box table-responsive">
                                    <div class="col-4 offset-md-4">
										<div class="service_wrp">
											<c:choose>
												<c:when test="${not empty ERRORMSG}">
													<label class="error">${ERRORMSG}</label>
												</c:when>
												<c:when test="${not empty SUCCESSMSG}">
													<label class="success">${SUCCESSMSG}</label>
												</c:when>
											</c:choose>
											<form action="${pageContext.request.contextPath}/Master/ChangePassword" id="changePassword" method="post">
												<div class="form-group">
													<label>Current Password:</label>
													<input type="password" placeholder="Enter your password" class="form-control" id="currentPassword" name="currentPassword" maxlength="12"/>													
													<span id="error_currentPassword" class="error"></span>
												</div>
												<div class="form-group">
													<label>New Password:</label>
													<input type="password" placeholder="Enter your new password" class="form-control" id="newPassword" name="newPassword" maxlength="12"/>													
													<span id="error_newPassword" class="error"></span>
												</div>
												<div class="form-group">
													<label>Confirm Password:</label>
													<input type="password" placeholder="Enter your password" class="form-control" id="confirmPassword" name="confirmPassword" maxlength="12"/>													
													<span id="error_confirmPassword" class="error"></span>
												</div>
												<center><button class="btn btn-info" type="button" onclick="formSubmit();">Submit</button></center>
											</form>
										</div>                                    	
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end row -->
                    </div> <!-- container -->
                </div> <!-- content -->
                <footer class="footer text-right">
                    2018 © Copyright IMoney.
                </footer>
            </div>
        </div>
        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
        <script>
	        $(".js-example-basic-multiple").select2({
	        	  theme: "bootstrap",
	        	  width: "resolve"
	        });
	        function formSubmit() {
	        	var valid = true; 
	        	var currentPassword = $('#currentPassword').val();
	        	var newPassword = $('#newPassword').val();
	        	var confirmPassword = $('#confirmPassword').val();
	        	if (currentPassword.length <= 0) {
	        		valid = false;
	        		$('#error_currentPassword').html('Please enter current password');
	        	}
	        	if (newPassword.length <= 0) {
	        		valid = false;
	        		$('#error_newPassword').html('Please enter new password');
	        	}
	        	if (confirmPassword.length <= 0) {
	        		valid = false;
	        		$('#error_confirmPassword').html('Please enter confirm password');
	        	}
	        	if(newPassword != confirmPassword){
	        		valid = false;
	        		$('#error_confirmPassword').html('New password and confirm password does not match');
	        	}
	        	if (valid) {
	        		$('#changePassword').submit();
	        	}
	        	var timeout = setTimeout(function(){
					$('#error_currentPassword').html("");
					$('#error_newPassword').html("");
					$('#error_confirmPassword').html("");
			    }, 3000);
	        }
        </script>

    </body>
    
</html>
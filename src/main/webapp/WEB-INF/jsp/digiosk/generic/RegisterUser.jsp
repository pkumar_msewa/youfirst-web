<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
	response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
	response.setDateHeader("Expires", 0);
	response.setHeader("Pragma", "no-cache");
%>
<html>
<head>
<meta charset="utf-8" />
<title>Master | Add User</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- App favicon -->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png">

<!-- Responsive datatable examples -->
<link
	href="{pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css"
	rel="stylesheet" type="text/css" />

<!-- App css -->
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css"
	rel="stylesheet" type="text/css" />

<style>
fieldset {
	min-width: 0;
	padding: 10px 30px;
	margin: 0;
	border: 1px solid #efefef;
}

fieldset legend {
	width: auto;
	font-size: 1.3rem;
}

.fileUpload {
	position: relative;
	overflow: hidden;
	height: 43px;
	margin-top: 0;
}

.error {
	color: red;
}

.fileUpload input.uploadlogo {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	font-size: 20px;
	cursor: pointer;
	opacity: 0;
	filter: alpha(opacity = 0);
	width: 100%;
	height: 42px;
}

/*Chrome fix*/
input::-webkit-file-upload-button {
	cursor: pointer !important;
	height: 42px;
	width: 100%;
}

.blue-btn:hover, .blue-btn:active, .blue-btn:focus, .blue-btn {
	background: transparent;
	border: solid 1px #27a9e0;
	border-radius: 3px;
	color: #27a9e0;
	font-size: 14px;
	margin-bottom: 20px;
	outline: none !important;
	padding: 10px 20px;
}

.error {
	color: red; 
	font-size: 14px;
}

.success {
	color: green; 
	font-size: 16px;
}
</style>

</head>


<body>

	<!-- Begin page -->
	<div id="wrapper">
		<!-- Top Bar Start -->
		<jsp:include page="/WEB-INF/jsp/digiosk/generic/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/digiosk/generic/LeftMenu.jsp" />
	
	<!-- Left Sidebar End -->
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12">
						<div class="page-title-box">
							<h4 class="page-title float-left">
								<c:choose>
									<c:when test="${role eq 'ROLE_SUPER_ADMIN,ROLE_AUTHENTICATED' || role eq 'ROLE_ADMINISTRATOR,ROLE_AUTHENTICATED'}">
										Add Super Distributor
									</c:when>
									<c:when test="${role eq 'ROLE_SUPER_DISTRIBUTOR,ROLE_AUTHENTICATED'}">
										Add Distributor
									</c:when>
									<c:when test="${role eq 'ROLE_DISTRIBUTOR,ROLE_AUTHENTICATED'}">
										Add Agent
									</c:when>
								</c:choose>
							</h4>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<!-- end row -->
				<c:choose>
					<c:when test="${not empty ERRORMSG}">
						<strong><center id="errormsg" class="error">${ERRORMSG}</center></strong>
					</c:when>
					<c:when test="${not empty SUCCESSMSG}">
						<strong><center id="successmsg" class="success">${SUCCESSMSG}</center></strong>
					</c:when>
				</c:choose>
				<div class="row">
					<div class="col-12">
						<div class="card-box">
							<form:form action="${pageContext.request.contextPath}/Master/RegisterUser" modelAttribute="registerUser" method="post" id="formId" enctype="multipart/form-data">
								<div class="row">
									<div class="col">
										<fieldset>
											<c:choose>
												<c:when test="${role eq 'ROLE_SUPER_ADMIN,ROLE_AUTHENTICATED' || role eq 'ROLE_ADMINISTRATOR,ROLE_AUTHENTICATED'}">
													<legend>Super Distributor Details</legend>
												</c:when>
												<c:when test="${role eq 'ROLE_SUPER_DISTRIBUTOR,ROLE_AUTHENTICATED'}">
													<legend>Distributor Details</legend>
												</c:when>
												<c:when test="${role eq 'ROLE_DISTRIBUTOR,ROLE_AUTHENTICATED'}">
													<legend>Agent Details</legend>
												</c:when>
											</c:choose>
											<div class="form-group">
												<label for="name">Full Name</label> 
												<form:input type="text" path="name" placeholder="Enter Name" id="f_Name" class="form-control" maxlength="25"
													onkeypress="return isAlphKey(event);" />
												<span class="error" id="error_fullName"></span>
											</div>
											<div class="form-group">
												<label for="mobile">Mobile</label> 
												<form:input type="text" path="contactNo" placeholder="Enter Mobile" id="mobile" class="form-control" maxlength="10"
													onkeypress="return isNumberKey(event);" />
												<span class="error" id="error_mobile"></span>
											</div>
											<div class="form-group">
												<label for="email">Email</label> 
												<form:input type="email" path="email" placeholder="Enter Email" id="email" class="form-control" />
												<span class="error" id="error_email"></span>
											</div>
											<c:choose>
												<c:when test="${role eq 'ROLE_SUPER_ADMIN,ROLE_AUTHENTICATED' || role eq 'ROLE_ADMINISTRATOR,ROLE_AUTHENTICATED'}">
													<div class="form-group">
		                                                <label for="ifsc">Choose User Type</label>
		                                                <form:input type="text" path="userType" class="form-control" onkeypress="return isAlphNumberKey(event);" value="Super Distributor" readonly="true" />
		                                                <span class="error" id="error_userType"></span>
		                                            </div>
												</c:when>
												<c:when test="${role eq 'ROLE_SUPER_DISTRIBUTOR,ROLE_AUTHENTICATED'}">
													<div class="form-group">
		                                                <label for="ifsc">Choose User Type</label>
		                                                <form:input type="text" path="userType" class="form-control" onkeypress="return isAlphNumberKey(event);" value="Distributor" readonly="true" />
		                                                <span class="error" id="error_userType"></span>
		                                            </div>
												</c:when>
												<c:when test="${role eq 'ROLE_DISTRIBUTOR,ROLE_AUTHENTICATED'}">
													<div class="form-group">
		                                                <label for="ifsc">Choose User Type</label>
		                                                <form:input type="text" path="userType" class="form-control" onkeypress="return isAlphNumberKey(event);" value="Agent" readonly="true" />
		                                                <span class="error" id="error_userType"></span>
		                                            </div>
												</c:when>
											</c:choose>
											<div class="form-group">
												<label for="acc">Account Number</label> 
												<form:input type="text" path="bankAccountNo" placeholder="Enter Account Number" id="account_No" class="form-control" maxlength="16"
													onkeypress="return isNumberKey(event);" />
												<span class="error" id="error_bankAccNo"></span>
											</div>
											<div class="form-group">
												<label for="ifsc">IFSC Code</label> 
												<form:input type="text" path="ifscCode" placeholder="Enter IFSC Code" id="ifsc_Code" class="form-control"
													onkeypress="return isAlphNumberKey(event);" maxlength="11" />
												<span class="error" id="error_ifscCode"></span>
											</div>
										</fieldset>
									</div>
									<div class="col">
										<fieldset>
											<legend>Upload Documents</legend>
											<div class="form-group">
												<div class="fileUpload blue-btn btn width100">
													<span>Upload Your Aadhaar Card Front Side</span> 
													<form:input type="file" class="uploadlogo" path="aadharImage1" id="aadhar_Image1" />
												</div>
												<span class="error" id="error_aaadharCardImg1"></span>
											</div>
											<div class="form-group">
												<div class="fileUpload blue-btn btn width100">
													<span>Upload Your Aadhaar Card Back Side</span> 
													<form:input type="file" class="uploadlogo" path="aadharImage2" id="aadhar_Image2" />
												</div>
												<span class="error" id="error_aaadharCardImg2"></span>
											</div>
											<div class="form-group">
												<div class="fileUpload blue-btn btn width100">
													<span>Upload Your PAN Card</span> 
													<form:input type="file" class="uploadlogo" path="panCardImage" id="pan_Image" />
												</div>
												<span class="error" id="error_panCardImg"></span>
											</div>
										</fieldset>
									</div>
								</div>
								<center>
									<button type="button" id="addAgent"
										class="btn btn-primary mt-4" onclick="addCorporateAgent()">Submit</button>
								</center>
							</form:form>
						</div>
					</div>
				</div>
				<!-- end row -->

			</div>
			<!-- container -->

		</div>
		<!-- content -->

		<footer class="footer text-right"> 2018 © Copyright MadMove
			Global. </footer>

	</div>


	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->


	</div>
	
	<!-- END wrapper -->



	<!-- jQuery  -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
	<script type="text/javascript"
		src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

	<!-- App js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>



	<script>
                // You can modify the upload files to pdf's, docs etc
            //Currently it will upload only images

            $(document).ready(function($) {

              // Upload btn on change call function
              $(".uploadlogo").change(function() {
                var filename = readURL(this);
                $(this).parent().children('span').html(filename);
              });

              // Read File and return value  
              function readURL(input) {
                var url = input.value;
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                if (input.files && input.files[0] && (
                  ext == "png" || ext == "jpeg" || ext == "jpg" || ext == "gif"
                  )) {
                  var path = $(input).val();
                  var filename = path.replace(/^.*\\/, "");
                  // $('.fileUpload span').html('Uploaded Proof : ' + filename);
                  return "Uploaded file : "+filename;
                } else {
                  $(input).val("");
                  return "Only image formats are allowed!";
                }
              }
              // Upload btn end

            });
            </script>





	<script type="text/javascript">
           function addCorporateAgent(){
        		var valid=true;
        		var ifscPattern= "[A-Z]{4}[0-9]{7}$";
        		var panPattern= "[A-Z]{5}[0-9]{4}[A-Za-z]{1}";
        		var mobilePattern= "[7-9]{1}[0-9]{9}";
        		var fullName=$('#f_Name').val();
        		var bankAccNo=$('#account_No').val();
        		var mobile=$("#mobile").val();
        		var email=$('#email').val();
        		var ifscCode=$('#ifsc_Code').val();
        		var aadharCardImg1=$('#aadhar_Image1').val();
        		var aadharCardImg2=$('#aadhar_Image2').val();
        		var panCardImg=$('#pan_Image').val();
        		var emailPattern = "^[a-zA-Z0-9]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        		
        		console.log("Pancared image"+panCardImg);
        		
        		if(fullName=='') {
        			$("#error_fullName").html("Please enter full name");
        			valid = false;
        		}
        		if(bankAccNo=='') {
        			$("#error_bankAccNo").html("Please enter bank account number");
        			valid = false;
        		} else if (bankAccNo.length < 11) {
        			$("#error_bankAccNo").html("Please enter valid bank account number");
        			valid = false;
        		}
        		if(mobile=='') {
        			console.log("blank");
        			$("#error_mobile").html("Please enter contact no");
        			valid = false;
        		} else if (mobile.length != 10 ) {
        			console.log("10 digit");
        			$("#error_mobile").html("Please enter 10 digit contact no");
        			valid = false;
        		} 
        		if (!mobile.match(mobilePattern)) {
        			console.log("invalid contact");
        			$("#error_mobile").html("Please enter 10 digit contact no");
        			valid = false;
        		}
        		if(email=='') {
        			$("#error_email").html("Please enter email id");
        			valid = false;
        		}
        		
        		if(!email.match(emailPattern)) {
          			console.log("invalid pattern");
          			$('#error_email').html('Please enter your valid email id');
          			valid = false;
          		}
        		
        		if(ifscCode=='') {
        			$("#error_ifscCode").html("Please enter bank ifsc code");
        			valid = false;
        		}
        		if(!ifscCode.match(ifscPattern)) {
        			$("#error_ifscCode").html("Please enter valid IFSC Code");
        			valid = false;	
        		}
        		
        		if (ifscCode.length >11) {
        			$("#error_ifscCode").html("Please enter valid IFSC Code");
        			valid = false;
        		}
        		if(panCardImg=='') {
        			$("#error_panCardImg").html("Please attach pan card image");
        			valid = false;
        		}
        		if (panCardImg!= "") {
        		    if (!(panCardImg.split(".")[1].toUpperCase() == "PNG"
        		      || panCardImg.split(".")[1].toUpperCase() == "JPEG" || panCardImg.split(".")[1].toUpperCase() == "JPG")) {
        		     $("#error_panCardImg").html("File with " + panCardImg.split(".")[1]
        		     + " is invalid. Upload a valid file");
        		     valid = false;
        		    }
        		   }
        		if (aadharCardImg1!= "") {
        		    if (!(aadharCardImg1.split(".")[1].toUpperCase() == "PNG"
        		      || aadharCardImg1.split(".")[1].toUpperCase() == "JPEG" || aadharCardImg1.split(".")[1].toUpperCase() == "JPG")) {
        		     $("#error_aaadharCardImg1").html("File with " + aadharCardImg1.split(".")[1]
        		     + " is invalid. Upload a valid file");
        		     valid = false;
        		    }
        		   }
        		if (aadharCardImg2!= "") {
        		    if (!(aadharCardImg2.split(".")[1].toUpperCase() == "PNG"
        		      || aadharCardImg2.split(".")[1].toUpperCase() == "JPEG" || aadharCardImg2.split(".")[1].toUpperCase() == "JPG")) {
        		     $("#error_aaadharCardImg1").html("File with " + aadharCardImg2.split(".")[1]
        		     + " is invalid. Upload a valid file");
        		     valid = false;
        		    }
        		   }
        		if(aadharCardImg1=='') {
        			$("#error_aaadharCardImg1").html("Please attach aadhaar card front image");
        			valid = false;
        		}
        		if(aadharCardImg2=='') {
        			$("#error_aaadharCardImg2").html("Please attach aadhaar card back image");
        			valid = false;
        		}
        		if (valid) {
        			$('#formId').submit();
        			$("#addAgent").addClass("disabled");
//         			$("#addAgent").html(spinnerUrl);
        		}
        		var timeout = setTimeout(function(){
        			$("#error_fullName").html("");
        			$("#error_email").html("");
        			$("#error_mobile").html("");
        			$("#error_bankAccNo").html("");
        			$("#error_ifscCode").html("");
        			$("#error_aaadharCardImg1").html("");
        			$("#error_aaadharCardImg2").html("");
        			$("#error_panCardImg").html("");
        		}, 3000);
        	}

        	</script>

	<script type="text/javascript">
		function isAlphNumberKey(evt){
		    var k = (evt.which) ? evt.which : evt.keyCode
		    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
		}
		function isAlphKey(evt){
		    var charCode = (evt.which) ? evt.which : evt.keyCode
		    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
		}
		function isNumberKey(evt){
		    var charCode = (evt.which) ? evt.which : evt.keyCode
		    return !(charCode > 31 && (charCode < 48 || charCode > 57));
		}
	</script>

</body>
</html>
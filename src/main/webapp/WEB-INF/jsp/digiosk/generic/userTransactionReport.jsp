<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
   response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
   response.setDateHeader("Expires", 0);
   response.setHeader("Pragma", "no-cache");
%>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Master | Customer</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png">
        
        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/custom.css" rel="stylesheet" type="text/css" />

        <!-- DataTables -->
        <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>

        

	<style>
	/* The Modal (background) */
	.modal {
	    display: none; /* Hidden by default */
	    position: fixed; /* Stay in place */
	    z-index: 1; /* Sit on top */
	    padding-top: 70px; /* Location of the box */
	    
	    left: 0;
	    top: 0;
	    width: 100%; /* Full width */
	    height: 100%; /* Full height */
	    overflow: auto; /* Enable scroll if needed */
	    background-color: rgb(0,0,0); /* Fallback color */
	    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}
	
	/* Modal Content */
	.modal-content {
	    background-color: #fefefe;
	    margin: auto;
	    padding: 20px;
	    padding-left: 60px;
	    
	    border: 1px solid #888;
	    width: 80%;
	}
	
	/* The Close Button */
	.close {
	    color: #aaaaaa;
	    float: right;
	    font-size: 28px;
	    font-weight: bold;
	}
	
	.close:hover,
	.close:focus {
	    color: #000;
	    text-decoration: none;
	    cursor: pointer;
	}
	
	.modal-backdrop.show{
	z-index:0;
	}
	
	.sidenav {
  height: 89%;
  margin-top: 5%;
  width: 0;
  position: fixed;
  z-index: 4;
  top: 0;
  right: 0;
  background-color: white;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 60px;
}

.sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}

.sidenav a:hover {
  color: #f1f1f1;
}

.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}

#overlay {
	position: fixed; /* Sit on top of the page content */
	display: none; /* Hidden by default */
	width: 100%; /* Full width (cover the whole page) */
	height: 100%; /* Full height (cover the whole page) */
	top: 0; 
	left: 0;
	right: 0;
	bottom: 0;
	background-color: rgba(0,0,0,0.5); /* Black background with opacity */
	z-index: 2; /* Specify a stack order in case you're using a different order for other elements */
}
	
	
	</style>
</head>
<body>
	<div id="overlay"></div>
	<div id="wrapper">
		<jsp:include page="/WEB-INF/jsp/digiosk/generic/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/digiosk/generic/LeftMenu.jsp" />
		<div class="content-page">
			<div class="content">
				<div class="container-fluid" id="closeNavId">
					<div class="row">
		                <div class="col-12">
		                    <div class="page-title-box">
		                        <h4 class="page-title float-left">Transaction Report</h4>
		                        <div class="clearfix"></div>
		                    </div>
		                </div>
		            </div>
		            <input type ="hidden" id="hiddenId" value="${value}">
		            <div class="row">
		            	<div class="col-12">
		            		<div class="card-box">
		            			<div class="col-md-12">
		            				<div class="tabble" style="overflow-x: scroll;">
		            					<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
		            					<thead>
	                                          <tr><th>S.No</th>
													<th>Email	</th>
													<th>Mobile</th>
													<th>Card&nbsp;Proxy&nbsp;Number</th>
													<th>Billing&nbsp;Amount&nbsp;Debit</th>
													<th>Billing&nbsp;Amount&nbsp;Credit</th>
													<th>Debit&nbsp;Credit&nbsp;Indicator</th>
													<th>Debit&nbsp;Credit&nbsp;Text</th>
													<th>Merchant&nbsp;Name</th>
													<th>MCC</th>
													<th>Terminal&nbsp;Id</th>
													<th>Description</th>
													<th>Transaction&nbsp;Date</th>
													<th>Card&nbsp;Balance</th>
													<th>Transaction&nbsp;Amount</th>
													<th>Transaction&nbsp;Ref&nbsp;No</th>
													<th>Transaction&nbsp;Status</th>
													<th>Transaction&nbsp;Type</th>
													<th>Ref&nbsp;Table</th>
												</tr>
	                                    </thead>
	                                    <tbody id="genericUserList" style="height: 100px;overflow-y: auto;">
	                                    </tbody>
		            				</table>
		            				<nav>
										<ul class="pagination" id="paginationn"></ul>
									</nav>
	            				</div>
		            			</div>
		            		</div>
		            	</div>
		            </div>
				</div>
			</div>
		</div>
	</div>



	<div id="mySidenav" class="sidenav">
	<div style="margin-top:-6%;" >
	<a href="javascript:void(0)" class="closebtn" onclick="closeNav()" style="margin-top:-5%; color:white;">&times;</a>
	</div>
		<div class="row" id="upperBlock"
			style="background-color: rgb(46, 98, 177); max-height: 300px; height: 220px;">
			<div class="layout-column">
				<span class="subtitle ng-scope"
					style="color: white; margin-left: 20px; font-size: 20px;">First
					Name:</span> <span
					style="color: white; margin-left: 20px; font-size: 20px;"
					id="fnameId"></span> <br /> <span class="subtitle ng-scope"
					style="color: white; margin-left: 20px; font-size: 20px;">Last
					Name:</span> <span
					style="color: white; margin-left: 20px; font-size: 20px;"
					id="lnameId"></span> <br /> <span class="subtitle ng-scope"
					style="color: white; margin-left: 20px; font-size: 20px;">Email:</span>
				<span style="color: white; margin-left: 20px; font-size: 20px;"
					id="eamilId"></span> <br /> <span
					class="subtitle ng-scope"
					style="color: white; margin-left: 20px; font-size: 20px;">Kyc
					Status:</span> <span
					style="color: white; margin-left: 20px; font-size: 20px;"
					id="acctTypeId"></span>
			</div>

		</div>

		<div class="row"
			style="background-color: white; max-height: 300px; height: 300px">
			<div class="layout-column">
				<div id="hrefId">
					<a href="${pageContext.request.contextPath}/Master/getTransaction/"><i
						class="fa fa-list">&nbsp;Transaction</i></a>
				</div>

				<!-- <span class="subtitle ng-scope"
					style="color: black; margin-left: 20px; font-size: 20px;">
					Wallet Balance:</span> <span
					style="color: black; margin-left: 20px; font-size: 20px;"
					id="balId"></span> <br /> <span class="subtitle ng-scope"
					style="color: black; margin-left: 20px; font-size: 20px;">
					Card Number:</span> <span
					style="color: black; margin-left: 20px; font-size: 20px;"
					id="cardNoId"></span> <br /> <span
					class="subtitle ng-scope"
					style="color: black; margin-left: 20px; font-size: 20px;">
					Mobile:</span><span
					style="color: black; margin-left: 20px; font-size: 20px;"
					id="mobileNoId"></span> <br />
					<span
					class="subtitle ng-scope"
					style="color: black; margin-left: 20px; font-size: 20px;">
					Card Holder:</span><span
					style="color: black; margin-left: 20px; font-size: 20px;"
					id="cardHolderId"></span> -->
			</div>
		</div>


	</div>



	<script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
    <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

    <!-- App js -->
    <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
    <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
    <script src="${pageContext.request.contextPath}/resources/corporate/assets/js/all.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
	
	<script type="text/javascript">
		var contextPath="${pageContext.request.contextPath}";
		var value = "${value}";
		if(value == null){
			value='All';
		}
		var role = "${role}";
		$(document).ready(function(){
    		getTransactionList(0);
    	});
		
		$(function() {
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		    function cb(start, end) {
		        $('#reportrange').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
		    }
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'DD/MM/YYYY'
		        },
		        dateLimit: {
		            "days": 60
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		    cb(start, end);
		});
		
		function getTransactionList(page) {
    		var trHtml='';
    		var data;
    		var userId = $('#hiddenId').val();
    		
    		$.ajax({
    			type : "POST",
	    		url:contextPath+"/Master/getTransaction/"+userId,
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"page" : page,
	   				"size" : 10,
	   			}),
	   			success : function(response) {
	   				console.log("response: "+response);
	   				console.log("details: "+response.details);
	   				console.log("length: "+response.details.length);
					$('#genericUserList').empty();
					if (response.details !=null && response.details !='null' &&  response.details.length > 0) {
						for (var i = 0; i < response.details.length; i++) {
							trHtml ='';
							var data = response.details[i];
							trHtml = trHtml + '<tr><td>'+(i+1)+'</td>';
							trHtml = trHtml + '<td>'+data.email+'</td>';
							trHtml = trHtml + '<td>'+data.mobile+'</td>';
							trHtml = trHtml + '<td>'+data.cardProxyNumber+'</td>';
							trHtml = trHtml + '<td>'+data.debitAmount+'</td>';
							trHtml = trHtml + '<td>'+data.creditAmount+'</td>';
							trHtml = trHtml + '<td>'+data.indicator+'</td>';
							trHtml = trHtml + '<td>'+data.indicatorText+'</td>';
							trHtml = trHtml + '<td>'+data.merchantName+'</td>';
							trHtml = trHtml + '<td>'+data.mcc+'</td>';
							trHtml = trHtml + '<td>'+data.terminal+'</td>';
							trHtml = trHtml + '<td>'+data.message+'</td>';
							trHtml = trHtml + '<td>'+data.date+'</td>';
							trHtml = trHtml + '<td>'+data.walletBalance+'</td>';
							trHtml = trHtml + '<td>'+data.txnAmount+'</td>';
							trHtml = trHtml + '<td>'+data.txnRefNo+'</td>';
							trHtml = trHtml + '<td>'+data.status+'</td>';
							trHtml = trHtml + '<td>'+data.txnType+'</td>';
							trHtml = trHtml + '<td>'+data.refTable+'</td>';
							trHtml = trHtml + '</tr>';
							$('#genericUserList').append(trHtml);
						}
						$(function () {
	   	   					$('#paginationn').twbsPagination({
	    					 	totalPages: response.totalPage,
	    					 	visiblePages: 10,
	    		         	 	onPageClick: function (event, page) {
	    		         	 		if($('#paginationId').val() !=1){
	    		         	 			getTransactionList(page-1);
	    		         	 		}else{
					                $('#paginationId').val(2);
	    		         	 		}
	    			         	}
				 			});
		   				});
					} else {
						trHtml = '<tr>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td>No data found</td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '</tr>';
						$('#genericUserList').append(trHtml);
					}
	   			}
    		});
    	}
		
		
		
		function formatDate(d){
        	d=new Date(d);
        	var month = d.getMonth();
        	var day = d.getDate();
        	month = month + 1;
        	month = month + "";
        	if (month.length == 1)
        	{
        	month = "0" + month;
        	}
        	day = day + "";
        	if (day.length == 1)
        	{
        	day = "0" + day;
        	}
        	return d.getFullYear()+ '-' +month + '-' + day ;
       	}
	</script>
	
		<script>
		function openNav(username,firstName,lastName,email,kycStatus , id) {
    		 $('#upperBlock').css('margin-top','-17%');
   		  $('#mySidenav').css('width','350px');
   		  $('#overlay').css('display','block');
   		  
   		  console.log(id);
   		
				$('#fnameId').html(firstName);
				$('#lnameId').html(lastName);
				$('#eamilId').html(email);
				$('#acctTypeId').html(kycStatus);
				$('#hrefId').html('<a href="${pageContext.request.contextPath}/Master/getTransaction/'+id+'"><i class="fa fa-list">&nbsp;Transaction</i></a>');
				
				
				$('#cardNoId').html('');
				$('#mobileNoId').html('');
				$('#cardHolderId').html('');
				$('#balId').html('');

				if (username != null && username != '') {
					$('#tableHeader0').css('display', 'block');
					$
							.ajax({
								type : "POST",
								url : contextPath + "/Master/WalletDetails",
								dataType : "json",
								contentType : "application/json",
								data : JSON.stringify({
									"username" : "" + username + "",
								}),
								success : function(response) {
									if (response != null) {
										if (response.balance != '0') {
											$('#balId').html(
													response.balance + " INR");
										} else {
											$('#balId').html("0.00 INR");
										}if(response.cardNumber !=null && response.cardNumber!=''){
											$('#cardNoId').html(response.cardNumber);	
										}else{
											$('#cardNoId').html('NA');
										}if(response.cardHolder!=null && response.cardHolder!=''){
											$('#cardHolderId').html(response.cardHolder);
										}else{
											$('#cardHolderId').html('NA');
										}
										$('#mobileNoId').html(username);
										;
									}
								}
							});
				}
			}

			function closeNav() {
				/* document.getElementById("mySidenav").style.width = "0"; */
				$('#mySidenav').css('width', '0');
				$('#overlay').css('display', 'none');
				/* document.getElementById('overlay').style.display = 'none'; */
			}
		</script>
</body>
</html>
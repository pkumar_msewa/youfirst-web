<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
    
	<head>
        <meta charset="utf-8" />
        <title>Master | User Details</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
       	<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png">

        <!-- App css -->
       	<link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- Daterangepicker -->
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

        <!-- google fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>


        <style>
            @font-face {
                font-family: "OCRAExtended";
                src: url("${pageContext.request.contextPath}/resources/admin/assets/fonts/OCRAExtended.eot");
                src:
                url("${pageContext.request.contextPath}/resources/admin/assets/fonts/OCRAExtended.woff") format("ttf"), 
                url("${pageContext.request.contextPath}/resources/admin/assets/fonts/OCRAExtended.ttf") format("woff"),
                url("${pageContext.request.contextPath}/resources/admin/assets/fonts/OCRAExtended.svg#filename") format("svg");
            }
            fieldset {
            	min-width: initial;
            	padding: 15px;
            	margin: initial;
            	border: 1px solid #e5e5e5;
            }

            fieldset legend {
            	width: auto;
            	font-size: 1.3rem;
            }

            .artboard {
			  	width: 140px;
			  	position: relative;
			  	margin: 10px 67px 0;
			}

			.switch {
			  	position: relative;
			  	display: inline-block;
			  	width: 95px;
    			height: 42px;
			  	margin: 0 auto;
			}
			.switch input {
			  	display: none;
			}

			.slider {
			  	position: absolute;
			  	cursor: pointer;
			  	top: 0;
			  	left: 0;
			  	right: 0;
			  	bottom: 0;
			  	background-color: white;
			  	transition: 0.4s;
			  	border-radius: 34px;
			  	box-shadow: 0px 0px 15px 0px rgba(0, 0, 0, 0.24);
			}
			.slider .btn {
			  	box-sizing: border-box;
			  	position: absolute;
			  	height: 36px;
			    width: 36px;
			    left: 6px;
			    bottom: 3px;
			  	background-color: #ffdde4;
			  	transition: 0.4s;
			  	border-radius: 50%;
			  	border: 2px solid #9293ee;
			}
			.slider .btn:after, .slider .btn:before {
			  	position: absolute;
			  	content: "";
			  	top: 21px;
			  	left: 8px;
			  	background: #9293ee;
			  	height: 7px;
			  	width: 7px;
			  	border-radius: 50%;
			}
			.slider .btn:before {
			  	top: 4px;
			}
			.slider .btn .mouth {
			  	position: absolute;
			  	top: 12px;
    			left: 19px;
			  	width: 7px;
			  	height: 7px;
			  	background: transparent;
			  	border-radius: 45%;
			  	transform: rotate(45deg);
			  	border-top: 2px solid #9293ee;
			  	border-right: 2px solid #9293ee;
			}
			.slider .btn img {
			  	opacity: 0;
			  	transition: all 0.5s;
			  	position: absolute;
			  	top: 1px;
			    left: 1px;
			    transform: rotate(-10deg);
			    width: 30px;
			    z-index: 999;
			}

			input:focus + .slider {
			  	box-shadow: 0 0 1px #2196F3;
			}
			input:checked + .slider > .btn {
			  	transform: translateX(48px) rotate(180deg);
			}
			input:checked + .slider img {
			 	opacity: 1;
			}
			.gender {
				margin: 0 78px;
				font-size: 14px;
			}
			
        </style>

    </head>


    <body>

        <!-- Begin page -->
        <div id="wrapper">
			<!-- Top Bar Start -->
           	<jsp:include page="/WEB-INF/jsp/digiosk/generic/Header.jsp" />
			<jsp:include page="/WEB-INF/jsp/digiosk/generic/LeftMenu.jsp" />
           	<!-- Top Bar End -->
            <!-- Left Sidebar End -->
			
			<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="row">
                                    	<div class="col-md-6">
	                                    	<h4 class="page-title float-left">User Details</h4>
	                                    </div>
	                                    <div class="col-md-6 text-right">
	                                    	<strong>Mobile Token: </strong>
	                                    	<code>
		                                    	<c:choose>
		                                    		<c:when test="${not empty userData.mobileToken}">${userData.mobileToken}</c:when>
		                                    		<c:otherwise>NA</c:otherwise>
		                                    	</c:choose>
	                                    	</code>
	                                    </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                        	<div class="col-12">
                        		<div class="usrOptions">
                        			<ul class="justify-content-center">
	                        			<li>
	                        				<a href="javascript:void(0);" class="active" id="usrBtn" title="User Info" data-toggle="tab"><img src="${pageContext.request.contextPath}/resources/admin/assets/images/usr_info.png" class="">&nbsp;<span>User Info</span></a>
	                        			</li>
	                        			<%-- <li>
	                        				<a href="javascript:void(0);" class="" id="cardBtn" title="Card Info" data-toggle="tab"><img src="${pageContext.request.contextPath}/resources/admin/assets/images/card_info.png" class="">&nbsp;<span>Card Info</span></a>
	                        			</li> --%>
	                        		</ul>
                        		</div>
                        	</div>
                        </div>

                        <div class="row" id="UsrInfoWrap">
                        	<div class="col-12">
                        		<div class="card-box">
	                        		<div class="card-body">
	                        			<div class="row">
	                        				<div class="col-3">
				                        		<div class="imgDtls-wrp">
				                        			<div class="usrImg">
				                        				<img src="${pageContext.request.contextPath}/resources/admin/assets/images/avatar.png" class="img-fluid">
				                        			</div>
				                        			<%-- <div class="artboard">
													  	<!-- <h1>GENDER</h1> -->
													  	<label class="switch">
													    	<input type="checkbox" onclick="check(this)" id="statusUpdt"/><span class="slider"><span class="btn"><span class="mouth"></span><img src="${pageContext.request.contextPath}/resources/admin/assets/images/block.svg"/></span></span>
													  	</label>
													</div>
													<strong class="gender" >UNBLOCK</strong> --%>
				                        		</div>
				                        	</div>
				                        	<div class="col-9">
				                        		<fieldset>
				                        			<legend>&nbsp;Basic Info&nbsp;</legend>
				                        			<div class="basicInfo">
					                        			<div class="row">
					                        				<div class="col-6">
					                        					<div class="row">
					                        						<div class="col-4">
					                        							<p><strong>Name:</strong></p>
					                        						</div>
					                        						<div class="col-8">
					                        							<span>
					                        								<c:choose>
					                        									<c:when test="${not empty userData.userDetail.lastName}">
					                        										${userData.userDetail.firstName} ${userData.userDetail.lastName}
					                        									</c:when>
					                        									<c:otherwise>${userData.userDetail.firstName}</c:otherwise>
					                        								</c:choose>
			                        									</span>
			                        									
					                        						</div>
					                        					</div>
					                        				</div>
					                        				<div class="col-6">
					                        					<div class="row">
					                        						<div class="col-4">
					                        							<p><strong>Mobile No.:</strong></p>
					                        						</div>
					                        						<div class="col-8">
					                        							<p><span>+91 </span><span>${userData.userDetail.contactNo}</span></p>
					                        						</div>
					                        					</div>
					                        				</div>
					                        				<div class="col-6">
					                        					<div class="row">
					                        						<div class="col-4">
					                        							<p><strong>Email-Id:</strong></p>
					                        						</div>
					                        						<div class="col-8">
					                        							<p><span>${userData.userDetail.email}</span></p>
					                        						</div>
					                        					</div>
					                        				</div>
					                        				<div class="col-6">
					                        					<div class="row">
					                        						<div class="col-4">
					                        							<p><strong>DOB:</strong></p>
					                        						</div>
					                        					
                                                                   <div class="col-8">					                        						
	                                                                  <c:choose>
	                                                                  	<c:when test="${not empty userData.userDetail.dateOfBirth}">
	                                                                  		<p><span><fmt:formatDate pattern="dd-MM-yyyy" value="${userData.userDetail.dateOfBirth}" /></span></p>
	                                                                  	</c:when>    
	                                                                  	<c:otherwise>
	                                                                  		<p><span>NA</span></p>
	                                                                  	</c:otherwise>
	                                                                  </c:choose>
                                                                  </div>
					                        					</div>
					                        				</div>
					                        				<div class="col-6">
					                        					<div class="row">
					                        						<div class="col-4">
					                        							<p><strong>Acc Type:</strong></p>
					                        						</div>
					                        						<div class="col-8">
					                        							<p><span>${userData.accountDetail.accountType.code}</span></p>
					                        						</div>
					                        					</div>
					                        				</div>
					                        				<div class="col-6">
					                        					<div class="row">
					                        						<div class="col-4">
					                        							<p><strong>DO-Regd:</strong></p>
					                        						</div>
					                        						<div class="col-8">
					                        							<p><span><fmt:formatDate pattern="dd-MM-yyyy" value="${userData.created}" /></span></p>
					                        						</div>
					                        					</div>
					                        				</div>
					                        				<div class="col-6">
					                        					<div class="row">
					                        						<div class="col-4">
					                        							<p><strong>Status:</strong></p>
					                        						</div>
					                        						<div class="col-8">
					                        							<p><span>${userData.mobileStatus}</span></p>
					                        						</div>
					                        					</div>
					                        				</div>
					                        				
					                        				<div class="col-6">
					                        					<div class="row">
					                        						<div class="col-4">
					                        							<p><strong>Token:</strong></p>
					                        						</div>
					                        						<div class="col-8">
					                        							<p><span>${userData.mobileToken}</span></p>
					                        						</div>
					                        					</div>
					                        				</div>

														<c:if test="${isAgent == true}">
															<div class="col-6">
																<div class="row">
																	<div class="col-4">
																		<p>
																			<strong>Agent:</strong>
																		</p>
																	</div>
																	<div class="col-8">
																		<a href="${pageContext.request.contextPath}/Master/UserDetails?username=${userAgent}">${userAgent}</a>
																	</div>
																</div>
															</div>
															<div class="col-6">
																<div class="row">
																	<div class="col-4">
																		<p>
																			<strong>Distributor:</strong>
																		</p>
																	</div>
																	<div class="col-8">
																		<a href="${pageContext.request.contextPath}/Master/UserDetails?username=${userDistributer}">${userDistributer}</a>
																	</div>
																</div>
															</div>
														</c:if>

													</div>
					                        		
					                        		</div>
				                        		</fieldset>
				                        	</div>
	                        			</div> <!-- end row -->
	                        			<div class="row">
	                        				<div class="col-12">
	                        					<div class="text-right">
	                        						<small><a href="javascript:void(0);" id="showKyc">Show KYC Details</a></small>
	                        					</div>
	                        				</div>
	                        			</div>
	                        		</div>
	                        	</div>

	                        	<div class="collapse" id="collapseKyc">
								  	<div class="card card-body mb-3">
								  		<div class="row kycHeader">
								  			<button class="close" type="button" id="closeKyc"><img src="${pageContext.request.contextPath}/resources/admin/assets/images/cancel.png"></button>
								  			<h4 class="kycTitle">KYC Details</h4>
								  		</div>
								    	<div class="row">
								    		<div class="col-md-4">
								    			<div class="col-12">
									    			<div class="docImg mb-2">
									    				<center>
									    					<h5>Aadhaar Image front: </h5>
									    					<img src="${kycDetails.aadharImage}" class="img-fluid" style="height: 167px;width: 185px;">
									    				</center>
									    			</div>
									    		</div>
									    		<div class="col-12">
									    			<div class="docImg">
									    				<center>
									    					<h5>Aadhaar Image back: </h5>
									    					<img src="${kycDetails.aadharImage1}" class="img-fluid" style="height: 167px;width: 185px;">
									    				</center>
									    			</div>
									    		</div>
								    		</div>
								    		<div class="col-md-6 offset-md-1">
								    			<div class="idWrp">
								    				<img src="${pageContext.request.contextPath}/resources/admin/assets/images/id.png" class="img-fluid">
								    				<div class="idDetails">
								    					<div class="idType mb-3">
								    						<h5>ID Type: </h5>
								    						<span>${userData.userDetail.idType}</span>
								    					</div>
								    					<div class="idNum">
								    						<h5>ID Number: </h5>
								    						<span>${userData.userDetail.idNo}</span>
								    					</div>
								    				</div>
								    			</div>
								    		</div>
								    	</div>
								  	</div>
								</div>
                        	</div>
                        </div>
                    </div> <!-- container -->
                </div> <!-- content -->

                <footer class="footer text-right">
                    <script type="text/javascript">var year = new Date(); document.write(year.getFullYear());</script> © IMoney Pvt. Ltd.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->
         <!-- Modal starts here -->
        <div class="modal fade" id="myModal" style="z-index: 9999;">
		  <div class="modal-dialog modal-sm">
		    <div class="modal-content" style="padding: 0; padding-left: 0; width: auto;">
		
		      <!-- Modal Header -->
		      <div class="modal-header">
		        <h4 class="modal-title">Card Block</h4>
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>
		
		      <!-- Modal body -->
		      <div class="modal-body">
		        <div class="blck_resnm">
		        	<form>
		        		<div class="form-group">
		        			<label for="option">Reason</label>
							  <select class="form-control" id="request_Type">
							    <option value="suspend">Suspend</option>
							    <option value="Lost">Lost</option>
							    <option value="Stolen">Stolen</option>
							    <option value="Damaged">Damaged</option>
							  </select>
		        		</div>
		        		<center><button class="btn btn-primary" type="button" onclick="block()" id="blockCard">Submit</button></center>
		        	</form>
		        </div>
		      </div>
		
		    </div>
		  </div>
		</div>



        <!-- jQuery  -->
       	<script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>

        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		<!-- Include Date Range Picker -->
		<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

        <!-- Required datatable js -->
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        
        <!-- Buttons examples -->
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
        <!-- Responsive examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
        <script src="${pageContext.request.contextPath}/resources/corporate/assets/js/all.js"></script>

        <script>
        	function check(obj){
			  if (obj.checked) {
			    $(".gender").html("BLOCK");
			    	 var va="${userdetails.userDetail.contactNo}";
			    	 var auth="ROLE_USER,ROLE_LOCKED";
			    		$.ajax({
			    			type : "POST",
			    			contentType : "application/json",
			    			url : "${pageContext.request.contextPath}/Admin/Status/block/unblock",
			    			dataType : 'json',
			    			data : JSON.stringify({
			    				"authority" :" "+auth+"",
			    				"userName" :""+va+""
			    			}),
			    			success : function(response) {
			    				 $("#statusUpdt").html(response.message);
			    			},
			    		});
			     
			  } else {
			    $(".gender").html("UNBLOCK");
			         var va="${userdetails.userDetail.contactNo}";
			    	 var auth="ROLE_USER,ROLE_AUTHENTICATED";
			    		$.ajax({
			    			type : "POST",
			    			contentType : "application/json",
			    			url : "${pageContext.request.contextPath}/Admin/Status/block/unblock",
			    			dataType : 'json',
			    			data : JSON.stringify({
			    				"authority" :" "+auth+"",
			    				"userName" :""+va+""
			    			}),
			    			success : function(response) {
			    				 $("#statusUpdt").html(response.message);
			    			},
			    		});
			  }
			}; 
        </script>

        <!-- datatablles -->
        <script type="text/javascript">
			$(document).ready(function() {
			    var table = $('#example').DataTable( {
			    	searching: false,
			        lengthChange: false,
			        buttons: [ 'csv', 'excel', 'pdf' ]
			    } );
			 
			    table.buttons().container()
			        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
			} );

        </script>

        <!-- datatablles -->
        <script type="text/javascript">
			$(document).ready(function() {
		        $('#pcard_tab').DataTable({
		          info: false,
		          searching: false,
		          responsive: true
		        });
		    });

		    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		      $($.fn.dataTable.tables(true)).DataTable()
		        .columns.adjust()
		        .responsive.recalc();
		    }); 

        </script>

        <script>
		$(function() {
		
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		
		    function cb(start, end) {
		        $('#reportrange').html(start.format('MM-dd-yyyy') + ' - ' + end.format('MM-dd-yyyy'));
		    }
		
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'YYYY-MM-DD'
		        },
		        dateLimit: {
		            "days": 30
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		
		    cb(start, end);
		    
		});
		</script>

		<!-- kyc collapse -->
		<script>
			$(document).ready(function() {
				$('#showKyc').click(function() {
					$("#collapseKyc").collapse('show');
				});
				$('#closeKyc').click(function() {
					$("#collapseKyc").collapse('hide');
				});
			});
		</script>

		<script>
			$(document).ready(function() {
				$('#virtual_Trxn').click(function() {
					$("#collapseTrxns").slideDown();
				});
				$('#closeVirtu').click(function() {
					$("#collapseTrxns").slideUp();
				});
				$('#physical_Trxn').click(function() {
					$("#collapseTrxns1").slideDown();
				});

				$('#closePhy').click(function() {
					$("#collapseTrxns1").slideUp();
				});
			});
		</script>

		<script>
			$('#cardBtn').click(function() {
				$('#CardInfoWrap').fadeIn();
				$('#UsrInfoWrap').fadeOut();
				$('#cardBtn').addClass('active');
				$('#usrBtn').removeClass('active');
			});
			$('#usrBtn').click(function() {
				$('#UsrInfoWrap').fadeIn();
				$('#CardInfoWrap').fadeOut();
				$('#usrBtn').addClass('active');
				$('#cardBtn').removeClass('active');
			});
		</script>

		<script>
			$('#Ptab').click(function() {
				$('.phySection').show();
				$('.vSection').hide();
				$('#Ptab').addClass('active');
				$('#Vtab').removeClass('active');
			});
			$('#Vtab').click(function() {
				$('.vSection').show();
				$('.phySection').hide();
				$('#Vtab').addClass('active');
				$('#Ptab').removeClass('active');
			});
		</script>
		
	<script type="text/javascript">
    $(document).ready(function(){
    	console.log('here...');
    	var auth="${userdetails.authority}";
    	console.log(auth);
            if (auth.trim()=="ROLE_USER,ROLE_LOCKED") {
            	console.log('true');  
            	$("#statusUpdt").attr('checked','checked')
            } 
        });
    
</script>

<script>
// When the user clicks anywhere outside of the modal, close it

    function block(){
    var cardId="${physicalCard.cardId}";
    var requestType=$("#request_Type").val();
    	$.ajax({
    	    type:"POST",
    	    contentType : "application/json",
    	    url: "${pageContext.request.contextPath}/Admin/BlockCard",
    	    dataType : 'json',
			data : JSON.stringify({
				"cardId" : "" + cardId + "",
				"requestType" : ""+requestType+""
			}),
			success : function(response) {
				$("#myModal").modal("hide");
    	        if (response.code.includes("S00")) {
					swal("Success!!", response.message, "success");}
    	        else{
    	        	swal({
						  type: 'error',
						  title: 'Sorry!!',
						  text: response.message
						});
    	        }
    	    }
    	});
    }
</script>

<script type="text/javascript">
function unblk(){
	var cardId="${physicalCard.cardId}";
	$.ajax({
	    type:"POST",
	    contentType : "application/json",
	    url: "${pageContext.request.contextPath}/Admin/UnblockCard",
	    dataType : 'json',
		data : JSON.stringify({
			"cardId" : "" + cardId + "",
			"requestType" : "Active"
		}),
		success : function(response) {
	        if (response.code.includes("S00")) {
	        	$('#myModal').modal('hide');
				swal("Success!!", response.message, "success");}
	        else{
	        	$('#myModal').modal('hide');
	        	swal({
					  type: 'error',
					  title: 'Sorry!!',
					  text: response.message
					});
	        }
	    }
	});
}
</script>
<script>
function myFunction(){
	var numberr= "${userdetails.userDetail.contactNo}";
	$("#k").click(function(){
		$("#number").html(numberr);
		});
	}

</script>
</body>
</html>
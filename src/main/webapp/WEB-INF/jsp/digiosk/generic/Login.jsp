<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
   response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
   response.setDateHeader("Expires", 0);
   response.setHeader("Pragma", "no-cache");
%>
<html lang="en">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
<meta name="keywords"
	content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
<meta name="author" content="pixelstrap">
<link rel="icon" href="${pageContext.request.contextPath}/resources/assets/images/favicon.png" type="image/x-icon" />
<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets/images/favicon.png"
	type="image/x-icon" />
<title>Home</title>

<!--Google font-->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700"
	rel="stylesheet">

<!-- Font Awesome -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.css">

<!-- ico-font -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/icofont/icofont.css">

<!-- Themify icon -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/themify-icons/themify-icons.css">
<!-- Flag icon -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/flag-icon.css">

<!-- prism css -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/prism.css">

<!-- Owl css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/owlcarousel.css">

<!-- Bootstrap css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.css">

<!-- App css -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/style.css">

<!-- Responsive css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/responsive.css">

<!-- linearicons css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/linearicons/style.css">
<!-- mystyle  css-->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/mystyle.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

        <style>
            body {
                overflow: hidden;
            }
            footer {
                position: fixed;
                width: 100%;
                bottom: 0;
            }
            .input-group-prepend {
                margin-right: -1px;
            }
            .input-group-append, .input-group-prepend {
                display: flex;
            }
            .input-group>.input-group-append:last-child>.btn:not(:last-child):not(.dropdown-toggle), .input-group>.input-group-append:last-child>.input-group-text:not(:last-child), .input-group>.input-group-append:not(:last-child)>.btn, .input-group>.input-group-append:not(:last-child)>.input-group-text, .input-group>.input-group-prepend>.btn, .input-group>.input-group-prepend>.input-group-text {
                    border-top-right-radius: 0;
                    border-bottom-right-radius: 0;
            }
            .input-group-text {
                display: flex;
                align-items: center;
                padding: .375rem .75rem;
                font-size: .875rem;
                font-weight: 300;
                line-height: 1.5;
                color: #4f5467;
                text-align: center;
                background-color: #f8f9fa;
                border: 1px solid #e9ecef;
                border-radius: 2px;
            }

            .btn-group-toggle>.btn, .btn-group-toggle>.btn-group>.btn, .custom-control-label, .custom-file, .dropdown-header, .input-group-text, .nav {
                margin-bottom: 0;
            }

            .badge, .btn, .dropdown-header, .dropdown-item, .input-group-text, .navbar-brand, .progress-bar {
                white-space: nowrap;
            }
            .input-group>.custom-file, .input-group>.custom-select, .input-group>.form-control {
                position: relative;
                flex: 1 1 auto;
                width: 1%;
                margin-bottom: 0;
            }
            .form-control-lg {
                padding: .5rem 1rem;
                font-size: 1.09375rem;
                line-height: 1.5;
                border-radius: 2px;
            }
            .custom-control {
                position: relative;
                display: block;
                min-height: 1.5rem;
                padding-left: 1.5rem;
            }
            .custom-control-input {
                position: absolute;
                z-index: -1;
                opacity: 0;
            }
            .custom-checkbox .custom-control-label::before {
                border-radius: 2px;
            }
            .custom-control-label::after, .custom-control-label::before {
                top: .15rem;
            }
            .custom-control-label::before {
                pointer-events: none;
                user-select: none;
                background-color: #dee2e6;
            }
            .custom-control-label::after, .custom-control-label::before {
                position: absolute;
                display: block;
                width: 1rem;
                height: 1rem;
                content: "";
                left: 0;
            }
            .custom-control-label::after {
                background-repeat: no-repeat;
                background-position: center center;
                background-size: 50% 50%;
            }
            .custom-checkbox .custom-control-input:checked~.custom-control-label::before, .custom-checkbox .custom-control-input:indeterminate~.custom-control-label::before {
                background-color: #4798e8;
            }
            .custom-control-input:checked~.custom-control-label::before {
                color: #fff;
                background-color: #4798e8;
            }
            .custom-checkbox .custom-control-input:checked~.custom-control-label::after {
                background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 8 8'%3E%3Cpath fill='%23fff' d='M6.564.75l-3.59 3.612-1.538-1.55L0 4.26 2.974 7.25 8 2.193z'/%3E%3C/svg%3E");
            }
            .error {
            	color: red;
            	font-size: 13px;
            }
        </style>

    </head>


    <body class="">
        <!-- HOME -->
        <section>
            
            <!-- ======================================================================================= -->

		<div class="main-wrapper">
			<div
				class="auth-wrapper d-flex no-block justify-content-center align-items-center"
				style="background: url(${pageContext.request.contextPath}/resources/admin/assets/images/auth-bg.jpg) no-repeat center center;">
				<div class="auth-box">
					<div id="loginfrom">
						<div class="logo">
							<span class="db"> <img
								src="${pageContext.request.contextPath}/resources/digiosk-assets/img/logo.png"
								alt="logo" style="width: 40%;"> <!-- <h6 class="font-medium mb-3">Sign In to Admin</h6> -->
							</span>
						</div>
						<!-- form -->
						<div class="row">
							<c:if test="${not empty ERRORMSG}">
								<label id="errorMesg" style="margin-left: 20%;" class="error">${ERRORMSG}</label>
							</c:if>
							<div class="col-12">
							<span id="ajaxError" style="color:red;">fgfdgdfgdf</span>
								<form:form class="form-horizontal mt-3" method="post"
									action="${pageContext.request.contextPath}/Master/Home"
									modelAttribute="genericLogin" id="formId">

									<input type="hidden" name="otp" id="otpId">

									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1"> <i
												class="ti-user"></i>
											</span>
										</div>
										<form:input type="email" id="emailId"
											class="form-control form-control-lg" placeholder="Username"
											aria-label="Username" aria-describedby="basic-addon1"
											path="username" required="required" />
									</div>
									<p id="errorUser" class="error"></p>
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon2"> <i
												class="ti-lock"></i>
											</span>
										</div>
										<form:input type="password" id="passId"
											class="form-control form-control-lg" placeholder="Password"
											aria-label="Username" aria-describedby="basic-addon2"
											path="password" required="required" />
									</div>
									<p id="errorPass" class="error"></p>
									<div class="form-group row">
										<div class="col-md-12">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input"
													id="customCheck1"> <label
													class="custom-control-label" for="customCheck1">Remember
													Me</label>
												 <a href="#" style="margin-left: 30%;" onclick="forgetPassword();">Forget Password</a>
											</div>
										</div>
									</div>
									<div class="form-group text-center">
										<div class="col-xs-12 p-b-20">
											<button class="btn btn-block btn-lg btn-info" id="loginBtn" type="button"
												onclick="verifyLogin();">Log In</button>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<!-- ========== login otp modal======= -->

	<div id="otpModal" class="modal fade" role="dialog" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content" style="margin-top: 40%; margin-left: 10%;  margin-right: 10%;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 align="center" class="modal-title" style="margin-left: 25%;">OTP Verification</h4>
				</div>
				<div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6" align="center" style="margin-left: 25%;" >
								<div class="form-group" align="center">
									 <input type="password" id="otpNo"
										placeholder="Enter OTP" class="form-control" maxlength="6"
										onkeypress="return isNumberKey(event);">
								</div>
								<p id="otpError" style="color: red"></p>
								<p id="otpMesg" style="color: red; margin-left: -3%;"></p>
							</div>
						</div>
						<div class="row">
						<div class="col-md-3"></div>
								<div class="col-md-3">
										<button class="btn btn-info" type="button"
										onclick="verifyLogin();">Resend</button>
								</div>
								<div class="col-md-3" align="right">
									<button class="btn btn-info" type="button"
										onclick="formSubmit();">Submit</button>
								</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</div>

<!-- ===================forget password modal ================ -->

<div id="fpModal" class="modal fade" role="dialog" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content" style="margin-top: 40%; margin-left: 10%;  margin-right: 10%;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 align="center" class="modal-title" style="margin-left: 25%;">Forget Password</h4>
				</div>
				<div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-8" align="center" style="margin-left: 17%;" >
								<div class="form-group" align="center">
									 <input type="text" id="loginEmail"
										placeholder="Enter Email" class="form-control"">
								</div>
								<p id="fpError" style="color: red"></p>
							</div>
						</div>
						<div class="row">
						<div class="col-md-3"></div>
								<div class="col-md-3" align="center" style="margin-left: 10%;">
									<button class="btn btn-info" type="button" id="fpBtnId"
										onclick="getForgetPassOtp();">Submit</button>
								</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</div>


<!-- =============forget password otp verification======== -->


<div id="fpModalOtp" class="modal fade" role="dialog" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content" style="margin-top: 30%; margin-left: 10%;  margin-right: 10%;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 align="center" class="modal-title" style="margin-left: 25%;">Forget Password</h4>
				</div>
				<div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6" align="center" style="margin-left: 25%;" >
								<div class="form-group" align="center">
									 <input type="text" id="fpOtp"
										placeholder="Enter OTP" class="form-control" maxlength="6" 
										onkeypress="return isNumberKey(event);">
								</div>
								<p id="fpOtpError" style="color: red"></p>
								<div class="form-group" align="center">
									 <input type="text" id="epId"
										placeholder="Enter Password" class="form-control" maxlength="15">
								</div>
								<p id="fpEpError" style="color: red"></p>
								<div class="form-group" align="center">
									 <input type="text" id="cpId"
										placeholder="Confirm Password" class="form-control" maxlength="15">
								</div>
								<p id="fpCpError" style="color: red"></p>
								<p id="fpOtpMesg" style="color: green"></p>
							</div>
						</div>
						<div class="row">
						<div class="col-md-3"></div>
								<div class="col-md-3" align="right" style="margin-left: 12%;">
									<button class="btn btn-info" type="button" id="updatePassBtn"
										onclick="updatePassword();">Submit</button>
								</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</div>



		<!-- END HOME -->
		<footer>
			<div class="row">
				<div class="col-sm-12 text-right">
					<div style="padding: 10px;">
						<span>2018 &copy; Copyright | All Right Reserved</span>
					</div>
				</div>
			</div>
		</footer>


		<!-- jQuery  -->
			<script src="${pageContext.request.contextPath}/resources/assets/js/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/popper.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/bootstrap.js"></script>
	<!-- Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/chart/Chart.min.js"></script>
	<!-- Morris Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/raphael.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/morris.js"></script>
	<!-- owlcarousel js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/owlcarousel/owl.carousel.js"></script>
	<!-- Sidebar jquery-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/sidebar-menu.js"></script>
	<!--Sparkline  Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/sparkline/sparkline.js"></script>
	<!--Height Equal Js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/height-equal.js"></script>
	<!-- prism js -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/prism/prism.min.js"></script>
	<!-- clipboard js -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/clipboard/clipboard.min.js"></script>
	<!-- custom card js  -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/custom-card/custom-card.js"></script>
	<!-- Theme js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/script.js"></script>
	<!-- <script src="${pageContext.request.contextPath}/resources/assets/js/theme-customizer/customizer.js"></script> -->
	<!-- <script src="${pageContext.request.contextPath}/resources/assets/js/chat-sidebar/chat.js"></script> -->
	<!-- Counter js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.waypoints.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.counterup.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/counter-custom.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/notify/bootstrap-notify.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/notify/index.js"></script>
	 <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

</body>
    
    
    
    <script type="text/javascript">
    
    function verifyLogin(){
    	$("#otpMesg").html("");
    	var email= $('#emailId').val();
    	var password= $('#passId').val();
    	
    	var valid =true;
    	
    	if(email == null || email.length <=0){
    		valid = false;
    	}if(password == null || password.length <=0){
    		valid = false;
    	}
    	
    	if(valid){
    		$('#loginBtn').attr("disabled", "disabled");
    		$.ajax({
    			type : "POST",
	    		url:"${pageContext.request.contextPath}/Master/verifyLogin/",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"username" : "" + email + "",
	   				"password" : "" + password + "",
	   			}),
	   			success : function(response) {
	   				console.log(response);
	   				if(response.code == 'S00'){
	   					$('#otpMesg').html("OTP sent to Admin");
	   					$('#otpModal').modal('show');
	   				}else{
	   					$('#ajaxError').html(response.message);
// 	   					setTimeout(function(){
// 	   				    	$("#ajaxError").html("");
// 	   				    }, 4000);
	   				}
	   				$('#loginBtn').removeAttr("disabled", "disabled");
	   			}
    		});
    	}
    }
    
    
    function formSubmit(){
	var otp = $('#otpNo').val();
	var email= $('#emailId').val();
	var password= $('#passId').val();
	
	var valid =true;
	
	if(email == null || email.length <=0){
		$('#errorUser').html("Please enter username");
		valid = false;
	}if(password == null || password.length <=0){
		$('#errorPass').html("Please enter password");
		valid = false;
	}if(otp == null || otp.length <=0){
		$('#otpError').html("Please enter OTP");
		valid = false;
	}
	
	 var timeout = setTimeout(function(){
	    	$("#errorUser").html("");
	    	$("#errorPass").html("");
	    	$("#otpError").html("");
	    }, 4000);
	 
	if(valid){
		$('#otpId').val(otp);
		$('#formId').submit();
	}
	
    }
    
    
    function forgetPassword(){
    	$('#fpModal').modal('show');	
    }
    
    function getForgetPassOtp(){
    	$("#loginEmail").html("");
    	var email= $('#loginEmail').val();
    	var valid =true;
    	if(email == null || email.length <=0){
    		$('#fpError').html("Please enter a valid email")
    		valid = false;
    	}
    	if(valid){
    		$('#fpBtnId').attr("disabled", "disabled");
    		$.ajax({
    			type : "POST",
	    		url:"${pageContext.request.contextPath}/Master/ForgetPassword",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"username" : "" + email + ""
	   			}),
	   			success : function(response) {
	   				console.log(response);
	   				if(response.code == 'S00'){
	   					$('#fpOtpMesg').html("OTP sent to registered User ");
	   					$('#fpModal').modal('hide');	
	   					$('#fpModalOtp').modal('show');
	   				}else{
	   					$('#fpError').html(response.message);
// 	   					setTimeout(function(){
// 	   				    	$("#fpError").html("");
// 	   				    }, 3000);
	   				}
	   				$('#fpBtnId').removeAttr("disabled", "disabled");
	   			}
    		});
    	}
    	setTimeout(function(){
		    	$("#fpError").html("");
		    	$("#fpOtpMesg").html("");
		    }, 3000);
    }

    function updatePassword(){
    	var email= $('#loginEmail').val();
    	console.log(email);
    	var otp = $('#fpOtp').val();
    	var enterPass= $('#epId').val();
    	var confPass = $('#cpId').val();
    	
    	var valid =true;
    	if(otp == null || otp.length < 6){
    		$('#fpOtpError').html("Please enter valid Otp ")
    		valid = false;
    	}if(enterPass == null || enterPass.length <=0){
    		$('#fpEpError').html("Please enter password.")
    		valid = false;
    	}if(confPass == null || confPass.length <=0){
    		$('#fpCpError').html("Please confirm password.")
    		valid = false;
    	}else if(enterPass != confPass){
    		$('#fpCpError').html("Password mismatch.")
    		valid = false;
    	}
    	
    	
    	if(valid){
    		$('#updatePassBtn').attr("disabled", "disabled");
    		$.ajax({
    			type : "POST",
	    		url:"${pageContext.request.contextPath}/Master/ForgetPasswordWithOtp",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"userName" : "" + email + "",
	   				"otp" : "" + otp + "",
	   				"newPassword" : "" + enterPass + "",
	   				"confirmPassword" : "" + confPass + ""
	   			}),
	   			success : function(response) {
	   				console.log(response);
	   				if(response.code == 'S00'){
	   					$('#ajaxError').html("Password update successfully");
	   					$('#fpModalOtp').modal('hide');
	   					$('#fpModal').modal('hide');	
	   				}else{
	   					$('#fpCpError').html(response.message);
	   					setTimeout(function(){
	   				    	$("#fpError").html("");
	   				    }, 3000);
	   				}
	   				$('#updatePassBtn').removeAttr("disabled", "disabled");
	   			}
    		});
    	}setTimeout(function(){	
    		$('#fpOtpError').html("")
    		$('#fpEpError').html("")
    		$('#fpCpError').html("")
		    }, 3000);
    }
    
    </script>

<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

</html>
<script src="${pageContext.request.contextPath}/resources/admin/assets/js/status.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="topbar">
    <!-- LOGO -->
    <div class="topbar-left">
        <a href="${pageContext.request.contextPath}/Master/Home" class="logo">
            <span>
                <img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/logo.png" style="width: 150px;height: 69px;">
            </span>
            <i>
                <img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png" alt="" height="50">
            </i>
        </a>
    </div>
    
    <nav class="navbar-custom">
		
		<ul class="list-unstyled topbar-right-menu float-right mb-0">

        
			            <li class="dropdown notification-list">
			                <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" id="dropdownMenuLink" href="#" role="button"
			                               aria-haspopup="true" aria-expanded="false">
			                    <img src="${pageContext.request.contextPath}/resources/admin/assets/images/avatar.png" alt="user" class="rounded-circle"><span class="ml-1"><i class="mdi mdi-chevron-down"></i> </span>
			                </a>
			                <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="dropdownMenuLink">
			                    
			                    <!-- item-->
			                    <a href="${pageContext.request.contextPath}/Master/Logout" class="dropdown-item notify-item">
			                        <i class="fi-power"></i> <span>Logout</span>
			                    </a>
			
			                </div>
			            </li>

        </ul>
		<c:choose>
			<c:when
				test="${role eq 'ROLE_SUPER_ADMIN,ROLE_AUTHENTICATED' || role eq 'ROLE_ADMINISTRATOR,ROLE_AUTHENTICATED'}">
				<ul class="list-unstyled topbar-right-menu float-right mb-0 mr-2">
					<li class=""><a class="nav-link"><b>Settlement Balance:</b> <span><i
								class="fa fa-inr"></i>&nbsp;5000.00</span> </a></li>&nbsp;&nbsp;
				</ul>
				<ul class="list-unstyled topbar-right-menu float-right mb-0 mr-2">
					<li class=""><a class="nav-link"><b>Tree Balance:</b> <span><i
								class="fa fa-inr"></i>&nbsp;9000.00</span> </a></li>&nbsp;&nbsp;
				</ul>
				
				<ul class="list-unstyled topbar-right-menu float-right mb-0 mr-2">
					<li class=""><a class="nav-link"><b>Balance:</b> <span><i
								class="fa fa-inr"></i>&nbsp;89000.00</span> </a></li>&nbsp;&nbsp;
				</ul>
			</c:when>
			<c:otherwise>
				<ul class="list-unstyled topbar-right-menu float-right mb-0 mr-2">
					<li class=""><a class="nav-link"><b>Prefund Balance:</b> <span><i
								class="fa fa-inr"></i>&nbsp;89000.00</span> </a></li>&nbsp;&nbsp;
				</ul>
			</c:otherwise>
		</c:choose>
		<ul class="list-inline menu-left mb-0">
					<li class="float-left">
						<button
							class="button-menu-mobile open-left waves-light waves-effect">
							<i class="dripicons-menu"></i>
						</button>
					</li>
				</ul>
    </nav>

</div>
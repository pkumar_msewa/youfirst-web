<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
   response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
   response.setDateHeader("Expires", 0);
   response.setHeader("Pragma", "no-cache");
%>
<!doctype html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Master | Add Category</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/img/favicon.png">

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
         <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
		
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
		
		<style>
 			.select2-container .select2-selection--multiple { 
 				width: 166%; 
 			}
 			.error {
 				color: red;
 			}
 			.success {
 				color: green;
 			}
		</style>
		
		
 <!-- <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script> -->
    
    <script type="text/javascript">
    	var context_path="${pageContext.request.contextPath}";
    </script>
    </head>
    <body>
        <!-- Begin page -->
        <div id="wrapper">
            <!-- Top Bar Start -->
            <jsp:include page="/WEB-INF/jsp/digiosk/generic/Header.jsp" />
		    <jsp:include page="/WEB-INF/jsp/digiosk/generic/LeftMenu.jsp" />
            <div class="content-page">
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Add Category</h4>
                                    <div class="clearfix"></div>
                                    <span id="stst" style="margin-left: 40%; color: #3c86d8fa;">${statusUpdt}</span>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                        <div class="row">
                            <div class="col-12">
                                <div class="card-box table-responsive">
                                    <div class="col-4 offset-md-4">
										<div class="service_wrp">
											<c:choose>
												<c:when test="${not empty ERRORMSG}">
													<label class="error">${ERRORMSG}</label>
												</c:when>
												<c:when test="${not empty SUCCESSMSG}">
													<label class="success">${SUCCESSMSG}</label>
												</c:when>
											</c:choose>
											<form:form action="${pageContext.request.contextPath}/Master/AddCategory" id="addServiceForm" modelAttribute="addCategory" method="post">
												<div class="form-group">
													<label>Fund/Pocket Name:</label>
													<form:input type="text" placeholder="Enter fund name" class="form-control" path="fundName" onkeypress=" return isAlphNumberKey(event)"/>													
													<span id="error_fundName" class="error"></span>
												</div>
												<div class="form-group">
													<label>Amount:</label>
													<form:input type="text" placeholder="Enter Amount" class="form-control" path="amount" onkeypress="return isNumberKey(event)"/>													
													<span id="error_amount" class="error"></span>													
												</div>
												<div class="form-group">
													<label>Country:</label>
													<form:input type="text" class="form-control" path="country" value="IND" readonly="true"/>													
													<span id="error_country" class="error"></span>													
												</div>
												<div class="form-group">
													<label>MCC code:</label>
													<form:input type="text" class="form-control" path="mccCode" onkeypress="return isNumberKey(event)"/>													
													<span id="error_mccCode" class="error"></span>													
												</div>
												<center><button class="btn btn-info" type="button" onclick="formSubmit();">Submit</button></center>
											</form:form>
										</div>                                    	
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end row -->
                    </div> <!-- container -->
                </div> <!-- content -->
                <footer class="footer text-right">
                    2018 © Copyright IMoney.
                </footer>
            </div>
        </div>
        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
        <script>
	        function formSubmit() {
	        	var valid = true;
	        	var fundName = $('#fundName').val();
	        	var amount = $('#amount').val();
	        	var mccCode = $('#mccCode').val();
	        	if (fundName.length <= 0) {
	        		valid = false;
	        		$('#error_fundName').html('Please enter fund name');
	        	}
	        	if (amount == 0) {
	        		valid = false;
	        		$('#error_amount').html('Please enter amount');
	        	}
	        	if (mccCode.length <= 0) {
	        		valid = false;
	        		$('#error_mccCode').html('Please enter MCC code');
	        	}
	        	if (valid) {
	        		$('#addServiceForm').submit();
	        	}
	        	var timeout = setTimeout(function(){
					$('#error_fundName').html("");
					$('#error_amount').html("");
					$('#error_mccCode').html("");
			    }, 3000);
	        }
	        
	        function isNumberKey(evt){
			    var charCode = (evt.which) ? evt.which : evt.keyCode
			    return !(charCode > 31 && (charCode < 48 || charCode > 57));
			}	
			
			function isAlphNumberKey(evt){
			    var k = (evt.which) ? evt.which : evt.keyCode
			    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
			}
			
			function isAlphKey(evt){
			    var charCode = (evt.which) ? evt.which : evt.keyCode
			    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
			}
        </script>

    </body>
    
</html>
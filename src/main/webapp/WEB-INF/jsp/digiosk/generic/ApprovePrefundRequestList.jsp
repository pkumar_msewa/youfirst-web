<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
   response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
   response.setDateHeader("Expires", 0);
   response.setHeader("Pragma", "no-cache");
%>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Master | Pending prefund request</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png">
        
        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/custom.css" rel="stylesheet" type="text/css" />

        <!-- DataTables -->
        <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>

        

	<style>
	/* The Modal (background) */
	.modal {
	    display: none; /* Hidden by default */
	    position: fixed; /* Stay in place */
	    z-index: 1; /* Sit on top */
	    padding-top: 70px; /* Location of the box */
	    
	    left: 0;
	    top: 0;
	    width: 100%; /* Full width */
	    height: 100%; /* Full height */
	    overflow: auto; /* Enable scroll if needed */
	    background-color: rgb(0,0,0); /* Fallback color */
	    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}
	
	/* Modal Content */
	.modal-content {
	    background-color: #fefefe;
	    margin: auto;
	    padding: 20px;
	    padding-left: 60px;
	    
	    border: 1px solid #888;
	    width: 80%;
	}
	
	/* The Close Button */
	.close {
	    color: #aaaaaa;
	    float: right;
	    font-size: 28px;
	    font-weight: bold;
	}
	
	.close:hover,
	.close:focus {
	    color: #000;
	    text-decoration: none;
	    cursor: pointer;
	}
	
	</style>
</head>
<body>
	<div id="wrapper">
		<jsp:include page="/WEB-INF/jsp/digiosk/generic/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/digiosk/generic/LeftMenu.jsp" />
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
		                <div class="col-12">
		                    <div class="page-title-box">
		                        <h4 class="page-title float-left">Pending Prefund Request Details</h4>
		                        <div class="clearfix"></div>
		                    </div>
		                </div>
		            </div>
		            <div class="row">
		            	<div class="col-12">
		            		<div class="card-box">
		            			<div class="col-md-12">
		            				<div class="tabble">
		            					<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
		            					<thead>
	                                        <tr>
	                                            <th>S. No</th>
	                                            <th>Prefund request user</th>
	                                            <th>Amount</th>
	                                            <th>Reference No</th>
	                                            <th>Date</th>
	                                            <th>Status</th>
	                                            <th>Prefunding user</th>
	                                            <th>Remarks</th>
	                                            <th>Action</th>
	                                        </tr>
	                                    </thead>
	                                    <tbody id="pendingRefundList">
	                                    </tbody>
		            				</table>
		            				<nav>
										<ul class="pagination" id="paginationn"></ul>
									</nav>
	            				</div>
	            			</div>
	            		</div>
	            	</div>
	            </div>
			</div>
		</div>
	</div>
</div>
	
	
	
	
	
	
	<!-- jQuery  -->
        <%-- <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script> --%>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

        <!-- Required datatable js -->
        <!-- <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
        Buttons examples
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.bootstrap4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
        Responsive examples
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap4.min.js"></script> -->

        <!-- Include Date Range Picker -->
        <script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
        <script src="${pageContext.request.contextPath}/resources/corporate/assets/js/all.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
        <script type="text/javascript">
        $(document).ready(function() {
        	getPendingRefundList(0);
        });
        
        var contextPath="${pageContext.request.contextPath}";
        
        function getPendingRefundList(page) {
        	var trHtml='';
        	$.ajax({
        		type : "POST",
	    		url:contextPath+"/Master/ApprovePrefundRequestList",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"page" : page,
	   				"size" : 10,
	   			}),
	   			success : function(response) {
	   				$('#pendingRefundList').empty();
	   				if (response.details.content.length > 0) {
	   					console.log("value available");
	   					for (var i = 0; i < response.details.content.length; i++) {
	   						var data = response.details.content[i];
	   						trHtml = '<tr>';
	   						trHtml = trHtml + '<td>' +(i+1)+ '</td>';
	   						if(data.prefundRequestUser.authority == 'ROLE_AGENT,ROLE_AUTHENTICATED'){
	   							trHtml = trHtml + '<td>Agent</td>';
	   						} else if(data.prefundRequestUser.authority == 'ROLE_DISTRIBUTOR,ROLE_AUTHENTICATED'){
	   							trHtml = trHtml + '<td>Distributor</td>';
	   						} else if(data.prefundRequestUser.authority == 'ROLE_SUPER_DISTRIBUTOR,ROLE_AUTHENTICATED'){
	   							trHtml = trHtml + '<td>Super Distributor</td>';
	   						}
	   						trHtml = trHtml + '<td>' +(data.amount)+ '</td>';
	   						trHtml = trHtml + '<td>' +(data.senderReferenceNo)+ '</td>';
	   						trHtml = trHtml + '<td>' +formatDate(data.created)+ '</td>';
	   						trHtml = trHtml + '<td id="tdata'+i+'">' +(data.status)+ '</td>';
	   						if(data.prefundedUser.authority == 'ROLE_AGENT,ROLE_AUTHENTICATED'){
	   							trHtml = trHtml + '<td>Agent</td>';
	   						} else if(data.prefundedUser.authority == 'ROLE_DISTRIBUTOR,ROLE_AUTHENTICATED'){
	   							trHtml = trHtml + '<td>Distributor</td>';
	   						} else if(data.prefundedUser.authority == 'ROLE_SUPER_DISTRIBUTOR,ROLE_AUTHENTICATED'){
	   							trHtml = trHtml + '<td>Super Distributor</td>';
	   						} else if(data.prefundedUser.authority == 'ROLE_ADMINISTRATOR,ROLE_AUTHENTICATED'){
	   							trHtml = trHtml + '<td>Admin</td>';
	   						}
	   						trHtml = trHtml + '<td>' +(data.remarks)+ '</td>';
	   						
	   						trHtml = trHtml + '<td id="td'+i+'"><button class="btn btn-primary" id="accept'+i+'" onclick="accept('+i+','+(data.id)+',true);" type="button">Approve</button>'
	   						+ '&nbsp;&nbsp;<button class="btn btn-primary" id="reject'+i+'" onclick="accept('+i+','+data.id+',false);" type="button">Reject</button></td>';
	   						trHtml = trHtml + '<tr>';
	   						$('#pendingRefundList').append(trHtml);
	   					}
	   				} else {
	   					console.log("value not available");
	   					trHtml = '<tr>';
   						trHtml = trHtml + '<td></td>';
   						trHtml = trHtml + '<td></td>';
   						trHtml = trHtml + '<td></td>';
   						trHtml = trHtml + '<td>No result found</td>';
   						trHtml = trHtml + '<td></td>';
   						trHtml = trHtml + '<td></td>';
   						trHtml = trHtml + '<td></td>';
   						trHtml = trHtml + '<td></td>';
   						trHtml = trHtml + '<tr>';
   						$('#pendingRefundList').append(trHtml);
	   				}
	   				$(function () {
   	   					console.log("inside funt...and total pages:"+response.totalPage);
   	   					$('#paginationn').twbsPagination({
    					 	totalPages: response.totalPage,
    					 	visiblePages: 10,
    		         	 	onPageClick: function (event, page) {
    		         	 		getPendingRefundList(page-1);
    			         	}
			 			});
	   				});
	   			}
        	});
        }
        /* function accept(index,id,value) {
        	console.log("index.. "+index);
        	console.log("refNo.. "+id);
        	console.log("value.. "+value);
        	if(value){
        		console.log("true");
        	} else {
        		console.log("false");
        	}
        } */
       	function accept(index,id,value){
        	$('#accept'+index).addClass('disabled','true');
        	$('#reject'+index).addClass('disabled','true');
        	console.log("index.. "+index);
        	console.log("value.. "+value);
        	console.log("id.. "+id);
        	$.ajax({
        		type:"POST",
        	    contentType : "application/json",
        	    url: contextPath+"/Master/ApproveOrRejectPrefundRequest",
        	    dataType : 'json',
    			data : JSON.stringify({
    				"id" : id,
    				"value" : ""+value+""
    			}),
    			success : function(response){
    				if (response.code.includes("S00")) {
    					var html;
    					var html1;
    					if(value) {
    						html = 'Prefund request accepted';
    						html1 = 'Success';
    					} else {
    						html = 'Prefund request cancelled';
    						html1 = 'Cancel';
    					}
    					$('#td'+index).html(html);
    					$('#tdata'+index).html(html1);
    					console.log("success");
    				}
    			}
        	});
        }
        
        function formatDate(d){
        	d=new Date(d);
        	var month = d.getMonth();
        	var day = d.getDate();
        	month = month + 1;
        	month = month + "";
        	if (month.length == 1)
        	{
        	month = "0" + month;
        	}
        	day = day + "";
        	if (day.length == 1)
        	{
        	day = "0" + day;
        	}
        	return d.getFullYear()+ '-' +month + '-' + day ;
       	}

        </script>
</body>
</html>
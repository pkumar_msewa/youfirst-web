<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Admin | Corporate Bulk Upload List</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png">

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
 <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script>
    
    <script type="text/javascript">
    var context_path="${pageContext.request.contextPath}";
    </script>

    </head>


    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
          <jsp:include page="/WEB-INF/jsp/digiosk/generic/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/digiosk/generic/LeftMenu.jsp" />
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
          
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Uploaded File Report</h4>
                                    <div class="clearfix"></div>
                                    <span id="stst" style="margin-left: 40%; color: #3c86d8fa;">${statusUpdt}</span>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

				<div class="row">

						<div class="col-md-4 col-sm-4 col-xs-4">
							<label>Select Agent</label> <select id="userId" name="val"
								class="form-control"  onchange ="getByCorporate(this.value)">
								<option value="All">All Agent</option>
								<c:if
									test="${requestScope.corporateList !=null && !empty corporateList}">
									<c:forEach var="corporateList"
										items="${requestScope.corporateList}">
										<option value="${corporateList.username}"
											${fn:contains(username,corporateList.username) ? 'selected="selected"' : ''}>
											<c:out value="${corporateList.username}" /></option>
									</c:forEach>
								</c:if>
							</select>
							<p id="userMsg"></p>
						</div>
					</div>


                       <div class="row">
                            <div class="col-12">
                                <div class="card-box table-responsive">
                                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4>
                                    <p class="text-muted font-14 m-b-30">
                                        DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                                    </p> -->

                                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
										<tr>
											<th>S. No</th>
											<th>Category</th>
											<th>File URL</th>
											<th>Requested Corporate</th>
											<th>Success Creation</th>
											<th>Partial Creation</th>
											<th>Failed Creation</th>
											<th>Request Date</th>
											<th>Processed</th>
											<th>Status</th>
											<th>Action</th>

										</tr>
									</thead>
                                       <tbody>
                                        <c:forEach items="${CatalogueList}" var="card"
									   varStatus="loopCounter">
											<tr>
												<td>${loopCounter.count}</td>
												<td><c:out value="${card.categoryType}" default=""
														escapeXml="true" /></td>
												<td><a href="${card.s3Path}" download>DOWNLOAD</a></td>
												<td><c:out value="${card.agent.username}"
														default="" escapeXml="true" /></a></td>
												<td>${card.successCreation}</td>
												<td>${card.partialCreation}</td>
												<td>${card.failedCreation}</td>
												<td><c:out value="${card.created}" default=""
														escapeXml="true" /></td>
												<td><c:out value="${card.schedulerStatus}" default=""
														escapeXml="true" /></td>
												<c:choose>
													<c:when test="${card.reviewStatus==true}">
														<td><h6>Already Reviewed</h6></td>
													</c:when>
													<c:when test="${card.fileRejectionStatus==true}">
														<td><h6>File Rejected</h6></td>
													</c:when>
													<c:otherwise>
														<form
															action="${pageContext.request.contextPath}/Master/ReviewCorporate"
															method="post" id="corpSub${card.id}">
															<input type="hidden" value="${card.id}" name="fileId" />
															<input type="hidden" id="corp${card.id}" name="action" />
															<td>
																<button type="button" class="btn btn-sm btn-success"
																	onclick="clickMe('accept', ${card.id})">Accept</button>&nbsp;
																<button type="button" class="btn btn-sm btn-success"
																	onclick="clickMe('reject' , ${card.id})">Reject</button>
															</td>
														</form>
													</c:otherwise>
												</c:choose>

												<td><c:choose>
														<c:when test="${card.reviewStatus==true}">
															<c:out value="Reviewed/Done" default="" escapeXml="true" />

														</c:when>
														<c:otherwise>
															<c:out value="Pending" default="" escapeXml="true" />

														</c:otherwise>
													</c:choose></td>

											</tr>
										</c:forEach>
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2018 © Copyright iMoney.
                </footer>

            </div>


 <form action="${pageContext.request.contextPath}/Master/getFileByCorporate"
 			method="post" id="byCorporate">

			<input type="hidden" name="username" id="cId">

		</form>

            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>

        <!-- Required datatable js -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jszip.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/pdfmake.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/vfs_fonts.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.html5.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.print.min.js"></script>
        <!-- Responsive examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').DataTable({
                    responsive: true
                });

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
                
                
                var timeout = setTimeout(function(){
            		$("#stst").html("");
            	}, 3000); 
                
                
            } );

        </script>


 <script>
 
 function clickMe(value, id){
	 console.log(value);
	 $('#corp'+id).val(value);
	 $('#corpSub'+id).submit();
 }
 
 
 function getByCorporate(username){
	 if(username!=null && username !=""){
		 $('#cId').val(username);
			$('#byCorporate').submit();		 
	 }
 }
 
 
 </script>

    </body>
</html>
<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
	response.setHeader("Cache-Control",
			"no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
	response.setDateHeader("Expires", 0);
	response.setHeader("Pragma", "no-cache");
%>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Master | Customer</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<!-- App favicon -->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png">

<!-- App css -->
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/css/custom.css"
	rel="stylesheet" type="text/css" />

<!-- DataTables -->
<link
	href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap4.min.css"
	rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link
	href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap4.min.css"
	rel="stylesheet" type="text/css" />
<script
	src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>


<style>
	/* The Modal (background) */
	.modal {
	    display: none; /* Hidden by default */
	    position: fixed; /* Stay in place */
	    padding-top: 70px; /* Location of the box */
	    
	    left: 0;
	    top: 0;
	    width: 100%; /* Full width */
	    height: 100%; /* Full height */
	    overflow: auto; /* Enable scroll if needed */
	    background-color: rgb(0,0,0); /* Fallback color */
	    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}
	
	/* Modal Content */
	.modal-content {
	    background-color: #fefefe;
	    margin: auto;
	    padding: 20px;
	    padding-left: 60px;
	    
	    border: 1px solid #888;
	    width: 80%;
	}
	
	#InfoModalID .modal-content {
		padding: 0;
		width: 100%;
	}
	
	/* The Close Button */
	.close {
	    color: #aaaaaa;
	    float: right;
	    font-size: 28px;
	    font-weight: bold;
	}
	
	.close:hover,
	.close:focus {
	    color: #000;
	    text-decoration: none;
	    cursor: pointer;
	}
	
	/* .modal-backdrop.show{
	z-index:0;
	} */
	
	.sidenav {
  height: 89%;
  margin-top: 5%;
  width: 0;
  position: fixed;
  z-index: 4;
  top: 0;
  right: 0;
  background-color: white;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 60px;
}

.sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}

.sidenav a:hover {
  color: #f1f1f1;
}

.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}

@media (min-width: 992px)
.modal-lg {
    max-width: 900px;
}

.linksBlock {
	border: 1px solid #787878;
	height: 85px;
	margin: 0 auto;
	padding: 17px 0;
}

.relatedLinks a {
	color: #00508f;
}
	
	
	</style>

</head>
<body>
	<div id="overlay"></div>
	<div id="wrapper">
		<jsp:include page="/WEB-INF/jsp/digiosk/generic/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/digiosk/generic/LeftMenu.jsp" />
		<div class="content-page">
			<div class="content">
				<div class="container-fluid" id="closeNavId">
					<div class="row">
						<div class="col-12">
							<div class="page-title-box">
								<h4 class="page-title float-left">Wallet Details </h4>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<input type="hidden" id="hiddenId" value="${value}">
					<div class="row">
						<div class="col-12">
							<div class="card-box">
								<div class="col-md-12">
									<div class="tabble" style="overflow-x: scroll;">
										<table id="example"
											class="table table-striped table-bordered dt-responsive nowrap"
											style="width: 100%">
											<thead>
												<tr>
													<th>S.No</th>
													<th>First&nbsp;Name</th>
													<th>Last&nbsp;Name</th>
													<th>Email</th>
													<th>Mobile</th>
													<th>Card&nbsp;Number</th>
													<th>Proxy&nbsp;Number</th>
													<th>Expiry&nbsp;Date</th>
													<th>Card&nbsp;Status</th>
													<th>Preferred&nbsp;Name</th>
													<th>Card&nbsp;Type</th>
													<th>Wallet&nbsp;Hash&nbsp;Id</th>
													<th>Card&nbsp;Hash&nbsp;Id</th>
													<th>Created&nbsp;Date</th>
												</tr>
											</thead>
											<tbody id="genericUserList"
												style="height: 100px; overflow-y: auto;">
											</tbody>
										</table>
										<nav>
											<ul class="pagination" id="paginationn"></ul>
										</nav>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal" tabindex="-1" role="dialog" id="InfoModalID">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Customer Details</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <div class="row">
				<div class="col-6" style="border-right: 1px solid #efefef">
					<div class="row mb-2">
						<div class="col-5"><h6>First Name:</h6></div>
						<div class="col-7"><strong id="fnameId"></strong></div>					
					</div>
					<div class="row mb-2">
						<div class="col-5"><h6>Last Name:</h6></div>
						<div class="col-7"><strong id="lnameId"></strong></div>					
					</div>
					<div class="row mb-2">
						<div class="col-5" ><h6>Email Id:</h6></div>
						<div class="col-7"><strong id="emailId"></strong></div>					
					</div>
					<div class="row mb-2">
						<div class="col-5"><h6>Mobile:</h6></div>
						<div class="col-7"><strong id="mobileId"></strong></div>					
					</div>
				</div>
				<div class="col-6 relatedLinks">
					<div class="row">
						<div class="col-4 p-0" id="hrefCardId">
							<a href="#">
								<div class="linksBlock text-center">
									<div class="linksIcon">
										<i class="fa fa-credit-card"></i>								
									</div>
									<div class="linksTxt">Cards</div>
								</div>		
							</a>				
						</div>
						<div class="col-4 p-0" id="hrefId">
							<a href="${pageContext.request.contextPath}/Master/getTransaction/">
								<div class="linksBlock text-center">
									<div class="linksIcon">
										<i class="fa fa-list-alt"></i>								
									</div>
									<div class="linksTxt">Transactions</div>
								</div>		
							</a>				
						</div>
					</div>
				</div>        	
	        </div>
	      </div>
	    </div>
	  </div>
	</div>



	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
	<script type="text/javascript"
		src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

	<!-- App js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/assets/js/all.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
	<link rel="stylesheet" type="text/css"
		href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

	<script type="text/javascript">
		var contextPath = "${pageContext.request.contextPath}";
		var value = "${value}";
		if (value == null) {
			value = 'All';
		}
		var role = "${role}";
		$(document).ready(function() {
			getCardList(0);
		});
		$(function() {
			var start = moment().subtract(29, 'days');
			var end = moment();
			function cb(start, end) {
				$('#reportrange').html(
						start.format('DD/MM/YYYY') + ' - '
								+ end.format('DD/MM/YYYY'));
			}
			$('#reportrange').daterangepicker(
					{
						startDate : start,
						endDate : end,
						locale : {
							format : 'DD/MM/YYYY'
						},
						dateLimit : {
							"days" : 60
						},
						ranges : {
							'Today' : [ moment(), moment() ],
							'Yesterday' : [ moment().subtract(1, 'days'),
									moment().subtract(1, 'days') ],
							'Last 7 Days' : [ moment().subtract(6, 'days'),
									moment() ],
							'Last 30 Days' : [ moment().subtract(29, 'days'),
									moment() ],
							'This Month' : [ moment().startOf('month'),
									moment().endOf('month') ],
							'Last Month' : [
									moment().subtract(1, 'month').startOf(
											'month'),
									moment().subtract(1, 'month')
											.endOf('month') ]
						}
					}, cb);
			cb(start, end);
		});

		function getCardList(page) {
			var trHtml = '';
			var data;
			var userId = $('#hiddenId').val();

			$
					.ajax({
						type : "POST",
						url : contextPath + "/Master/getWallet/" + userId,
						dataType : "json",
						contentType : "application/json",
						data : JSON.stringify({
							"page" : page,
							"size" : 10,
						}),
						success : function(response) {
							console.log("response: " + response);
							console.log("details: " + response.details);
							$('#genericUserList').empty();
							if (response.details != 'null'
									&& response.details != null
									&& response.details.length > 0) {
								for (var i = 0; i < response.details.length; i++) {
									trHtml = '';
									var data = response.details[i];
									trHtml = '<tr title="click here" style="cursor: pointer;" <a href="#" onclick="openNav(\''+data.mobile+ '\',\''+data.firstName+ '\',\''
									+data.lastName+ '\',\''+data.email+ '\',\''+userId+ '\');"></>">';
									trHtml = trHtml + '<td>' + (i + 1)
											+ '</td>';
									trHtml = trHtml + '<td>' + data.firstName
											+ '</td>';
									trHtml = trHtml + '<td>' + data.lastName
											+ '</td>';
									trHtml = trHtml + '<td>' + data.email
											+ '</td>';
									trHtml = trHtml + '<td>' + data.mobile
											+ '</td>';
									trHtml = trHtml + '<td>' + data.cardNo
											+ '</td>';
									trHtml = trHtml + '<td>' + data.proxyNo
											+ '</td>';
									trHtml = trHtml + '<td>' + data.expiryDate
											+ '</td>';
									trHtml = trHtml + '<td>' + data.cardStatus
											+ '</td>';
									trHtml = trHtml + '<td>'
											+ data.preferredName + '</td>';
									trHtml = trHtml + '<td>' + data.cardType
											+ '</td>';
									trHtml = trHtml + '<td>'
											+ data.walletHashId + '</td>';
									trHtml = trHtml + '<td>' + data.cardHashId
											+ '</td>';
									trHtml = trHtml + '<td>' + data.created
											+ '</td>';
									trHtml = trHtml + '</tr>';
									$('#genericUserList').append(trHtml);
								}
								$(function() {
									$('#paginationn').twbsPagination({
										totalPages : response.totalPage,
										visiblePages : 10,
										onPageClick : function(event, page) {
											if ($('#paginationId').val() != 1) {
												getCardList(page - 1);
											} else {
												$('#paginationId').val(2);
											}
										}
									});
								});
							} else {
								trHtml = '<tr>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td>No data found</td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '</tr>';
								$('#genericUserList').append(trHtml);
							}
						}
					});
		}

		function formatDate(d) {
			d = new Date(d);
			var month = d.getMonth();
			var day = d.getDate();
			month = month + 1;
			month = month + "";
			if (month.length == 1) {
				month = "0" + month;
			}
			day = day + "";
			if (day.length == 1) {
				day = "0" + day;
			}
			return d.getFullYear() + '-' + month + '-' + day;
		}
	</script>

	<script>
	function openNav(username,firstName,lastName,email , id) {
		  $('#InfoModalID').modal('show');
		  console.log(id);
		
			$('#fnameId').html(firstName);
			$('#lnameId').html(lastName);
			$('#emailId').html(email);
			$('#mobileId').html(username);
			
			$('#hrefId').html('<a href="${pageContext.request.contextPath}/Master/getTransaction/'+id+'"><div class="linksBlock text-center"><div class="linksIcon"><i class="fa fa-list-alt"></i></div><div class="linksTxt">Transactions</div></div></a>')
			$('#hrefCardId').html('<a href="${pageContext.request.contextPath}/Master/getCards/'+id+'"><div class="linksBlock text-center"><div class="linksIcon"><i class="fa fa-credit-card"></i></div><div class="linksTxt">Cards</div></div></a>')
			$('#hrefWalletId').html('<a href="${pageContext.request.contextPath}/Master/getWallet/'+id+'"><div class="linksBlock text-center"><div class="linksIcon"><i class="fa fa-list-alt"></i></div><div class="linksTxt">Wallets</div></div></a>')
			
			
			
			
// 			$('#cardNoId').html('');
// 			$('#mobileNoId').html('');
// 			$('#cardHolderId').html('');
// 			$('#balId').html('');

// 			if (username != null && username != '') {
// 				$('#tableHeader0').css('display', 'block');
// 				$
// 						.ajax({
// 							type : "POST",
// 							url : contextPath + "/Master/WalletDetails",
// 							dataType : "json",
// 							contentType : "application/json",
// 							data : JSON.stringify({
// 								"username" : "" + username + "",
// 							}),
// 							success : function(response) {
// 								if (response != null) {
// 									if (response.balance != '0') {
// 										$('#balId').html(
// 												response.balance + " INR");
// 									} else {
// 										$('#balId').html("0.00 INR");
// 									}if(response.cardNumber !=null && response.cardNumber!=''){
// 										$('#cardNoId').html(response.cardNumber);	
// 									}else{
// 										$('#cardNoId').html('NA');
// 									}if(response.cardHolder!=null && response.cardHolder!=''){
// 										$('#cardHolderId').html(response.cardHolder);
// 									}else{
// 										$('#cardHolderId').html('NA');
// 									}
// 									$('#mobileNoId').html(username);
// 									;
// 								}
// 							}
// 						});
// 			}
		}

		function closeNav() {
			/* document.getElementById("mySidenav").style.width = "0"; */
			$('#mySidenav').css('width', '0');
			$('#overlay').css('display', 'none');
			/* document.getElementById('overlay').style.display = 'none'; */
		}
	</script>
</body>
</html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <c:choose>
				<c:when test="${role eq 'ROLE_ADMINISTRATOR,ROLE_AUTHENTICATED'}">
					<ul class="metismenu" id="side-menu">
						<c:if test="${adminMenu.dashboard}">
							<li><a href="${pageContext.request.contextPath}/Master/Home">
									<i class="fa fa-home"></i> <span>Dashboard</span>
							</a></li>
						</c:if>
						<c:if
							test="${adminMenu.userList ==true || adminMenu.kycRequest ==true  }">
							<li><a href="javascript: void(0);"><i
									class="fa fa-users"></i> <span>Users</span> <span
									class="menu-arrow"></span></a>
								<ul class="nav-second-level" aria-expanded="false">
									<c:if test="${adminMenu.userList == true}">
										<li><a
											href="${pageContext.request.contextPath}/Master/GenericUserList/All">User
												List</a></li>
									</c:if>
									<c:if test="${adminMenu.kycRequest == true  }">
										<li><a
											href="${pageContext.request.contextPath}/Master/generic/KycRequest">KYC
												Request</a></li>
									</c:if>
								</ul></li>
						</c:if>
						<c:if
							test="${adminMenu.virtualCardList ==true || adminMenu.assignPhysicalCard ==true  }">
							<li><a href="javascript: void(0);"><i
									class="fa fa-credit-card"></i> <span>Cards</span> <span
									class="menu-arrow"></span></a>
								<ul class="nav-second-level" aria-expanded="false">
									<c:if test="${adminMenu.virtualCardList ==true}">
										<li><a
											href="${pageContext.request.contextPath}/Master/VirtualCardList">Virtual
												Cards List</a></li>
									</c:if>
									<c:if test="${adminMenu.assignPhysicalCard ==true  }">
										<li><a
											href="${pageContext.request.contextPath}/Master/AssignPhysicalCard">Assign
												Physical Card</a></li>
									</c:if>
								</ul></li>
						</c:if>
						<c:if
							test="${adminMenu.loadWallet ==true || adminMenu.loadMoneyUpi ==true  || adminMenu.mdexTransaction ==true}">
							<li><a href="javascript: void(0);"><i
									class="fa fa-file-text-o"></i> <span>Transactions</span> <span
									class="menu-arrow"></span></a>
								<ul class="nav-second-level" aria-expanded="false">
									<c:if test="${adminMenu.loadWallet ==true}">
										<li><a
											href="${pageContext.request.contextPath}/Master/LoadWalletList">Load
												Wallet</a></li>
									</c:if>
									<c:if test="${ adminMenu.loadMoneyUpi ==true}">
										<li><a
											href="${pageContext.request.contextPath}/Master/LoadMoney/TransactionsUPI">Load
												Money UPI</a></li>
									</c:if>
									<c:if test="${adminMenu.mdexTransaction ==true}">
										<li><a
											href="${pageContext.request.contextPath}/Master/LoadMoney/MdexTransaction">Mdex
												Transactions</a></li>
									</c:if>
								</ul></li>
						</c:if>
						<c:if test="${adminMenu.addSuperDist ==true}">
							<li><a
								href="${pageContext.request.contextPath}/Master/RegisterUser"><i
									class="fa fa-user-secret"></i><span>Add Super
										Distributor</span></a></li>
						</c:if>
						<c:if
							test="${adminMenu.pendingPrefundReq ==true || adminMenu.prefundRequestList ==true}">
							<li><a href="javascript: void(0);"><i class="fa fa-cog"></i><span>Prefund</span>
									<span class="menu-arrow"></span></a>
								<ul class="nav-second-level" aria-expanded="false">
									<c:if test="${adminMenu.pendingPrefundReq ==true}">
										<li><a
											href="${pageContext.request.contextPath}/Master/ApprovePrefundRequestList">Pending
												Prefund Request List</a></li>
									</c:if>
									<c:if test="${adminMenu.prefundRequestList ==true}">
										<li><a
											href="${pageContext.request.contextPath}/Master/PrefundRequestHistory">Prefund
												Request History</a></li>
									</c:if>
								</ul></li>
						</c:if>
						<li><c:if test="${adminMenu.sendNotification == true}">
								<a href="javascript: void(0);"><i class="fa fa-bell-o"></i><span>Notification
										Centre</span> <span class="menu-arrow"></span></a>
								<ul class="nav-second-level" aria-expanded="false">
									<li><a
										href="${pageContext.request.contextPath}/Master/SendNotification">Send
											Notification</a></li>
								</ul>
							</c:if></li>
						<c:if
							test="${adminMenu.setCommission ==true || adminMenu.commissionReport ==true}">
							<li><a href="javascript: void(0);"><i class="fa fa-cog"></i><span>Commission</span>
									<span class="menu-arrow"></span></a>
								<ul class="nav-second-level" aria-expanded="false">
									<c:if test="${adminMenu.setCommission ==true}">
										<li><a
											href="${pageContext.request.contextPath}/Master/SetCommission">Set
												commission</a></li>
									</c:if>
									<c:if test="${adminMenu.commissionReport ==true}">
										<li><a
											href="${pageContext.request.contextPath}/Master/CommissionSlabReport">Commission
												Report</a></li>
									</c:if>
								</ul></li>
						</c:if>

						<c:if test="${adminMenu.updateVersion ==true}">
							<li><a href="javascript: void(0);"><i
									class="fa fa-code-fork"></i> <span>Version</span> <span
									class="menu-arrow"></span></a>
								<ul class="nav-second-level" aria-expanded="false">
									<li><a
										href="${pageContext.request.contextPath}/Master/UpdateVersion">Update
											Version</a></li>
								</ul></li>
						</c:if>
						<c:if
							test="${adminMenu.addService ==true || adminMenu.serviceList ==true}">
							<li><a href="javascript: void(0);"><i class="fa fa-cog"></i><span>Services</span>
									<span class="menu-arrow"></span></a>
								<ul class="nav-second-level" aria-expanded="false">
									<c:if test="${adminMenu.addService ==true}">
										<li><a
											href="${pageContext.request.contextPath}/Master/AddService">Add
												Service</a></li>
									</c:if>
									<c:if test="${adminMenu.serviceList ==true}">
										<li><a
											href="${pageContext.request.contextPath}/Master/ServiceListData">Service
												List</a></li>
									</c:if>
								</ul></li>
						</c:if>
						<c:if
							test="${adminMenu.addCategory ==true || adminMenu.categoryList ==true}">
							<li><a href="javascript: void(0);"><i class="fa fa-cog"></i><span>Category</span>
									<span class="menu-arrow"></span></a>
								<ul class="nav-second-level" aria-expanded="false">
									<c:if
										test="${adminMenu.addCategory ==true || adminMenu.categoryList ==true}">
										<li><a
											href="${pageContext.request.contextPath}/Master/AddCategory">Add
												Category</a></li>
									</c:if>
									<c:if
										test="${adminMenu.addCategory ==true || adminMenu.categoryList ==true}">
										<li><a
											href="${pageContext.request.contextPath}/Master/CategoryList">Category
												List</a></li>
									</c:if>
								</ul></li>
						</c:if>
						<c:if test="${adminMenu.changePassword}">
							<li><a
								href="${pageContext.request.contextPath}/Master/ChangePassword"><i
									class="fa fa-user-secret"></i><span>Change Password</span></a></li>
						</c:if>
					</ul>
				</c:when>
				<c:otherwise>
            <ul class="metismenu" id="side-menu">
                <li>
                    <a href="${pageContext.request.contextPath}/Master/Home">
                        <i class="fa fa-home"></i> <span>Dashboard</span>
                    </a>	
                </li>
                <li>
                    <a href="javascript: void(0);"><i class="fa fa-users"></i> <span>Users</span> <span class="menu-arrow"></span></a>
                    <c:choose>
                    	<c:when test="${role eq 'ROLE_SUPER_ADMIN,ROLE_AUTHENTICATED'}">
                    		<ul class="nav-second-level" aria-expanded="false">
                    		 <li><a href="${pageContext.request.contextPath}/Master/GenericUserList/userDetails">Customers</a></li> 
		                        <li><a href="${pageContext.request.contextPath}/Master/GenericUserList/All">Users By Role</a></li> 
		                    </ul>
                    	</c:when>
                    	<c:when test="${role eq 'ROLE_SUPER_DISTRIBUTOR,ROLE_AUTHENTICATED'}">
                    		<ul class="nav-second-level" aria-expanded="false">
                    			<li><a href="${pageContext.request.contextPath}/Master/GenericUserList/All">Distributor List</a></li>
                    		</ul>
                    	</c:when>
                    	<c:when test="${role eq 'ROLE_DISTRIBUTOR,ROLE_AUTHENTICATED'}">
                    		<ul class="nav-second-level" aria-expanded="false">
                    			<li><a href="${pageContext.request.contextPath}/Master/GenericUserList/All">Agent List</a></li>
                    		</ul>
                    	</c:when>
                    </c:choose>
                </li>
                
                <c:if test="${role eq 'ROLE_SUPER_ADMIN,ROLE_AUTHENTICATED'}">
                 <li>
                  <a href="javascript: void(0);"><i class="fa fa-user-md"></i> <span>Admin</span> <span class="menu-arrow"></span></a>
                 <ul class="nav-second-level" aria-expanded="false">
                    		<li><a href="${pageContext.request.contextPath}/Master/CreateAdmin">Create Admin</a></li>
                    		<li><a href="${pageContext.request.contextPath}/Master/adminList">Admin List</a></li>
		                    </ul>
                 </li>
                </c:if>
                
                

                <li>
                    <a href="javascript: void(0);"><i class="fa fa-credit-card"></i> <span>Cards</span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="${pageContext.request.contextPath}/Master/getAllCards">Cards</a></li>
                         <li><a href="${pageContext.request.contextPath}/Master/AssignPhysicalCard">Assign Physical Card</a></li> 
                    </ul>
                </li>
				
				
				<c:choose>
					<c:when test="${role eq 'ROLE_SUPER_ADMIN,ROLE_AUTHENTICATED' || role eq 'ROLE_ADMINISTRATOR,ROLE_AUTHENTICATED' }">
						<li>
		                    <a href="javascript: void(0);"><i class="fa fa-file-text-o"></i> <span>Report</span> <span class="menu-arrow"></span></a>
		                    <ul class="nav-second-level" aria-expanded="false">
  							<li><a href="${pageContext.request.contextPath}/Master/SettlementReport">Settlement Report</a></li>
  							<li><a href="${pageContext.request.contextPath}/Master/fetchBulkUploadList">Bulk Upload List</a></li>
  							<li><a href="${pageContext.request.contextPath}/Master/generic/KycRequest">KYC Request</a></li>
  							<li><a href="${pageContext.request.contextPath}/Master/customerReport/customer">Customer</a></li>
		                    </ul>
		                </li>
					</c:when>
				</c:choose>
				
				<c:choose>
					<c:when test="${role eq 'ROLE_SUPER_ADMIN,ROLE_AUTHENTICATED' || role eq 'ROLE_ADMINISTRATOR,ROLE_AUTHENTICATED'}">
						<li>
		                    <a href="javascript: void(0);"><i class="fa fa-file-text-o"></i> <span>Transactions</span> <span class="menu-arrow"></span></a>
		                    <ul class="nav-second-level" aria-expanded="false">
		                    <li><a href="${pageContext.request.contextPath}/Master/mccTransaction">MCC Transaction</a></li>
		                        <li><a href="${pageContext.request.contextPath}/Master/LoadWalletList">Load Wallet</a></li>
		                        <li><a href="${pageContext.request.contextPath}/Master/moneyTransfer">Money Transfer</a></li>
<%-- 		                        <li><a href="${pageContext.request.contextPath}/Master/LoadMoney/TransactionsUPI">Load Money UPI</a></li> --%>
<%-- 		                        <li><a href="${pageContext.request.contextPath}/Master/LoadMoney/MdexTransaction">Transactions Report</a></li> --%>
		                    </ul>
		                </li>
					</c:when>
					<c:otherwise>
						<li>
		                    <a href="javascript: void(0);"><i class="fa fa-file-text-o"></i> <span>Transactions</span> <span class="menu-arrow"></span></a>
		                    <ul class="nav-second-level" aria-expanded="false">
		                        <li><a href="${pageContext.request.contextPath}/Master/LoadWalletList">Load Wallet</a></li>
		                    </ul>
		                </li>
					</c:otherwise>
				</c:choose>
				
                <c:choose>
					<c:when test="${role eq 'ROLE_SUPER_ADMIN,ROLE_AUTHENTICATED' || role eq 'ROLE_ADMINISTRATOR,ROLE_AUTHENTICATED'}">
						<li><a href="${pageContext.request.contextPath}/Master/RegisterUser"><i class="fa fa-user-secret"></i><span>Add Super Distributor</span></a></li>
					</c:when>
					<c:when test="${role eq 'ROLE_SUPER_DISTRIBUTOR,ROLE_AUTHENTICATED'}">
						<li><a href="${pageContext.request.contextPath}/Master/RegisterUser"><i class="fa fa-user-secret"></i><span>Add Distributor</span></a></li>
					</c:when>
					<c:when test="${role eq 'ROLE_DISTRIBUTOR,ROLE_AUTHENTICATED'}">
						<li><a href="${pageContext.request.contextPath}/Master/RegisterUser"><i class="fa fa-user-secret"></i><span>Add Agent</span></a></li>
					</c:when>
				</c:choose>
				
				
				<c:choose>
					<c:when test="${role eq 'ROLE_SUPER_DISTRIBUTOR,ROLE_AUTHENTICATED' || role eq 'ROLE_DISTRIBUTOR,ROLE_AUTHENTICATED'}">
						<li>
		                    <a href="javascript: void(0);"><i class="fa fa-cog"></i><span>Prefund</span> <span class="menu-arrow"></span></a>
		                    <ul class="nav-second-level" aria-expanded="false">
		                        <li><a href="${pageContext.request.contextPath}/Master/RaisePrefundRequest">Raise Prefund Request</a></li>
		                        <li><a href="${pageContext.request.contextPath}/Master/ApprovePrefundRequestList">Pending Prefund Request List</a></li>
		                        <li><a href="${pageContext.request.contextPath}/Master/PrefundRequestHistory">Prefund Request History</a></li>
		                         <li><a href="${pageContext.request.contextPath}/Master/ReversePrefund">Reverse Prefund</a></li>
		                    </ul>
		                </li>
					</c:when>
					<c:when test="${role eq 'ROLE_SUPER_ADMIN,ROLE_AUTHENTICATED' || role eq 'ROLE_ADMINISTRATOR,ROLE_AUTHENTICATED'}">
						<li>
		                    <a href="javascript: void(0);"><i class="fa fa-cog"></i><span>Prefund</span> <span class="menu-arrow"></span></a>
		                    <ul class="nav-second-level" aria-expanded="false">
		                        <li><a href="${pageContext.request.contextPath}/Master/ApprovePrefundRequestList">Pending Prefund Request List</a></li>
		                        <li><a href="${pageContext.request.contextPath}/Master/PrefundRequestHistory">Prefund Request History</a></li>
		                    </ul>
		                </li>
					</c:when>
				</c:choose>
                <li>
                    <a href="javascript: void(0);"><i class="fa fa-bell-o"></i><span>Notification Centre</span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="${pageContext.request.contextPath}/Master/SendNotification">Send Notification</a></li>
                    </ul>
                </li>
                <c:choose>
                	<c:when test="${role eq 'ROLE_SUPER_ADMIN,ROLE_AUTHENTICATED' || role eq 'ROLE_ADMINISTRATOR,ROLE_AUTHENTICATED'}">
		                <li>
		                    <a href="javascript: void(0);"><i class="fa fa-cog"></i><span>Commission</span> <span class="menu-arrow"></span></a>
		                    <ul class="nav-second-level" aria-expanded="false">
		                        <li><a href="${pageContext.request.contextPath}/Master/SetCommission">Set commission</a></li>
		                        <li><a href="${pageContext.request.contextPath}/Master/CommissionSlabReport">Commission Report</a></li>
		                    </ul>
		                </li>
	                </c:when>
                </c:choose>    
                <c:if test="${role eq 'ROLE_SUPER_ADMIN,ROLE_AUTHENTICATED' || role eq 'ROLE_ADMINISTRATOR,ROLE_AUTHENTICATED'}">     
                <li>
                    <a href="javascript: void(0);"><i class="fa fa-code-fork"></i> <span>Version</span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <!-- <li><a href="${pageContext.request.contextPath}/Admin/AddVersion">Add Version</a></li> -->
                        <li><a href="${pageContext.request.contextPath}/Master/UpdateVersion">Update Version</a></li>
                    </ul>
                </li></c:if>
                <c:if test="${role eq 'ROLE_SUPER_ADMIN,ROLE_AUTHENTICATED' || role eq 'ROLE_ADMINISTRATOR,ROLE_AUTHENTICATED'}">
                	<li>
	                    <a href="javascript: void(0);"><i class="fa fa-cog"></i><span>Services</span> <span class="menu-arrow"></span></a>
	                    <ul class="nav-second-level" aria-expanded="false">
	                        <li><a href="${pageContext.request.contextPath}/Master/AddService">Add Service</a></li>
	                        <li><a href="${pageContext.request.contextPath}/Master/ServiceListData">Service List</a></li>
	                    </ul>
	                </li>
                </c:if>
                <c:if test="${role eq 'ROLE_SUPER_ADMIN,ROLE_AUTHENTICATED' || role eq 'ROLE_ADMINISTRATOR,ROLE_AUTHENTICATED'}">
                	<li>
	                    <a href="javascript: void(0);"><i class="fa fa-cog"></i><span>Category</span> <span class="menu-arrow"></span></a>
	                    <ul class="nav-second-level" aria-expanded="false">
	                        <li><a href="${pageContext.request.contextPath}/Master/AddCategory">Add Category</a></li>
	                        <li><a href="${pageContext.request.contextPath}/Master/CategoryList">Category List</a></li>
	                    </ul>
	                </li>
                </c:if>
                
                 <c:if test="${role eq 'ROLE_SUPER_ADMIN,ROLE_AUTHENTICATED'}">
                	<li>
	                    <a href="javascript: void(0);"><i class="fa fa-bars"></i><span>Menu</span> <span class="menu-arrow"></span></a>
	                    <ul class="nav-second-level" aria-expanded="false">
	                        <li><a href="${pageContext.request.contextPath}/Master/AddMenu">Add Menu</a></li>
<%-- 	                        <li><a href="${pageContext.request.contextPath}/Master/CategoryList">Category List</a></li> --%>
	                    </ul>
	                </li>
                </c:if>
                
                <li><a href="${pageContext.request.contextPath}/Master/ChangePassword"><i class="fa fa-user-secret"></i><span>Change Password</span></a></li>
            </ul>
            </c:otherwise>
            </c:choose>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->
</div>
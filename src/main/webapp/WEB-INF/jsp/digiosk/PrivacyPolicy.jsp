<!DOCTYPE html>
<html>
<head>
	<title>DigiOsk | Privacy Policy</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

  	<!-- Favicon -->
  	<link rel="icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png" type="image/x-icon" />

  	<!-- bootstrap css -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/bootstrap/css/bootstrap.css">

  	<!-- Slick slider -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/slick/slick.css">

  	<!-- fonts -->
  	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,900" rel="stylesheet">
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/css/style.css">
  	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/WEB-INF/jsp/digiosk/Header.jsp" />
	<section class="policy_wrp pb-5" style="padding-top: 100px; background: url(${pageContext.request.contextPath}/resources/digiosk-assets/img/shape-8.png); background-repeat: no-repeat; background-position: center; background-size: contain;">
		<div class="container">
			<div class="row clearfix mb-5">
				<div class="col-12">
					<div class="content_heading mb-0">
						<h2>Privacy Policy</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<b class="mb-2" style="display: block;">Our corporate office is situated at B-22, BASEMENT, IN FRONT OF BALAJI HOSPITAL, NEAR POLICE STATION, Bhadra, Rajasthan 335501, here in after is referred to as "DigiOsk". At DigiOsk, we give utmost value to your trust and to your privacy.</b>

					<h4 class="terms_seb_heading">Your privacy is important to us</h4>
					<p class="text-justify">This Privacy Policy provides to you with details about the manner in which your data is collected, stored and used by us. Please read this Privacy Policy carefully before using any of our services or products. By visiting DigiOsk's Web Applications or Mobile Applications you expressly give us consent to use and disclose your personal information in accordance with this Privacy Policy. If you do not agree to the terms of the policy, please do not use or access DigiOsk website, WAP site or mobile applications or any of our services. Kindly review this Privacy Policy periodically as we may change our policy or the terms contained in it without any prior notice. This Privacy Policy shall apply uniformly to DigiOsk website, DigiOsk mobile WAP site & DigiOsk mobile applications or any of our services.</p>

					<h4 class="terms_seb_heading">General</h4>
					<p class="text-justify">We will not sell, share or rent your personal information to any third party or use your email address/mobile number for unsolicited emails and/or SMS's. Any emails and/or SMS sent by DigiOsk will only be in connection with the provision of agreed services products and this Privacy Policy. We reserve the right to communicate your personal information to any third party that makes a legally-compliant request for its disclosure. General Statistical Information about DigiOsk its users, such as number of visitors, number and type of goods and services purchased, etc. may be revealed .</p>

					<h4 class="terms_seb_heading">Personal Information</h4>
					<p class="text-justify">Personal Information means and includes all information that can be linked to a specific individual or to identify any individual, such as name, address, mailing address, telephone number, email ID, credit card number, cardholder name, card expiration date, information about your mobile phone, DTH service, data card, electricity connection, Smart Tags and any details that may have been voluntarily provide by the user in connection with availing any of the services on DigiOsk When you browse through DigiOsk, we may collect information regarding the domain and host from which you access the internet, the Internet Protocol [IP] address of the computer or Internet service provider [ISP] you are using, and anonymous site statistical data.</p>

					<h4 class="terms_seb_heading">Use of Personal Information</h4>
					<p class="text-justify">We use personal information to provide you with services products you explicitly requested for, to resolve disputes, troubleshoot concerns, help promote safe services, collect money, measure consumer interest in our services, inform you about offers, products, services, updates, customize your experience, detect protect us against error, fraud and other criminal activity, enforce our terms and conditions, etc. We also use your contact information to send you offers based on your previous orders and interests. We may occasionally ask you to complete optional online surveys. These surveys may ask you for contact information and demographic information (like zip code, age, gender, etc.). We use this data to customize your experience at DigiOsk, providing you with content that we think you might be interested in and to display content according to your preferences.</p>

					<h4 class="terms_seb_heading">Cookies</h4>
					<p class="text-justify">A "cookie" is a small piece of information stored by a web server on a web browser so it can be later read back from that browser. DigiOsk uses cookie and tracking technology depending on the features offered. No personal information will be collected via cookies and other tracking technology; however, if you previously provided personally identifiable information, cookies may be tied to such information. Aggregate cookie and tracking information may be shared with third parties.</p>

					<h4 class="terms_seb_heading">Links to Other Sites</h4>
					<p class="text-justify">Our site links to other websites that may collect personally identifiable information about you. DigiOsk is not responsible for the privacy practices or the content of those linked websites.</p>

					<h4 class="terms_seb_heading">Security</h4>
					<p class="text-justify">DigiOsk has stringent security measures in place to protect the loss, misuse, and alteration of the information under our control. Whenever you change or access your account information, we offer the use of a secure server. Once your information is in our possession we adhere to strict security guidelines, protecting it against unauthorized access.</p>

					<h4 class="terms_seb_heading">Consent</h4>
					<p class="text-justify">By using DigiOsk and/or by providing your information, you consent to the collection and use of the information you disclose on DigiOsk in accordance with this Privacy Policy, including but not limited to your consent for sharing your information as per this privacy policy.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="downloadApp">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="app_txt text-center">
						<p>Can't wait ? Download app now and get started.</p>
					</div>
					<div class="app_img row">
						<div class="col-6 text-right">
							<a href="#"><img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/app.png"></a>
						</div>
						<div class="col-6">
							<a href="#"><img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/play.png"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<jsp:include page="/WEB-INF/jsp/digiosk/Footer.jsp" />
	<div class="backtoTop" style="display: none;"><img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/back_to_top.png" class="img-fluid"></div>
	<!-- Scripts starts here -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/js/jquery.min.js"></script>

	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>


	<!-- Custom Js -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/js/custom.js"></script>
</body>
</html>
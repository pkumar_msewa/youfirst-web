<footer>
		<div class="container">
			<div class="row foot_links">
				<div class="col-4">
					<div class="foot_nav">
						<span class="foo_head_links">Contact us</span>
						<div class="foo_slide">
							<ul>
								<li class="foo_loc">B-22, Basement, In Front of Balaji Hospital, Near Ploice Station, Bhadra, Rajasthan 335501</li>
								<li class="foo_mail"><a href="mailto:support@digiosk.com" title="Mail">support@imoney.com</a></li>
								<li class="foo_ph" title="Phone"><a href="tel:+08 6192 37710">+911161266265</a></li>
							</ul>
						</div>
						<!-- <div class="social_links">
							<ul>
								<li><a href="https://www.facebook.com/Digiosk-India-Private-Limited-361603804319236/" target="_blank" title="Facebook"><i class="fab fa-facebook-square"></i></a></li>
								<li><a href="javascript:void(0);" target="_blank" title="Instagram"><i class="fab fa-instagram"></a></i></li>
								<li><a href="javascript:void(0);" target="_blank" title="Twitter"><i class="fab fa-twitter-square"></i></a></li>
							</ul>
						</div> -->
					</div>
				</div>
				<div class="col-4">
					<div class="foot_nav">
						<span class="foo_head_links">About us</span>
						<div class="foo_slide">
							<ul>
								<li>As a leading digital payments enterprise, we aim to create the best payment experiences for Individuals, Corporates and Partner Merchants in a highly secure environment. We provide users to complete their daily transactions without having to reach for their physical wallets.<br/> 
								<a href="${pageContext.request.contextPath}/Aboutus" style="font-weight: 500;">Read More</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-4">
					<div class="foot_nav useful_links">
						<span class="foo_head_links">Useful links</span>
						<div class="foo_slide">
							<ul>
								<li><a href="${pageContext.request.contextPath}/Aboutus">About Us</a></li>
								<li><a href="javascript:void(0);">Offers</a></li>
								<li><a href="${pageContext.request.contextPath}/Terms&Conditions">Terms &amp; Conditions</a></li>
								<li><a href="${pageContext.request.contextPath}/PrivacyPolicy">Privacy Policy</a></li>
								<li><a href="javascript:void(0);">FAQs</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Copyright panel -->
		<div class="copyright_panel">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<p>Copyright &copy; <script type="text/javascript">document.write(new Date().getFullYear());</script> IMoney. All Right Reserved</p>
					</div>
				</div>
			</div>
		</div>
	</footer>
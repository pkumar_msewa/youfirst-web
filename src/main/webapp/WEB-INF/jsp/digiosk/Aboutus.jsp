<!DOCTYPE html>
<html>
<head>
	<title>IMoney | About Us</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

  	<!-- Favicon -->
  	<link rel="icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png" type="image/x-icon" />

  	<!-- bootstrap css -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/bootstrap/css/bootstrap.css">

  	<!-- Slick slider -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/slick/slick.css">

  	<!-- fonts -->
  	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,900" rel="stylesheet">
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/css/style.css">
  	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>
<body>

	<jsp:include page="/WEB-INF/jsp/digiosk/Header.jsp" />
	<section class="panel_wrapper"></section>

	<section class="pt-9 pb-6" style="background: url(${pageContext.request.contextPath}/resources/digiosk-assets/img/shape-8.png); background-repeat: no-repeat; background-position: center; background-size: cover;">
		<div class="container">
			<div class="row clearfix mb-5">
				<div class="col-12">
					<div class="content_heading mb-0">
						<h2>About Us</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="txt_about">
						<p class="text-justify"><strong>IMoney</strong> was started in 2018, our team has extensive experience across Banking, Payments and Technology.</p>

						<p class="text-justify">As a leading digital payments enterprise, we aim to create the best payment experiences for Individuals, Corporates and Partner Merchants in a highly secure environment. We provide users to complete their daily transactions without having to reach for their physical wallets.</p>

						<p class="text-justify">As we grow with you, the options that would be on offer through our partners will always be aimed at making your life simpler and offer a Payment Solution that you wouldn't want to let go. You can currently avail of services like Making Bill Payments for Electric, Gas and Phones (Mobile & Landline), Recharging Mobile Phones and DTH Connections, Making Travel Booking like airline tickets, hotel reservations, etc..</p>

						<p class="text-justify">Our support enables convergence of compliant domestic and cross-border payments, peer-to-peer, business-to-customer, business-to-business, merchant payments, shopping and other such transactions through a single platform.</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="downloadApp">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="app_txt text-center">
						<p>Can't wait ? Download app now and get started.</p>
					</div>
					<div class="app_img row">
						<div class="col-6 text-right">
							<a href="#"><img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/app.png"></a>
						</div>
						<div class="col-6">
							<a href="#"><img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/play.png"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<jsp:include page="/WEB-INF/jsp/digiosk/Footer.jsp" />

	<div class="backtoTop" style="display: none;"><img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/back_to_top.png" class="img-fluid"></div>


	<!-- Scripts starts here -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/js/jquery.min.js"></script>

	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>


	<!-- Custom Js -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/js/custom.js"></script>

</body>
</html>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page isELIgnored="false"%>
<%
   response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
   response.setDateHeader("Expires", 0);
   response.setHeader("Pragma", "no-cache");
%>
<!DOCTYPE html>
<html>
<head>
	<title>IMoney | Home</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

  	<!-- Favicon -->
  	<link rel="icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png" type="image/x-icon" />

  	<!-- bootstrap css -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/bootstrap/css/bootstrap.css">

  	<!-- Slick slider -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/slick/slick.css">
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/WOW-master/css/libs/animate.css">

  	<!-- fonts -->
  	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,900" rel="stylesheet">
  	<!-- <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet"> -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/css/style.css">
  	
  	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
  	
</head>
<body>

	<jsp:include page="/WEB-INF/jsp/digiosk/Header.jsp" />
	<!-- Hero section -->
	<section class="hero">
		<div class="container">
			<div class="row mb-10">
				<div class="col-7">
					<div class="hero-txt">
						<h1 class="txt1 wow fadeInUp" data-wow-delay="0.3s">It's everywhere you want to be.</h1>
						<h3 class="txt3 wow fadeInUp" data-wow-delay="0.4s">Enjoy the convenience of hassle-free processes while simultaneously increasing efficiency.</h3>
						<a href="#" class="btn btn-hero mt-3 wow fadeInUp" data-wow-delay="0.5s">Get Started</a>
					</div>
				</div>
				<div class="col-5">
					<div class="hero-icon text-left wow fadeInRight" data-wow-delay="0.7s">
						<img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/card-image.png" class="prod-img">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-12">
					<div class="content_heading">
						<h2>Our Features</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-6">
					<div class="feat_txt wow fadeInLeft" data-wow-delay="1s">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>

						<a href="#" class="btn btn-hero">Read More</a>
					</div>
				</div>
				<div class="col-6">
					<div class="feat_icons wow fadeInUp " data-wow-delay="1.15s">
						<center>
							<img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/features.png" class="img-fluid" style="width: 90%;">
						</center>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="services">
		<div class="container">
			<div class="row">
				<div class="col-5">
					<img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/card-image-dashboard.png" class="image-serv-one wow fadeInLeft " data-wow-delay="1.3s">
					<img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/shape-10.png" class="image-serv-two">
				</div>
				<div class="col-7 text-wrapper">
					<div class="theme-title wow fadeInDown" data-wow-delay="0.8s">
						<h2>Services makes your Payment simpler.</h2>
					</div>
					<ul>
						<li class="wow fadeInRight " data-wow-delay="1s">
							<h6>Mobile Recharge</h6>
							<p>Instant Online mobile recharge Iisque persius interesset his et, business operations and execution by adopting an identity platform that supports</p>
						</li>
						<li class="wow fadeInRight " data-wow-delay="1.3s">
							<h6>Bill Payment</h6>
							<p>Online Bill Payment Iisque persius interesset his et, operations and execution by adopting an identity platform that supports</p>
						</li>
						<li class="wow fadeInRight " data-wow-delay="1.5s">
							<h6>Fund Transfer</h6>
							<p>Fund Transfer Iisque persius interesset his et, operations and execution by adopting an identity platform that supports</p>
						</li>
					</ul>
				</div>
				
			</div>
		</div>
	</section>

	<section class="download-section">
		<div class="container">
			<div class="row">
				<div class="col-5">
					<img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/shape-10.png" class="shape-one">
					<img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/shape-11.png" class="shape-two">
					<img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/homePageImage.png" class="img-wrapper-two">
				</div>
				<div class="col-7">
					<div class="download_title wow fadeInDown " data-wow-delay="1s">
						<h2>Download Our App</h2>
						<p>Make payments, Move funds, Recharge your phone, DTH etc. in just a few taps with our IMoney App.</p>
					</div>
					<div class="download_now wow fadeInDown " data-wow-delay="1.3s">
						<a href="#" target="_blank" title="AppStore"><img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/app.png" class="down-one"></a>
						<a href="#" target="_blank" title="PlayStore"><img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/play.png" class="down-two"></a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<jsp:include page="/WEB-INF/jsp/digiosk/Footer.jsp" />

	<div class="backtoTop" style="display: none;"><img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/back_to_top.png" class="img-fluid"></div>


	<!-- Scripts starts here -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/js/jquery.min.js"></script>

	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

	<!-- Font Awesome -->

	<!-- Tweenmax js -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/WOW-master/dist/wow.min.js"></script>

	<!-- Slick Slider -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/slick/slick.js"></script>

	<!-- Custom Js -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/js/custom.js"></script>

</body>
</html>
<div class="container-fluid">
	<div class="btm_menu">
		<div class="menus_wrp text-center">
			<ul class="menu_line">
				<li class="single_menu child_br active">
					<div class="menu_one">
						<a href="${pageContext.request.contextPath}/User/Login/Process">
							<div class="menu_imgtxt">
								<img src="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/img/home.png">
								<span>Home</span>
							</div>
						</a>
					</div>
				</li>
				<li class="single_menu child_br">
					<div class="menu_one">
						<a href="${pageContext.request.contextPath}/User/PhysicalCardRequest">
							<div class="menu_imgtxt">
								<img src="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/img/pcard.png">
								<span>Physical Card</span>
							</div>
						</a>
					</div>
				</li>
				<li class="single_menu child_br">
					<div class="menu_one">
						<a href="${pageContext.request.contextPath}/User/Expenses">
							<div class="menu_imgtxt">
								<img src="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/img/txns.png">
								<span>Statement of Transaction</span>
							</div>
						</a>
					</div>
				</li>
				<%-- <li class="single_menu child_br">
					<div class="menu_one">
						<a href="">
							<div class="menu_imgtxt">
								<img src="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/img/pin.png">
								<span>Reset Pin</span>
							</div>
						</a>
					</div>
				</li> --%>
				<li class="single_menu child_br">
					<div class="menu_one">
						<a href="${pageContext.request.contextPath}/User/MyProfile">
							<div class="menu_imgtxt">
								<img src="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/img/profile.png">
								<span>Profile</span>
							</div>
						</a>
					</div>
				</li>
				<li class="single_menu">
					<div class="menu_one">
						<a href="${pageContext.request.contextPath}/User/Login/Logout">
							<div class="menu_imgtxt">
								<img src="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/img/logout.png">
								<span>Sign Out</span>
							</div>
						</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>
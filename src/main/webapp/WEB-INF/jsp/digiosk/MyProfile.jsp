<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page isELIgnored="false"%>
<%
   response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
   response.setDateHeader("Expires", 0);
   response.setHeader("Pragma", "no-cache");
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

	<title>DigiOsk | Transaction</title>

	<!-- favicon -->
  	<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png">
  	
	<!-- css starts here -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/css/style.css">

	<!-- century Gothic Font -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/CenturyGothic/styles.css">

	<link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet">
</head>
<body>

<!-- top nav -->
<div class="top_nav">
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span> 
	      </button>
	      <a class="navbar-brand" href="${pageContext.request.contextPath}/User/Login/Process">
	      	<img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/logo.png" class="img-responsive">
	      </a>
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
	      
	      <ul class="nav navbar-nav navbar-right">
	      	<li>
	        	<a href="#">
	        		<div class="welc_txtWrp text-right">
	        			<span class="welcom">Welcome</span><br>
	        			<span class="welcom_usrnme">${user.userDetail.firstName}</span>
	        		</div>
	        	</a>
	        </li>
	        <li>
	        	<a href="#">
	        		<img src="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/img/user.png" class="img-responsive">
	        	</a>
	        </li>
	      </ul>
	    </div>
	  </div>
	</nav>
</div>

<!-- Main Content -->
<div id="wrapper">
	<jsp:include page="/WEB-INF/jsp/digiosk/UserHeader.jsp" />
	<section id="cardSect">
		<div class="container-fluid">
			<div class="card_wrapper card-white">
				<div class="cdetails-wrp">
					<div class="phycard_head text-center">
						<h3>My Profile</h3>
					</div>
					<div class="row mt-30 profile_sec">
						<div class="col-sm-10 col-sm-offset-1">
							<div class="col-sm-7">
								<div class="card-white">
									<div class="prof_wrp">
										<div class="prof_heading">
											<h3>Profile Details</h3>
										</div>
										<div class="prof_body">
											<div class="row">
												<div class="col-sm-6">
													<div class="left">
                                                        <div class="m-20">
                                                            <h4 class="fw-600">Name</h4>
                                                        </div>
                                                        <div class="m-20">
                                                            <h4 class="fw-600">Email</h4>
                                                        </div>
                                                        <div class="m-20">
                                                            <h4 class="fw-600">Mobile No.</h4>
                                                        </div>
                                                        <!-- <div class="m-20">
                                                            <h4 class="fw-600">Card Status</h4>
                                                        </div> -->
                                                   	</div>
												</div>
												<div class="col-sm-6">
													<div class="left">
                                                        <div class="m-20">
                                                            <h4 class="fw-600">${user.userDetail.firstName} ${user.userDetail.lastName}</h4>
                                                        </div>
                                                        <div class="m-20">
                                                            <h4 class="fw-600">${user.userDetail.email}</h4>
                                                        </div>
                                                        <div class="m-20">
                                                            <h4 class="fw-600">+91 ${user.userDetail.contactNo}</h4>
                                                        </div>
                                                        <!-- <div class="m-20">
                                                            <h4 class="fw-600">Active</h4>
                                                        </div> -->
                                                   	</div>
												</div>
                                                <div class="upgrade_Acc">
                                                    <center><button class="btn btn-ekyc" type="button">Upgrade Account</button></center>
                                                </div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-5">
								<div class="card-white">
									<div class="reset_pwd-wrp">
										<div class="pwd_heading">
											<h3>Change Password</h3>
										</div>
										<div class="pwd_body">
											<form>
												<div class="form-group">
													<label for="" class="sr-only">Password</label>
													<input type="password" id="password" class="form-control" placeholder="Password" maxlength="6">
													<span class="error" id="error_Password" style="color: red;"></span>
												</div>
												<div class="form-group">
													<label for="" class="sr-only">New Password</label>
													<input type="password" id="newPassword" class="form-control" placeholder="New Password" maxlength="6">
													<span class="error" id="error_newPassword" style="color: red;"></span>
												</div>
												<div class="form-group">
													<label for="" class="sr-only">Confirm New Password</label>
													<input type="password" id="confirmPassword" maxlength="6" class="form-control" placeholder="Confirm Password">
													<span class="error" id="error_confirmPassword" style="color: red;"></span>
												</div>
												<div class="form-group">
													<span class="error" id="error_message" style="color: red;"></span>
													<span class="error" id="success_message" style="color: green;"></span>
												</div>
												<center><button class="btn btn-load" id="changePasswordBtn" type="button" onclick="changePassword();">Submit</button></center>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<!-- /Main Content -->

<!-- script starts here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/js/bootstrap.min.js"></script>
<script type="text/javascript">
function changePassword(){
	var cont_path = "${pageContext.request.contextPath}";
	var currentPassword=$('#password').val();
	var newPassword=$('#newPassword').val();
	var confirmPassword=$('#confirmPassword').val();
	var valid=true;
	if(currentPassword.length<=0){
		$("#error_Password").html("Enter Your Current Password.");
		valid = false;
	}
	if(newPassword.length<=0){
		$("#error_newPassword").html("Enter Your New Password.");
		valid = false;
	}
	if(confirmPassword.length<=0){
		$("#error_confirmPassword").html("Enter Confirm Password.");
		valid = false;
	}
	if(newPassword != confirmPassword){
		$("#error_confirmPassword").html("Password and confirm password does not match.");
		valid = false;
	}
	if(valid == true) {
		$('#changePasswordBtn').addClass("disabled");
		console.log("valid");
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : cont_path+"/Api/User/Windows/en/ChangePasswordWeb",
			dataType : 'json',
			data : JSON.stringify({
				"password" : "" + currentPassword + "",
				"newPassword":""+newPassword+"",
				"confirmPassword":""+confirmPassword+""
			}),
			success : function(response) {
				console.log(response);
				$('#changePasswordBtn').removeClass("disabled");
				$("#register_resend_otp").removeClass("disabled");
				if (response.code.includes("S00")) {
					 $("#newPassword").val("");
			    	 $("#confirmPassword").val("");
				     $("#password").val("");
					console.log(response.details)
					//swal("Success!!", response.details, "success");
					$('#success_message').html(response.details);
					$('#error_message').html('');
				}
				if(response.code.includes("F00")){
					/* swal({
						  type: 'error',
						  title: 'Sorry!!',
						  text: response.details
					}); */
					$('#error_message').html(response.details);
					$('#success_message').html('');
				}
			}
		});	
	}
	
	var timeout = setTimeout(function(){
    	$("#error_Password").html("");
        $("#error_newPassword").html("");
    	$("#error_confirmPassword").html("");
    	$("#frgtMessage_success").html("");
    }, 3000);
	
}
</script>
</body>
</html>
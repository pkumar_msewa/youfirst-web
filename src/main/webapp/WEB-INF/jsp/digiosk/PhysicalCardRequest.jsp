<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page isELIgnored="false"%>
<%
   response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
   response.setDateHeader("Expires", 0);
   response.setHeader("Pragma", "no-cache");
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

	<title>IMoney | Physical Card</title>

	<!-- favicon -->
  	<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png">
  	
	<!-- css starts here -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/css/style.css">

	<!-- century Gothic Font -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/CenturyGothic/styles.css">

	<link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet">
</head>
<body>

<!-- top nav -->
<div class="top_nav">
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span> 
	      </button>
	      <a class="navbar-brand" href="${pageContext.request.contextPath}/User/Login/Process">
	      	<img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/logo.png" class="img-responsive">
	      </a>
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
	      
	      <ul class="nav navbar-nav navbar-right">
	      	<li>
	        	<a href="#">
	        		<div class="welc_txtWrp text-right">
	        			<span class="welcom">Welcome</span><br>
	        			<span class="welcom_usrnme">${user.userDetail.firstName}</span>
	        		</div>
	        	</a>
	        </li>
	        <li>
	        	<a href="#">
	        		<img src="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/img/user.png" class="img-responsive">
	        	</a>
	        </li>
	      </ul>
	    </div>
	  </div>
	</nav>
</div>

<!-- Main Content -->
<div id="wrapper">
	<jsp:include page="/WEB-INF/jsp/digiosk/UserHeader.jsp" />
	<section id="cardSect">
		<div class="container-fluid">
			<div class="card_wrapper card-white">
				<div class="cdetails-wrp">
					<div class="phycard_head text-center">
						<h3>Physical Card</h3>
					</div>
					<div class="col-sm-12 blue_line mt-110">
						<div class="col-sm-8 col-sm-offset-1">
							<!-- <div class="col-sm-6">
								<div class="avail_bal">
									<span>Available Balance : </span>
									<span><i class="fa fa-inr"></i>&nbsp; <strong>50,547</strong></span>
								</div>
							</div> -->
							<div class="col-sm-4">
								<div class="load_mny text-center">
									<button class="btn btn-load"><i class="fa fa-plus-circle"></i>&nbsp; Apply Now</button>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="load_mny text-center">
									<button class="btn btn-load"><i class="fa fa-plus-circle"></i>&nbsp; Activate Now</button>
								</div>
							</div>
						</div>
					</div>
					<div class="row phys_card">
						<div class="col-sm-3 col-sm-offset-8">
							<div class="card">
                                <img src="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/img/card-dashboard.png" class="img-responsive">
                                <div class="card-details custm_pos text-trans">
                                	<div class="cvv text-right"><span class="c1">cvv</span>&nbsp;<span class="c2">${cardResponse.cvv}</span></div>
                                    <div class="card_num fs-20">${cardResponse.walletNumber}</div>
                                    <div class="exp_dt"><span class="c1">valid</span><br><span class="c2">${cardResponse.expiryDate}</span></div>
                                    <div class="card_holdr fs-20">${cardResponse.holderName}</div>
                                </div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<!-- /Main Content -->

<!-- script starts here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/js/bootstrap.min.js"></script>
</body>
</html>
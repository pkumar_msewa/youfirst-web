<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page isELIgnored="false"%>
<%
   response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
   response.setDateHeader("Expires", 0);
   response.setHeader("Pragma", "no-cache");
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

	<title>IMoney | Dashboard</title>

	<!-- favicon -->
  	<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png">

	<!-- css starts here -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/css/style.css">

	<!-- century Gothic Font -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/CenturyGothic/styles.css">

	<link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet">
</head>
<body>
	<!-- top nav -->
	<div class="top_nav">
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span> 
		      </button>
		      <a class="navbar-brand" href="${pageContext.request.contextPath}/User/Login/Process">
		      	<img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/logo.png" class="img-responsive">
		      </a>
		    </div>
		    <div class="collapse navbar-collapse" id="myNavbar">
		      
		      <ul class="nav navbar-nav navbar-right">
		      	<li>
		        	<a href="#">
		        		<div class="welc_txtWrp text-right">
		        			<span class="welcom">Welcome</span><br>
		        			<span class="welcom_usrnme">${user.userDetail.firstName}</span>
		        		</div>
		        	</a>
		        </li>
		        <li>
		        	<a href="#">
		        		<img src="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/img/user.png" class="img-responsive">
		        	</a>
		        </li>
		      </ul>
		    </div>
		  </div>
		</nav>
	</div>	
	<div id="wrapper">
		<jsp:include page="/WEB-INF/jsp/digiosk/UserHeader.jsp" />
		<section id="cardSect">
			<div class="container-fluid">
				<div class="card_wrapper card-white">
					<div class="cdetails-wrp">
						<div class="details_head text-center">
							<h3>Welcome to IMoney</h3>
						</div>
						<div class="col-sm-12 blue_line">
							<div class="col-sm-8 col-sm-offset-1">
								<div class="col-sm-6">
									<div class="avail_bal">
										<span>Total Balance : </span>
										<span><i class="fa fa-inr"></i>&nbsp; <strong>${balance}</strong></span>
										<!-- <span title="Click to view your Shared Balance" style="margin-left: 20px; cursor: pointer;" onclick="sharedBlc();">
											<i class="fa fa-share-alt" aria-hidden="true"></i>
										</span> -->
									</div>
								</div>
								<div class="col-sm-4">
									<div class="load_mny">
										<button class="btn btn-load" data-toggle="modal" data-target="#loadMny"><i class="fa fa-plus-circle"></i>&nbsp; Load Money</button>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3 col-sm-offset-8">
								<div class="card">
	                                <img src="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/img/card-dashboard.png" class="img-responsive">
	                                <div class="card-details custm_pos text-trans">
	                                	<div class="cvv text-right"><span class="c1" style="color:black;" >cvv</span>&nbsp;<span class="c2" style="color:black;">${resultSet.cardDetails.cvv}</span></div>
	                                    <div class="card_num fs-20" style="color:black;" >${resultSet.cardDetails.walletNumber}</div>
	                                    <div class="exp_dt"><span class="c1" style="color:black;" >valid</span><br><span class="c2" style="color:black;" >${resultSet.cardDetails.expiryDate}</span></div>
	                                    <div class="card_holdr fs-20" style="color:black;" >${resultSet.cardDetails.holderName}</div>
	                                </div>
	                            </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		</div>
		<!-- modal for load money -->
		<div id="loadMny" class="modal fade-scale" role="dialog">
		  <div class="modal-dialog modal-sm">
		
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Add Money to Card</h4>
		      </div>
		      <div class="modal-body">
		        <div class="row">
		        	<div class="col-sm-12">
		        		<div class="add_mnyWrp">
		        			<form>
		        				<div class="form-group">
		        					<label for="">Enter Money</label>
		        					<input type="text" name="" class="form-control" placeholder="0.00">
		        				</div>
		        				<center><button class="btn btn-primary">Add</button></center>
		        			</form>
		        		</div>
		        	</div>
		        </div>
		      </div>
		    </div>
		
		  </div>
		</div>
		
		<!-- Modal -->
		  <div class="modal fade" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Shared Balance</h4>
		        </div>
		        <div class="modal-body" id="sharedBlc">
		        	<table class="table table-bordered">
		        		
		        	</table>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
		      </div>
		    </div>
		  </div>
<!-- /Main Content -->

<!-- script starts here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/js/bootstrap.min.js"></script>
<script type="text/javascript">
	function sharedBlc() {
		var contextPath = "${pageContext.request.contextPath}";
		var username = "${user.username}";
		console.log("username>>>>>> "+username);
		var trHTML;
		$.ajax({
			type : "POST",
    		url:contextPath+"/User/SharedBalance/"+username,
    		dataType:"json",
   			contentType : "application/json",
   			data : JSON.stringify({
   				
   			}),
   			success : function(response) {
				console.log(response);
				if(response.code == "S00"){
					trHTML = '<thead>';
					trHTML = trHTML + '<tr>';
					trHTML = trHTML + '<th>' + response.details.DEFAULT+ '</th>';
					trHTML = trHTML + '</tr>';
					trHTML = trHTML + '</thead>';
					trHTML = trHTML + '<tbody>';
					trHTML = trHTML + '<tr>';
					trHTML = trHTML + '<td>' + response.details.DEFAULT.available.amount + '</td>' ;
					trHTML = trHTML + '</tr>';
					trHTML = trHTML + '</tbody>';
					$('#myModal').modal();
				}
			}
		});
	}
</script>
</body>
</html>
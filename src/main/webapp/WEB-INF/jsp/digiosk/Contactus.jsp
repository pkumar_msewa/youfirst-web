<!DOCTYPE html>
<html>
<head>
	<title>DigiOsk | Contact Us</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

  	<!-- Favicon -->
  	<link rel="icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png" type="image/x-icon" />

  	<!-- bootstrap css -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/bootstrap/css/bootstrap.css">

  	<!-- Slick slider -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/slick/slick.css">
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/WOW-master/css/libs/animate.css">

  	<!-- fonts -->
  	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,900" rel="stylesheet">
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/css/style.css">
  	
  	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>
<body>

	<jsp:include page="/WEB-INF/jsp/digiosk/Header.jsp" />

	<section class="contact_wrp" style="padding-top: 100px; background: url(${pageContext.request.contextPath}/resources/digiosk-assets/img/shape-8.png); background-repeat: no-repeat; background-position: center; background-size: contain;">
		<div class="container">
			<div class="row clearfix mb-5">
				<div class="col-12">
					<div class="content_heading mb-0">
						<h2>Contact Us</h2>
					</div>
				</div>
			</div>

			<div class="row contact_strip mb-5 clearfix">
				<div class="col-4">
					<div class="contact_location wow fadeInDown animated" data-wow-delay="1s">
						<div class="cont_icon">
							<i class="fas fa-map-marker-alt"></i>
						</div>
						<div class="cont_txt">
							<strong>B-22, BASEMENT,<br/> IN FRONT OF BALAJI HOSPITAL, NEAR POLICE STATION, Bhadra, Rajasthan 335501</strong>
						</div>
					</div>
				</div>
				<div class="col-4">
					<div class="contact_location wow fadeInDown animated" data-wow-delay="2s">
						<div class="cont_icon">
							<i class="fas fa-phone"></i>
						</div>
						<div class="cont_txt">
							<strong>+911161266265</strong>
						</div>
					</div>
				</div>
				<div class="col-4">
					<div class="contact_location wow fadeInDown animated" data-wow-delay="3s">
						<div class="cont_icon">
							<i class="fas fa-globe"></i>
						</div>
						<div class="cont_txt">
							<strong>
								<span>Email: </span><a href="mailto:support@digiosk.com">support@digiosk.com</a><br/>
								<span>Web: </span><a href="https://digiosk.in/" target="_blank">https://digiosk.in/</a>
							</strong>
						</div>
					</div>
				</div>
			</div>

			<div class="row clearfix mb-5">
				<div class="col-7">
					<div class="my_map">
						<div class="mapouter"><div class="gmap_canvas"><iframe width="100%" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q=digiosk&t=&z=9&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><style>.mapouter{text-align:left;height:400px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:400px;width:100%;}</style></div>
					</div>
				</div>
				<div class="col-5">
					<div class="qury_frm">
						<h2>Get In Touch</h2>
						<form>
							<div class="form-group">
								<label class="sr-only">Your Name</label>
								<input type="text" name="" class="form-control" placeholder="Your FullName*">
							</div>
							<div class="form-group">
								<label class="sr-only">Mail</label>
								<input type="email" name="" class="form-control" placeholder="Mail-Id*">
							</div>
							<div class="form-group">
								<label class="sr-only">Subject</label>
								<input type="text" name="" class="form-control" placeholder="Subject*">
							</div>
							<div class="form-group">
								<label class="sr-only">Your Message</label>
								<textarea class="form-control" placeholder="Type Your Message..." rows="5"></textarea>
							</div>
							<div class="form-group">
								<button class="btn btn-custom">Send</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="downloadApp">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="app_txt text-center">
						<p>Can't wait ? Download app now and get started.</p>
					</div>
					<div class="app_img row">
						<div class="col-6 text-right">
							<a href="#"><img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/app.png"></a>
						</div>
						<div class="col-6">
							<a href="#"><img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/play.png"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<jsp:include page="/WEB-INF/jsp/digiosk/Footer.jsp" />

	<div class="backtoTop" style="display: none;"><img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/back_to_top.png" class="img-fluid"></div>


	<!-- Scripts starts here -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/WOW-master/dist/wow.min.js"></script>

	<!-- Custom Js -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/js/custom.js"></script>

</body>
</html>
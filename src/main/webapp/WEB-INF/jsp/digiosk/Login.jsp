<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page isELIgnored="false"%>
<%
   response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
   response.setDateHeader("Expires", 0);
   response.setHeader("Pragma", "no-cache");
%>
<!DOCTYPE html>
<html>
<head>
	<title>IMoney | Login</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

  	<!-- Favicon -->
  	<link rel="icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png" type="image/x-icon" />

  	<!-- bootstrap css -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/bootstrap/css/bootstrap.css">
  	<!-- fonts -->
  	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,900" rel="stylesheet">
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/css/style.css">
  	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />
</head>
<body>

	<jsp:include page="/WEB-INF/jsp/digiosk/Header.jsp" />
	
	<input type="hidden" id="errormsg" value="${errormsg}">
	<input type="hidden" id="errormsgg" value="${errormsgg}">
	<section class="auth-section">
		<div class="container">
			<div class="row clearfix">
				<div class="col-5 custm_center" id="auth1">
					<div class="auth-wrapper">
						<div class="auth-logo text-center mb-5">
							<img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/logo.png" style="width: 40%;">
						</div>
						<c:if test="${not empty errormsgg}">
							<div class="form-group">
                           		<span style="color:red">${errormsgg}</span>
                        	</div>
						</c:if>
						<form class="login_form" action="${pageContext.request.contextPath}/User/Login/Process" method="Post">
							<div class="form-group">
								<label class="sr-only">Mobile Number</label> 
								<input type="text" id="username" name="username"
									placeholder="Enter Mobile Number" class="form-control"
									autocomplete="off" maxlength="10" onkeypress="return isNumberKey(event);">
							</div>
							<div class="form-group">
								<label class="sr-only">Password</label>
								<input type="password" id="password1" maxlength="20" name="password" placeholder="Enter Password" class="form-control" autocomplete="off">
								<div class="text-right">
									<small><a href="${pageContext.request.contextPath}/User/ForgetPassword" id="frgt_pwd">Forget Password?</a></small>
								</div>
							</div>
							<div class="form-group">
								<button class="btn btn-primary btn-block" type="submit">Sign In</button>
							</div>
							<div class="form-group new_usr text-center">
								<span>New User?&nbsp;<a href="javascript:void(0);" id="sign_up">Sign Up</a> now!</span>
							</div>
						</form>

						<!-- Forgot Pwd form -->
						<form class="forgt_form" style="display: none;">
							<div class="form-group">
								<label class="sr-only">Mobile Number</label>
								<input type="text" name="" placeholder="Enter Mobile Number" class="form-control">
							</div>
							<div class="form-group row">
								<div class="col-6">
									<button class="btn btn-warning btn-block cancl_btn" type="button" id="fgt_cancel">Cancel</button>	
								</div>
								<div class="col-6">
									<button class="btn btn-info btn-block nxt_btn" type="button">Next</button>
								</div>
							</div>
						</form>

						<!-- OTP form -->
						<form class="otp_form" style="display: none;">
							<div class="form-group">
								<label class="sr-only">Enter OTP</label>
								<input type="text" name="" placeholder="Enter OTP" class="form-control">
							</div>
							<div class="form-group">
								<label class="sr-only">New Password</label>
								<input type="text" name="" placeholder="Enter New Password" class="form-control">
							</div>
							<div class="form-group">
								<label class="sr-only">Confirm Password</label>
								<input type="text" name="" placeholder="Confirm Password" class="form-control">
							</div>
							<div class="form-group row">
								<div class="col-6">
									<button class="btn btn-warning btn-block resnd_btn" type="button">Resend OTP</button>	
								</div>
								<div class="col-6">
									<button class="btn btn-info btn-block ok_btn" type="button">Submit</button>
								</div>
							</div>
						</form>
					</div>
				</div>

				<div class="col-6 custm_center" id="auth2" style="display: none;">
					<div class="auth-wrapper">
						<div class="auth-logo text-center mb-5">
							<img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/logo.png" style="width: 180px;">
						</div>
						<c:if test="${not empty errormsg}">
							<div class="form-group">
                           		<span style="color:red">${errormsg}</span>
                        	</div>
						</c:if>
						<form:form action="${pageContext.request.contextPath}/User/Registration" id="registrationForm" modelAttribute="register" method="post">
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label class="sr-only">First Name</label>
										<form:input type="text" class="form-control" path="firstName" placeholder="Enter Firstname" id="firstName" maxlength="50" autocomplete="off" onkeypress="return isAlphKey(event);" />
										<span id="error_firstName" style="color:red; font-size:12px;"></span>
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<label class="sr-only">Last Name</label>
										<form:input type="text" class="form-control" path="lastName" placeholder="Enter Lastname" id="lastName" maxlength="50" autocomplete="off" onkeypress="return isAlphKey(event);" />
										<span id="error_lastName" style="color:red; font-size:12px;"></span>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col">
									<div class="form-group">
										<label class="sr-only">Email-Id</label>
										<form:input type="text" class="form-control" id="email" path="email" placeholder="Enter Email-Id" autocomplete="off" maxlength="60" />
										<span id="error_email" style="color:red; font-size:12px;"></span>
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<label class="sr-only">Mobile Number</label>
										<form:input type="text" class="form-control" id="phone" path="contactNo" maxlength="10" placeholder="Enter Mobile Number" onkeypress="return isNumberKey(event)"/>
										<span id="error_phone" style="color:red; font-size:12px;"></span>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col">
									<div class="form-group">
										<label class="sr-only">DOB</label>
										<form:input type="text" class="form-control" path="dateOfBirth" id="dob" placeholder="Enter DOB" autocomplete="off" readonly="true"/>
										<span id="error_dob" style="color:red; font-size:12px;"></span>
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<label class="sr-only">Password</label>
										<form:input type="password" id="password" path="password" class="form-control" maxlength="6"  placeholder="Enter password" autocomplete="off" />
										<span id="error_password" style="color:red; font-size:12px;"></span>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col">
									<div class="form-group">
										<label class="sr-only">Select Id</label>
										<form:select class="form-control" id="idType" path="idType" onchange="addMaxLength();">
											<option value="#">-- Select Id Type --</option>
											<option value=aadhaar>Aadhar Card </option>
				                            <option value="pan">Pan Card</option>
										</form:select>
										<span id="error_idType" style="color:red; font-size: 12px;"></span>
									</div>
								</div>
								
								<div class="col">
									<div class="form-group">
										<form:select class="form-control" id="genderId" path="gender">
											<option value="M">Male </option>
				                            <option value="F">Female</option>
										</form:select>
									</div>
								</div>
								
								<div class="col">
									<div class="form-group">
										<label class="sr-only">Id Number</label>
										<form:input type="text" class="form-control" id="idNo" path="idNo" autocomplete="off" maxlength="12" placeholder="Enter Id Number" />
										<span id="error_idNo" style="color:red; font-size:12px;"></span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<button class="btn btn-primary btn-block" type="button" onclick="validateForm();">SignUp</button>
							</div>
							<div class="text-center">
								<small>By signing up, You agree to our <a href="${pageContext.request.contextPath}/Terms&Conditions" target="_blank">Terms &amp; Conditions.</a></small>
							</div>
							<div class="form-group new_usr text-center">
								<span>Already have an account?&nbsp;<a href="javascript:void(0);" id="sign_in">Sign In</a> here!</span>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</section>

	<jsp:include page="/WEB-INF/jsp/digiosk/Footer.jsp" />

	<div class="backtoTop" style="display: none;"><img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/back_to_top.png" class="img-fluid"></div>


	<!-- Scripts starts here -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/js/jquery.min.js"></script>

	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>


	<!-- Tweenmax js -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax.min.js"></script>

	<!-- Custom Js -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/js/custom.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		$(document).ready(function() {
			var errormsg = $('#errormsg').val();
			var errormsgg = $('#errormsgg').val();
			if (errormsg != null && errormsg.length > 0) {
				$('#auth1').css('display' , 'none');
				$('#auth2').css('display' , 'block');
			}
			if (errormsgg != null && errormsgg.length > 0) {
				$('#auth1').css('display' , 'block');
				$('#auth2').css('display' , 'none');
			}
		});
		function isAlphKey(evt){
		    var charCode = (evt.which) ? evt.which : evt.keyCode
		    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
		}
		
		function isNumberKey(evt){
		    var charCode = (evt.which) ? evt.which : evt.keyCode
		    return !(charCode > 31 && (charCode < 48 || charCode > 57));
		}
		
		function validateForm() {
			var valid = true;
			var firstName = $('#firstName').val();
			var aadharPattern = "[0-9]{12}";
			var panPattern= "[A-Z]{5}[0-9]{4}[A-Z]{1}";
			var contactNoPattern = "[7-9]{1}[0-9]{9}";
			if (firstName.length<=0){
				valid = false;
				$('#error_firstName').html("Please enter first name.");
			}
			
			var lastName = $('#lastName').val();
			if (lastName.length<=0){
				valid = false;
				$('#error_lastName').html("Please enter last name.");
			}
			
			var email = $('#email').val();
			if (email.length<=0){
				valid = false;
				$('#error_email').html("Please enter email id.");
			}
			
			var phone = $('#phone').val();
			if (phone.length<=0){
				valid = false;
				$('#error_phone').html("Please enter phone no.");
			} else if (phone.length != 10){
				valid = false;
				$('#error_phone').html("Please enter 10 digit phone no.");
			} else if(phone.length == 10) {
				if(!phone.match(contactNoPattern)) {
	    			console.log("invalid contact no");
	    			$("#error_phone").html("Please enter valid contact number");
		    		valid = false; 
	    		}
			}
			var today = new Date();
	    	var dd = today.getDate();
	    	var mm = today.getMonth()+1;
	    	var yyyy = today.getFullYear();

	    	today = yyyy+'-'+"0"+mm+'-'+dd;
	    	console.log("current date:::::"+today);
	    	
			var dob  = $('#dob').val();
			if(dob.length  <= 0){
	    		$("#error_dob").html("Please select date of birth");
	    		valid = false; 
	    	} else if(!isValidDate(dob)){
	    		console.log("wrong date format");
	    		valid = false;
	    		$("#error_dob").html("Please choose the proper format.(yyyy-mm-dd)");
	    	} else if(new Date(dob) >= new Date("1999-12-31")) {
				$("#error_dob").html("You must be at least 18 years old to sign up");
				valid = false;
	    	}
			
	    	
			
			var password = $('#password').val();
			if (password.length<=0){
				valid = false;
				$('#error_password').html("Please enter password.");
			}
			
			var idType = $('#idType').val();
			if (idType == "#"){
				valid = false;
				$('#error_idType').html("Please choose id type.");
			} 
			
			var idNo = $('#idNo').val();
			if (idNo.length<=0){
				valid = false;
				$('#error_idNo').html("Please enter id no.");
			} else {
				if (idType == 'aadhaar') {
		    		if(idNo.length == 12) {
		    			if(!idNo.match(aadharPattern)) {
			    			$("#error_idNo").html("Please enter valid aadhaar number");
				    		valid = false; 
			    		}
		    		} else {
		    			$("#error_idNo").html("Please enter 12 digit aadhaar number");
			    		valid = false; 
		    		}
		    	} else if (idType == 'pan') {
		    		if(idNo.length == 10) {
		    			if(!idNo.match(panPattern)) {
			    			$("#error_idNo").html("Please enter valid pan number");
				    		valid = false; 
			    		}
		    		} else {
		    			$("#error_idNo").html("Please enter 10 digit pan number");
			    		valid = false; 
		    		}
		    	}
			}
			if(valid){
				$('#registrationForm').submit();
			}
			var timeout = setTimeout(function(){
				$('#error_firstName').html("");
				$('#error_lastName').html("");
				$('#error_email').html("");
				$('#error_phone').html("");
				$('#error_dob').html("");
				$('#error_password').html("");
				$('#error_idNo').html("");
				$('#error_idType').html("");
		    }, 3000);
		}
		
		function addMaxLength() {
			var idType = $('#idType').val();
			var idNo = $('#idNo').val();
			if (idType == 'aadhaar'){
				$('#idNo').attr('maxlength','12');
			} else if (idType == 'pan') {
				$('#idNo').attr('maxlength','10');
			}
		}
		
		$('#dob').datepicker({
            uiLibrary: 'bootstrap4',
            format : 'yyyy-mm-dd',
        });
		
		function isValidDate(dateString) {
			if(dateString.length>0){
				  var regEx = /^\d{4}-\d{2}-\d{2}$/;
				  if(!dateString.match(regEx)) return false;  // Invalid format
				  var d = new Date(dateString);
				  if(!d.getTime() && d.getTime() !== 0) return false; // Invalid date
				  return d.toISOString().slice(0,10) === dateString;
			}
		}
		
	</script>

</body>
</html>
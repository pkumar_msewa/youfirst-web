<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page isELIgnored="false"%>
<%
   response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
   response.setDateHeader("Expires", 0);
   response.setHeader("Pragma", "no-cache");
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

	<title>IMoney | Transaction</title>

	<!-- favicon -->
  	<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png">
  	
	<!-- css starts here -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/css/style.css">

	<!-- Datatables -->
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css">

	<!-- century Gothic Font -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/CenturyGothic/styles.css">

	<link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet">
</head>
<body>

<!-- top nav -->
<div class="top_nav">
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span> 
	      	</button>
	      	<a class="navbar-brand" href="${pageContext.request.contextPath}/User/Login/Process">
	      		<img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/logo.png" class="img-responsive">
	      	</a>
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
	      
	      <ul class="nav navbar-nav navbar-right">
	      	<li>
	        	<a href="#">
	        		<div class="welc_txtWrp text-right">
	        			<span class="welcom">Welcome</span><br>
	        			<span class="welcom_usrnme">${user.userDetail.firstName}</span>
	        		</div>
	        	</a>
	        </li>
	        <li>
	        	<a href="#">
	        		<img src="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/img/user.png" class="img-responsive">
	        	</a>
	        </li>
	      </ul>
	    </div>
	  </div>
	</nav>
</div>

<!-- Main Content -->
<div id="wrapper">
	<jsp:include page="/WEB-INF/jsp/digiosk/UserHeader.jsp" />
	<section id="cardSect">
		<div class="container-fluid">
			<div class="card_wrapper card-white">
				<div class="cdetails-wrp">
					<div class="phycard_head text-center" style="margin-bottom: 38px;">
						<h3>Statement of Transaction</h3>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="col-sm-3">
						      <ul class="nav nav-pills nav-stacked">
						        <li class="active text-center"><a data-toggle="tab" href="#PCard">Physical Card</a></li>
						        <li class="text-center"><a data-toggle="tab" href="#VCard">Virtual Card</a></li>
						      </ul>
						    </div>
						    <div class="col-sm-9">
						    	<div class="tab-content">
								  <div id="PCard" class="tab-pane fade in active">
								    <table id="Ptable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
								        <thead>
								            <tr>
								                <th>Sl No</th>
								                <th>Date/Time</th>
								                <th>Transaction Type</th>
								                <th>Description</th>
								                <th>Amount</th>
								                <th>Status</th>
								            </tr>
								        </thead>
								        <tbody>
								            <c:forEach items="${Ptransactions}" var="card"   varStatus="loopCounter">
								            	<tr>
													<td>${loopCounter.count}</td>
													<td> <c:out value="${card.date}" default="" escapeXml="true" /></td>
													<c:choose>
													<c:when test="${card.transactionType == 'credit'}">
														<td style="color: #0cce0c;"> <c:out value="${card.transactionType}" default="" escapeXml="true" /></td>
													</c:when>
													<c:otherwise>
														<td style="color: #f10c1b;"> <c:out value="${card.transactionType}" default="" escapeXml="true" /></td>
													</c:otherwise>
													</c:choose>	
														<td><c:out value="${card.description}" default="" escapeXml="true" /></td>
														<td> <c:out value="${card.amount}" default="" escapeXml="true" /></td>
													<td> <c:out value="${card.status}" default="" escapeXml="true" /></td>
												</tr>
								            </c:forEach>
								        </tbody>
								    </table>
								  </div>
								  <div id="VCard" class="tab-pane fade">
								    <div id="P_card" class="tab-pane fade in active">
									    <table id="Vtable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
									        <thead>
									            <tr>
									                <th>Sl No</th>
									                <th>Date/Time</th>
									                <th>Transaction Type</th>
									                <th>Description</th>
									                <th>Amount</th>
									                <th>Status</th>
									            </tr>
									        </thead>
									        <tbody>
								            	<c:forEach items="${Vtransactions}" var="cards"  varStatus="loopCounter">
													<tr>
														<td>${loopCounter.count}</td>
														<td> <c:out value="${cards.date}" default="" escapeXml="true" /></td>
															<c:choose>
														<c:when test="${cards.transactionType == 'credit'}">
															<td style="color: #0cce0c;"> <c:out value="${cards.transactionType}" default="" escapeXml="true" /></td>
														</c:when>
														<c:otherwise>
															<td style="color: #f10c1b;"> <c:out value="${cards.transactionType}" default="" escapeXml="true" /></td>
														</c:otherwise>
														</c:choose>
															<td><c:out value="${cards.description}" default="" escapeXml="true" /></td>
															<td> <c:out value="${cards.amount}" default="" escapeXml="true" /></td>
														<td> <c:out value="${cards.status}" default="" escapeXml="true" /></td>
													</tr>
												</c:forEach>
									        </tbody>
									    </table>
									  </div>
								  </div>
								</div>
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<!-- /Main Content -->

<!-- script starts here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/ui-assets/assets/js/bootstrap.min.js"></script>

<!-- Datatables -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>

<script>
	$(document).ready(function() {
	    $('#Ptable').DataTable({
	    	responsive: true,
	    	searching: false,
	    	info: false,
	    	paging: false
	    });
	} );
</script>
<script>
	$(document).ready(function() {
	    $('#Vtable').DataTable({
	    	responsive: true,
	    	searching: false,
	    	info: false,
	    	paging: false
	    });
	} );
</script>
</body>
</html>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page isELIgnored="false"%>
<%
   response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
   response.setDateHeader("Expires", 0);
   response.setHeader("Pragma", "no-cache");
%>
<!DOCTYPE html>
<html>
	<head>
		<title>DigiOsk | Verify Mobile</title>
		<meta charset="utf-8">
	  	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	  	<!-- Favicon -->
	  	<link rel="icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png" type="image/x-icon" />
	
	  	<!-- bootstrap css -->
	  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/bootstrap/css/bootstrap.css">
	  	<!-- fonts -->
	  	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,900" rel="stylesheet">
	  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/digiosk-assets/css/style.css">
	  	<!-- Font Awesome -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	</head>
	<body>
		<!-- header -->
		<jsp:include page="/WEB-INF/jsp/digiosk/Header.jsp" />
		<input type="hidden" value="${successMessage}" id="successMsg">
		<section class="auth-section">
			<div class="container">
				<div class="row clearfix">
					<div class="col-5 custm_center" id="auth1">
						<div class="auth-wrapper">
							<div class="auth-logo text-center mb-5">
								<img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/logo.png" style="width: 180px;">
							</div>
	
							<!-- Forgot Pwd form -->
							<form:form class="forgt_form" action="${pageContext.request.contextPath}/User/Activate/Mobile" id="otpForm">
								<div class="form-group">
									<label class="sr-only">OTP</label>
									<input type="text" name="key" id="key" placeholder="Enter OTP" maxlength="6" class="form-control"  onkeypress="return isNumberKey(event)">
									<input type="hidden" name="mobileNumber" value="${username}">
									<span id="error_key" style="color:red;"></span>
								</div>
								<c:if test="${not empty errormsg}">
									<div class="form-group">
										<span style="color:red;">${errormsg}</span>
									</div>
								</c:if>
								<div class="form-group row">
									<div class="col-6">
										<button class="btn btn-warning btn-block cancl_btn" type="button" id="resendotp" onclick="Resend();">Resend</button>	
									</div>
									<div class="col-6">
										<button class="btn btn-info btn-block nxt_btn" id="submitBtn" type="button" onclick="formSubmit();">Submit</button>
									</div>
									<div id="frgtMessage_success"></div>
                     		   		<div id="fpMessage"></div>
								</div>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<!-- footer -->
		<jsp:include page="/WEB-INF/jsp/digiosk/Footer.jsp" />
		
		<div class="backtoTop" style="display: none;"><img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/back_to_top.png" class="img-fluid"></div>
		
		<!-- Scripts starts here -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/js/jquery.min.js"></script>
	
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	
	
		<!-- Tweenmax js -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax.min.js"></script>
	
		<!-- Custom Js -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/digiosk-assets/js/custom.js"></script>
	
		<script>
			var contextPath = "${pageContext.request.contextPath}";
			$(document).ready(function() {
				var successMsg = $('#successMsg').val();
				if(successMsg != null && successMsg.length > 0) {
					swal({
						  title: "Congrats!!",
						  text: "Account Created Successfully",
						  icon: "success",
						  button: "Okay",
						  closeOnClickOutside: false,
						}).then((Okay) => {
							window.location.href = contextPath + "/User/Login";
						});
					}
				});
			
			function isNumberKey(evt){
			    var charCode = (evt.which) ? evt.which : evt.keyCode
			    return !(charCode > 31 && (charCode < 48 || charCode > 57));
			}
			
			function formSubmit() {
				var valid = true;
				var key = $('#key').val();
				if(key.length <=0){
					valid = false;
					$('#error_key').html("Please enter OTP.")
				}
				if(valid){
					$('#otpForm').submit();
				}
				var timeout = setTimeout(function(){
					$('#error_key').html("");
			    }, 3000);
			}
			
			function Resend(){
				$('#resendotp').addClass("disabled");
				$('#submitBtn').addClass("disabled");
				var dup=${username};
				var username=$('#mobileNumber').val();
				console.log("username:"+username+" "+dup);
				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : contextPath+"/Api/User/Windows/en/Resend/Mobile/OTP",
					dataType : 'json',
					data : JSON.stringify({
						"mobileNumber" : "" + dup + ""
					}),
					success : function(response) {
						console.log(response);
						$('#resendotp').removeClass("disabled");
						$('#submitBtn').removeClass("disabled");
						if (response.code.includes("S00")) {
							console.log(response.details)
							$("#fpMessage").html("");
							$("#frgtMessage_success").html(response.details);
							$("#frgtMessage_success").css("color", "green");
						}
						if(response.code.includes("F00")){
							$("#frgtMessage_success").html(response.details);
							$("#frgtMessage_success").css("color", "red");;
						}
					}
				});	
			}
		</script>
		
	</body>
</html>
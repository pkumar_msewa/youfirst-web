<header>
		<!-- Disosk Navbar -->
		<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
	  		<div class="container">
		  		<a class="navbar-brand" href="${pageContext.request.contextPath}/Home">
		  			<img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/logo.png" alt="IMoney" title="IMoney" style="width: 120px;">
		  		</a>
			  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			    	<span class="navbar-toggler-icon"></span>
			  	</button>
	
			  	<div class="collapse navbar-collapse" id="navbarSupportedContent">
			    	<ul class="navbar-nav ml-auto">
				      	<li class="nav-item">
				        	<a class="nav-link" href="${pageContext.request.contextPath}/Aboutus" title="About Us">About Us</a>
				      	</li>
				      	<li class="nav-item">
				        	<a class="nav-link" href="javascript:void(0);" title="Features">Features</a>
				      	</li>
				      <%-- 	<li class="nav-item">
				        	<a class="nav-link" href="${pageContext.request.contextPath}/ContactUs" title="Contact Us">Contact Us</a>
				      	</li> --%>
				      	<li class="nav-item">
				        	<a class="nav-link nav-btn" href="${pageContext.request.contextPath}/User/Login" title="SignIn"><i class="fas fa-user"></i>Sign in</a>
				      	</li>
			    	</ul>
			  	</div>
		  	</div>
		</nav>
	</header>
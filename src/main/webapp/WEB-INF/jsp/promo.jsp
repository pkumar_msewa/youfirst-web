<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<head>
    	<!-- meta -->
    	<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="Cashier Comming Soon">
		<meta name="author" content="">
        <meta name="keywords" content="Cashier, participate, countdown">
        
        <!-- title -->
    	<title>Cashier</title>
		
        <!-- ============ CSS IMPORT ============ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/demo/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/demo/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/demo/css/animate.min.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/demo/css/simple-line-icons.css" type="text/css">
        
        <!-- ============ Fonts ============ -->
        

        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/demo/CenturyGothic/styles.css" type="text/css">
        
        <!-- ============ Main CSS ============ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/demo/css/style.css" type="text/css">
        <link rel="stylesheet" id="current-theme" href="${pageContext.request.contextPath}/resources/demo/css/theme-3.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/demo/css/custom.css" type="text/css">
    	<!--[if lt IE 9]>
      	<script src="js/html5shiv.js"></script>
      	<script src="js/respond.min.js"></script>
    	<![endif]-->		
    	
    	<style>
    		.modal-content {
    			border-radius: 0;
    			width: 350px;
    			margin-top: 30%;
    			box-shadow: 0 5px 20px 17px rgba(0, 0, 0, 0.12);
    			-webkit-box-shadow: 0 5px 20px 17px rgba(0, 0, 0, 0.12);
    		}
    		#succ_msg h3 {
    			color: #751844;
			    font-size: 18px;
			    line-height: 24.5px;
    		}
    		#succ_msg .modal-title {
    			color: #000;
    		}
    	</style>
  	</head>
    <body>
    	
    	<!-- preloader -->
        <div id="preloader" class="animated">
        	<div class="loader"></div>
        </div>
        <!-- end preloader -->

       <div class="row">
           <div class="col-md-12">
                <!-- Round Head -->
                <div class="roundHead">
                    <div class="terms_privacy">
                        <span><a href="${pageContext.request.contextPath}/Aboutus">About Us</a></span>&nbsp;|&nbsp;<span><a href="${pageContext.request.contextPath}/TermsnConditions">Terms &amp; Conditions</a></span>&nbsp;|&nbsp;<span><a href="${pageContext.request.contextPath}/PrivacyPolicy">Privacy Policy</a></span>|&nbsp;<span><a href="${pageContext.request.contextPath}/ContactUs">Contact Us</a></span>
                    </div>
                    <img src="${pageContext.request.contextPath}/resources/demo/img/roundhead.svg" class="img-responsive">
                    <div class="official">
                        <img src="${pageContext.request.contextPath}/resources/demo/img/partner-black.png" class="img-responsive">
                    </div>
                    <div class="top-logo">
                        <center><img src="${pageContext.request.contextPath}/resources/demo/img/logo.svg" class="img-responsive"></center>
                    </div>
                </div>
           </div>
       </div>
        <!-- Round Head -->
                
        <!-- overlay -->
        <div class="overlay"></div>
        <!-- end overlay -->

        <!-- Black overlay -->
        <div class="overlay_black"></div>
        <!-- end overlay -->
        
        <!-- home wrapper -->
        <div id="home-wrapper" class="animated">
        	<div class="container">
            	<div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 home-container">
                        <!-- header -->
                        <div class="animated" id="header">
                            <div class="header-content">
                                <div class="container">
                                    <div class="countdown text-center">
                                        <h1 class="animated" style="text-transform: uppercase;">Beginning our innings with IPL in</h1>
                                        <span id="clock"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end header -->
                    </div>
                        <!-- home -->
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                
                                <!-- more info link -->
                                <center>
                                    <div class="info-link hover-effect-1 text-center hide-after animated">
                                        <a href="javascript:void(0)" class="more-info">
                                            PARTICIPATE NOW
                                        </a>
                                    </div>
                                </center>
                                <!-- end more info link -->
                            </div>
                        </div>
                        <!-- home -->
                    </div>
                </div>
            </div>
        </div>
        <!-- end home wrapper -->
        	
        <!-- info wrapper -->
        <div id="info-wrapper">
           	<div class="container-fluid">
            	<div class="col-lg-12 info-container">
                	<!-- close button -->
                    <div class="close-link hover-effect-1 animated">
                    	<a href="javascript:void(0)" class="close-info"><span aria-hidden="true" class="icon-arrow-up"></span>Home</a>
                    </div>
                    <!-- end close button -->
                    
                    <!-- about us -->
                    <div class="about animated">
                    	<div class="info-logo">
                            <img src="${pageContext.request.contextPath}/resources/demo/img/logo.svg" class="img-responsive" style="width: 180px;">
                        </div>
                        
                        <!-- cards -->
                            <div class="mt-50 col-lg-6 col-md-6 col-xs-hidden">
                                <div class="card-img mb-25"><img src="${pageContext.request.contextPath}/resources/demo/img/cards.png" class="img-responsive" style="width: 80%;"></div>
                            </div>
                            <!-- end cards -->
                            
                            <!-- team -->
                            <div class="col-lg-6 col-md-6 col-xs-12">
                                <div class="team-headings">
                                    <h3>GET A CHANCE TO WATCH</h3>
                                    <h2>IPL LIVE!</h2>
                                </div>
                                <div class="team-img col-lg-10 col-md-offset-1">
                                    <img src="${pageContext.request.contextPath}/resources/demo/img/team-img.png" class="img-responsive">
                                </div>
                                <div class="btn-participate">
                                    <a href="#participate_form" class="btn btn-primary">Participate Now</a>
                                </div>
                            </div>
                            <!-- end team -->
                    </div>
                    <!-- end about us -->
                    
                    <!-- subscribe -->
                    <div class="container">
                        <div class="subscribe animated" id="participate_form">
                            <div class="col-lg-12 col-md-12 col-xs-12 subscribe-container">
                                <h3 class="info-title">PARTICIPATE HERE</h3>
                                
                                <div class="col-lg-10 col-md-12 col-lg-offset-1 col-xs-12">
                                    <div class="col-lg-12 col-md-12 col-xs-12 subscribe-form-container">
                                        <!-- form -->
                                        <form class="subscribe-form" action="#" method="post" enctype="multipart/form-data">
                                            <div class="col-md-6 col-xs-12">
                                                <input autocomplete="off" id="firstName" onkeyup="validateName()" type="text" name="" placeholder="First Name" required />
                                                                                        <span id="firstError" style="color:red; font-size:12px;"></span>
                                            
                                            </div>
                                            
                                            <div class="col-md-6 col-xs-12">
                                                <input autocomplete="off" id="lastName" onkeyup="validateLastName()" type="text" name="" placeholder="Last Name" required />
                                                                                      <span id="lastError" style="color:red; font-size:12px;"></span>
                                          
                                            </div>
                                            
                                            <div class="col-md-6 col-xs-12">
                                                <input autocomplete="off" id="contactNo" onkeyup="validateMobile()" type="text" name="" placeholder="Mobile" maxlength="10" pattern="[0-9.]+"  autocomplete="off" onkeypress="return isNumberKey(event)"required/>
                                                                                      <span id="ferror" style="color:red; font-size:12px;"></span>
                                           
                                            </div>
                                            
                                            <div class="col-md-6 col-xs-12">
                                                <input autocomplete="off" id="email" type="email" onkeyup="validateEmail()" name="" placeholder="Email" required/>
                                                                                       <span id="mailError" style="color:red; font-size:12px;"></span>
                                           
                                            </div>
                                            
                                            <div class="col-md-6 col-md-offset-3 col-xs-12">
                                                <button type="button" name="submit" id="notifyme" onclick="notifyMe()" class="btn-block submit-subscribe" style="margin-top: 20px;" disabled="disabled">NOTIFY ME</button>
                                            </div>
                                        </form>
                                        <!-- end form -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end subscribe -->
                    
                </div>
            </div>
            <!-- Modal -->
			<div id="succ_msg" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
			  <div class="modal-dialog">
			
			    <center>
			    	<!-- Modal content-->
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal">&times;</button>
					        <center><h4 class="modal-title">THANK YOU FOR PARTICIPATING !</h4></center>
					      </div>
					      <div class="modal-body">
					      	<div class="succ_img">
					      		<center><img alt="success" src="${pageContext.request.contextPath}/resources/images/checked.png"></center>
					      	</div>
					        <h3>Download the app and be among the top 100 users to win IPL tickets!</h3>
					        <hr>
					        <small style="color: red;">*App will be available at play store by 26th March</small>
					      </div>
					    </div>
			    </center>
			
			  </div>
			</div>
        </div>
        <!-- end info wrapper -->
        
        
        <!-- ============ JAVASCRIPT IMPORT =============== -->       
        
        <!-- jquery -->
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/demo/js/jquery.js"></script>
        <!-- bootstrap -->
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/demo/js/bootstrap.min.js"></script>
        <!-- countdown -->
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/demo/js/jquery.lwtCountdown-1.0.js"></script>
        <!-- backstretch -->
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/demo/js/jquery.backstretch.min.js"></script>
        <!-- jquery validate -->
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/demo/js/jquery.validate.min.js"></script>
        <!-- easing -->
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/demo/js/jquery-easing.js"></script>
        <!-- countdown -->
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/demo/js/jquery.countdown.min.js"></script>
        <!-- main js -->
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/demo/js/main.js"></script>


 <script type="text/javascript">
    $('#firstName, #lastName, #email, #contactNo').bind('keyup', function() {
        if(allFilled()) $('#notifyme').removeAttr('disabled');
    });

    function allFilled() {
        var filled = true;
        $('form input').each(function() {
            if($(this).val() == '') filled = false;
        });
        return filled;
    }
        </script>
<script type="text/javascript">

var ph=true;

function validateName() {
var fname = $("#firstName").val();
if(fname==null || fname=="")
{
	$("#firstError").html("name cannot be blank");
ph=false;
}
else{
	$("#firstError").html("");
	ph=true;
}
}

function validateLastName() {
var lname = $("#lastName").val();
if(lname==null || lname=="")
{
	$("#lastError").html("Last name cannot be blank");
ph=false;
}
else{
	$("#lastError").html("");
	ph=true;
}
}

function validateEmail() {
var myEmail = $("#email").val();
var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

if(!re.test(String(myEmail).toLowerCase())){
	$("#mailError").html("enter valid email id");
ph=false;
}

else{
	$("#mailError").html("");
	ph=true;
}
}

function validateMobile(){
var number = $("#contactNo").val();
var re1 =  /(7|8|9)\d{9}/;
if(!re1.test(String(number).toLowerCase())||number==""||number==null)
{
$("#ferror").html("Enter valid 10 digits Number");
ph=false;
}
else{
  $("#ferror").html("");
     ph=true;
}
}


</script>

<script type="text/javascript">
            function isNumberKey(evt)
            {
              var charCode = (evt.which) ? evt.which : evt.keyCode;
              if (charCode != 46 && charCode > 31  && (charCode < 48 || charCode > 57))
              return false;
              else{

              return true;
              }

             }
          </script>


        <script type="text/javascript">
            function notifyMe(){
                var email=$('#email').val();
                var firstName=$('#firstName').val();
                var lastName=$('#lastName').val();
                var contactNo=$('#contactNo').val();
                
                $.ajax({
                    type : "POST",
                    contentType : "application/json",
                    url : "${pageContext.request.contextPath}/RegisterDemo",
                    dataType : 'json',
                    data : JSON.stringify({
                        "email" : "" + email + "",
                        "firstName" : "" + firstName + "",
                        "lastName":""+lastName+"",
                        "contactNo":""+contactNo+""
                    }),
                    success : function(response) {
                    	 $("#succ_msg").modal()
                    	// Set a timeout to hide the element again
        /* setTimeout(function(){
            $("#succ_msg").hide();
        }, 100000); */
                        console.log(response);
                        $('#email').val('');
                        $('#firstName').val('');
                        $('#lastName').val('');
                        $('#contactNo').val('');
                        
                    }
            });
            }
        
        </script>
        
    </body>

</html>
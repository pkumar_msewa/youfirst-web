<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

	<title>Cashier Card | Help</title>
	<!-- css starts here -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/newui/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/newui/assets/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/newui/assets/css/style.css">
	
	<!-- App Favicon -->
         <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/User/assets/images/favicon.png">
	

	<!-- century Gothic Font -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/newui/assets/CenturyGothic/styles.css">

	<link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet">
	<script type="text/javascript">
var context_path="${pageContext.request.contextPath}";
</script>
</head>
<body>
	
<!-- top nav -->
<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />

<!-- Main Content -->
	<div id="wrapper">
	<jsp:include page="/WEB-INF/jsp/User/Menu.jsp" />
	

	<section id="cardSect">
		<div class="">
			<div class="card_wrapper card-white bg-2">
				<div class="cdetails-wrp">
					<div class="phycard_head text-center">
						<h3>Need Help?</h3>
					</div>
					<div class="row contact_sect">
						<div class="col-md-3 col-sm-6 col-xs-12 col-md-offset-2">
							<div class="card-white pd-20 br-12 mt-30">
								<div class="row">
									<div class="col-md-3 col-sm-3">
										<div>
											<img src="${pageContext.request.contextPath}/resources/newui/assets/img/email.png" class="img-responsive">
										</div>
									</div>
									<div class="col-sm-9">
										<div>
											<h2 style="font-size: 25px;">Mail</h2>
		                                    <p>For any support-related queries, email us at:</p>
		                                    <p><a href="mailto:">care@madmoveglobal.com</a></p>
										</div>
									</div>
								</div>
							</div>
							<br>
							<div class="card-white pd-20 br-12">
								<div class="row">
									<div class="col-sm-3">
										<div>
											<img src="${pageContext.request.contextPath}/resources/newui/assets/img/telephone.png" class="img-responsive">
										</div>
									</div>
									<div class="col-sm-9">
										<div>
											<h2 style="font-size: 25px;">Phone</h2>
		                                    <p>Call us between 10 AM - 7 PM all days, except public holidays</p>
		                                    <p><a href="javascript:void(0);">+91-7259195449</a></p>
					`					</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 hidden-xs">
							<img src="${pageContext.request.contextPath}/resources/newui/assets/img/trio.png" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<!-- /Main Content -->

<!-- script starts here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/newui/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/newui/assets/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/userDetail.js"></script>
</body>
</html>
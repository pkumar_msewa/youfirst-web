<%-- <!DOCTYPE html>
<html>
<head>
	<title>Cashier Card | IMPS</title>

	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/User/assets/images/favicon.png">

  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/bootstrap/css/bootstrap.css">

  	<!-- google font -->
  	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

  	<!-- font-awesome css -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/font-awesome/css/font-awesome.css">
  	<!-- custom css start here -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/css/custom.css">

    <script type="text/javascript">
var context_path="${pageContext.request.contextPath}";
var spinnerUrl = "Please wait <img src='${pageContext.request.contextPath}/resources/images/spinner.gif' height='20' width='20'>";
</script>
<script type="text/javascript">
        function noBack() {
            window.history.forward();
        }
    </script>
</head>
<body>

<!-- Navbar starts here -->
<jsp:include page="/WEB-INF/jsp/User/Navabar.jsp"></jsp:include>

<!-- main content starts here -->
<section>
	<div class="container">
		<div class="innner-body">
			<div class="bimg-sec">
        <div class="bimg-head text-center">
          <span>IMPS Fund Transfer</span>
          <!-- <div class="abot_playrs">
            <center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/players/trio.png" class="img-responsive"></center>
          </div> -->
        </div>   
      </div>

      <div class="content-sec">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <div class="formTrasf row">
            	<form>
            		<div class="col-sm-6">
            			<div class="form-group">
	            			<label>Account Number</label>
	            			<input type="password" name="" id="accountNo" class="form-control pasteDis" placeholder="Enter Account Number" onkeypress="return isNumberKey(event);" maxlength="20">
	            		  <p id="error_accountNo" style="color: red"></p>
	            		</div>
            		</div>
            		<div class="col-sm-6">
            			<div class="form-group">
	            			<label>Confirm Account Number</label>
	            			<input type="text" name="" id="confirmAccountNo" class="form-control pasteDis" placeholder="Confirm Account Number" onkeypress="return isNumberKey(event);" maxlength="20">
	            		  <p id="error_confirmAccountNo" style="color: red"></p>
	            		</div>
            		</div>
            		<div class="col-sm-6">
            			<div class="form-group">
	            			<label>IFSC Code</label>
	            			<input type="text" id="ifscCode" name="" maxlength="11" class="form-control" placeholder="Enter IFSC Code" onkeypress="return isAlphNumberKey(event);">
	            		  <p id="error_ifscCode" style="color: red"></p>
	            		</div>
            		</div>
            		<div class="col-sm-6">
            			<div class="form-group">
	            			<label>Amount</label>
	            			<input type="text" id="amnt" name="" maxlength="4" class="form-control" placeholder="Enter Amount" onkeypress="return isNumberKey(event);">
	            		  <p id="error_amount" style="color: red"></p>
	            		</div>
            		</div>
            		<div class="col-sm-6 col-sm-offset-3">
            			<div class="form-group">
	            			<label>Description</label>
	            			<textarea class="form-control" id="description" placeholder="Message(Optional)" rows="3" maxlength="250"></textarea>
	            		</div>
            		</div>
            		<div class="col-sm-12">
	            			<center>
            				<button class="btn btn-theme" id="imps_request" type="button" onclick="impsService()">Confirm</button>
            			</center>
            		</div>
            	</form>
            </div>

           <!--  <div class="back-link text-center mb-30">
              <a href="#">go back to home</a>
            </div> -->
          </div>
        </div>
      </div>
		</div>
	</div>
</section>

<!-- footer starts here -->
<jsp:include page="/WEB-INF/jsp/User/Footer.jsp"></jsp:include>
<!-- scripts starts here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/bootstrap/js/bootstrap.min.js"></script>

<script src="${pageContext.request.contextPath}/resources/js/userDetail.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/all.js"></script>
<!-- custom js starts here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/js/custom.js"></script>

<script>
	$(document).ready(function(){
		$('.pasteDis').on("cut copy paste",function(e) {
			e.preventDefault();
		});
	});
</script>


<script type="text/javascript">
function impsService(){
	var ifscPattern= "^[A-Za-z]{4}0[A-Z0-9a-z]{6}$";
	var valid = true;
	var amount  = $('#amnt').val();
	var accountNo  = $('#accountNo').val();
	var ConfirmAccountNo  = $('#confirmAccountNo').val();
	var ifscNo = $('#ifscCode').val() ;
	var description = $('#description').val() ;
	
	if(amount.length <= 0){
	$("#error_amount").html("Please enter the amount");
	valid = false;
	}
	if(amount<1){
		$("#error_amount").html("Please enter the valid amount");
		valid = false;
	}
	if(accountNo.length <= 0){
		$("#error_accountNo").html("Please enter the Account No");
		valid = false;
		}
	if(ifscNo.length <= 0){
		$("#error_ifscCode").html("Please enter the IFSC Code");
		valid = false;
		}
	if(!ifscNo.match(ifscPattern)) {
		$("#error_ifscCode").html("Enter valid IFSC Code");
		valid = false;	
	}
	
	if(ConfirmAccountNo.length <= 0){
		$("#error_confirmAccountNo").html("Please enter the confirm account no");
		valid = false;
		}
	else if(!accountNo.match(ConfirmAccountNo)) {
		$("#error_confirmAccountNo").html("Account Number mismatch");
		valid = false;	
	}
	if(valid==true){
		$("#imps_request").addClass("disabled");
		$("#imps_request").html(spinnerUrl);
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : context_path+"/Api/v1/User/Android/en/User/IMPS/ProcessWeb",
			dataType : 'json',
			data : JSON.stringify({
				"accountNo" : "" + accountNo + "",
				"amount":""+amount+"",
				"ifscNo":""+ifscNo+"",
				"description":""+description+"",
				"confirmAcountNo":""+ConfirmAccountNo+""
			}),
			success : function(response) {
				$("#imps_request").removeClass("disabled");
				$("#imps_request").html("Submit");
				console.log(response);
				if (response.code.includes("S00")) {
					swal("Congrats!!", response.message, "success");
// 					$("#imps_success").html(response.message);
// 					$("#imps_success").css("color", "green");
					$('#amnt').val("");
					 $('#ifscCode').val("");
					$('#accountNo').val("") ;
					$('#confirmAccountNo').val("");
					$('#description').val("") ;
				}
				else if (response.code.includes("F00")) {
// 					$("#imps_success").html(response.message);
// 					$("#imps_success").css("color", "red");
					swal({
						  type: 'error',
						  title: 'Sorry!!',
						  text: response.message
						});
					$('#amnt').val("");
					 $('#ifscCode').val("");
					$('#accountNo').val("") ;
					$('#confirmAccountNo').val("");
					$('#description').val("") ;
				}
			}
		});	
		}
		var timeout = setTimeout(function(){
			$("#error_amount").html("");
			$("#error_accountNo").html("");
			$("#error_ifscCode").html("");
			$("#error_confirmAccountNo").html("");
		}, 3000);
}
</script>


 
    <script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>
    
    <script type="text/javascript">
function isAlphNumberKey(evt){
    var k = (evt.which) ? evt.which : evt.keyCode
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
}
</script>

<script type="text/javascript">
function isAlphKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
}
</script>
    
    
</body>
</html> --%>

















<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
	<title>Cashier Card | IMPS</title>

	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/User/assets/images/favicon.png">

  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/bootstrap/css/bootstrap.css">

  	<!-- google font -->
  	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

  	<!-- font-awesome css -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/font-awesome/css/font-awesome.css">
  	<!-- custom css start here -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/css/custom.css">

    <script type="text/javascript">
var context_path="${pageContext.request.contextPath}";
var spinnerUrl = "Please wait <img src='${pageContext.request.contextPath}/resources/images/spinner.gif' height='20' width='20'>";
</script>
<script type="text/javascript">
        function noBack() {
            window.history.forward();
        }
    </script>
</head>
<body>

<!-- Navbar starts here -->
<jsp:include page="/WEB-INF/jsp/User/Navabar.jsp"></jsp:include>

<!-- main content starts here -->
<section>
	<div class="container">
		<div class="innner-body">
			<div class="bimg-sec">
        <div class="bimg-head text-center">
          <span>Instant Money Transfer</span>
          <!-- <div class="abot_playrs">
            <center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/players/trio.png" class="img-responsive"></center>
          </div> -->
        </div>   
      </div>

      <div class="content-sec">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <div class="benif_wrp">
              <div class="benif_hed text-center">
                <span>Select Benificary</span>
              </div>
              <div class="benif_bdy">
                <div class="row">
                <c:forEach items="${beneficiary}" var="bene" varStatus="dd">
                  <div class="col-sm-3 mb-15">
                  <input type="hidden" id="name_${dd.count}" value="${bene.name}">
                  <input type="hidden" id="acc_${dd.count}" value="${bene.accountNo}">
                  <input type="hidden" id="ifsc_${dd.count}" value="${bene.ifscNo}">
                  <input type="hidden" id="bank_${dd.count}" value="${bene.bankname}">
                    <button class="btn btn-default txt-upper" onclick="selectBene('${dd.count}')" type="button">
                      <strong>${bene.name}</strong><br>
                      <small>${bene.accountNo}</small> | <small>${bene.bankname}</small>
                    </button>
                  </div>
                  </c:forEach>
                </div>
              </div>
            </div>
            
            <div class="formTrasf row">
            	<form>
            		<div class="col-sm-6">
            			<div class="form-group">
	            			<label> Name</label>
	            			<input type="text" name="" id="name" class="form-control pasteDis" placeholder="Enter Account Name">
	            			<p id="error_Name" style="color: red"></p>
	            		</div>
            		</div>
            		<div class="col-sm-6">
            			<div class="form-group">
	            			<label>Benificiary Account No.</label>
	            			<input type="password" name="" id="accountNo" class="form-control pasteDis" placeholder="Enter Account Number" onkeypress="return isNumberKey(event);" maxlength="20">
	            		  <p id="error_accountNo" style="color: red"></p>
	            		</div>
            		</div>
            		<div class="col-sm-6">
            			<div class="form-group">
	            			<label>Confirm Account No.</label>
	            		<input type="text" name="" id="confirmAccountNo" class="form-control pasteDis" placeholder="Confirm Account Number" onkeypress="return isNumberKey(event);" maxlength="20">
	            		  <p id="error_confirmAccountNo" style="color: red"></p>
	            		</div>
            		</div>
            		<div class="col-sm-6">
            			<div class="form-group">
	            			<label>IFSC Details</label>
	            			<input type="text" id="ifscCode" name="" maxlength="11" class="form-control" style="text-transform: uppercase;" placeholder="Enter IFSC Code" onkeypress="return isAlphNumberKey(event);">
	            		  <p id="error_ifscCode" style="color: red"></p>
	            		</div>
            		</div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Bank Name</label>
                    <input type="text" name="" id="bankName" class="form-control" placeholder="Enter Bank Name">
                  <p id="error_bankName" style="color: red"></p>
                  </div>
                </div>
            		<div class="col-sm-6">
            			<div class="form-group">
	            			<label>Amount</label>
	            			<input type="text" id="amnt" name="" maxlength="4" class="form-control" placeholder="Enter Amount" onkeypress="return isNumberKey(event);">
	            		  <p id="error_amount" style="color: red"></p>
	            		</div>
            		</div>
            		<div class="col-sm-6 col-sm-offset-3">
            			<div class="form-group">
	            			<label>Message</label>
	            		<textarea class="form-control" id="description" placeholder="Message(Optional)" rows="1" maxlength="100"></textarea>
	            		</div>
            		</div>
                <!-- <div class="col-sm-6">
                  <div class="form-group">
                    <div class="checkbox mt-30">
                      <label><input type="checkbox" id="bene" value="true"><strong>Save Benificary</strong></label>
                    </div>  
                  </div>
                </div> -->
            		<div class="col-sm-12 row">
            		<div class="col-sm-8" style="pull-right">
            		<center style="margin-left:50%"><button class="btn btn-theme" id="imps_request" type="button" onclick="impsService()">Proceed</button></center>
            		</div>
            			<div class="col-sm-4">
            			<label><input type="checkbox" id="bene" value="true" style="margin: 10px 0 0;margin-left: -55%;"><strong>Save Benificary</strong></label>
            			</div>
            		 
            		</div>
            	</form>
            </div>

           <!--  <div class="back-link text-center mb-30">
              <a href="#">go back to home</a>
            </div> -->
          </div>
        </div>
      </div>
		</div>
	</div>
</section>

<!-- footer starts here -->
<jsp:include page="/WEB-INF/jsp/User/Footer.jsp"></jsp:include>
<!-- scripts starts here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/bootstrap/js/bootstrap.min.js"></script>

<script src="${pageContext.request.contextPath}/resources/js/userDetail.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/all.js"></script>
<!-- custom js starts here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/js/custom.js"></script>

<script>
	$(document).ready(function(){
		$('.pasteDis').on("cut copy paste",function(e) {
			e.preventDefault();
		});
	});
</script>


<script type="text/javascript">
function impsService(){
	var ifscPattern= "^[A-Za-z]{4}0[A-Z0-9a-z]{6}$";
	var valid = true;
	var amount  = $('#amnt').val();
	var accountNo  = $('#accountNo').val();
	var ConfirmAccountNo  = $('#confirmAccountNo').val();
	var ifscNo = $('#ifscCode').val() ;
	var description = $('#description').val() ;
	var bankName= $('#bankName').val();
	var Name= $('#name').val();
	if($('#bene').prop('checked')) {
		var bene= true;
	} else {
		var bene= false;
	}
	
	if(amount.length <= 0){
	$("#error_amount").html("Please enter the amount");
	valid = false;
	}
	if(amount<1){
		$("#error_amount").html("Please enter the valid amount");
		valid = false;
	}
	if(accountNo.length <= 0){
		$("#error_accountNo").html("Please enter the Account No");
		valid = false;
		}
	if(ifscNo.length <= 0){
		$("#error_ifscCode").html("Please enter the IFSC Code");
		valid = false;
		}
	if(!ifscNo.match(ifscPattern)) {
		$("#error_ifscCode").html("Enter valid IFSC Code");
		valid = false;	
	}
	if(bankName.length <= 0){
		$("#error_bankName").html("Please enter the Bank name");
		valid = false;
		}
	if(Name.length <= 0){
		$("#error_Name").html("Please enter the name");
		valid = false;
		}
	
	if(ConfirmAccountNo.length <= 0){
		$("#error_confirmAccountNo").html("Please enter the confirm account no");
		valid = false;
		}
	else if(!accountNo.match(ConfirmAccountNo)) {
		$("#error_confirmAccountNo").html("Account number mismatch");
		valid = false;	
	}
	if(valid==true){
		$("#imps_request").addClass("disabled");
		$("#imps_request").html(spinnerUrl);
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : context_path+"/Api/v1/User/Android/en/User/IMPS/ProcessWeb",
			dataType : 'json',
			data : JSON.stringify({
				"accountNo" : "" + accountNo + "",
				"amount":""+amount+"",
				"ifscNo":""+ifscNo+"",
				"description":""+description+"",
				"confirmAcountNo":""+ConfirmAccountNo+"",
				"saveBeneficiary":bene,
				"name":""+Name+"",
				"bankName":""+bankName+""
			}),
			success : function(response) {
				$("#imps_request").removeClass("disabled");
				$("#imps_request").html("Submit");
				console.log(response);
				if (response.code.includes("S00")) {
					swal("Congrats!!", response.message, "success");
// 					$("#imps_success").html(response.message);
// 					$("#imps_success").css("color", "green");
					$('#amnt').val("");
					 $('#ifscCode').val("");
					$('#accountNo').val("") ;
					$('#confirmAccountNo').val("");
					$('#description').val("") ;
					$('#bankName').val("");
					$('#name').val("") ;
				}
				else if (response.code.includes("F00")) {
// 					$("#imps_success").html(response.message);
// 					$("#imps_success").css("color", "red");
					swal({
						  type: 'error',
						  title: 'Sorry!!',
						  text: response.message
						});
					$('#amnt').val("");
					 $('#ifscCode').val("");
					$('#accountNo').val("") ;
					$('#confirmAccountNo').val("");
					$('#description').val("") ;
					$('#bankName').val("");
					$('#name').val("") ;
				}
			}
		});	
		}
// 		var timeout = setTimeout(function(){
// 			$("#error_amount").html("");
// 			$("#error_accountNo").html("");
// 			$("#error_ifscCode").html("");
// 			$("#error_confirmAccountNo").html("");
// 			$("#error_bankName").html("");
// 			$("#error_Name").html("");
// 		}, 3000);
}
</script>


 
    <script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>
    
    <script type="text/javascript">
function isAlphNumberKey(evt){
    var k = (evt.which) ? evt.which : evt.keyCode
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
}
</script>

<script type="text/javascript">
function isAlphKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
}
</script>

<script>
function selectBene(va){
	var name=$('#name_'+va).val();
	var acc=$('#acc_'+va).val();
	var ifsc=$('#ifsc_'+va).val();
	var Bname=$('#bank_'+va).val();
	$('#name').val(name);
	$('#accountNo').val(acc);
	$('#confirmAccountNo').val(acc);
	$('#ifscCode').val(ifsc);
	$('#bankName').val(Bname);
}
</script>

</body>
</html>




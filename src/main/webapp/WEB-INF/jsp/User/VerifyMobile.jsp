<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>CASHIER Cards | Verify</title>

    <!-- Favicon -->
    <link rel="icon" href="${pageContext.request.contextPath}/resources/img/core-img/favicon.png">

    <!-- Core Stylesheet -->
    <link href="${pageContext.request.contextPath}/resources/css/Style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/custom.css" rel="stylesheet">

    <!-- Responsive CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/responsive.css" rel="stylesheet">
         <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script>
        

    <style>
        /* width */
        ::-webkit-scrollbar {
            width: 10px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1; 
        }
         
        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #888; 
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #555; 
        }
        .main_topheader {
            -webkit-box-shadow: 0 1px 0 rgba(12,13,14,.1), 0 1px 3px rgba(12,13,14,.1), 0 4px 20px rgba(12,13,14,.035), 0 1px 1px rgba(12,13,14,.025);
            box-shadow: 0 1px 0 rgba(12,13,14,.1), 0 1px 3px rgba(12,13,14,.1), 0 4px 20px rgba(12,13,14,.035), 0 1px 1px rgba(12,13,14,.025);
            background: #fff;
            color: #8e8e8e;
            padding-top: 1px;
            padding-bottom: 1px;
            posi/tion: initial;
            background: #efefef;
        }
        .form-box {
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            -webkit-box-shadow: 0 12px 200px #e7e7e7;
            box-shadow: 0 12px 200px #e7e7e7;
            background-color: #fff;
            color: #7b7b7b;
            min-height: 288px;
            max-width: 425px;
            margin-right: auto;
            margin-left: auto;
            padding: 70px 4.5% 50px;
        }
        .cta-actions {
            padding-top: 25px;
        }
        .signLog_btn {
            border-radius: 100px;
            background: -webkit-linear-gradient(#ef1821, #291859);
            background: -moz-linear-gradient(#ef1821, #291859);
            background: -ms-linear-gradient(#ef1821, #291859);
            background: -o-linear-gradient(#ef1821, #291859);
            background: linear-gradient(#ef1821, #291859);
            color: #fff;
            font-size: 14px;
            line-height: 32px;
            min-width: 160px;
            text-align: center;
            border: none;
            transition: all .4s ease-in-out;
            cursor: pointer;
            display: inline-block;
            text-decoration: none;
            -webkit-box-shadow: #4bc5ac 0 21px 35px -17px;
            -moz-box-shadow: #4bc5ac 0 21px 35px -17px;
            box-shadow: 0 21px 35px -17px #4bc5ac;
        }
        .tnc {
            color: #c3c3c3;
            padding-top: 5px;
            font-size: 11px;
            line-height: 1.64;
        }
        .auth_form label {
            float: left;
        }
        .first-selection {
            background: url(${pageContext.request.contextPath}/resources/img/bg-img/waves2.svg);
            background-repeat: no-repeat;
            color: #fff;
            background-position: bottom;
            position: relative;
        }
    </style>

</head>

<body>
    <!-- Preloader Start -->
    <div id="preloader">
        <div class="colorlib-load"></div>
    </div>

    <!-- ***** Header Area Start ***** -->
    <header class="header_area1 animated sticky">
        <div class="container-fluid">
            <div class="row align-items-center">
                <!-- Menu Area Start -->
                <div class="col-12 col-lg-12">
                    <div class="menu_area">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <!-- Logo -->
                            <a class="navbar-brand" href="/">
                                <img src="${pageContext.request.contextPath}/resources/User/assets/images/logo1.png" class="img-fluid">
                            </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                            <!-- Menu Area -->
                            <div class="collapse navbar-collapse" id="ca-navbar">
                                <ul class="navbar-nav ml-auto" id="nav">
                                    <!-- <li class="nav-item active"><a class="nav-link" href="${pageContext.request.contextPath}/Home">Home</a></li> -->
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/Aboutus">About</a></li>
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/faq">FAQs</a></li>
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/ContactUs">Contact</a></li>
                                    <li class="nav-item"><a class="nav-link" href="javascript:void(0);">Login</a></li>
                                    <li class="nav-item"><a class="nav-link" href="javascript:void(0);">Signup</a></li>
                                </ul>
                                <!-- <div class="sing-up-button d-lg-none">
                                    <a href="${pageContext.request.contextPath}/User/Login">Login</a>
                                </div> -->
                            </div>
                        </nav>
                    </div>
                </div>
                <!-- Signup btn -->
                <!-- <div class="col-12 col-lg-2">
                    <div class="sing-up-button d-none d-lg-block">
                        <a href="/User/Registration">Sign Up</a>
                    </div>
                </div> -->
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->
    
    <section class="bg-white first-selection" id="" style="padding-top: 86px; padding-bottom: 45px;">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <!-- Heading Text  -->
                    <div class="section-heading">
                        <h2>OTP Verification</h2>
                        <div class="line-shape"></div>
                    </div>

                    <div class="form-box">
                        <form class="auth_form" action="${pageContext.request.contextPath}/User/Activate/Mobile" method="Post">
                          
                          <div class="form-group">
                            <label for="otp">Enter OTP:</label>
                            <input type="otp" class="form-control" id="" name="key" placeholder="Enter your OTP" maxlength="6" onkeypress="return isNumberKey(event)">
                         	<input type="hidden" name="mobileNumber" value="${username}">
                          </div>
                          <div class="cta-actions text-center">
                                <center>
                                    <button class="btn signLog_btn" title="Resend" id="resendotp" onclick="Resend()" type="button">RESEND OTP</button>
                                    <button class="btn signLog_btn" title="Submit" type="submit">SUBMIT</button>
                                </center>
                                 <div id="frgtMessage_success"></div>
                     		   <div id="fpMessage"></div>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
        
    </section>

    <footer>
        
    </footer>
    
<script type="text/javascript">
var cont_path="${pageContext.request.contextPath}";

function Resend(){
	var dup=${username};
	var username=$('#mobileNumber').val();
	console.log("username:"+username+" "+dup);
	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : cont_path+"/Api/User/Windows/en/Resend/Mobile/OTP",
		dataType : 'json',
		data : JSON.stringify({
			"mobileNumber" : "" + dup + ""
		}),
		success : function(response) {
			console.log(response)
			$("#register_resend_otp").removeClass("disabled");
			if (response.code.includes("S00")) {
				console.log(response.details)
				$("#fpMessage").html("");
				$("#frgtMessage_success").html(response.details);
				$("#frgtMessage_success").css("color", "green");
			}
			if(response.code.includes("F00")){
				$("#frgtMessage_success").html(response.details);
				$("#frgtMessage_success").css("color", "red");;
			}
		}
	});	
}

</script>

    <!-- Jquery-2.2.4 JS -->
    <script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script>
    <!-- Bootstrap-4 Beta JS -->
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <!-- All Plugins JS -->
    <script src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>
    <!-- Slick Slider Js-->
    <script src="${pageContext.request.contextPath}/resources/js/slick.min.js"></script>
    <!-- Footer Reveal JS -->
    <script src="${pageContext.request.contextPath}/resources/js/footer-reveal.min.js"></script>
    <!-- Active JS -->
    <script src="${pageContext.request.contextPath}/resources/js/active.js"></script>
    
    <script type="text/javascript">
	function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>
</body>

</html>
    
<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html lang="en">
   
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
    <meta Http-Equiv="Cache-Control" Content="no-cache">
    <meta Http-Equiv="Pragma" Content="no-cache">
    <meta Http-Equiv="Expires" Content="0">
	<sec:csrfMetaTags/>
	 <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script>
	

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/User/assets/images/favicon.png">

        <!-- App title -->
        <title>CASHIER Card | MyProfile</title>

        <!-- Tour css -->
        <!-- <link href="assets/plugins/hopscotch/css/hopscotch.min.css" rel="stylesheet" type="text/css" /> -->

        <!-- Font-awesome css -->
        <link href="${pageContext.request.contextPath}/resources/User/assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <!--Animate CSS -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/User/assets/plugins/animate.less/animate.css">

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/User/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/User/assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/User/assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/User/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/User/assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/User/assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/User/assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/User/assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="${pageContext.request.contextPath}/resources/User/assets/js/modernizr.min.js"></script>
<script type="text/javascript">
var cont_path="${pageContext.request.contextPath}";
</script>

    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <jsp:include page="/WEB-INF/jsp/User/Header.jsp" />
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
		<jsp:include page="/WEB-INF/jsp/User/LeftMenu.jsp" />
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="">
                        <div class="row">
                            <div class="col-sm-4">
                                <!-- <button type="button" class="btn btn-purple btn-rounded w-md waves-effect waves-light m-b-20">Create Project</button> -->
                                <h3>KYC Request</h3>
                            </div>
                           
                        </div><hr>
                        <!-- end row -->
                        <div class="row">
                                    
                                <div class="col-md-6 ">
                                        <div id="frgtMessage_success"></div>

                                            <form action="${pageContext.request.contextPath}/User/Upgrade/Account" method="post" id="formId" enctype="multipart/form-data">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="address1" id="address1" placeholder="Address1" maxlength="60">
                                                    <p class="error" id="error_address1" style="color: red;"></p>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="city" id="city" placeholder="City" maxlength="25" onkeypress="return isAlphKey(event);">
                                                    <p class="error" id="error_city"  style="color: red;"></p>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="pincode" id="pinCode" maxlength="6" placeholder="Pin Code" onkeypress="return isNumberKey(event);">
                                                	<p class="error" id="error_pinCode" class="error" style="color: red;"></p>
                                                </div>
                                                <div class="form-group">
                                                <select id="idType" name="idType" class="form-control">Military Id
                                                    <option value="#" >Select Id Type</option>
                                                    <option value="Aadhar card" >Aadhar card</option>
                                                    <option value="Pan card" >Pan card</option>
                                                    <option value="Driving Licence" >Driving Licence</option>
                                                    <option value="Voter Id" >Voter Id</option>
                                                    <option value="Military Id" >Military Id</option>
                                                    <option value="Work Permit" >Work Permit</option>
                                                    <option value="S PASS No" >S PASS No</option>
                                                    <option value="EP FIN No" >EP FIN No</option>
                                                    <option value="Letter of introduction" >Letter Of Introduction</option>
                                                    <option value="Shop License" >Shop License</option>
                                                    <option value="Utility Bill" >Utility Bill</option>
                                                    <option value="Family Ration Card" >Family Ration Card</option>
                                                     <option value="Valid Passport" >Valid Passport</option>
                                                 </select>
                                                	<p class="error" id="error_idType" class="error" style="color: red;"></p>
                                                </div>
                                                <div class="form-group">
                                                			 <p>Upload Id Proof</p>
                                                     <input type="file" class="uploadlogo" name="image"  id="image" placeholder="Upload Image"/>
                                                          <p class="error" id="error_image"></p>
                                                </div>
                                            
                                        </div>
                                <div class="col-md-6 ">
                                        <div id="frgtMessage_success"></div>

                                         
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="address2" id="address2" placeholder="Address2" maxlength="50">
                                                    <p class="error" id="error_address2" style="color: red;"></p>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="state" id="state" placeholder="State" onkeypress="return isAlphKey(event);">
                                                    <p class="error" id="error_state"  style="color: red;"></p>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="country" id="country" placeholder="Country" onkeypress="return isAlphKey(event);">
                                                	<p class="error" id="error_country" class="error" style="color: red;"></p>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="idNumber" id="idNumber" placeholder="Id Number" onkeypress="return isAlphNumberKey(event);">
                                                	<p class="error" id="error_idNumber" class="error" style="color: red;"></p>
                                                </div>
                                                
                                             </form>
                                        </div>
                                        </div>
                                            <center><button type="button"  style="width: 30%;" class="btn btn-purple" onclick="KYCRequest()">Submit</button></center>
                                       
                                           
                            </div>
                        </div>
                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer">
                    2017 © Copyright Cashier.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <div class="side-bar right-bar">
                <a href="javascript:void(0);"  class="right-bar-toggle animated">
                    <i class="zmdi zmdi-close-circle-o"></i>
                </a>
                <h4 class="">Notifications</h4>
                <div class="notification-list nicescroll">
                    <ul class="list-group list-no-border user-list">
                        <li class="list-group-item">
                            <a href="#" class="user-list-item">
                                <div class="avatar">
                                    <img src="${pageContext.request.contextPath}/resources/User/assets/images/users/avatar-2.jpg" alt="">
                                </div>
                                <div class="user-desc">
                                    <span class="name">Michael Zenaty</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">2 hours ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#" class="user-list-item">
                                <div class="icon bg-info">
                                    <i class="zmdi zmdi-account"></i>
                                </div>
                                <div class="user-desc">
                                    <span class="name">New Signup</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">5 hours ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#" class="user-list-item">
                                <div class="icon bg-pink">
                                    <i class="zmdi zmdi-comment"></i>
                                </div>
                                <div class="user-desc">
                                    <span class="name">New Message received</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">1 day ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item active">
                            <a href="#" class="user-list-item">
                                <div class="avatar">
                                    <img src="${pageContext.request.contextPath}/resources/User/assets/images/users/avatar-3.jpg" alt="">
                                </div>
                                <div class="user-desc">
                                    <span class="name">James Anderson</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">2 days ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item active">
                            <a href="#" class="user-list-item">
                                <div class="icon bg-warning">
                                    <i class="zmdi zmdi-settings"></i>
                                </div>
                                <div class="user-desc">
                                    <span class="name">Settings</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">1 day ago</span>
                                </div>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
            <!-- /Right-bar -->

        </div>
        <!-- END wrapper -->





        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/jquery.scrollTo.min.js"></script>

        <!-- Tour page js -->
        <!-- <script src="assets/plugins/hopscotch/js/hopscotch.min.js"></script> -->

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/main.js"></script>

        <!-- <script>
            $(document).ready(function () {
                var placementRight = 'right';
                var placementLeft = 'left';

                // Define the tour!
                var tour = {
                    id: "my-intro",
                    steps: [
                        {
                            target: "logo-tour",
                            title: "Current online user",
                            content: "You can find here status of user who's currently online.",
                            placement: placementRight,
                            yOffset: 10
                        },
                        {
                            target: 'page-title-tour',
                            title: "User settings",
                            content: "You can edit you profile info here.",
                            placement: 'bottom',
                            zindex: 999
                        },
                        {
                            target: 'notification-tour',
                            title: "Configuration tools",
                            content: "Here you can change theme skins and other features.",
                            placement: 'left',
                            zindex: 999
                        }
                    ],
                    showPrevButton: true
                };

                // Start the tour!
                hopscotch.startTour(tour);
            });
        </script> -->
        
        
        
<!--         UPGRADE ACCOUNT -->
        
   <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center">Upload Documents</h4>
        </div>
        <div class="modal-body">
          <div class="col">
          										 <form action="${pageContext.request.contextPath}/User/Upgrade/Account" method="post" id="" enctype="multipart/form-data">
                          						 <fieldset>
                                                      <div class="col-sm-6">
                                                    <div class="form-group">
                                                            <p>Upload Your Aadhar Card</p>
                                                            <input type="file" class="uploadlogo" name="aadharImage"  id="aadhar_Image"/>
                                                          <p class="error" id="error_aaadharCardImg"></p>
                                                    </div>
                                                    </div>
                                                </fieldset>
                                                <hr>
                                                 <center><button type="button" id="kycre" onclick="kycRequest()" class="btn btn-primary">Submit</button></center>
                                                </form>
                                            </div>
        </div>
      </div>
    </div>
  </div>
  
        

    </body>

<script type="text/javascript">
function changePassword(){
	var currentPassword=$('#password').val();
	var newPassword=$('#newPassword').val();
	var confirmPassword=$('#confirmPassword').val();
	var valid=true;
	if(currentPassword.length<=0){
		$("#error_Password").html("Enter Your Current Password.");
		valid = false;
	}
	if(newPassword.length<=0){
		$("#error_newPassword").html("Enter Your New Password.");
		valid = false;
	}
	if(confirmPassword.length<=0){
		$("#error_confirmPassword").html("Enter Confirm Password.");
		valid = false;
	}
	if(valid == true) {
		console.log("valid");
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : cont_path+"/Api/User/Windows/en/ChangePassword",
			dataType : 'json',
			data : JSON.stringify({
				"password" : "" + currentPassword + "",
				"newPassword":""+newPassword+"",
				"confirmPassword":""+confirmPassword+""
			}),
			success : function(response) {
				console.log(response)
				$("#register_resend_otp").removeClass("disabled");
				if (response.code.includes("S00")) {
					 $("#newPassword").val("");
				    	$("#confirmPassword").val("");
				    	$("#password").val("");
					console.log(response.details)
					$("#frgtMessage_success").html(response.details);
					$("#frgtMessage_success").css("color", "green");;
				}
				if(response.code.includes("F00")){
					$("#frgtMessage_success").html(response.details);
					$("#frgtMessage_success").css("color", "red");;
				}
			}
		});	
	}
	
	var timeout = setTimeout(function(){
    	$("#error_Password").html("");
        $("#error_newPassword").html("");
    	$("#error_confirmPassword").html("");
    	$("#frgtMessage_success").html("");
    }, 3000);
	
} 

</script>


<script type="text/javascript">

function KYCRequest(){
var valid=true;
var IdImg=$('#image').val();
var address1=$('#address1').val();
var address2=$('#address2').val();
var city=$('#city').val();
var state=$('#state').val();
var country=$('#country').val();
var pinCode=$('#pinCode').val();
var idType=$('#idType').val();
var idNumber=$('#idNumber').val();

if(address1=='') {
	$("#error_address1").html("Please enter the address1");
	valid = false;
	console.log("valid: "+"panCardImg");
}
if(city=='') {
	$("#error_city").html("Please enter the city");
	valid = false;
	console.log("valid: "+"panCardImg");
}if(state=='') {
	$("#error_state").html("Please enter the state");
	valid = false;
	console.log("valid: "+"panCardImg");
}if(country=='') {
	$("#error_country").html("Please enter the country");
	valid = false;
	console.log("valid: "+"panCardImg");
}if(pinCode=='') {
	$("#error_pinCode").html("Please enter the pincode");
	valid = false;
	console.log("valid: "+"panCardImg");
}if(idType=='#') {
	$("#error_idType").html("Please select the id type");
	valid = false;
	console.log("valid: "+"panCardImg");
}if(idNumber=='') {
	$("#error_idNumber").html("Please enter the id number");
	valid = false;
	console.log("valid: "+"panCardImg");
}
if (IdImg!= "") {
    if (!(IdImg.split(".")[1].toUpperCase() == "PNG"
      || IdImg.split(".")[1].toUpperCase() == "JPEG" || IdImg.split(".")[1].toUpperCase() == "JPG")) {
     $("#error_image").html("File with " + IdImg.split(".")[1]
     + " is invalid. Upload a valid file");
     valid = false;
    }
   }
if(IdImg=='') {
	$("#error_image").html("Please upload the image");
	valid = false;
	console.log("valid: "+"panCardImg");
}
if (valid) {
	$('#formId').submit();
	$("#kycre").addClass("disabled");
//		$("#addAgent").html(spinnerUrl);
}

var timeout = setTimeout(function(){
	$("#error_address1").html("");
    $("#error_city").html("");
	$("#error_state").html("");
	$("#error_country").html("");
	$("#error_pinCode").html("");
    $("#error_idType").html("");
	$("#error_idNumber").html("");
	$("#error_image").html("");
}, 3000);

}
</script>



<script type="text/javascript">
function isAlphNumberKey(evt){
    var k = (evt.which) ? evt.which : evt.keyCode
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
}
</script>

<script type="text/javascript">
function isAlphKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
}
</script>

<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>
</html>
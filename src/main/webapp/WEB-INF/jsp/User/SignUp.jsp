<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Expires" content="-1">

    <!-- Title -->
    <title>CASHIER Cards | Signup</title>

    <!-- Favicon -->
    <link rel="icon" href="${pageContext.request.contextPath}/resources/img/core-img/favicon.ico">

    <!-- Core Stylesheet -->
    <link href="${pageContext.request.contextPath}/resources/css/Style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/custom.css" rel="stylesheet">

    <!-- Responsive CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/responsive.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.3/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />
        
 <!-- <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script> -->

    <style>
    /* width */
        ::-webkit-scrollbar {
            width: 10px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1; 
        }
         
        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #888; 
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #555; 
        }
        .main_topheader {
            -webkit-box-shadow: 0 1px 0 rgba(12,13,14,.1), 0 1px 3px rgba(12,13,14,.1), 0 4px 20px rgba(12,13,14,.035), 0 1px 1px rgba(12,13,14,.025);
            box-shadow: 0 1px 0 rgba(12,13,14,.1), 0 1px 3px rgba(12,13,14,.1), 0 4px 20px rgba(12,13,14,.035), 0 1px 1px rgba(12,13,14,.025);
            background: #fff;
            color: #8e8e8e;
            padding-top: 1px;
            padding-bottom: 1px;
            posi/tion: initial;
            background: #efefef;
        }
        .form-box {
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            -webkit-box-shadow: 0 12px 200px #e7e7e7;
            box-shadow: 0 12px 200px #e7e7e7;
            background-color: #fff;
            color: #7b7b7b;
            min-height: 314px;
            max-width: 425px;
            margin-right: auto;
            margin-left: auto;
            padding: 70px 4.5% 50px;
        }
        .cta-actions {
            padding-top: 20px;
        }
        .signLog_btn {
            border-radius: 100px;
            /* background-color: #4bc5ac; */
            background: -webkit-linear-gradient(#ef1821, #291859);
		    background: -moz-linear-gradient(#ef1821, #291859);
		    background: -ms-linear-gradient(#ef1821, #291859);
		    background: -o-linear-gradient(#ef1821, #291859);
		    background: linear-gradient(#ef1821, #291859);
            color: #fff;
            font-size: 12px;
            line-height: 42px;
            min-width: 177px;
            text-align: center;
            border: none;
            transition: all .4s ease-in-out;
            cursor: pointer;
            display: inline-block;
            text-decoration: none;
            -webkit-box-shadow: #4bc5ac 0 21px 35px -17px;
            -moz-box-shadow: #4bc5ac 0 21px 35px -17px;
            box-shadow: 0 21px 35px -17px #4bc5ac;
        }
        .tnc {
            color: #c3c3c3;
            padding-top: 5px;
            font-size: 11px;
            line-height: 1.64;
        }
        .auth_form label {
            float: left;
        }
        .first-selection {
            background: url(${pageContext.request.contextPath}/resources/img/bg-img/waves2.svg);
            background-repeat: no-repeat;
            color: #fff;
            background-position: center;
            position: relative;
        }
        .gj-datepicker-bootstrap [role=right-icon] button {
            padding: 18px 0;
            border-radius: 0;
        }
    </style>

</head>

<body>
    <!-- Preloader Start -->
    <div id="preloader">
        <div class="colorlib-load"></div>
    </div>

    <!-- ***** Header Area Start ***** -->
    <header class="header_area1 animated sticky">
        <div class="container-fluid">
            <div class="row align-items-center">
                <!-- Menu Area Start -->
                <div class="col-12 col-lg-12">
                    <div class="menu_area">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <!-- Logo -->
                            <a class="navbar-brand" href="${pageContext.request.contextPath}/Home">
                                <img src="${pageContext.request.contextPath}/resources/User/assets/images/logo1.png" class="img-fluid">
                            </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                            <!-- Menu Area -->
                            <div class="collapse navbar-collapse" id="ca-navbar">
                                <ul class="navbar-nav ml-auto" id="nav">
                                    <!-- <li class="nav-item active"><a class="nav-link" href="${pageContext.request.contextPath}/Home">Home</a></li> -->
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/Aboutus">About</a></li>
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/faq">FAQs</a></li>
<%--                                     <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/ContactUs">Contact</a></li> --%>
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/User/Login">Login</a></li>
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/User/SignUp">Signup</a></li>
                                </ul>
                                <!-- <div class="sing-up-button d-lg-none">
                                    <a href="#">Login</a>
                                </div> -->
                            </div>
                        </nav>
                    </div>
                </div>
                <!-- Signup btn -->
                <!-- <div class="col-12 col-lg-2">
                    <div class="sing-up-button d-none d-lg-block">
                        <a href="${pageContext.request.contextPath}/User/Login">Login</a>
                    </div>
                </div> -->
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->
    
    <section class="bg-white first-selection" id="" style="padding-top: 86px; padding-bottom: 45px;">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <!-- Heading Text  -->
                    <div class="section-heading">
                        <h2>Welcome, let's get started</h2>
                        <div class="line-shape"></div>
                        
                    </div>
                        <span id="errormsg" style="color:red; font-size:20px;" >${Errormsg}</span>

                    <div class="form-box">
                        <form class="auth_form" action="${pageContext.request.contextPath}/User/Registration" id="formId" method="Post">
                          <div class="form-group">
                            <label for="name">First Name:</label>
                            <input type="text" class="form-control" id="firstName" name="firstName" placeholder="Enter Your FirstName" autocomplete="off" onkeypress="return isAlphKey(event);">
                            <span id="firstError" style="color:red; font-size:12px;"></span>
                          </div>
                          
                           <div class="form-group">
                            <label for="name">Last Name:</label>
                            <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Enter Your LastName" autocomplete="off" onkeypress="return isAlphKey(event);">
                            <span id="lastError" style="color: red; font-size: 12px;"></span>
                          </div>
                          
                          <div class="form-group">
                            <label for="email">Email address:</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter Your Email" autocomplete="off" >
                            <span id="mailError" style="color:red; font-size: 12px;"></span>
                          </div>
                          <div class="form-group">
                            <label for="dob">Date of Birth:</label>
                            <input type="text" class="form-control datepicker" id="dob" name="dateOfBirth" placeholder="Enter DOB" autocomplete="off" readonly="readonly">
                            <span id="doberror" style="color:red; font-size: 12px;"></span>
                          </div>
                          <div class="form-group">
                            <label for="phone">Phone:</label>
                            <input type="text" class="form-control" id="phone" name="contactNo" placeholder="Enter Your Number"  maxlength="10" autocomplete="off" onkeypress="return isNumberKey(event)">
                            <span id="ferror" style="color:red; font-size: 12px;"></span>
                          </div>
                          
                          <div class="form-group">
                            <label for="pincode">PinCode:</label>
                            <input type="text" class="form-control" id="pinCode" name="locationCode" placeholder="Enter Your PinCode" autocomplete="off"  maxlength="6" onkeypress="return isNumberKey(event)">
                            <span id="codeError" style="color:red; font-size:12px;"></span>
                          </div>
                          
                          <div class="form-group">
                            <label for="pwd">Password:</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Enter Your Password" autocomplete="off" minlength="4" maxlength="6">
                            <span id="passError" style="color:red; font-size: 12px;"></span>
                          </div>
                          <div class="form-group">
                            <label for="pwd">Id Type:</label>
                            <select id="idType" name="idType" class="form-control">
                            <option value="#">Select Id type </option>
                            <option value="Aadhaar Card">Aadhaar Card </option>
                            <option value="Driving License">Driving License</option>
                            <option value="Pan Card">Pan Card</option>
                            <option value="Passport">Passport</option>
                            <option value="Voter Id">Voter Id</option>
                            </select>
                            <span id="idTyeError" style="color:red; font-size: 12px;"></span>
                          </div>
                           <div class="form-group">
                            <label for="pwd">Id Number:</label>
                            <input type="text" class="form-control" id="idNo" name="idNo" placeholder="Enter Your Id number" autocomplete="off"  maxlength="16">
                            <span id="idNoError" style="color:red; font-size: 12px;"></span>
                          </div>
                          <div class="cta-actions text-center">
                              <button class="btn signLog_btn" type="button" onclick="validateform()" title="CREATE ACCOUNT" id="register">CREATE ACCOUNT</button>
                          </div>
                        </form>
                        <div class="text-center tnc">
                            By clicking on the Create Account you accept the <br><a href="${pageContext.request.contextPath}/TermsnConditions" target="_blank">Terms and Conditions.</a>
                        </div>
                        <p class="mt-2">
                            Already have an account? <a href="${pageContext.request.contextPath}/User/Login">LOG IN</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        
    </section>

    <footer>
        
    </footer>
    

    <!-- Jquery-2.2.4 JS -->
    <script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script>
    <!-- Bootstrap-4 Beta JS -->
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <!-- All Plugins JS -->
    <script src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>
    <!-- Slick Slider Js-->
    <script src="${pageContext.request.contextPath}/resources/js/slick.min.js"></script>
    <!-- Footer Reveal JS -->
    <script src="${pageContext.request.contextPath}/resources/js/footer-reveal.min.js"></script>
    <!-- Active JS -->
    <script src="${pageContext.request.contextPath}/resources/js/active.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.3/combined/js/gijgo.min.js" type="text/javascript"></script>
    <!--validations-->
    
    <!-- <script type="text/javascript">
    $('#firstName, #lastName, #email, #email, #phone, #pinCode, #password').bind('keyup', function() {
        if(allFilled()) $('#register').removeAttr('disabled');
    });

    function allFilled() {
        var filled = true;
        $('body input').each(function() {
            if($(this).val() == '') filled = false;
        });
        return filled;
    }
        </script> -->
    <script type="text/javascript">
    
    var ph=true;

    function validateName() {
    var fname = $("#firstName").val();
    if(fname==null || fname=="")
    {
   	$("#firstError").html("name cannot be blank");
    ph=false;
    }
    else{
    	$("#firstError").html("");
    	ph=true;
    }
    }

    function validateLastName() {
    var lname = $("#lastName").val();
    if(lname==null || lname=="")
    {
   	$("#lastError").html("Last name cannot be blank");
    ph=false;
    }
    else{
    	$("#lastError").html("");
    	ph=true;
    }
    }

    function validateEmail() {
    var myEmail = $("#email").val();
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if(!re.test(String(myEmail).toLowerCase())){
   	$("#mailError").html("enter valid email id");
    ph=false;
    }

    else{
    	$("#mailError").html("");
    	ph=true;
    }
    }
    
    function validateMobile(){
    var number = $("#phone").val();
    var re1 =  /(7|8|9)\d{9}/;
    if(!re1.test(String(number).toLowerCase())||number==""||number==null)
    {
    $("#ferror").html("Enter valid 10 digits Number");
    ph=false;
    }
    else{
      $("#ferror").html("");
         ph=true;
    }
    }
    
  /*   function validatePin(){
        var number = $("#pinCode").val();
        var re1 =  /(7|8|9|5|6|4|3|2|1)\d{6}/;
        if(!re1.test(String(number).toLowerCase())||number==""||number==null)
        {
        $("#ferror").html("Enter valid PinCode");
        ph=false;
        }
        else{
          $("#ferror").html("");
             ph=true;
        }
        }
   */  
   /*  function validatePassword(){
    var pass=$("#password").val();
    if(pass<4||pass>7){
    $("#passError").html("password should be greater than 4 and less than 7");
        ph=false;
    }
    else{
        $("#passError").html("");
        ph=true;
      }
    } */

    function validatePinCode(){
      var pin = $("#pinCode").val();
      var ree = /^\d{6}$/ ;
      if(!ree.test(String(pin).toLowerCase())||pin==""||pin==null)
      {
       $("#codeError").html("Enter valid 6 digits");
       ph=false;
      }
       else{
      $("#codeError").html("");
      ph=true;
     }
     }











/* 
    $(document).ready(function(){
    	$("#firstName").keyup(validateName);
    	$("#lastName").keyup(validateLastName);
    	$("#email").keyup(validateEmail);
    	$("#phone").keyup(validateMobile);
   	$("#pinCode").keyup(validatePinCode);
    	$("#btn1").click(function(){
     validateName();
     validateLastName();
     validateEmail();
     validateMobile();
  //   validatePassword();
     validatePinCode();
     
    });
    }); */


 </script>

         <script type="text/javascript">
            function isNumberKey(evt)
            {
              var charCode = (evt.which) ? evt.which : evt.keyCode;
              if (charCode != 46 && charCode > 31  && (charCode < 48 || charCode > 57))
              return false;
              else{return true;
              } }
          </script>

          <script>
            $('.datepicker').datepicker({
                uiLibrary: 'bootstrap4'
            });
    </script>
    
    
    
    <script>
    	
    	function validateform(){
    	var valid = true;
    	var pattern = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|in)";
    	var passwordPattern = "[a-zA-z0-9]"; //pattern for password
    	var firstName=$("#firstName").val();
    	var lastName  = $('#lastName').val();
    	var contactNo  = $('#phone').val();
    	var email = $('#email').val() ;
    	var dateOfBirth  = $('#dob').val();
    	var password  = $('#password').val();
    	var pinCode  = $('#pinCode').val();
    	var idNo  = $('#idNo').val();
    	var idType  = $('#idType').val();
    	console.log("email:: "+email);
    	if(firstName.length <= 0){
    	$("#firstError").html("Please enter your first name");

    	valid = false;
    	}
    	 if(lastName.length <= 0){
    		$("#lastError").html("Please enter your last name");
    		valid = false;
    	}
    	
    	if(contactNo.length <=0){
    		$("#ferror").html("Please enter your contact no");
    		valid = false;
    	}
    	if(email.length  <= 0){
    		$("#mailError").html("Please enter your email Id ");
    		valid = false; 
    	}else if(!email.match(pattern)) {
    		console.log("inside mail")
    		$("#mailError").html("Enter valid email Id");
    		valid = false;	
    	}
    	if(pinCode.length  <= 0){
    		$("#codeError").html("Please enter pincode");
    		valid = false; 
    	}
    	
    	if(password.length  <= 0){
    		$("#passError").html("Please enter password");
    		valid = false; 
    	}
    	
    	if(dateOfBirth.length  <= 0){
    		$("#doberror").html("Please select date of birth");
    		valid = false; 
    	}
    	
    	if(idNo.length  <= 0){
    		$("#idNoError").html("Please enter id number");
    		valid = false; 
    	}
    	
    	if(idType == '#'){
    		$("#idTyeError").html("Please select id type");
    		valid = false; 
    	}
    	
    	var today = new Date();
    	var dd = today.getDate();
    	var mm = today.getMonth()+1;
    	var yyyy = today.getFullYear();

    	today = yyyy+'-'+"0"+mm+'-'+dd;
    	console.log("current date:::::"+today);
    	if(new Date(dateOfBirth) >= new Date("1998-12-31")) {
    				$("#doberror").html("You must be at least 18 years old to sign up");
    				valid = false;
    	}
    	
    	console.log("valid: "+valid);

    if(valid == true) {
    	$("#formId").submit();
    } 
    
    var timeout = setTimeout(function(){
        $("#firstError").html("");
    	$("#lastError").html("");
    	$("#ferror").html("");
    	$("#mailError").html("");
    	$("#codeError").html("");
    	$("#passError").html("");
    	$("#doberror").html("");
    	$("#idTyeError").html("");
    	$("#idNoError").html("");
    }, 3000);
    }
    </script>
    
    
    <script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>
    
    <script type="text/javascript">
function isAlphNumberKey(evt){
    var k = (evt.which) ? evt.which : evt.keyCode
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
}
</script>

<script type="text/javascript">
function isAlphKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
}
</script>
    
    
    
</body>

</html>
    
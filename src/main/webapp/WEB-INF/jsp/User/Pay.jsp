<%-- <!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>

<html>
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Load Funds</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="/resources/css/assets/css/bootstrap.min.css" rel="stylesheet" />
    
    <link href="/resources/css/assets/css/style.css" rel="stylesheet" />
    
    <style type="text/css">
    button {
	display: inline-block;
	background: rgba(10, 20, 30, .3);
	border: 1px solid transparent;
	color: white;
	text-decoration: none;
	font-size: 1.2rem;
	padding: 15px 15px;
	border-radius: 4px;
	margin: .25rem 0;
	vertical-align: middle;
	line-height: 1;
	overflow: visible;
	white-space: nowrap;
	cursor: pointer;
}

button:hover {
	border: 1px solid rgba(255, 255, 255, .8);
	color: white;
	background: rgba(255, 255, 255, .1);
}

button.close {
/* 	margin: 0 0 1rem; */
	background: #FC6468;
}

button.close:hover {
	border: 1px solid #FC6468;
	color: #FC6468;
	background: transparent;
}

input {
	display: inline-block;
	    background: rgba(10, 20, 30, 0.55);
    border: 1px solid transparent;
    color: white;
    text-decoration: none;
    font-size: 1.2rem;
    padding: 15px 15px 15px 8px;
    border-radius: 7px;
    margin: .25rem 0;
    vertical-align: middle;
    width: 35%;
    line-height: 1;
	overflow: visible;
	white-space: nowrap;
	cursor: pointer;
	margin-left: 90px;
}

input:hover {
	border: 1px solid rgba(10, 20, 30, 0.55);
	color: rgba(10, 20, 30, 0.55);
	background: rgba(255, 255, 255, .1);
}

input.close {
	margin: 0 0 1rem;
	background: #FC6468;
}

input.close:hover {
	border: 1px solid #FC6468;
	color: #FC6468;
	background: transparent;
}
    
    
    </style>
</head>

<body class="login-page sidebar-collapse">
    <div class="page-header">
        <div class="page-header-image" style="background-image:url(${pageContext.request.contextPath}/resources/images/pattern.jpg)"></div>
        <div class="container">
            <div class="col-md-4 content-center">
                <div class="card card-plain card-login">
                    <div class="text-center pb-30">
                        <h2>Add Money</h2>
                    </div>
                    <div class="content pb-30">
                        <div class="row">
                            <div class="col-md-12">
                                <p>Do you really want to add money to your wallet?</p>
                            </div>
							<div>
							 <form  action="${pageContext.request.contextPath}/User/LoadMoney/Response" method="POST">
							<!-- Note that the amount is in paise = 50 INR -->
										<a href="#" onclick="goBack()" class="btn btn-primary btn-lg">Cancel</a>
										<script
											src="https://checkout.razorpay.com/v1/checkout.js"
											data-key="${key}"
											data-amount="${amount}"
											data-buttontext="Continue"
											data-name="${name}"
											data-description="Add Money"
											data-image="${pageContext.request.contextPath}/${logo}"
											data-prefill.name="${name}"
											data-prefill.email="${email}"
											data-prefill.contact="${contactNo}"
											data-theme.color="#F37254">
										</script>
										<input type="hidden" value="Hidden Element" name="hidden">
							 </form>
							</div>
                        </div>
                    </div>
                    <div class="footer text-center pb-30">
<!--                         <a href="#" class="btn btn-primary btn-lg">Cancel</a> -->
<!--                         <a href="#" class="btn btn-primary btn-lg">Continue</a> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="/resources/css/assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="/resources/css/assets/js/core/bootstrap.min.js" type="text/javascript"></script>


<script>
function goBack() {
    window.history.back()
}
</script>
</html> --%>






<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>

<html>
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/User/assets/images/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Cashier Cards | Add Money</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="${pageContext.request.contextPath}/resources/css/assets/css/bootstrap.min.css" rel="stylesheet" />
    
    <link href="${pageContext.request.contextPath}/resources/css/assets/css/style.css" rel="stylesheet" />
    
    <style type="text/css">
    button {
	display: inline-block;
	background: rgba(10, 20, 30, .3);
	border: 1px solid transparent;
	color: white;
	text-decoration: none;
	font-size: 1.2rem;
	padding: 15px 15px;
	border-radius: 4px;
	margin: .25rem 0;
	vertical-align: middle;
	line-height: 1;
	overflow: visible;
	white-space: nowrap;
	cursor: pointer;
}

button:hover {
	border: 1px solid rgba(255, 255, 255, .8);
	color: white;
	background: rgba(255, 255, 255, .1);
}

button.close {
/* 	margin: 0 0 1rem; */
	background: #FC6468;
}

button.close:hover {
	border: 1px solid #FC6468;
	color: #FC6468;
	background: transparent;
}

input {
	display: inline-block;
	    background: rgba(10, 20, 30, 0.55);
    border: 1px solid transparent;
    color: white;
    text-decoration: none;
    font-size: 1.2rem;
    padding: 15px 15px 15px 8px;
    border-radius: 7px;
    margin: .25rem 0;
    vertical-align: middle;
    width: 35%;
    line-height: 1;
	overflow: visible;
	white-space: nowrap;
	cursor: pointer;
	margin-left: 90px;
}

input:hover {
	border: 1px solid rgba(10, 20, 30, 0.55);
	color: rgba(10, 20, 30, 0.55);
	background: rgba(255, 255, 255, .1);
}

input.close {
	margin: 0 0 1rem;
	background: #FC6468;
}

input.close:hover {
	border: 1px solid #FC6468;
	color: #FC6468;
	background: transparent;
}
    
    
    </style>
</head>

<body class="login-page sidebar-collapse">
    <div class="page-header">
        <div class="page-header-image" style="background-image:url(${pageContext.request.contextPath}/resources/images/pattern.jpg)"></div>
        <div class="container">
            <div class="col-md-4 content-center">
                <div class="card card-plain card-login">
                    <div class="text-center pb-30">
                        <h2>Add Money</h2>
                    </div>
                    <div class="content pb-30">
                        <div class="row">
                            <div class="col-md-12">
                                <p>Do you really want to add money to your card?</p>
                            </div>
							<div>
							 <form  action="${pageContext.request.contextPath}/Api/v1/User/Web/en/LoadMoney/Response" method="POST">
							<!-- Note that the amount is in paise = 50 INR -->
										<a href="#" onclick="goBack()" class="btn btn-primary btn-lg">Cancel</a>
										<script
											src="https://checkout.razorpay.com/v1/checkout.js"
											data-key="${key}"
											data-amount="${amount}"
											data-buttontext="Continue"
											data-name="${name}"
											data-description="Add Money"
											data-image="${pageContext.request.contextPath}/resources/User/assets/images/logo.png"
											data-prefill.name="${name}"
											data-prefill.email="${email}"
											data-prefill.contact="${contactNo}"
											data-theme.color="#F37254">
										</script>
										<input type="hidden" value="Hidden Element" name="hidden">
							 </form>
							</div>
                        </div>
                    </div>
                    <div class="footer text-center pb-30">
<!--                         <a href="#" class="btn btn-primary btn-lg">Cancel</a> -->
<!--                         <a href="#" class="btn btn-primary btn-lg">Continue</a> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="${pageContext.request.contextPath}/resources/css/assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/css/assets/js/core/bootstrap.min.js" type="text/javascript"></script>


<script>
function goBack() {
    window.history.back()
}
</script>
</html>
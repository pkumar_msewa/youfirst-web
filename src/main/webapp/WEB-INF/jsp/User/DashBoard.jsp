<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
	<title>Cashier Card | Home</title>

	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<!-- favicon -->
  	<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/User/assets/images/favicon.png">

  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/bootstrap/css/bootstrap.css">

  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/css/easy-autocomplete.css">

  	<!-- google font -->
  	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/font/styles.css">

  	<!-- font-awesome css -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/font-awesome/css/font-awesome.css">

  	<!-- custom css start here -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/css/custom.css">

<script type="text/javascript">
var context_path="${pageContext.request.contextPath}";
</script>

 
  
</head>
<body>
<jsp:include page="/WEB-INF/jsp/User/Navabar.jsp"></jsp:include>
<!-- main content starts here -->
<section>
	<div class="container">
		<div class="innner-body">
			<div class="bimg-sec">
		        <div class="bimg-head text-center">
		          <span>#AbCashNahiCashierChalega</span>
		        </div>
		        <div class="text-center" style="color: white">
				<span>${LoadMoney}</span>
				</div>
		        <div class="row">
		          <div class="col-md-12" style="margin-top: 5%;">
		            <div class="col-md-4">
		              	<div class="dash-plyrs-1">
		                	<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/players/ash_1.png" class="img-responsive"></center>
		              	</div>
		            </div>
		            <div class="col-md-4">
		              <div class="usrcardWrp mt-30">
		                <div class="img-card">
<%-- 		                  <img src="${pageContext.request.contextPath}/resources/assets-newui/img/card.png" class="img-responsive"> --%>
		                <c:choose>
			             	<c:when test="${cardstatus == 'false'}">
			             	<img src="${pageContext.request.contextPath}/resources/newui/assets/img/card.png" class="img-responsive">
			                </c:when>
			                  <c:otherwise>
			                 <img src="${pageContext.request.contextPath}/resources/images/Bcard.png" class="img-responsive">
			                  </c:otherwise>
			              </c:choose>
		                  <div class="holderDtls">
		                    <div class="holdercvv text-right">
		                      <span class="cvv">cvv</span>&nbsp;<span>${resultSet.cardDetails.cvv}</span>
		                    </div>
		                    <div class="holderNum">
		                      <span>${vcardNo}</span>
		                    </div>
		                    <div class="holderVal">
		                      <span>valid </span>&nbsp;<span>${resultSet.cardDetails.expiryDate}</span>
		                    </div>
		                    <div class="holderNme">
		                      <span>${resultSet.cardDetails.holderName}</span>
		                    </div>
		                  </div>
		                </div>
		                <div class="reset-pin text-center">
		                <c:choose>
		                <c:when test="${physicalCard == 'true'}">
		                </c:when>
		                </c:choose>	
		                </div>
		              </div>
		            </div>
		            <div class="col-md-4">
		              	<div class="dash-plyrs-2">
		                	<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/players/seh_1.png" class="img-responsive"></center>
		              	</div>
		            </div>
		          </div>
		        </div>
		      </div>


		      <div class="row">
		        <div class="col-md-12">
		          <div class="use_wrp">
		            <div class="use_hed text-center">
		              <span>How to use</span>
		            </div>
		            <div class="use_bdy">
		              <div class="row">
		                <div class="col-md-6 col-sm-6 col-xs-12 hwtous">
		                  <div class="">
		                      <center>
		                      	<h4>Step 1: Download App</h4>
		                      	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/stores.png" class="img-responsive">
		                      	<p>Download Cashier Prepaid Card from Google Play Store or App store.</p>
		                      </center>
		                  </div>
		                </div>
		                <div class="col-md-6 col-sm-6 col-xs-12 hwtous">
		                  <div class="">
		                      <center>
		                      	<h4>Step 2: Register</h4>
		                      	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/regd.png" class="img-responsive">
		                      	<p>Easily register using your mobile number and start using cashier card.</p>
		                      </center>
		                  </div>
		                </div>
		                <div class="col-md-6 col-sm-6 col-xs-12 hwtous">
		                  <div class="">
		                      <center>
		                      	<h4>Step 3: LoadMoney</h4>
		                      	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/load-money.png" class="img-responsive">
		                      	<p>Load money instantly to cashier card and experience secured and hassel free transactions.</p>
		                      </center>
		                  </div>
		                </div>
		                <div class="col-md-6 col-sm-6 col-xs-12 hwtous">
		                  <div class="">
		                      <center>
		                      	<h4>Step 4: Generate Physical Card</h4>
		                      	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/phy-card.png" class="img-responsive">
		                      	<p>Apply for your Cashier physical card and enjoy seamless offline payment facility at all mastercard accepted merchant.</p>
		                      </center>
		                  </div>
		                </div>
		              </div>
		            </div>
		          </div>
		        </div>
		      </div>

		      <div class="">
		        <div class="col-md-12 downld_wrp">
		          <div class="col-md-4">
		            <div class="downld_hed">
		              <h4>Download mobile app now</h4>
		            </div>
		          </div>
		          <div class="col-md-4">
		            <div class="applink">
		              <center><a href="https://play.google.com/store/apps/details?id=in.MadMoveGlobal.CashierCard" target="_blank"><img src="${pageContext.request.contextPath}/resources/assets-newui/img/playstore.png"></a></center>
		            </div>
		            <div class="applink">
		              <center><a href="https://itunes.apple.com/in/app/cashier-prepaid-cards/id1374882309?mt=8" target="_blank"><img src="${pageContext.request.contextPath}/resources/assets-newui/img/appstore.png"></a></center>
		            </div>
		          </div>
		          <div class="col-md-4">
		            <div class="ashimg">
		              <img src="">
		            </div>
		          </div>
		        </div>
		      </div>
			</div>
		</div>
	</section>
	<!-- footer starts here -->
	<jsp:include page="/WEB-INF/jsp/User/Footer.jsp"></jsp:include>	

	<!-- scripts starts here -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/js/jquery.min.js"></script>
	<!-- <script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/js/jquery-ui.min.js"></script> -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/js/jquery.easy-autocomplete.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/bootstrap/js/bootstrap.min.js"></script>
	<!-- custom js starts here -->
	<script src="${pageContext.request.contextPath}/resources/User/assets/js/all.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/userDetail.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/js/custom.js"></script>
	
	
	
	<!-- RESET PIN DISCLAIMER -->
	<div class="modal fade-scale" id="restpn" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content col-xs-10 col-xs-offset-2" style="z-index:9999; margin-top:85px;">
        <div class="modal-header">
        <center><h3>Disclaimer</h3></center>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body ">
            <div class="form-group">
              <div id="">Your 4 digit PIN will be sent to your registered email Id.</div>
            </div>
            <div class="form-group">
    		      <center><button id="btn1" class="btn  btn-primary" onclick="restpin()">Continue</button></center>
            </div>
        </div>
        <div class="modal-footer" style="border-top: none;">
         <div id="proxyError"></div>
        </div>
      </div>
    </div>
  </div> 


 <div id="brows_plan" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <div class="row">
	        	<div  class="col-sm-3">
	        		<h3 class="modal-title">Browse Plans</h3>
	        	</div>
	        	<div class="col-sm-3">
	        		<div class="form-group">
	        			<input type="text" name="" class="form-control" placeholder="Operator">
	        		</div>
	        	</div>
	        	<div class="col-sm-3">
	        		<div class="form-group">
	        			<input type="text" name="" class="form-control" placeholder="Circle">
	        		</div>
	        	</div>
	        </div>
	      </div>
	      <div class="modal-body">
	        <div class="row">
	        	<div class="padLft15 padTop15 col-sm-12">
	        		<div class="col-sm-3 pr-0">
		        		<div class="brsw-desc left-brows">
		        			<div class="scrol-y">
		        				<!-- Loop starts here -->
		        				<a href="#" class="active">All Plans</a>
		        				<!-- Loop ends here -->
		        			</div>
		        		</div>
		        	</div>
		        	<div class="col-sm-9 pd-0">
		        		<div class="vtop">
		        			<div class="scrol-y">
		        				<!-- Loop starts here -->
		        				<a href="#" class="col-sm-12 rdown vtop pad15">
		        					<div class="col-sm-2 pl-0">
		        						<button class="btn btn-primary nobg dpInBLock plns btn-style"><i class="fa fa-inr"></i>&nbsp;90</button>
		        					</div>
		        					<div class="col-sm-9 pl-0">
		        						<div class="vtop">
			        						<p class="pbtm5 tgreyteel marg0">Talktime Rs.85</p>
			        						<p class="f12 tgreyteel marg0">Unlimited</p>
			        					</div>
		        					</div>
		        				</a>
		        				<!-- Loop ends here -->
		        			</div>
		        		</div>
		        	</div>
	        	</div>
	        </div>
	      </div>
	    </div>

	  </div>
	</div>


 
    <script type="text/javascript">
    function restpin(){
    	$.ajax({
			type : "POST",
			contentType : "application/json",
			url : "${pageContext.request.contextPath}/Api/v1/User/Windows/en/ResetPinWeb",
			dataType : 'json',
			data : JSON.stringify({
				"amount" :" ",
			}),
			
			success : function(response) {
				console.log("response:: "+response.code);
				console.log("response:: "+response.message);
				$("#restpn").modal('hide')
// 				$("#registerButton").removeClass("disabled");
// 				$("#registerButton").html("Sign Up");
				if (response.code.includes("S00")) {
					swal("Congrats!!", response.message, "success");
					}else{
						swal({
							  type: 'error',
							  title: 'Sorry!!',
							  text: response.message
							});
				}			
			}
		});
    	
    }
    
    </script>
	 <script>
    $(document).ready(function() {
    	getMobileOperator();
		
     

	 });
	</script>
	<!-- Mobile Operator -->
	
	   
	   <script type="text/javascript">
           function filterFunction() {
           /*  if(document.getElementById("template-icon-left").value == ""){
             document.getElementById("searchResultList").style.display = "none";
            } else {
            */  document.getElementById("searchResultList").style.display = "block";
                var input, filter, ul, li, a, i;
                input = document.getElementById("template-icon-left");
                filter = input.value.toUpperCase();
                div = document.getElementById("myDropdown");
                a = div.getElementsByTagName("a");
                console.log("a.length >> " + a.length);
                for (i = 0; i < a.length; i++) {
                    if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
                        a[i].style.display = "";
                    } else {
                        a[i].style.display = "none";
                    }
                }
            
           }
           </script> 
           
           
           <script type="text/javascript">
          $(document).ready(function(){
        	  console.log("Inside autocomplete");
        	  $.ajax({
  				type : "GET",
  				url:"${pageContext.request.contextPath}/Api/v1/User/Website/en/MdexTransaction/OperatorAndCircles/PRE",
  				dataType:"json",

  				success : function(response) {
  		          var content = '';
  		          var obj=JSON.parse(response.cardDetails);
  		          console.log(obj.status);
  					for (i = 0; i < obj.details.length; i++) {
  						
  			          content += '<a  onclick="setVal('+'\''+obj.details[i].code+'\''+','+'\''+obj.details[i].serviceLogo+'\''+','+'\''+obj.details[i].name+'\''+')"><img class="drop-img" src="'+obj.details[i].serviceLogo+'"/>'+obj.details[i].name+'</a>';

  					}
 		             $("#searchResultList").append(content);


  	    	
  	    }
  	    	});

          });
          </script>
          
          <script>
          function setVal(value,logo,name) {
        	  
           $("#opt_val").val(value);
           var img='<img src="'+logo+'"/>';
           $("#template-icon-left").val(name);
           $("#template-icon-left").css('background:',' white url('+logo+') left no-repeat');
           document.getElementById("searchResultList").style.display = "none";
           
          }
          </script>
	   
	   
	<!--   <script>
		  var options = {
		data: [ {name: "Airtel", type: "air", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/m-operator/Airtel.svg"},
			{name: "Aircel", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/m-operator/Aircel.svg"},
			{name: "Vodafone", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/m-operator/Vodafone.svg"},
			{name: "Idea", type: "air", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/m-operator/Idea.svg"},
			{name: "Jio", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/m-operator/Jio.svg"},
			{name: "BSNL", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/m-operator/BSNL.svg"},
			{name: "DOCOMO", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/m-operator/DOCOMO.svg"},
			{name: "MTNL", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/m-operator/MTNL.svg"},
			{name: "T24", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/m-operator/T24.svg"},
			{name: "Telenor", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/m-operator/Telenor.svg"}],

		getValue: "name",

		template: {
			type: "iconLeft",
			fields: {
				iconSrc: "icon"
			}
		}
	};

	$("#template-icon-left").easyAutocomplete(options);	
	  </script> -->

	  <!-- DTH Operator -->
	 <!--  <script>
	  
		  var options = {
		data: [ {name: "Airtel Digital TV", type: "air", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/dth-operator/Airtel.svg"},
			{name: "Dish TV", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/dth-operator/DishTV.svg"},
			{name: "Relaince Digital TV", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/dth-operator/Reliance.svg"},
			{name: "TataSky", type: "air", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/dth-operator/TataSky.svg"},
			{name: "Sun Direct", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/dth-operator/SunDirect.svg"},
			{name: "Videocon D2h", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/dth-operator/D2h.svg"}],

		getValue: "name",

		template: {
			type: "iconLeft",
			fields: {
				iconSrc: "icon"
			}
		}
	};

	$("#dth_Operator").easyAutocomplete(options);	
	  </script>
 -->
	  <!-- Landline Operator -->
	  <!-- <script>
		  var options = {
		data: [ {name: "Airtel", type: "air", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/m-operator/Airtel.svg"},
			{name: "BSNL", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/m-operator/BSNL.svg"}],

		getValue: "name",

		template: {
			type: "iconLeft",
			fields: {
				iconSrc: "icon"
			}
		}
	};

	$("#lLine_Operator").easyAutocomplete(options);	
	  </script>
 -->
	  <!-- Electricity Operator -->
	<!--   <script>
		  var options = {
		data: [ {name: "Eastern Power Distribution Company of Andhra Pradesh Ltd.", type: "air", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/e-provider/APEPDCL.svg"},
			{name: "Reliance Energy - Mumbai", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/e-provider/Reliance.svg"},
			{name: "South Bihar Power Distribution Company Limited", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/e-provider/SBPDCL.svg"},
			{name: "SNDL Nagpur", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/e-provider/SNDL.svg"},
			{name: "Bangalore Electricity Supply Company Ltd. (BESCOM)", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/e-provider/BESCOM.svg"},
			{name: "Tripura State Electricity Corporation Limited", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/e-provider/TSECL.svg"},
			{name: "Uttarakhand Power Corporation Ltd. (UPCL)", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/e-provider/UPCL.svg"},
			{name: "Uttar Pradesh Power Corporation Ltd. (UPPCL)", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/e-provider/UPPCL.svg"}],

		getValue: "name",

		template: {
			type: "iconLeft",
			fields: {
				iconSrc: "icon"
			}
		}
	};

	$("#electr_Operator").easyAutocomplete(options);	
	  </script>
 -->
	  <!-- Gas Operator -->
	  <!-- <script>
		  var options = {
		data: [ {name: "Eastern Power Distribution Company of Andhra Pradesh Ltd. (APEPDCL)", type: "air", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/e-provider/APEPDCL.svg"},
			{name: "BSES Yamuna - Delhi", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/e-provider/BSES.svg"},
			{name: "Reliance Energy - Mumbai", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/e-provider/Reliance.svg"},
			{name: "South Bihar Power Distribution Company Limited", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/e-provider/SBPDCL.svg"},
			{name: "SNDL Nagpur", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/e-provider/SNDL.svg"},
			{name: "Tamil Nadu Electricity Board (TNEB)", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/e-provider/TNEB.svg"},
			{name: "Tripura State Electricity Corporation Limited", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/e-provider/TSECL.svg"},
			{name: "Uttarakhand Power Corporation Ltd. (UPCL)", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/e-provider/UPCL.svg"},
			{name: "Uttar Pradesh Power Corporation Ltd. (UPPCL)", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/e-provider/UPPCL.svg"}],

		getValue: "name",

		template: {
			type: "iconLeft",
			fields: {
				iconSrc: "icon"
			}
		}
	};

	$("#gas_Provider").easyAutocomplete(options);	
	  </script>
 -->
	  <!-- Gas Operator -->
	<!--   <script>
		  var options = {
		data: [ {name: "Airtel", type: "air", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/m-operator/Airtel.svg"},
			{name: "BSNL", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/m-operator/BSNL.svg"},
			{name: "MTNL", type: "ground", icon: "${pageContext.request.contextPath}/resources/assets-newui/img/m-operator/MTNL.svg"}],

		getValue: "name",

		template: {
			type: "iconLeft",
			fields: {
				iconSrc: "icon"
			}
		}
	};

	$("#ins_Provider").easyAutocomplete(options);	
	  </script>
 -->
	  <!-- <script>
		  	var options = {
			data: ["Andhara Pradesh", "Assam", "Bihar", "Chennai", "Orissa"]
		};

		$("#basics").easyAutocomplete(options);
	  </script>
 -->
	  <script>
	  	$(document).ready(function(){
		  // Add smooth scrolling to all links
		  $("a").on('click', function(event) {

		    // Make sure this.hash has a value before overriding default behavior
		    if (this.hash !== "") {
		      // Prevent default anchor click behavior
		      event.preventDefault();

		      // Store hash
		      var hash = this.hash;

		      // Using jQuery's animate() method to add smooth page scroll
		      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
		      $('html, body').animate({
		        scrollTop: $(hash).offset().top
		      }, 800, function(){
		   
		        // Add hash (#) to URL when done scrolling (default click behavior)
		        window.location.hash = hash;
		      });
		    } // End if
		  });
		});
	  </script>
	
	<script>
		$('#M-Click').click(function(){
			$('#M-Recharge').removeAttr('style');
			$('#D-Recharge').css('display', 'none');
			$('#L-Recharge').css('display', 'none');
			$('#E-Recharge').css('display', 'none');
			$('#G-Recharge').css('display', 'none');
			$('#I-Recharge').css('display', 'none');
		});
		$('#D-Click').click(function(){
			$('#D-Recharge').removeAttr('style');
			$('#M-Recharge').css('display', 'none');
			$('#L-Recharge').css('display', 'none');
			$('#E-Recharge').css('display', 'none');
			$('#G-Recharge').css('display', 'none');
			$('#I-Recharge').css('display', 'none');
		});
		$('#L-Click').click(function(){
			$('#L-Recharge').removeAttr('style');
			$('#D-Recharge').css('display', 'none');
			$('#M-Recharge').css('display', 'none');
			$('#E-Recharge').css('display', 'none');
			$('#G-Recharge').css('display', 'none');
			$('#I-Recharge').css('display', 'none');
		});
		$('#E-Click').click(function(){
			$('#E-Recharge').removeAttr('style');
			$('#D-Recharge').css('display', 'none');
			$('#L-Recharge').css('display', 'none');
			$('#M-Recharge').css('display', 'none');
			$('#G-Recharge').css('display', 'none');
			$('#I-Recharge').css('display', 'none');
		});
		$('#G-Click').click(function(){
			$('#G-Recharge').removeAttr('style');
			$('#D-Recharge').css('display', 'none');
			$('#L-Recharge').css('display', 'none');
			$('#E-Recharge').css('display', 'none');
			$('#M-Recharge').css('display', 'none');
			$('#I-Recharge').css('display', 'none');
		});
		$('#I-Click').click(function(){
			$('#I-Recharge').removeAttr('style');
			$('#D-Recharge').css('display', 'none');
			$('#L-Recharge').css('display', 'none');
			$('#E-Recharge').css('display', 'none');
			$('#G-Recharge').css('display', 'none');
			$('#M-Recharge').css('display', 'none');
		});
	</script>
	
 <!-- <script>
  function preventBack(){window.history.forward();}
  setTimeout("preventBack()", 0);
  window.onunload=function(){null};
</script> -->

<!-- <script type="text/javascript" >
   function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
</script> -->
  
  

	</body>
</html>
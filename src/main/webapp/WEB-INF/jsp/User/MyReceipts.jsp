<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
	<title>Cashier Card | Transaction Statement</title>

	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/User/assets/images/favicon.png">

  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/bootstrap/css/bootstrap.css">

  	<!-- google font -->
  	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

  	<!-- font-awesome css -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/font-awesome/css/font-awesome.css">

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/datatables/DataTables/css/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/datatables/Responsive/css/responsive.bootstrap.css">
  	<!-- custom css start here -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/css/custom.css">
	<script type="text/javascript">
var context_path="${pageContext.request.contextPath}";
</script>
</head>
<body>

<!-- Navbar starts here -->
<jsp:include page="/WEB-INF/jsp/User/Navabar.jsp"></jsp:include>

<!-- main content starts here -->
<section>
	<div class="container">
		<div class="innner-body">
			<div class="bimg-sec">
        <div class="bimg-head text-center">
          <span>Transaction Statement</span>
        </div>   
      </div>
      <div class="row">
        <div class="col-md-3 col-md-offset-1">
          <div class="load_plyr-1">
            <img src="${pageContext.request.contextPath}/resources/assets-newui/img/players/rah_1.png" class="img-responsive">
          </div>
        </div>
        <div class="col-md-4 text-center">
          <p class="avail_bal"><span>your available balance:</span>&nbsp;<i class="fa fa-inr"></i>&nbsp;<span id="crdBalance"></span></p>
        </div>
        <div class="col-md-3">
          <div class="load_plyr-2">
            <img src="${pageContext.request.contextPath}/resources/assets-newui/img/players/moh_1.png" class="img-responsive">
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="trans_box">
            <ul class="nav nav-tabs nav-justified nav-center">
              <li class="active"><a data-toggle="tab" href="#VCard">Virtual Card</a></li>
              <li><a data-toggle="tab" href="#PCard">Physical Card</a></li>
            </ul>

            <div class="tab-content">
              <div id="VCard" class="tab-pane fade in active">
                <table id="vcard_tab" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                   <thead>
                                            <tr>
                                                <th>Sl No</th>
                                                <th>Date</th>
                                                <th>TransactionType</th>
                                                <th>Description</th>
                                                <th>Amount</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                       <tbody>
                                        <c:forEach items="${Vtransactions}" var="cards"  varStatus="loopCounter">
								<tr>
									<td>${loopCounter.count}</td>
									<td> <c:out value="${cards.date}" default="" escapeXml="true" /></td>
										<c:choose>
									<c:when test="${cards.transactionType == 'credit'}">
										<td style="color: #0cce0c;"> <c:out value="${cards.transactionType}" default="" escapeXml="true" /></td>
									</c:when>
									<c:otherwise>
										<td style="color: #f10c1b;"> <c:out value="${cards.transactionType}" default="" escapeXml="true" /></td>
									</c:otherwise>
									</c:choose>
										<td><c:out value="${cards.description}" default="" escapeXml="true" /></td>
										<td> <c:out value="${cards.amount}" default="" escapeXml="true" /></td>
									<td> <c:out value="${cards.status}" default="" escapeXml="true" /></td>
								</tr>
							</c:forEach>
                                    </tbody>
                </table>
              </div>
              <div id="PCard" class="tab-pane fade">
                <table id="pcard_tab" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                 <thead>
                                            <tr>
                                                <th>Sl No</th>
                                                <th>Date</th>
                                                <th>TransactionType</th>
                                                <th>Description</th>
                                                <th>Amount</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                       <tbody>
                                        <c:forEach items="${Ptransactions}" var="card"   varStatus="loopCounter">
								<tr>
									<td>${loopCounter.count}</td>
									<td> <c:out value="${card.date}" default="" escapeXml="true" /></td>
									<c:choose>
									<c:when test="${card.transactionType == 'credit'}">
										<td style="color: #0cce0c;"> <c:out value="${card.transactionType}" default="" escapeXml="true" /></td>
									</c:when>
									<c:otherwise>
										<td style="color: #f10c1b;"> <c:out value="${card.transactionType}" default="" escapeXml="true" /></td>
									</c:otherwise>
									</c:choose>	
										<td><c:out value="${card.description}" default="" escapeXml="true" /></td>
										<td> <c:out value="${card.amount}" default="" escapeXml="true" /></td>
									<td> <c:out value="${card.status}" default="" escapeXml="true" /></td>
								</tr>
							</c:forEach>
                                    </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="back-link text-center mb-30">
            <a href="${pageContext.request.contextPath}/User/Login/Process">go back to home</a>
          </div>
        </div>
      </div>
		</div>
	</div>
</section>

<section>
  <div class="container">
    <div class="footer">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-6">
            <span>All Rights Reserved. &copy; iMoney Pvt. Ltd.</span>
          </div>
          <div class="col-md-6 text-right">
            <span class="footer-links"><a href="#">Privacy Policy</a><a href="#">FAQ</a><a href="#">Press Release</a><a href="#">Home</a></span>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- scripts starts here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/datatables/DataTables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/datatables/DataTables/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/datatables/Responsive/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/datatables/Responsive/js/responsive.bootstrap.min.js"></script>

<!-- custom js starts here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/js/custom.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/userDetail.js"></script>
<script>
  $(document).ready(function() {
    $(document).ready(function() {
        $('#vcard_tab').DataTable({
          info: false,
          searching: false
        });
    });

    $(document).ready(function() {
        $('#pcard_tab').DataTable({
          info: false,
          searching: false
        });
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      $($.fn.dataTable.tables(true)).DataTable()
        .columns.adjust()
        .responsive.recalc();
    }); 
  })
</script>
</body>
</html>
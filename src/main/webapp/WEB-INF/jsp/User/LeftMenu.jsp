<%-- <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!-- User -->
                    <div class="user-box">
                         <div class="user-img">
                            <img src="${pageContext.request.contextPath}/resources/User/assets/images/users/avatar-10.jpg" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">
                        </div>
                        <h5><a href="#">${user.userDetail.firstName} ${user.userDetail.lastName}</a> </h5>
                        <!-- <ul class="list-inline">
                            <li>
                                <a href="#" >
                                    <i class="zmdi zmdi-settings"></i>
                                </a>
                            </li>

                            <li>
                                <a href="#" class="text-custom">
                                    <i class="zmdi zmdi-power"></i>
                                </a>
                            </li>

                        </ul> -->
                    </div>
                    <!-- End User -->

                    <!--- Sidebar -->
                    <div id="sidebar-menu">
                        <ul>
                            <!-- <li class="text-muted menu-title">Navigation</li> -->
                            <li>
                                <a href="${pageContext.request.contextPath}/User/Login/Process" class="waves-effect"><i class="fa fa-credit-card"></i> <span> Virtual Card </span> </a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/User/PhysicalCardRequest" class="waves-effect"><i class="fa fa-credit-card"></i> <span> Physical Card </span> </a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/User/Expenses" class="waves-effect"><i class="fa fa-line-chart"></i> <span> Expenses </span> </a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/User/LoadMoney" class="waves-effect"><i class="ti ti-wallet"></i> <span> Wallet </span> </a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/User/MyProfile" class="waves-effect"><i class="fa fa-user-circle"></i> <span> My Profile </span> </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-life-saver"></i> <span> Help </span> </a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/User/Login/Logout" class="waves-effect"><i class="fa fa-power-off"></i> <span> Logout </span> </a>
                            </li>

                            

                            <!-- <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-format-underlined"></i> <span> UI Elements </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="ui-buttons.html">Buttons</a></li>
                                    <li><a href="ui-cards.html">Cards</a></li>
                                    <li><a href="ui-draggable-cards.html">Draggable Cards</a></li>
                                    <li><a href="ui-checkbox-radio.html">Checkboxs-Radios</a></li>
                                    <li><a href="typography.html">Typography</a></li>
                                    <li><a href="ui-modals.html">Modals</a></li>
                                    <li><a href="ui-notification.html">Notification</a></li>
                                    <li><a href="ui-range-slider.html">Range Slider</a></li>
                                    <li><a href="ui-components.html">Components</a>
                                    <li><a href="ui-sweetalert.html">Sweet Alert</a>
                                    <li><a href="ui-treeview.html">Tree view</a>
                                    <li><a href="ui-widgets.html">Widgets</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-border-all"></i><span> Forms </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="form-elements.html">General Elements</a></li>
                                    <li><a href="form-advanced.html">Advanced Form</a></li>
                                    <li><a href="form-validation.html">Form Validation</a></li>
                                    <li><a href="form-wizard.html">Form Wizard</a></li>
                                    <li><a href="form-fileupload.html">Form Uploads</a></li>
                                    <li><a href="form-wysiwig.html">Wysiwig Editors</a></li>
                                    <li><a href="form-xeditable.html">X-editable</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-view-list"></i> <span> Tables </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="tables-basic.html">Basic Tables</a></li>
                                    <li><a href="tables-datatable.html">Data Table</a></li>
                                    <li><a href="tables-responsive.html">Responsive Table</a></li>
                                    <li><a href="tables-editable.html">Editable Table</a></li>
                                    <li><a href="tables-tablesaw.html">Tablesaw Table</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-chart-donut"></i><span> Charts </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="chart-flot.html">Flot Chart</a></li>
                                    <li><a href="chart-morris.html">Morris Chart</a></li>
                                    <li><a href="chart-chartist.html">Chartist Charts</a></li>
                                    <li><a href="chart-chartjs.html">Chartjs Chart</a></li>
                                    <li><a href="chart-other.html">Other Chart</a></li>
                                </ul>
                            </li>

                            <li>
                                <a href="calendar.html" class="waves-effect"><i class="zmdi zmdi-calendar"></i><span> Calendar </span></a>
                            </li>

                            <li>
                                <a href="inbox.html" class="waves-effect"><i class="zmdi zmdi-email"></i><span> Inbox Mail </span></a>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-google-maps"></i> <span> Maps </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="google-map.html">Google Maps</a></li>
                                    <li><a href="vector-map.html">Vector Maps</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-puzzle-piece"></i> <span> Icons </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="font-awesome-icons.html">FontAwesome Icons</a></li>
                                    <li><a href="material-icons.html">Material Icons</a></li>
                                    <li><a href="themify-icons.html">Themify Icons</a></li>
                                    <li><a href="dripicons.html">DripIcons</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-face"></i> <span> Account </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="page-profile.html">Profile</a></li>
                                    <li><a href="page-login.html">Login</a></li>
                                    <li><a href="register.html">Sign up</a></li>
                                    <li><a href="page-recoverpw.html">Recover Password</a></li>
                                    <li><a href="page-confirm-mail.html">Confirm Email</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-collection-item"></i><span> Pages </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="page-starter.html">Starter Page</a></li>
                                    
                                    <li><a href="extras-projects.html">Projects</a></li>
                                    <li><a href="extras-tour.html">Tour</a></li>
                                    <li><a href="extras-taskboard.html">Taskboard</a></li>
                                    <li><a href="extras-taskdetail.html">Task Detail</a></li>

                                    <li><a href="extras-contact.html">Contact list</a></li>
                                    <li><a href="extras-pricing.html">Pricing</a></li>
                                    <li><a href="extras-timeline.html">Timeline</a></li>
                                    <li><a href="extras-invoice.html">Invoice</a></li>
                                    <li><a href="extras-faq.html">FAQ</a></li>
                                    <li><a href="extras-gallery.html">Gallery</a></li>
                                    <li><a href="extras-email-template.html">Email template</a></li>
                                    <li><a href="extras-maintenance.html">Maintenance</a></li>
                                    <li><a href="extras-comingsoon.html">Coming soon</a></li>
                                    <li><a href="page-404.html">Error 404</a></li>
                                    <li><a href="page-500.html">Error 500</a></li>
                                </ul>
                            </li> -->

                            

                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>

            </div> --%>
            
            
            
            <div class="container-fluid">
		<!-- Menu nav -->
		<div class="btm_menu">
			<div class="menus_wrp text-center">
				<ul class="menu_line">
					<li class="single_menu child_br active">
						<div class="menu_one">
							<a href="${pageContext.request.contextPath}/User/Login/Process">
								<div class="menu_imgtxt">
									<img src="${pageContext.request.contextPath}/resources/newui/assets/img/home.png">
									<span>Home</span>
								</div>
							</a>
						</div>
					</li>
					<li class="single_menu child_br">
						<div class="menu_one">
							<a href="${pageContext.request.contextPath}/User/PhysicalCardRequest">
								<div class="menu_imgtxt">
									<img src="${pageContext.request.contextPath}/resources/newui/assets/img/pcard.png">
									<span>Physical Card</span>
								</div>
							</a>
						</div>
					</li>
					<li class="single_menu child_br">
						<div class="menu_one">
							<a href="${pageContext.request.contextPath}/User/Expenses">
								<div class="menu_imgtxt">
									<img src="${pageContext.request.contextPath}/resources/newui/assets/img/txns.png">
									<span>Transaction</span>
								</div>
							</a>
						</div>
					</li>
					<li class="single_menu child_br">
						<div class="menu_one">
							<a href="#">
								<div class="menu_imgtxt">
									<img src="${pageContext.request.contextPath}/resources/newui/assets/img/help.png">
									<span>Help</span>
								</div>
							</a>
						</div>
					</li>
					<li class="single_menu child_br">
						<div class="menu_one">
							<a href="#">
								<div class="menu_imgtxt">
									<img src="${pageContext.request.contextPath}/resources/newui/assets/img/affiliate.png">
									<span>Refer a Friend</span>
								</div>
							</a>
						</div>
					</li>
					<li class="single_menu">
						<div class="menu_one">
							<a href="${pageContext.request.contextPath}/User/Login/Logout">
								<div class="menu_imgtxt">
									<img src="${pageContext.request.contextPath}/resources/newui/assets/img/logout.png">
									<span>Sign Out</span>
								</div>
							</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<!-- /Menu nav -->
	</div>
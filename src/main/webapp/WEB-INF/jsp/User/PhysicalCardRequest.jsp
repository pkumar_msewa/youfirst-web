<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

	<title>Cashier | Physical Card</title>
	<!-- css starts here -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/newui/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/newui/assets/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/newui/assets/css/style.css">
	
	 <!-- App Favicon -->
         <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/User/assets/images/favicon.png">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

	<link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet">
	
	<script type="text/javascript">
  var contextPath="${pageContext.request.contextPath}";
  </script>
  <script type="text/javascript">
  var context_path="${pageContext.request.contextPath}";
  </script>
</head>
<body>

<!-- top nav -->
<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />
            <!-- Top Bar End -->
            <!-- ========== Left Sidebar Start ========== -->
            <!-- Left Sidebar End -->


<!-- Main Content -->
<div id="wrapper">
	<jsp:include page="/WEB-INF/jsp/User/Menu.jsp" />
	
	<c:choose>
	<c:when test="${cardstats == 'Active' }">
	<section id="cardSect">
    <div class="PhysCard-wrapped">
      <div class="card_wrapper card-white bg-2">
        <div class="cdetails-wrp">
          <div class="phycard_head text-center">
            <h3>Physical Card</h3>
          </div>
          <div class="container">
              <div class="row">
                <div class="col-sm-12">
                  <div class="col-md-8 col-sm-12 col-xs-12 card-pd">
                    <div class="col-md-8 col-md-offset-1 col-sm-12 col-xs-12">
                      <div class="card">
                        <img src="${pageContext.request.contextPath}/resources/newui/assets/img/card.png" class="img-responsive">
                          <div class="card-details custm_pos text-trans">
                            <div class="cvv text-right"><span class="c1">cvv</span>&nbsp;<span class="c2">${cardResponse.cvv}</span></div>
                            <div class="card_num fs-20">${cardResponse.walletNumber}</div>
                            <div class="exp_dt"><span class="c1">valid</span><br><span class="c2">${cardResponse.expiryDate}</span></div>
                            <div class="card_holdr fs-20">${cardResponse.holderName}</div>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-md-offset-1 col-sm-6 col-xs-6">
                      <div class="load_mny text-center">
                        <button class="btn btn-load" id="btn2"  data-toggle="modal" data-target="#restpn"><i class="fa fa-repeat"></i>&nbsp; Reset Pin</button>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                    <center>
                      <img src="${pageContext.request.contextPath}/resources/newui/assets/img/trio-1.png" class="trio-phy">
                    </center>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </section>
	</c:when>
	<c:when test="${cardstats == 'Requested' }">
	<section id="cardSect">
    <div class="PhysCard-wrapped">
      <div class="card_wrapper card-white bg-2">
        <div class="cdetails-wrp">
          <div class="phycard_head text-center">
            <h3>Physical Card</h3>
          </div>
          <div class="container">
              <div class="row">
                <div class="col-sm-12">
                  <div class="col-md-8 col-sm-12 col-xs-12 card-pd">
                    <div class="col-md-8 col-md-offset-1 col-sm-12 col-xs-12">
                      <div class="card">
                        <img src="${pageContext.request.contextPath}/resources/newui/assets/img/card.png" class="img-responsive">
                          <div class="card-details custm_pos text-trans">
                             <div class="cvv text-right"><span class="c1">cvv</span>&nbsp;<span class="c2">${cardResponse.cvv}</span></div>
                            <div class="card_num fs-20">${cardResponse.walletNumber}</div>
                            <div class="exp_dt"><span class="c1">valid</span><br><span class="c2">${cardResponse.expiryDate}</span></div>
                            <div class="card_holdr fs-20">${cardResponse.holderName}</div>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-md-offset-1 col-sm-6 col-xs-12">
                      <div class="load_mny text-center">
                        <center><button class="btn btn-load" id="btn2"  data-toggle="modal" data-target="#activeTab"><i class="fa fa-plus-circle"></i>&nbsp; Activate Now</button></center>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                    <center>
                      <img src="${pageContext.request.contextPath}/resources/newui/assets/img/trio.png" class="trio-phy">
                    </center>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </section>
	</c:when>
	<c:when test="${cardstats == 'Received'}">
	<section id="cardSect">
    <div class="PhysCard-wrapped">
      <div class="card_wrapper card-white bg-2">
        <div class="cdetails-wrp">
          <div class="phycard_head text-center">
            <h3>Physical Card</h3>
          </div>
          <div class="container">
              <div class="row">
                <div class="col-sm-12">
                  <div class="col-md-8 col-sm-12 col-xs-12 card-pd">
                    <div class="col-md-8 col-md-offset-1 col-sm-12 col-xs-12">
                      <div class="card">
                        <img src="${pageContext.request.contextPath}/resources/newui/assets/img/card.png" class="img-responsive">
                          <div class="card-details custm_pos text-trans">
                             <div class="cvv text-right"><span class="c1">cvv</span>&nbsp;<span class="c2">${cardResponse.cvv}</span></div>
                            <div class="card_num fs-20">${cardResponse.walletNumber}</div>
                            <div class="exp_dt"><span class="c1">valid</span><br><span class="c2">${cardResponse.expiryDate}</span></div>
                            <div class="card_holdr fs-20">${cardResponse.holderName}</div>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-md-offset-1 col-sm-6 col-xs-12">
                      <div class="load_mny text-center">
                        <center><button class="btn btn-load" id="btn2"  data-toggle="modal" data-target="#activationCodeTab"><i class="fa fa-plus-circle"></i>&nbsp; Activate Now</button></center>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                    <center>
                      <img src="${pageContext.request.contextPath}/resources/newui/assets/img/trio.png" class="trio-phy">
                    </center>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </section>
	</c:when>
	<c:otherwise>
	<section id="cardSect">
		<div class="PhysCard-wrapped">
			<div class="card_wrapper card-white bg-2">
				<div class="cdetails-wrp">
					<div class="phycard_head text-center">
						<h3>Physical Card</h3>
					</div>
					<div class="container">
              <div class="row">
                <div class="col-sm-12">
                  <div class="col-md-8 col-sm-12 col-xs-12 card-pd">
                    <div class="col-md-8 col-md-offset-1 col-sm-12 col-xs-12">
                      <div class="card">
                        <img src="${pageContext.request.contextPath}/resources/newui/assets/img/card.png" class="img-responsive">
                          <div class="card-details custm_pos text-trans">
                            <div class="cvv text-right"><span class="c1">cvv</span>&nbsp;<span class="c2">${cardResponse.cvv}</span></div>
                            <div class="card_num fs-20">${cardResponse.walletNumber}</div>
                            <div class="exp_dt"><span class="c1">valid</span><br><span class="c2">${cardResponse.expiryDate}</span></div>
                            <div class="card_holdr fs-20">${cardResponse.holderName}</div>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-md-offset-1 col-sm-6 col-xs-12">
                      <div class="load_mny text-center">
                        <center><button class="btn btn-load" id="btn1" onclick="balanceCheck()"><i class="fa fa-plus-circle"></i>&nbsp; Apply</button></center>
                      </div>
                    </div>
                   <!--  <div class="col-md-3 col-sm-6 col-xs-6">
                      <div class="load_mny text-center">
                        <button class="btn btn-load" id="btn2"  data-toggle="modal" data-target="#activeTab"><i class="fa fa-plus-circle"></i>&nbsp; Activate</button>
                      </div>
                    </div> -->
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                    <center>
                      <img src="${pageContext.request.contextPath}/resources/newui/assets/img/trio.png" class="trio-phy">
                    </center>
                  </div>
                </div>
              </div>
          </div>
				</div>
			</div>
		</div>
	</section>
	</c:otherwise>
	</c:choose>
</div>
<!-- /Main Content -->


<div class="modal fade" id="activeTab" role="dialog">
    <div class="modal-dialog modal-sm">
    
      <!-- Modal content-->
      <div class="modal-content" style="z-index:9999; margin-top:85px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        <center><h3>Physical Card</h3></center>
        </div>
        <div class="modal-body ">
        <div class="row">
          <form action="#" class="">
            <div class="col-sm-10 col-sm-offset-1">
              <div class="form-group">
                    <label for="Address">Kit No<span style="color:red">*</span></label>
                     <input type="text" id="proxyNo" class="form-control" maxlength="12" onkeypress="return isNumberKey(event);">
                     <p class="error" id="error_proxyNo" style="color: red;"></p>
                </div>

                     <div class="form-group">
              <center><button type="button" class="btn btn-primary" id="proxyNumbr" onclick="proxyCode()" >Next</button></center>
                   </div>
                 </div>
                </form>
        </div>
        </div>
        <div class="modal-footer" style="border-top: none;">
         <div id="proxyError"></div>
        </div>
       
      </div>
      
    </div>
  </div> 


<!-- 	Error	DISLCSAMER MODAL -->


<div class="modal fade-scale" id="errorDisclamer" role="dialog">
    <div class="modal-dialog modal-sm">
      <!-- Modal content-->
      <div class="modal-content" style="z-index:9999; margin-top:85px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <center><h3>Physical Card</h3></center>
        </div>
        <div class="modal-body ">
          <form action="#" class="">
            
            <div class="form-group">
              <div id="errorDisclamerMessage"></div>
            </div>
            <div class="form-group">
    					<center><button type="button" class="btn btn-primary" data-dismiss="modal">Okay</button></center>
            </div>
          </form>
        </div>
        <div class="modal-footer" style="border-top: none;">
         <div id="proxyError"></div>
        </div>
       
      </div>
      
    </div>
  </div> 




			<!-- SUCCESS DISCLAMER -->
	
<div class="modal fade-scale" id="successDisclamer" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content" style="z-index:9999; margin-top:85px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        <center><h3>Disclaimer</h3></center>
        </div>
        <div class="modal-body ">
            <div id="successDisclamerMessage"></div>
                            <br>
                     <div class="form-group col-sm-12">
    				<center><button id="btn1" class="btn  btn-apply" data-toggle="modal" data-dismiss="modal" data-target="#myModal">Apply</button></center>
                	 </div>
        </div>
        <div class="modal-footer" style="border-top: none;">
         <div id="proxyError"></div>
        </div>
      </div>
    </div>
  </div> 




		<!-- RESET PIN DISCLAIMER -->
	<div class="modal fade-scale" id="restpn" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content col-xs-10 col-xs-offset-2" style="z-index:9999; margin-top:85px;">
        <div class="modal-header">
        <center><h3>Disclaimer</h3></center>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body ">
            <div class="form-group">
              <div id="">Your 4 digit PIN will be sent to your registered mobile No.</div>
            </div>
            <div class="form-group">
    		      <center><button id="btn1" class="btn  btn-primary" onclick="restpin()">Continue</button></center>
            </div>
        </div>
        <div class="modal-footer" style="border-top: none;">
         <div id="proxyError"></div>
        </div>
      </div>
    </div>
  </div> 


    
    
    <div class="modal fade-scale" id="activationCodeTab" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content col-xs-10 col-xs-offset-2" style="z-index:9999; margin-top:85px;">
        <div class="modal-header">
        <center><h3>Activation Code</h3></center>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body ">
        <form action="#" class="">
        <div class="row">
            <div class="col-xs-6 col-sm-offset-3">
              <div class="form-group">
                    <label for="Address">Activation Code<span style="color:red">*</span></label>
                     <input type="text" id="activationCode" class="form-control" maxlength="6" onkeypress="return isNumberKey(event);">
                     <p class="error" id="error_activationCode" style="color: red;"></p>
                </div>
                 </div>
                 </div>
                      <div class="row text-center">
                      <button type="button" class="btn btn-primary" id="resendcd" onclick="resendActivationCode()">Resend Code</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    				<button type="button" class="btn btn-primary" id="codeactive" onclick="actvCode()">Activate</button>	
                	</div>
                </form>
        </div>
        <div class="modal-footer" style="border-top: none;">
         <div id="activeError"></div>
        </div>
       
      </div>
      
    </div>
  </div>

  <!-- Modal -->
  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <center><h4 class="modal-title">Physical Card</h4></center>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="add-1">Address 1<span style="color:red">*</span></label>
              <input type="text" id="address1" class="form-control" maxlength="100">
              <span class="error" id="error_address1" style="color: red;"></span>
            </div>
            <div class="form-group">
              <label for="add-1">Address 2<span style="color:red">*</span></label>
              <input type="text" id="address2" class="form-control" maxlength="100">
              <span class="error" id="error_address2" style="color: red;"></span>
            </div>
            <div class="form-group">
              <label for="add-1">City<span style="color:red">*</span></label>
              <input type="text" id="city" class="form-control" maxlength="26" onkeypress="return isAlphKey(event);">
              <span class="error" id="error_city" style="color: red;"></span>
            </div>
            <div class="form-group">
              <label for="add-1">State<span style="color:red">*</span></label>
              <input type="text" id="state" class="form-control" maxlength="26" onkeypress="return isAlphKey(event);">
              <span class="error" id="error_state" style="color: red;"></span>
            </div>
            <div class="form-group">
              <label for="add-1">Pincode<span style="color:red">*</span></label>
              <input type="text" id="pinCode" class="form-control" maxlength="6" onkeypress="return isNumberKey(event);">
              <span class="error" id="error_pinCode" style="color: red;"></span>
            </div>
            <div class="form-group">
              <center><button type="button" class="btn btn-primary" id="requestCard" onclick="requestPhysicalCard()">Request</button></center>
            </div>
          </form>
        </div>
      </div>

    </div>
  </div>
    
    <!-- script starts here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/newui/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/newui/assets/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/userDetail.js"></script>
<script src="${pageContext.request.contextPath}/resources/User/assets/js/all.js"></script>
    
     <script type="text/javascript">
    function requestPhysicalCard(){
    	var valid=true;
    	var address1=$('#address1').val();
    	var address2=$('#address2').val();
    	var city=$('#city').val();
    	var state=$('#state').val();
    	var pinCode=$('#pinCode').val();

    	if(address1=='') {
    		$("#error_address1").html("Please enter the address1");
    		valid = false;
    		console.log("valid: "+"panCardImg");
    	}
    	if(address2=='') {
    		$("#error_address2").html("Please enter the address2");
    		valid = false;
    		console.log("valid: "+"panCardImg");
    	}
    	if(city=='') {
    		$("#error_city").html("Please enter the city");
    		valid = false;
    		console.log("valid: "+"panCardImg");
    	}if(state=='') {
    		$("#error_state").html("Please enter the state");
    		valid = false;
    		console.log("valid: "+"panCardImg");
    	}if(pinCode=='') {
    		$("#error_pinCode").html("Please enter the pincode");
    		valid = false;
    		console.log("valid: "+"panCardImg");
    	}
    	if (valid) {
    		$("#requestCard").addClass("disabled");
//    			$("#addAgent").html(spinnerUrl);

    		$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : contextPath+"/Api/v1/User/Windows/en/PhysicalCardRequestWeb",
    			dataType : 'json',
    			hash:'123456',
    			data : JSON.stringify({
    				"address1" :"" + address1 + "",
    				"address2" : "" + address2 + "",
    				"city" : "" + city + "",
    				"state" : "" + state + "",
    				"pinCode" : "" + pinCode + ""
    			}),
    			
    			
    			success : function(response) {
    				console.log("response:: "+response.code);
    				$('#myModal').modal('hide');
    				console.log("response:: "+response.message);
    				$("#requestCard").removeClass("disabled");
//     				$("#registerButton").html("Sign Up");
    				if (response.code.includes("S00")) {
    						swal("Success!!", response.message, "success");
    					}else{
    						swal({
    							  type: 'error',
    							  title: 'Failure!!',
    							  text: response.message
    							});
    				}			
    			}
    		});
    	}
    	
    	var timeout = setTimeout(function(){
    		$("#error_address1").html("");
    		$("#error_address2").html("");
    	    $("#error_city").html("");
    		$("#error_state").html("");
    		$("#error_country").html("");
    		$("#error_pinCode").html("");
    	    $("#error_idType").html("");
    		$("#error_idNumber").html("");
    		$("#error_image").html("");
    	}, 3000);
    	
    	}
    </script>
    
     <script type="text/javascript">
    function proxyCode(){
    	var valid=true;
    	var proxyNo=$('#proxyNo').val();

    	if(proxyNo=='') {
    		$("#error_proxyNo").html("Please enter the proxy number");
    		valid = false;
    		console.log("valid: "+"panCardImg");
    	}
    	
    	if (valid) {
    		$("#proxyNumbr").addClass("disabled");
//    			$("#addAgent").html(spinnerUrl);

    		$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : contextPath+"/Api/v1/User/Windows/en/AddPhysicalCardWeb",
    			dataType : 'json',
    			data : JSON.stringify({
    				"proxy_number" :"" + proxyNo + "",
    			}),
    			success : function(response) {
    				console.log("response:: "+response.code);
    				console.log("response:: "+response.message);
    				$("#proxyNumbr").removeClass("disabled");
//     				$("#registerButton").html("Sign Up");
    				if (response.code.includes("S00")) {
    					$('#activeTab').modal('hide');
    					$('#activationCodeTab').modal('show');
    					$("#activeError").html(response.message);
    					}else{
    						$("#proxyError").html(response.message);
    				}			
    			}
    		});
    	}
    	
    	var timeout = setTimeout(function(){
    		$("#error_proxyNo").html("");
    	}, 3000);
    	
    	}
    </script>
    
    
    
     <script type="text/javascript">
    function actvCode(){
    	var valid=true;
    	var activationCode=$('#activationCode').val();

    	if(activationCode=='') {
    		$("#error_activationCode").html("Please enter the activation code");
    		valid = false;
    		console.log("valid: "+"panCardImg");
    	}
    	
    	if (valid) {
    		$("#codeactive").addClass("disabled");
//    			$("#addAgent").html(spinnerUrl);

    		$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : contextPath+"/Api/v1/User/Windows/en/ActivatePhysicalCardWeb",
    			dataType : 'json',
    			hash:'123456',
    			data : JSON.stringify({
    				"activationCode" :"" + activationCode + "",
    			}),
    			
    			success : function(response) {
    				console.log("response:: "+response.code);
    				console.log("response:: "+response.message);
    				$("#codeactive").removeClass("disabled");
//     				$("#registerButton").html("Sign Up");
    				if (response.code.includes("S00")) {
    					$('#activationCodeTab').modal('hide');
    					swal("Success!!", response.message, "success");
    					}else{
    						swal({
    							  type: 'error',
    							  title: 'Failure!!',
    							  text: response.message
    							});
    				}			
    			}
    		});
    	}
    	
    	var timeout = setTimeout(function(){
    		$("#error_activationCode").html("");
    	}, 3000);
    	
    	}
    </script>
    
    
    
    
     <script type="text/javascript">
    function resendActivationCode(){
//     		$("#kycre").addClass("disabled");
//    			$("#addAgent").html(spinnerUrl);
    		$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : contextPath+"/Api/v1/User/Windows/en/ResendActivationCodeWeb",
    			dataType : 'json',
    			hash:'123456',
    			data : JSON.stringify({
    				"activationCode" :" ",
    			}),
    			
    			success : function(response) {
    				console.log("response:: "+response.code);
    				console.log("response:: "+response.message);
//     				$("#registerButton").removeClass("disabled");
//     				$("#registerButton").html("Sign Up");
    				if (response.code.includes("S00")) {
    					$('#activeError').html(response.message);
    					}else{
    						$('#activeError').html(response.message);
    				}			
    			}
    		});
    	}
    </script>
    
    
    
    
    <script type="text/javascript">
    function balanceCheck(){
    	$.ajax({
			type : "POST",
			contentType : "application/json",
			url : contextPath+"/Api/v1/User/Windows/en/GetBalanceWeb",
			dataType : 'json',
			data : JSON.stringify({
				"sessionId" :" ",
			}),
			
			success : function(response) {
				console.log("response:: "+response.code);
				console.log("response:: "+response.message);
// 				$("#registerButton").removeClass("disabled");
// 				$("#registerButton").html("Sign Up");
				if (response.code.includes("S00")) {
					$("#successDisclamer").modal('show')
					$('#successDisclamerMessage').html(response.message);
					}else{
						$("#errorDisclamer").modal('show')
						$('#errorDisclamerMessage').html(response.message);
				}			
			}
		});
    	
    }
    
    </script>
    
    
    
    <script type="text/javascript">
    function restpin(){
    	$.ajax({
			type : "POST",
			contentType : "application/json",
			url : contextPath+"/Api/v1/User/Windows/en/ResetPinWeb",
			dataType : 'json',
			data : JSON.stringify({
				"amount" :" ",
			}),
			
			success : function(response) {
				console.log("response:: "+response.code);
				console.log("response:: "+response.message);
				$("#restpn").modal('hide')
// 				$("#registerButton").removeClass("disabled");
// 				$("#registerButton").html("Sign Up");
				if (response.code.includes("S00")) {
					swal("Congrats!!", response.message, "success");
					}else{
						swal({
							  type: 'error',
							  title: 'Sorry!!',
							  text: response.message
							});
				}			
			}
		});
    	
    }
    
    </script>
    
    
<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>
<script type="text/javascript">
function isAlphKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
}
</script>


</body>
</html>
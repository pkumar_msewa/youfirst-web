<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
   
<head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
	<meta Http-Equiv="Cache-Control" Content="no-cache">
    <meta Http-Equiv="Pragma" Content="no-cache">
    <meta Http-Equiv="Expires" Content="0">
	<sec:csrfMetaTags/>
		
        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/User/assets/images/favicon.png">

        <!-- App title -->
        <title>CASHIER Card | Wallet</title>

        <!-- Tour css -->
        <!-- <link href="assets/plugins/hopscotch/css/hopscotch.min.css" rel="stylesheet" type="text/css" /> -->

        <!-- Font-awesome css -->
        <link href="${pageContext.request.contextPath}/resources/User/assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <!--Animate CSS -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/User/assets/plugins/animate.less/animate.css">

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/User/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/User/assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/User/assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/User/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/User/assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/User/assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/User/assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/User/assets/css/custom.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="${pageContext.request.contextPath}/resources/User/assets/js/modernizr.min.js"></script>
 <!-- <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script> -->

<script type="text/javascript">
var context_path="${pageContext.request.contextPath}";
</script>
    </head>
	

    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
		
            <!-- Top Bar Start -->
            <jsp:include page="/WEB-INF/jsp/User/Header.jsp" />
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
		<jsp:include page="/WEB-INF/jsp/User/LeftMenu.jsp" />
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4">
                                <!-- <button type="button" class="btn btn-purple btn-rounded w-md waves-effect waves-light m-b-20">Create Project</button> -->
                                <h3>Wallet</h3>
                            </div>
                            <div class="col-sm-6">
                                <!-- <button type="button" class="btn btn-purple btn-rounded w-md waves-effect waves-light m-b-20">Create Project</button> -->
                                <span>${LoadMess}</span>
                            </div>
                        </div><hr>
                        <!-- end row -->

                        <div class="row wallet-wrap">
                            <div class="col-md-4 col-md-offset-1">
                                <div class="card-box">
                                    <div class="wallet-text mb-20">
                                        <h4>Wallet Balance</h4>
                                    </div><hr>
                                    <div class="wallet-body">
                                        <div class="wallet-img">
                                            <center><img src="${pageContext.request.contextPath}/resources/User/assets/images/wallet.png" class="img-responsive"></center>
                                        </div>
                                        <div class="wallet-bal">
                                            <center>
                                                <h1><i class="fa fa-inr"></i>&nbsp;<span id="cardBalance"><%-- ${user.accountDetail.balance} --%></span></h1>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-md-offset-1">
                                <div class="card-box">
                                    <div class="load_mny-wrp">
                                        <div class="wallet-text">
                                            <h4>Add Money to your wallet</h4>
                                        </div><hr>
                                        <div class="lod_money">
                                            <form action="${pageContext.request.contextPath}/Api/v1/User/web/en/LoadMoney/InitiateRequestWeb" method="post">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="amount" placeholder="Money">
                                                </div>
                                                <center><button class="btn btn-purple btn-lg" type="submit">Proceed</button></center>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer">
                    2017 © Copyright iMoney.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <div class="side-bar right-bar">
                <a href="javascript:void(0);"  class="right-bar-toggle animated">
                    <i class="zmdi zmdi-close-circle-o"></i>
                </a>
                <h4 class="">Notifications</h4>
                <div class="notification-list nicescroll">
                    <ul class="list-group list-no-border user-list">
                        <li class="list-group-item">
                            <a href="#" class="user-list-item">
                                <div class="avatar">
                                    <img src="${pageContext.request.contextPath}/resources/User/assets/images/users/avatar-2.jpg" alt="">
                                </div>
                                <div class="user-desc">
                                    <span class="name">Michael Zenaty</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">2 hours ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#" class="user-list-item">
                                <div class="icon bg-info">
                                    <i class="zmdi zmdi-account"></i>
                                </div>
                                <div class="user-desc">
                                    <span class="name">New Signup</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">5 hours ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#" class="user-list-item">
                                <div class="icon bg-pink">
                                    <i class="zmdi zmdi-comment"></i>
                                </div>
                                <div class="user-desc">
                                    <span class="name">New Message received</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">1 day ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item active">
                            <a href="#" class="user-list-item">
                                <div class="avatar">
                                    <img src="${pageContext.request.contextPath}/resources/User/assets/images/users/avatar-3.jpg" alt="">
                                </div>
                                <div class="user-desc">
                                    <span class="name">James Anderson</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">2 days ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item active">
                            <a href="#" class="user-list-item">
                                <div class="icon bg-warning">
                                    <i class="zmdi zmdi-settings"></i>
                                </div>
                                <div class="user-desc">
                                    <span class="name">Settings</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">1 day ago</span>
                                </div>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
            <!-- /Right-bar -->

        </div>
        <!-- END wrapper -->

         <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/jquery.scrollTo.min.js"></script>
		 <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/userDetail.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/main.js"></script>
        <!-- <script>
            $(document).ready(function () {
                var placementRight = 'right';
                var placementLeft = 'left';

                // Define the tour!
                var tour = {
                    id: "my-intro",
                    steps: [
                        {
                            target: "logo-tour",
                            title: "Current online user",
                            content: "You can find here status of user who's currently online.",
                            placement: placementRight,
                            yOffset: 10
                        },
                        {
                            target: 'page-title-tour',
                            title: "User settings",
                            content: "You can edit you profile info here.",
                            placement: 'bottom',
                            zindex: 999
                        },
                        {
                            target: 'notification-tour',
                            title: "Configuration tools",
                            content: "Here you can change theme skins and other features.",
                            placement: 'left',
                            zindex: 999
                        }
                    ],
                    showPrevButton: true
                };

                // Start the tour!
                hopscotch.startTour(tour);
            });
        </script> -->

    </body>

</html>
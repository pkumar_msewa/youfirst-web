<%-- <!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left" id="logo-tour">
                    <a href="${pageContext.request.contextPath}/User/Login/Process" class="logo"><span><img src="${pageContext.request.contextPath}/resources/User/assets/images/logo1.png" alt="invoice logo" style="width: 160px;"></span><i class="zmdi zmdi-layers"></i></a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <!-- <li>
                                <h4 class="page-title" id="page-title-tour">Tour</h4>
                            </li> -->
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- <li>
                                <!-- Notification --
                                <div class="notification-box" id="notification-tour">
                                    <ul class="list-inline m-b-0">
                                        <li>
                                            <a href="javascript:void(0);"  data-notifications="2" class="notification-number right-bar-toggle animated swing" id="notification">
                                                <i class="zmdi zmdi-notifications-none msg"></i>
                                            </a>
                                            
                                        </li>
                                    </ul>
                                </div>
                                <!-- End Notification bar --
                            </li> -->
                            <!-- <li class="hidden-xs">
                                <form role="search" class="app-search" id="search-tour">
                                    <input type="text" placeholder="Search..."
                                           class="form-control">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </form>
                            </li> -->
                            <li>
                               <a href="${pageContext.request.contextPath}/User/LoadMoney"  style="float: right;  font-size: 16px; font-weight: 600; margin-top: 11px;"><img src="${pageContext.request.contextPath}/resources/User/assets/images/wallet.png" alt="wallet-img" title="Cashier Card Wallet"  data-toggle="dropdown" class="profile-img img-responsive" style="float: left;margin-top: -12%;"> 
                                <a href="${pageContext.request.contextPath}/User/LoadMoney" style="float: right; font-size: 16px; font-weight: 600; margin-top: 11px;"><i class="fa fa-inr"></i> <span id="cardBalance"> ${user.accountDetail.balance} </span></a> 
                            </li>
                            <!-- <li>
                                <div class="btn-group">
                                    <img src="assets/images/users/avatar-10.jpg" alt="user-img" title="Mat Helme" data-toggle="dropdown"  class="profile-img img-circle img-responsive">
                                    <ul class="custom-dropdown dropdown-menu animated fadeInUp" role="menu">
                                        <li><a href="#"><i class="zmdi zmdi-account-o"></i> My Account</a></li>
                                        <li><a href="#"><i class="zmdi zmdi-settings"></i> Settings</a></li>
                                        <li><a href="#"><i class="zmdi zmdi-help-outline"></i> Support</a></li>
                                        <li><a href="#"><i class="zmdi zmdi-power"></i> Logout</a></li>
                                    </ul>   
                                </div>
                            </li> -->
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div> --%>
            
            
     <div class="top_nav">
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span> 
	      </button>
	      <a class="navbar-brand" href="${pageContext.request.contextPath}/User/Login/Process">
	      	<img src="${pageContext.request.contextPath}/resources/User/assets/images/logo1.png" class="img-responsive" style="padding-top: 2%">
	      </a>
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
	      
	      <ul class="nav navbar-nav navbar-right">
	      	<li>
	        	<a href="${pageContext.request.contextPath}/User/MyProfile">
	        		<div class="welc_txtWrp text-right">
	        			<span class="welcom">Welcome</span><br>
	        			<span class="welcom_usrnme" style="margin-right: -3%">${user.userDetail.firstName}</span>
	        			<br><span><i class="fa fa-inr"></i>&nbsp; <strong id="cardBalance"></strong></span>
	        		</div>
	        	</a>
	        </li>
	        <li>
	        	<a href="${pageContext.request.contextPath}/User/MyProfile">
	        		<img src="${pageContext.request.contextPath}/resources/newui/assets/img/user.png" class="img-responsive">
	        	</a>
	        </li>
	      </ul>
	    </div>
	  </div>
	</nav>
</div>
            
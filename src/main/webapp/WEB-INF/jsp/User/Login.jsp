<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta Http-Equiv="Cache-Control" Content="no-cache">
    <meta Http-Equiv="Pragma" Content="no-cache">
    <meta Http-Equiv="Expires" Content="0">
	<sec:csrfMetaTags/>
    <!-- Title -->
    <title>CASHIER Cards | Login</title>

    <!-- Favicon -->
    <link rel="icon" href="${pageContext.request.contextPath}/resources/img/core-img/favicon.png">

    <!-- Core Stylesheet -->
    <link href="${pageContext.request.contextPath}/resources/css/Style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/custom.css" rel="stylesheet">

    <!-- Responsive CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/responsive.css" rel="stylesheet">
    
     <script type="text/javascript">
        function noBack() {
            window.history.forward();
        }
     </script>
        

    <style>
        /* width */
        ::-webkit-scrollbar {
            width: 10px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1; 
        }
         
        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #888; 
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #555; 
        }
        .main_topheader {
            -webkit-box-shadow: 0 1px 0 rgba(12,13,14,.1), 0 1px 3px rgba(12,13,14,.1), 0 4px 20px rgba(12,13,14,.035), 0 1px 1px rgba(12,13,14,.025);
            box-shadow: 0 1px 0 rgba(12,13,14,.1), 0 1px 3px rgba(12,13,14,.1), 0 4px 20px rgba(12,13,14,.035), 0 1px 1px rgba(12,13,14,.025);
            background: #fff;
            color: #8e8e8e;
            padding-top: 1px;
            padding-bottom: 1px;
            posi/tion: initial;
            background: #efefef;
        }
        .form-box {
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            -webkit-box-shadow: 0 12px 200px #e7e7e7;
            box-shadow: 0 12px 200px #e7e7e7;
            background-color: #fff;
            color: #7b7b7b;
            min-height: 314px;
            max-width: 425px;
            margin-right: auto;
            margin-left: auto;
            padding: 55px 4.5% 50px;
        }
        .cta-actions {
            padding-top: 45px;
        }
        .signLog_btn {
            border-radius: 100px;
            /* background-color: #4bc5ac; */
            background: -webkit-linear-gradient(#ef1821, #291859);
		    background: -moz-linear-gradient(#ef1821, #291859);
		    background: -ms-linear-gradient(#ef1821, #291859);
		    background: -o-linear-gradient(#ef1821, #291859);
		    background: linear-gradient(#ef1821, #291859);
            color: #fff;
            font-size: 12px;
            line-height: 42px;
            min-width: 177px;
            text-align: center;
            border: none;
            transition: all .4s ease-in-out;
            cursor: pointer;
            display: inline-block;
            text-decoration: none;
            -webkit-box-shadow: #4bc5ac 0 21px 35px -17px;
            -moz-box-shadow: #4bc5ac 0 21px 35px -17px;
            box-shadow: 0 21px 35px -17px #4bc5ac;
        }
        .tnc {
            color: #c3c3c3;
            padding-top: 5px;
            font-size: 11px;
            line-height: 1.64;
        }
        .auth_form label {
            float: left;
        }
        .first-selection {
            background: url(${pageContext.request.contextPath}/resources/img/bg-img/waves2.svg);
            background-repeat: no-repeat;
            color: #fff;
            background-position: center;
            position: relative;
        }
    </style>

</head>

<body onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="">
    <!-- Preloader Start -->
    <div id="preloader">
        <div class="colorlib-load"></div>
    </div>

    <!-- ***** Header Area Start ***** -->
    <header class="header_area1 animated sticky">
        <div class="container-fluid">
            <div class="row align-items-center">
                <!-- Menu Area Start -->
                <div class="col-12 col-lg-12">
                    <div class="menu_area">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <!-- Logo -->
                            <a class="navbar-brand" href="${pageContext.request.contextPath}/Home">
                                <img src="${pageContext.request.contextPath}/resources/User/assets/images/logo1.png" class="img-fluid">
                            </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                            <!-- Menu Area -->
                            <div class="collapse navbar-collapse" id="ca-navbar">
                                <ul class="navbar-nav ml-auto" id="nav">
                                    <!-- <li class="nav-item active"><a class="nav-link" href="${pageContext.request.contextPath}/Home">Home</a></li> -->
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/Aboutus">About</a></li>
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/faq">FAQs</a></li>
<%--                                     <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/ContactUs">Contact</a></li> --%>
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/User/Login">Login</a></li>
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/User/SignUp">Signup</a></li>
                                </ul>
                                <!-- <div class="sing-up-button d-lg-none">
                                    <a href="${pageContext.request.contextPath}/User/Login">Login</a>
                                </div> -->
                            </div>
                        </nav>
                    </div>
                </div>
                <!-- Signup btn -->
                <!-- <div class="col-12 col-lg-2">
                    <div class="sing-up-button d-none d-lg-block">
                        <a href="${pageContext.request.contextPath}/User/SignUp">Sign Up</a>
                    </div>
                </div> -->
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->
    
    <section class="bg-white first-selection" id="" style="padding-top: 86px; padding-bottom: 45px;">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <!-- Heading Text  -->
                    <div class="section-heading">
                        <h2>Welcome, Login here</h2>
                        <div class="line-shape"></div>
                    </div>
                    <div style="color: red">${errormsgg}</div>
					<div id="frgtMessage_success"></div>
					
                    <div class="form-box">
                        <form class="auth_form" action="${pageContext.request.contextPath}/User/Login/Process" method="Post">
                          <!-- <div class="form-group">
                            <label for="name">Your Name:</label>
                            <input type="text" class="form-control" id="">
                          </div> -->
                          <div class="form-group">
                            <label for="email">Mobile Number:</label>
                            <input type="text" class="form-control" id="username" name="username" placeholder="Enter Your Number" autocomplete="off" pattern="[0-9.]+" maxlength="10" onkeypress="return isNumberKey(event);">
                          </div>
                          <!-- <div class="form-group">
                            <label for="phone">Phone:</label>
                            <input type="text" class="form-control" id="">
                          </div> -->
                          <div class="form-group">
                            <label for="pwd">Password:</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Enter Your Password" autocomplete="off" minlength="4">
                          </div>
                          <div class="form-group">
                            <label><input type="checkbox"> Remember me</label>
                            <p style="float: right;"><a href="${pageContext.request.contextPath}/User/ForgotPassword">Forgot Password?</a></p>
                          </div>
                          <div class="cta-actions text-center">
                              <button class="btn signLog_btn" title="LOGIN" type="submit" id="loggedin">LOGIN</button>
                          </div>
                        </form>
                        <!-- <div class="text-center tnc">
                            By clicking on the Create Account you accept the <br><a href="#" target="_blank">Terms and Conditions.</a>
                        </div> -->
                        <p class="mt-3">
                            Don't have an account? <a href="${pageContext.request.contextPath}/User/SignUp">Create Account</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        
    </section>

    <footer>
        
    </footer>
    <!-- <script type="text/javascript">
    $('#username, #password').bind('keyup', function() {
        if(allFilled()) $('#loggedin').removeAttr('disabled');
    });
    </script> -->

    <!-- Jquery-2.2.4 JS -->
    <script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script>
    <!-- Bootstrap-4 Beta JS -->
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <!-- All Plugins JS -->
    <script src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>
    <!-- Slick Slider Js-->
    <script src="${pageContext.request.contextPath}/resources/js/slick.min.js"></script>
    <!-- Footer Reveal JS -->
    <script src="${pageContext.request.contextPath}/resources/js/footer-reveal.min.js"></script>
    <!-- Active JS -->
    <script src="${pageContext.request.contextPath}/resources/js/active.js"></script>
    
    
  <script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>
<script type="text/javascript">
function isAlphKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
}
</script>
    
    
</body>

</html>
    
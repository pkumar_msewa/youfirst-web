<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
	<title>Cashier Card | My Profile</title>

	  <meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets-newui/img/favicon.png">

  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/bootstrap/css/bootstrap.css">

  	<!-- google font -->
  	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

  	<!-- font-awesome css -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/font-awesome/css/font-awesome.css">
  	<!-- custom css start here -->
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/css/custom.css">

<script type="text/javascript">
  var contextPath="${pageContext.request.contextPath}";
  </script>
  <script type="text/javascript">
  var context_path="${pageContext.request.contextPath}";
  </script>
</head>
<body>

<!-- Navbar starts here -->
<jsp:include page="/WEB-INF/jsp/User/Navabar.jsp"></jsp:include>
<!-- main content starts here -->
<section>
	<div class="container">
		<div class="innner-body">
			<div class="bimg-sec">
        <div class="row">
          <div class="col-md-12 prof-wrapper">
          <div class="upgrade_Acc">
          <div class="col-md-4 text-right">
              <div class="prof_acti-wrp">
              <c:choose>
              <c:when test="${mDetail == 'NONKYC'}">
                <div class="prof_acti-hed">
                  <span>Your Request is under process </span>
                </div>
                <div class="prof_acti-bdy">
                <button type="button" class="btn btn-white" data-toggle="modal" data-target="#kycModal" disabled="disabled">Upgrade Account.</button>
                </div>
                </c:when>
                <c:when test="${user.accountDetail.accountType.code == 'NONKYC'}">
                <div class="prof_acti-hed">
                  <span>Upgrade your Account to KYC</span>
                </div>
                <div class="prof_acti-bdy">
                  <button type="button" class="btn btn-white" data-toggle="modal" data-target="#kycModal">Upgrade Account.</button>
                </div>
                </c:when>
          	</c:choose> 
              </div>
            </div>
          </div>
            <div class="col-md-4">
              <div class="prof_acti-wrp">
                <center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/profile.svg" class="img-responsive"></center>
              </div>
            </div>
            <div class="col-md-4 text-left">
              <div class="prof_acti-wrp">
                <c:choose>
				<c:when test="${cardstats == 'Active' }">
                <div class="prof_acti-bdy">
<!--                   <button type="button" class="btn btn-white"  data-toggle="modal" data-target="#restpn">Apply Now</button> -->
                </div>
                </c:when>
                <c:when test="${cardstats == 'Requested' }">
                <div class="prof_acti-hed">
                  <span>Issue your physical card here.</span>
                </div>
                <div class="prof_acti-bdy">
                  <button type="button" class="btn btn-white"  data-toggle="modal" data-target="#kitModal">Activate Now</button>
                </div>
                </c:when>
                <c:when test="${cardstats == 'Shipped' }">
                <div class="prof_acti-hed">
                  <span>Issue your physical card here.</span>
                </div>
                <div class="prof_acti-bdy">
                  <button type="button" class="btn btn-white"  data-toggle="modal" data-target="#kitModal">Activate Now</button>
                </div>
                </c:when>
                <c:when test="${cardstats == 'Received' }">
                <div class="prof_acti-hed">
                  <span>Issue your physical card here.</span>
                </div>
                <div class="prof_acti-bdy">
                  <button type="button" class="btn btn-white"  data-toggle="modal" data-target="#activationCodeTab">Activate Now</button>
                </div>
                </c:when>
                <c:otherwise>
                <div class="prof_acti-hed">
                  <span>Issue your physical card here.</span>
                </div>
                <div class="prof_acti-bdy">
                  <button type="button" class="btn btn-white"  id="btn1" onclick="balanceCheck()">Apply Now</button>
                </div>
                </c:otherwise>
                </c:choose>
              </div>
            </div>
          </div>
        </div>   
      </div>
      <div class="row mt-130">
        <div class="col-md-10 col-md-offset-1">
          <div class="col-sm-6" style="border-right: 2px solid #9f201c">
            <div class="prof_dtls-Wrp">
              <div class="prof_dtls-hed text-center">
                <span>Profile Details</span>
              </div>
              <div class="prof_dtls-bdy">
                <div class="col-sm-6">
                  <div>
                    <span>Name: </span>
                  </div>
                  <div>
                    <span>Date of Birth: </span>
                  </div>
                  <div>
                    <span>Mobile No.: </span>
                  </div>
                  <div>
                    <span>E-Mail: </span>
                  </div>
                  <div>
                    <span>Account Type: </span>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div>
                    <span>${user.userDetail.firstName} ${user.userDetail.lastName}</span>
                  </div>
                  <div>
                    <span>${user.userDetail.dateOfBirth}</span>
                  </div>
                  <div>
                    <span>+91 <span>${user.userDetail.contactNo}</span></span>
                  </div>
                  <div>
                    <span>${user.userDetail.email}</span>
                  </div>
                  <div>
                    <span>${user.accountDetail.accountType.code}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="prof_pwd-Wrp">
              <div class="prof_pwd-hed text-center">
                <span>Change Password</span>
              </div>
              <div class="prof_pwd-bdy">
                <form>
                  <div class="form-group">
                    <label for="" class="sr-only">Existing Password</label>
                    <input type="password" class="form-control"  id="password" name="ePwd" placeholder="Existing Password" maxlength="6">
                    <p class="error" id="error_Password" style="color: red;"></p>
                  </div>
                  <div class="form-group">
                    <label for="" class="sr-only">New Password</label>
                    <input type="password" id="newPassword" class="form-control" name="nPwd" placeholder="New Password" maxlength="">
                    <p class="error" id="error_newPassword"  style="color: red;"></p>
                  </div>
                  <div class="form-group">
                    <label for="" class="sr-only">Confirm New Password</label>
                    <input type="password" class="form-control" name="cnPwd"  id="confirmPassword" placeholder="Confirm New Password" maxlength="6">
                    <p class="error" id="error_confirmPassword" class="error" style="color: red;"></p>
                  </div>
                  <div class="form-group text-right">
                    <button type="button" onclick="changePassword()" class="btn btn-red">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="text-center">
		<b><span>${KycMsg}</span></b>
		</div>
		</div>
	</div>
</section>

<jsp:include page="/WEB-INF/jsp/User/Footer.jsp"></jsp:include>	
<!-- Physical card apply modal starts here -->
<div id="phyModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
      <a href="#" class="close" data-dismiss="modal">&times;</a>
      <div class="modal-body">
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
          <div class="top-logo text-center">
            <img src="${pageContext.request.contextPath}/resources/assets-newui/img/cashround.svg" class="cashround">
          </div>
          <div class="kycWiz">
            <form id="regForm" action="#">
              <!-- Circles which indicates the steps of the form: -->
              <div class="text-center step-wrap">
                <span class="step"></span>
                <span class="step"></span>
              </div>
              <!-- One "tab" for each step in the form: -->
              <div class="tab">
                <div class="text-center instruct">
                  <p class="" id="successDisclamerMessage"></p>
                </div>
              </div>
              <div class="tab">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="postCod" class="sr-only">Address1</label>
                      <input type="text" id="address1" class="form-control" maxlength="100">
                      <span class="error" id="error_address1" style="color: red;"></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="cityNme" class="sr-only">Address2</label>
                       <input type="text" id="address2" class="form-control" maxlength="100">
              			<span class="error" id="error_address2" style="color: red;"></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="sr-only" for="stateNme">City</label>
                     <input type="text" id="city" class="form-control" maxlength="26" onkeypress="return isAlphKey(event);">
              				<span class="error" id="error_city" style="color: red;"></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="houseNme" class="sr-only">State</label>
                      <input type="text" id="state" class="form-control" maxlength="26" onkeypress="return isAlphKey(event);">
              				<span class="error" id="error_state" style="color: red;"></span>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="strtNme" class="sr-only">Pincode</label>
                      <input type="text" id="pinCode" class="form-control" maxlength="6" onkeypress="return isNumberKey(event);">
              			<span class="error" id="error_pinCode" style="color: red;"></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <button type="button" id="prevBtn" onclick="nextPrev(-1)" class="btn load-btn"><i class="fa fa-arrow-circle-left"></i>&nbsp;Back</button>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                    <button type="button" id="requestCard" onclick="requestPhysicalCard()">Request&nbsp;<i class="fa fa-arrow-circle-right"></i></button>
                  </div>
                </div>
            </form>
          </div>
        </div>
        <div class="hidden-xs hidden-sm">
          <img src="${pageContext.request.contextPath}/resources/assets-newui/img/players/seh_1.png" class="img-responsive sehwag-image">
        </div>
      </div>
    </div>

  </div>
</div>


<div id="phyModalError"  class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
      <a href="#" class="close" data-dismiss="modal">&times;</a>
      <div class="modal-body">
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
          <div class="top-logo text-center">
            <img src="${pageContext.request.contextPath}/resources/assets-newui/img/cashround.svg" class="cashround">
          </div>
          <div class="kycWiz">
            <form id="" action="#">
              <!-- Circles which indicates the steps of the form: -->
              <!-- <div class="text-center step-wrap">
                <span class="step"></span>
                <span class="step"></span>
              </div> -->
              <!-- One "tab" for each step in the form: -->
                <div class="text-center instruct">
                  <p class="txt-red" id="errorDisclamerMessage"></p>
                </div>
                  <div class="col-xs-12 col-sm-12 col-md-12">
                    <center><button type="button"   class="btn load-btn" data-dismiss="modal">Ok</button></center>
                  </div>
            </form>
          </div>
        </div>
        <div class="hidden-xs hidden-sm">
          <img src="${pageContext.request.contextPath}/resources/assets-newui/img/players/seh_1.png" class="img-responsive sehwag-image" style="width:30% ">
        </div>
      </div>
    </div>

  </div>
</div>



<!-- Kit Entered Modal -->
<div id="kitModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <a href="#" class="close" data-dismiss="modal">&times;</a>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="top-logo text-center">
              <img src="${pageContext.request.contextPath}/resources/assets-newui/img/cashround.svg" class="cashround">
            </div>
            <div class="kitWrp text-center">
              <form id="">
                <div class="form-group">
                  <h3>Activate Your Card</h3>
                </div>
                <div class="form-group">
                  <label class="sr-onl/y" for="kitNum">Kit Number</label>
                 <input type="text" id="proxyNo" class="form-control" maxlength="12" onkeypress="return isNumberKey(event);">
                     <p class="error" id="error_proxyNo" style="color: red;"></p>
                </div>
                <div class="form-group">
                  <button type="button" class="btn load-btn" id="proxyNumbr" onclick="proxyCode()">Activate</button>
                </div>
              </form>
            </div>
            <div>
            <span id="proxyError" style="color: red"></span>
            </div>
          </div>
          <div class="hidden-xs hidden-sm">
            <img src="${pageContext.request.contextPath}/resources/assets-newui/img/card.svg" class="kit-image">
          </div>
        </div>
      </div>
    </div>

  </div>
</div>



<!-- Activation code Entered Modal -->
<div id="activationCodeTab" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <a href="#" class="close" data-dismiss="modal">&times;</a>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="top-logo text-center">
              <img src="${pageContext.request.contextPath}/resources/assets-newui/img/cashround.svg" class="cashround">
            </div>
            <div class="kitWrp text-center">
              <form id="">
                <div class="form-group">
                  <h3>Activate Your Card</h3>
                </div>
                <div class="form-group">
                  <label class="sr-onl/y" for="kitNum">Activation Code</label>
                <input type="text" id="activationCode" class="form-control" maxlength="6" onkeypress="return isNumberKey(event);">
                     <p class="error" id="error_activationCode" style="color: red;"></p>
                </div>
                <div class="row">
                <div class="col-sm-8">
                 <div class="form-group">
                  <button type="button" class="btn load-btn" id="resendcd" onclick="resendActivationCode()">Resend Code</button>
                </div>
                </div>
                <div class="col-sm-4">
                <div class="form-group">
                  <button type="button" class="btn load-btn" id="codeactive" onclick="actvCode()">Activate</button>
                </div>
                </div>
                </div>
              </form>
            </div>
          </div>
          <div class="hidden-xs hidden-sm">
            <img src="${pageContext.request.contextPath}/resources/images/mobileOTP.png" class="kit-image" style="width: 35%">
          </div>
        </div>
      </div>
    </div>

  </div>
</div>


<!-- Modal for Kyc upgrade -->
<div id="kycModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <a href="#" class="close" data-dismiss="modal">&times;</a>
      <div class="modal-body">
        <div class="top-logo text-center">
          <img src="${pageContext.request.contextPath}/resources/assets-newui/img/cashround.svg" class="cashround">
        </div>
        <div class="UpgradWrp">
          <form action="${pageContext.request.contextPath}/User/Upgrade/Account" method="post" id="formId" enctype="multipart/form-data">
            <div class="form-group text-center">
              <h3>Upgrade your account!</h3>
            </div>
            <div class="form-group">
              <label for="">Id Type</label>
              <select class="form-control" id="idType" name="idType">
              	<option value="">Select Id Type</option>
                <option value="drivers_id">Driving License(DL)</option>
                <option value="passport">Passport</option>
                <option value="aadhaar">Aadhar Card</option>
              </select>
              <p id="error_idType" style="color:red "></p>
              
            </div>
            <div class="form-group">
              <label for="" class="sr-on/ly">Id Number</label>
              <input type="text" id="idNumber" name="idNumber"class="form-control" placeholder="eg.1288 8273 8123 1231" maxlength="15">
              <p id="error_idNumber" style="color:red "></p>
            </div>
            <div class="form-group">
              <label for="" class="sr-on/ly">Upload Id(Front)</label>
              <input type="file" name="image"  id="aadhar_Image" class="form-control">
              <p id="error_aaadharCardImg" style="color:red "></p>
            </div>
            <div class="form-group">
              <label for="" class="sr-on/ly">Upload Id(Back)</label>
              <input type="file" name="image2"  id="aadhar_Image2" class="form-control">
              <p id="error_aaadharCardImg2" style="color:red "></p>
            </div>
            <div class="form-group text-center">
              <button type="button" id="kycre" onclick="kycRequest()" class="btn load-btn">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>


<!-- scripts starts here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/bootstrap/js/bootstrap.min.js"></script>
<!-- custom js starts here -->
<script src="${pageContext.request.contextPath}/resources/User/assets/js/all.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/userDetail.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/js/custom.js"></script>

<script type="text/javascript">
function changePassword(){
	var currentPassword=$('#password').val();
	var newPassword=$('#newPassword').val();
	var confirmPassword=$('#confirmPassword').val();
	var valid=true;
	if(currentPassword.length<=0){
		$("#error_Password").html("Enter Your Current Password.");
		valid = false;
	}
	if(newPassword.length<=0){
		$("#error_newPassword").html("Enter Your New Password.");
		valid = false;
	}
	if(confirmPassword.length<=0){
		$("#error_confirmPassword").html("Enter Confirm Password.");
		valid = false;
	}
	if(valid == true) {
		console.log("valid");
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : cont_path+"/Api/User/Windows/en/ChangePasswordWeb",
			dataType : 'json',
			data : JSON.stringify({
				"password" : "" + currentPassword + "",
				"newPassword":""+newPassword+"",
				"confirmPassword":""+confirmPassword+""
			}),
			success : function(response) {
				console.log(response)
				$("#register_resend_otp").removeClass("disabled");
				if (response.code.includes("S00")) {
					 $("#newPassword").val("");
				    	$("#confirmPassword").val("");
				    	$("#password").val("");
					console.log(response.details)
					swal("Success!!", 	response.details, "success");
				}
				if(response.code.includes("F00")){
					swal({
						  type: 'error',
						  title: 'Sorry!!',
						  text: response.details
						});
				}
			}
		});	
	}
	
	var timeout = setTimeout(function(){
    	$("#error_Password").html("");
        $("#error_newPassword").html("");
    	$("#error_confirmPassword").html("");
    	$("#frgtMessage_success").html("");
    }, 3000);
	
} 

</script>


<script type="text/javascript">

function kycRequest(){
var valid=true;
var idNumber=$('#idNumber').val();
var idType=$('#idType').val();
var aadharCardImg=$('#aadhar_Image').val();
var aadharCardImg2=$('#aadhar_Image2').val();
// var panCardImg=$('#pan_Image').val();

if(idNumber=='') {
	$("#error_idNumber").html("Please enter the id number");
	valid = false;
}

if(idType=='') {
	$("#error_idType").html("Please select id type");
	valid = false;
}

if (aadharCardImg!= "") {
    if (!(aadharCardImg.split(".")[1].toUpperCase() == "PNG"
      || aadharCardImg.split(".")[1].toUpperCase() == "JPEG" || aadharCardImg.split(".")[1].toUpperCase() == "JPG")) {
     $("#error_aaadharCardImg").html("File with " + aadharCardImg.split(".")[1]
     + " is invalid. Upload a valid file");
     valid = false;
    }
   }
if(aadharCardImg=='') {
	$("#error_aaadharCardImg").html("Please attach  id card image(Front)");
	valid = false;
	console.log("valid: "+"panCardImg");
}
if (aadharCardImg2!= "") {
    if (!(aadharCardImg2.split(".")[1].toUpperCase() == "PNG"
      || aadharCardImg2.split(".")[1].toUpperCase() == "JPEG" || aadharCardImg2.split(".")[1].toUpperCase() == "JPG")) {
     $("#error_aaadharCardImg2").html("File with " + aadharCardImg2.split(".")[1]
     + " is invalid. Upload a valid file");
     valid = false;
    }
   }
if(aadharCardImg2=='') {
	$("#error_aaadharCardImg2").html("Please attach  id card image(Back)");
	valid = false;
	console.log("valid: "+"panCardImg");
}
if (valid == true) {
	$('#formId').submit();
	$("#kycre").addClass("disabled");
//		$("#addAgent").html(spinnerUrl);
}

var timeout = setTimeout(function(){
	$("#error_idType").html("");
    $("#error_idNumber").html("");
	$("#error_aaadharCardImg").html("");
	$("#error_aaadharCardImg2").html("");
}, 3000);

}
</script>




    <script type="text/javascript">
    function balanceCheck(){
    	$.ajax({
			type : "POST",
			contentType : "application/json",
			url : contextPath+"/Api/v1/User/Windows/en/GetBalanceWeb",
			dataType : 'json',
			data : JSON.stringify({
				"sessionId" :" ",
			}),
			
			success : function(response) {
				console.log("response:: "+response.code);
				console.log("response:: "+response.message);
// 				$("#registerButton").removeClass("disabled");
// 				$("#registerButton").html("Sign Up");
				if (response.code.includes("S00")) {
					$("#phyModal").modal({backdrop: "static"})
					$('#successDisclamerMessage').html(response.message);
					}else{
						$("#phyModalError").modal({backdrop: "static"})
						$('#errorDisclamerMessage').html(response.message);
				}			
			}
		});
    	
    }
    
    </script>
    
  <script type="text/javascript">
    function requestPhysicalCard(){
    	var valid=true;
    	var address1=$('#address1').val();
    	var address2=$('#address2').val();
    	var city=$('#city').val();
    	var state=$('#state').val();
    	var pinCode=$('#pinCode').val();

    	if(address1=='') {
    		$("#error_address1").html("Please enter the address1");
    		valid = false;
    		console.log("valid: "+"panCardImg");
    	}
    	if(address2=='') {
    		$("#error_address2").html("Please enter the address2");
    		valid = false;
    		console.log("valid: "+"panCardImg");
    	}
    	if(city=='') {
    		$("#error_city").html("Please enter the city");
    		valid = false;
    		console.log("valid: "+"panCardImg");
    	}if(state=='') {
    		$("#error_state").html("Please enter the state");
    		valid = false;
    		console.log("valid: "+"panCardImg");
    	}if(pinCode=='') {
    		$("#error_pinCode").html("Please enter the pincode");
    		valid = false;
    		console.log("valid: "+"panCardImg");
    	}
    	if (valid) {
    		$("#requestCard").addClass("disabled");
//    			$("#addAgent").html(spinnerUrl);

    		$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : contextPath+"/Api/v1/User/Windows/en/PhysicalCardRequestWeb",
    			dataType : 'json',
    			hash:'123456',
    			data : JSON.stringify({
    				"address1" :"" + address1 + "",
    				"address2" : "" + address2 + "",
    				"city" : "" + city + "",
    				"state" : "" + state + "",
    				"pinCode" : "" + pinCode + ""
    			}),
    			
    			
    			success : function(response) {
    				console.log("response:: "+response.code);
    				$('#myModal').modal('hide');
    				console.log("response:: "+response.message);
    				$("#requestCard").removeClass("disabled");
//     				$("#registerButton").html("Sign Up");
    				if (response.code.includes("S00")) {
    						swal("Success!!", response.message, "success");
    					}else{
    						swal({
    							  type: 'error',
    							  title: 'Failure!!',
    							  text: response.message
    							});
    				}			
    			}
    		});
    	}
    	
    	var timeout = setTimeout(function(){
    		$("#error_address1").html("");
    		$("#error_address2").html("");
    	    $("#error_city").html("");
    		$("#error_state").html("");
    		$("#error_country").html("");
    		$("#error_pinCode").html("");
    	    $("#error_idType").html("");
    		$("#error_idNumber").html("");
    		$("#error_image").html("");
    	}, 3000);
    	
    	}
    </script>
    
    
     <script type="text/javascript">
    function proxyCode(){
    	var valid=true;
    	var proxyNo=$('#proxyNo').val();

    	if(proxyNo=='') {
    		$("#error_proxyNo").html("Please enter the kit number");
    		valid = false;
    	}
    	
    	if (valid) {
    		$("#proxyNumbr").addClass("disabled");
//    			$("#addAgent").html(spinnerUrl);

    		$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : contextPath+"/Api/v1/User/Windows/en/AddPhysicalCardWeb",
    			dataType : 'json',
    			data : JSON.stringify({
    				"proxy_number" :"" + proxyNo + "",
    			}),
    			success : function(response) {
    				console.log("response:: "+response.code);
    				console.log("response:: "+response.message);
    				$("#proxyNumbr").removeClass("disabled");
//     				$("#registerButton").html("Sign Up");
    				if (response.code.includes("S00")) {
    					$('#activeTab').modal('hide');
    					$('#activationCodeTab').modal('show');
    					$("#activeError").html(response.message);
    					}else{
    						$("#proxyError").html(response.message);
    				}			
    			}
    		});
    	}
    	
    	var timeout = setTimeout(function(){
    		$("#error_proxyNo").html("");
    	}, 3000);
    	
    	}
    </script>
    
    
    
     <script type="text/javascript">
    function actvCode(){
    	var valid=true;
    	var activationCode=$('#activationCode').val();

    	if(activationCode=='') {
    		$("#error_activationCode").html("Please enter the activation code");
    		valid = false;
    		console.log("valid: "+"panCardImg");
    	}
    	
    	if (valid) {
    		$("#codeactive").addClass("disabled");
//    			$("#addAgent").html(spinnerUrl);

    		$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : contextPath+"/Api/v1/User/Windows/en/ActivatePhysicalCardWeb",
    			dataType : 'json',
    			hash:'123456',
    			data : JSON.stringify({
    				"activationCode" :"" + activationCode + "",
    			}),
    			
    			success : function(response) {
    				console.log("response:: "+response.code);
    				console.log("response:: "+response.message);
    				$("#codeactive").removeClass("disabled");
    				if (response.code.includes("S00")) {
    					$('#activationCodeTab').modal('hide');
    					swal("Success!!", response.message, "success");
    					}else{
    						swal({
    							  type: 'error',
    							  title: 'Failure!!',
    							  text: response.message
    							});
    				}			
    			}
    		});
    	}
    	
    	var timeout = setTimeout(function(){
    		$("#error_activationCode").html("");
    	}, 3000);
    	
    	}
    </script>
    
    
    
    
     <script type="text/javascript">
    function resendActivationCode(){
//     		$("#kycre").addClass("disabled");
//    			$("#addAgent").html(spinnerUrl);
    		$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : contextPath+"/Api/v1/User/Windows/en/ResendActivationCodeWeb",
    			dataType : 'json',
    			hash:'123456',
    			data : JSON.stringify({
    				"activationCode" :" ",
    			}),
    			
    			success : function(response) {
    				console.log("response:: "+response.code);
    				console.log("response:: "+response.message);
//     				$("#registerButton").removeClass("disabled");
//     				$("#registerButton").html("Sign Up");
    				if (response.code.includes("S00")) {
    					$('#activeError').html(response.message);
    					}else{
    						$('#activeError').html(response.message);
    				}			
    			}
    		});
    	}
    </script>
    
    
    


 <script type="text/javascript">
function isAlphNumberKey(evt){
    var k = (evt.which) ? evt.which : evt.keyCode
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
}
</script>
<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>
<script type="text/javascript">
function isAlphKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
}
</script>


</body>
</html>
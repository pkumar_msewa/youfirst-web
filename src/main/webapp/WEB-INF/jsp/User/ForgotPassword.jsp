<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta Http-Equiv="Cache-Control" Content="no-cache">
    <meta Http-Equiv="Pragma" Content="no-cache">
    <meta Http-Equiv="Expires" Content="0">
	<sec:csrfMetaTags/>
    <!-- Title -->
    <title>CASHIER Card | Forgot Password</title>

    <!-- Favicon -->
    <link rel="icon" href="${pageContext.request.contextPath}/resources/img/core-img/favicon.png">

    <!-- Core Stylesheet -->
    <link href="${pageContext.request.contextPath}/resources/css/Style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/custom.css" rel="stylesheet">

    <!-- Responsive CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/responsive.css" rel="stylesheet">
        
 <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script>

    <style>
    /* width */
        ::-webkit-scrollbar {
            width: 10px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1; 
        }
         
        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #888; 
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #555; 
        }
        .main_topheader {
            -webkit-box-shadow: 0 1px 0 rgba(12,13,14,.1), 0 1px 3px rgba(12,13,14,.1), 0 4px 20px rgba(12,13,14,.035), 0 1px 1px rgba(12,13,14,.025);
            box-shadow: 0 1px 0 rgba(12,13,14,.1), 0 1px 3px rgba(12,13,14,.1), 0 4px 20px rgba(12,13,14,.035), 0 1px 1px rgba(12,13,14,.025);
            background: #fff;
            color: #8e8e8e;
            padding-top: 1px;
            padding-bottom: 1px;
            posi/tion: initial;
            background: #efefef;
        }
        .form-box {
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            -webkit-box-shadow: 0 12px 200px #e7e7e7;
            box-shadow: 0 12px 200px #e7e7e7;
            background-color: #fff;
            color: #7b7b7b;
            min-height: 288px;
            max-width: 425px;
            margin-right: auto;
            margin-left: auto;
            padding: 70px 4.5% 50px;
        }
        .cta-actions {
            padding-top: 25px;
        }
        .signLog_btn {
            border-radius: 100px;
            background: -webkit-linear-gradient(#ef1821, #291859);
            background: -moz-linear-gradient(#ef1821, #291859);
            background: -ms-linear-gradient(#ef1821, #291859);
            background: -o-linear-gradient(#ef1821, #291859);
            background: linear-gradient(#ef1821, #291859);
            color: #fff;
            font-size: 14px;
            line-height: 32px;
            min-width: 160px;
            text-align: center;
            border: none;
            transition: all .4s ease-in-out;
            cursor: pointer;
            display: inline-block;
            text-decoration: none;
            -webkit-box-shadow: #4bc5ac 0 21px 35px -17px;
            -moz-box-shadow: #4bc5ac 0 21px 35px -17px;
            box-shadow: 0 21px 35px -17px #4bc5ac;
        }
        .tnc {
            color: #c3c3c3;
            padding-top: 5px;
            font-size: 11px;
            line-height: 1.64;
        }
        .auth_form label {
            float: left;
        }
        .first-selection {
            background: url(${pageContext.request.contextPath}/resources/img/bg-img/waves2.svg);
            background-repeat: no-repeat;
            color: #fff;
            background-position: bottom;
            position: relative;
        }
    </style>


<script type="text/javascript">
var cont_path="${pageContext.request.contextPath}";
</script>
</head>

<body>
    <!-- Preloader Start -->
    <div id="preloader">
        <div class="colorlib-load"></div>
    </div>

    <!-- ***** Header Area Start ***** -->
    <header class="header_area1 animated sticky">
        <div class="container-fluid">
            <div class="row align-items-center">
                <!-- Menu Area Start -->
                <div class="col-12 col-lg-12">
                    <div class="menu_area">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <!-- Logo -->
                            <a class="navbar-brand" href="#">
                                <img src="${pageContext.request.contextPath}/resources/User/assets/images/logo1.png" class="img-fluid">
                            </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                            <!-- Menu Area -->
                            <div class="collapse navbar-collapse" id="ca-navbar">
                                <ul class="navbar-nav ml-auto" id="nav">
                                    <!-- <li class="nav-item active"><a class="nav-link" href="${pageContext.request.contextPath}/Home">Home</a></li> -->
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/Aboutus">About</a></li>
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/faq">FAQs</a></li>
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/ContactUs">Contact</a></li>
                                    <li class="nav-item"><a class="nav-link" href="javascript:void(0);">Login</a></li>
                                    <li class="nav-item"><a class="nav-link" href="javascript:void(0);">Signup</a></li>
                                </ul>
                                <!-- <div class="sing-up-button d-lg-none">
                                    <a href="${pageContext.request.contextPath}/User/Login">Login</a>
                                </div> -->
                            </div>
                        </nav>
                    </div>
                </div>
                <!-- Signup btn -->
                <!-- <div class="col-12 col-lg-2">
                    <div class="sing-up-button d-none d-lg-block">
                        <a href="${pageContext.request.contextPath}/User/SignUp">Sign Up</a>
                    </div>
                </div> -->
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->
    
    <section class="bg-white first-selection" id="" style="padding-top: 86px; padding-bottom: 45px;">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <!-- Heading Text  -->
                    <div class="section-heading">
                        <h2>Forgot Password!</h2>
                        <div class="line-shape"></div>
                    </div>

                    <!-- Enter your mobile number form starts here -->
                    <div class="form-box" id="step1">
                        <div id="fpMessage_error"></div>
                        <form class="auth_form" action="">
                          <div class="form-group">
                            <label for="mobile">Enter Mobile Number:</label>
                            <input type="mobile"  class="form-control numbers" id="username" maxlength="10">
                            <p class="error" id="error_userName" style="color: red"></p>
                          </div>
                          <div class="cta-actions text-center">
                                <center>
                                   <a href="${pageContext.request.contextPath}/User/Login"> <button class="btn signLog_btn" type="button" title="Cancel">CANCEL</button></a>
                                    <button class="btn signLog_btn next-step" onclick="Forgot()" type="button" title="Send OTP">CONTINUE</button>
                                </center>
                            </div>
                        </form>
                        
                    </div>
                    <!-- Enter your mobile number form ends here -->

                    <!-- Change password form starts here -->
                    <div class="form-box" id="step2" style="display: none;">
                        <div id="frgtMessage_success"></div>
                        <div id="fpMessage"></div>
                        <form class="auth_form" action="">
                          <div class="form-group">
                            <label for="newPassword">Enter New Password:</label>
                            <input type="newPassword"  class="form-control" id="password">
                            <p class="error" id="error_password"  style="color: red;"></p>
                          </div>
                          <div class="form-group">
                            <label for="confirmPassword">Confirm New Password:</label>
                            <input type="confirmPassword" class="form-control" id="confirmPassword">
                            <p class="error" id="error_confirmPassword"  style="color: red;"></p>
                          </div>
                          <div class="form-group">
                            <label for="otp">Enter your OTP:</label>
                            <input type="otp" class="form-control numbers" id="key" maxlength="6">
                            <p class="error" id="error_key"  style="color: red;"></p>
                          </div>
                          <div class="cta-actions text-center">
                                <center>
                                    <button class="btn signLog_btn" type="button" onclick="Resend()" title="Cancel">RESEND OTP</button>
                                    <button class="btn signLog_btn" type="button" onclick="ProcessForgot()" title="Submit">SUBMIT</button>
                                </center>
                            </div>
                        </form>
                        
                    </div>
                    <!-- Change password form ends here -->
                </div>
            </div>
        </div>
        
    </section>

    <footer>
        
    </footer>
    

    <!-- Jquery-2.2.4 JS -->
    <script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script>
    <!-- Bootstrap-4 Beta JS -->
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <!-- All Plugins JS -->
    <script src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>
    <!-- Slick Slider Js-->
    <script src="${pageContext.request.contextPath}/resources/js/slick.min.js"></script>
    <!-- Footer Reveal JS -->
    <script src="${pageContext.request.contextPath}/resources/js/footer-reveal.min.js"></script>
    <!-- Active JS -->
    <script src="${pageContext.request.contextPath}/resources/js/active.js"></script>

    <!-- <script>
        $(document).ready(function(){
            $('.next-step').click(function(){
                $('#step1').hide();
                $('#step2').show();
            })
        })
    </script> -->
</body>

<script type="text/javascript">
function Forgot(){
	var userName=$('#username').val();;
	var valid=true;
	if(userName.length<=0){
		$("#error_userName").html("Enter Your UserName");
		valid = false;
	}
	if(valid==true){
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : cont_path+"/Api/User/Windows/en/ForgotPassword",
			dataType : 'json',
			data : JSON.stringify({
				"username" : "" + userName + ""
			}),
			success : function(response) {
				console.log(response)
				$("#register_resend_otp").removeClass("disabled");
				if (response.code.includes("S00")) {
					console.log(response.details)
					$("#fpMessage").html(response.details);
					$("#fpMessage").css("color", "green");
					$('#step2').show();
					$('#step1').hide();
				}
				if(response.code.includes("F00")){
					$("#fpMessage_error").html(response.details);
					$("#frgtMessage_error").css("color", "red");;
				}
			}
		});	
	}
	var timeout = setTimeout(function(){
    	$("#error_userName").html("");
    }, 3000);
}
</script>


<script type="text/javascript">
function Resend(){
	var username=$('#username').val();
	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : cont_path+"/Api/User/Windows/en/Resend/Mobile/OTP",
		dataType : 'json',
		data : JSON.stringify({
			"mobileNumber" : "" + username + ""
		}),
		success : function(response) {
			console.log(response)
			$("#register_resend_otp").removeClass("disabled");
			if (response.code.includes("S00")) {
				console.log(response.details)
				$("#fpMessage").html("");
				$("#frgtMessage_success").html(response.details);
				$("#frgtMessage_success").css("color", "green");
			}
			if(response.code.includes("F00")){
				$("#frgtMessage_success").html(response.details);
				$("#frgtMessage_success").css("color", "red");;
			}
		}
	});	
}

</script>


<script type="text/javascript">
function ProcessForgot(){
	var username=$('#username').val();
	var key=$('#key').val();
	var Password=$('#password').val();
	var confirmPassword=$('#confirmPassword').val();
	var valid=true;
	if(key.length<=0){
		$("#error_key").html("Enter Your OTP.");
		valid = false;
	}
	if(Password.length<=0){
		$("#error_password").html("Enter Your Password.");
		valid = false;
	}
	if(confirmPassword.length<=0){
		$("#error_confirmPassword").html("Enter Confirm Password.");
		valid = false;
	}
	if(valid == true) {
		console.log("valid");
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : cont_path+"/Api/User/Windows/en/ChangePasswordOTP",
			dataType : 'json',
			data : JSON.stringify({
				"password" : "" + Password + "",
				"username" : "" + username + "",
				"key":""+key+"",
				"confirmPassword":""+confirmPassword+""
			}),
			success : function(response) {
				console.log(response)
				$("#register_resend_otp").removeClass("disabled");
				if (response.code.includes("S00")) {
					 $("#password").val("");
				    	$("#confirmPassword").val("");
				    	$("#key").val("");
					console.log(response.details)
					$("#frgtMessage_success").html(response.details);
					$("#frgtMessage_success").css("color", "green");;
					window.location.href="${pageContext.request.contextPath}/User/Login";
					
				}
				if(response.code.includes("F00")){
					$("#frgtMessage_success").html(response.details);
					$("#frgtMessage_success").css("color", "red");;
				}
			}
		});	
	}
	
	var timeout = setTimeout(function(){
    	$("#error_password").html("");
        $("#error_key").html("");
    	$("#error_confirmPassword").html("");
    	$("#frgtMessage_success").html("");
    }, 3000);
}

</script>



<script type="text/javascript">
$("input.numbers").keypress(function(event) {
	  return /\d/.test(String.fromCharCode(event.keyCode));
	});
</script>

</html>

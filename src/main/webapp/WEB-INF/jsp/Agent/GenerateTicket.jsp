<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	response.setHeader("Cache-Control",
			"no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
	response.setDateHeader("Expires", 0);
	response.setHeader("Pragma", "no-cache");
%>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>Load card | Agent</title>
<meta
	content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
	name='viewport' />
<meta name="viewport" content="width=device-width" />
<link rel="icon"
	href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png"
	type="image/x-icon" />
<!-- Bootstrap core CSS     -->
<link
	href="${pageContext.request.contextPath}/resources/corporate/assets/css/bootstrap.min.css"
	rel="stylesheet" />
<!-- Animation library for notifications   -->
<link
	href="${pageContext.request.contextPath}/resources/corporate/assets/css/animate.min.css"
	rel="stylesheet" />
<!--  Paper Dashboard core CSS    -->
<link
	href="${pageContext.request.contextPath}/resources/corporate/assets/css/paper-dashboard.css"
	rel="stylesheet" />
<!--  CSS for Demo Purpose, don't include it in your project     -->
<link
	href="${pageContext.request.contextPath}/resources/corporate/assets/css/demo.css"
	rel="stylesheet" />
<!--  Fonts and icons     -->
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"
	rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Muli:400,300'
	rel='stylesheet' type='text/css'>
<link
	href="${pageContext.request.contextPath}/resources/corporate/assets/css/themify-icons.css"
	rel="stylesheet">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<style type="text/css">
fieldset {
	padding: .35em 1.625em .75em;
	margin: 0 2px;
	border: 1px solid silver;
	margin-bottom: 15px;
}

legend {
	width: auto;
}

.switch {
	position: relative;
	display: inline-block;
	width: 60px;
	height: 34px;
}

.switch input {
	display: none;
}

.slider {
	position: absolute;
	cursor: pointer;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	background-color: #ccc;
	-webkit-transition: .4s;
	transition: .4s;
}

.slider:before {
	position: absolute;
	content: "";
	height: 26px;
	width: 26px;
	left: 4px;
	bottom: 4px;
	background-color: white;
	-webkit-transition: .4s;
	transition: .4s;
}

input:checked+.slider {
	background-color: #2196F3;
}

input:focus+.slider {
	box-shadow: 0 0 1px #2196F3;
}

input:checked+.slider:before {
	-webkit-transform: translateX(26px);
	-ms-transform: translateX(26px);
	transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
	border-radius: 34px;
}

.slider.round:before {
	border-radius: 50%;
}
</style>
</head>
<body oncopy="return false" oncut="return false" onpaste="return false">
	<div class="wrapper">
		<jsp:include page="LeftMenu.jsp"></jsp:include>
		<div class="main-panel">
			<jsp:include page="Header.jsp"></jsp:include>
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-12">
							<div class="page-title-box">
								<h4 class="page-title float-left">Generate Ticket</h4>
								<div class="clearfix"></div>
								<div style="color: green; text-align: center;">${succsseMsg}</div>
								<div style="color: red; text-align: center;">${errorMsg}</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<form
										action="${pageContext.request.contextPath}/Agent/generateTicket"
										id="formId" method="post">
										<div class="col-md-6 col-md-offset-3">
											<fieldset>
												<legend>User Details</legend>
												<div class="form-group">
													<label for="mobile">Email Id</label> <input type="text"
														name="email" id="emailId" class="form-control" maxlength="50">
													<span id="emailError" class="error" style="color: red"></span>
												</div>
												<div class="form-group">
													<label for="amount">Requester</label> <input type="text"
														name="requester" id="requestId" class="form-control" maxlength="50">
													<span id="requestError" class="error" style="color: red"></span>
												</div>
												<div class="form-group">
	                                             	<label for="mobile">Mobile</label>
	                                             	<input type="text" name="contactNo" id="contactNo" class="form-control"  
	                                             	onkeypress="return isNumberKey(event);" maxlength="10">
	                                            	<span id="ferror" class="error" style="color: red" ></span>
	                                            </div>
												<div class="form-group">
	                                               	<label for="component">Component</label>
	                                           		<select class="form-control" name="component">
	                                           			<c:forEach var="component" items="${component}">
	                                           				<option value="${component.id}">${component.name}</option>
	                                           			</c:forEach>
	                                           		</select>
	                                           		<span id="categoryNameError" class="error" style="color: red" ></span>
	                                            </div>
												<div class="form-group">
													<label for="comment">Comment</label>
													<textarea rows="5" cols="46" maxlength="180" name="comment"
														id="comment" class="form-control"></textarea>
													<span id="commentError" class="error" style="color: red"></span>
												</div>
												<center>
													<button type="button" class="btn signLog_btn mt-4"
														onclick="validateform()">Create Issue</button>
												</center>
											</fieldset>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<form action="${pageContext.request.contextPath}/Agent/GetReceipt"
		id="getreceipt" method="post" target="_blank">
		<input type="hidden" name="loadWalletId" id="loadWalletId"
			value="${loadWalletId}">
	</form>


	<script type="text/javascript">
		var contextPath="${pageContext.request.contextPath}";
		function isNumberKey(evt){
		    var charCode = (evt.which) ? evt.which : evt.keyCode
		    return !(charCode > 31 && (charCode < 48 || charCode > 57));
		}
		
		function isAlphNumberKey(evt){
		    var k = (evt.which) ? evt.which : evt.keyCode
		    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
		}
		
		function isAlphKey(evt){
		    var charCode = (evt.which) ? evt.which : evt.keyCode
		    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
		}
		
		$("#check").change(function() {
		    if(this.checked) {
		        $('#proxy').css('display','block');
		        $('#isTransactionType').val(true);
		    } else {
		    	$('#proxy').css('display','none');
		    	$('#isTransactionType').val(false);
			}
		});
		
		function validateform(){
	    	var valid = true;
	    	var email = $('#emailId').val();
	    	var contactNo  = $('#contactNo').val();
	    	var request = $('#requestId').val();
	    	var contactNoPattern = "[7-9]{1}[0-9]{9}";
	    	if(contactNo.length <=0 || contactNo.length !=10){
	    		$("#ferror").html("Please enter 10 digit contact");
	    		valid = false;
	    	} else if(!contactNo.match(contactNoPattern)) {
    			console.log("invalid contact no");
    			$("#ferror").html("Please enter valid contact number");
	    		valid = false; 
    		}if(email.length <=0 ){
	    		$("#emailError").html("Please enter email Id");
	    		valid = false;
	    	}if(request.length<=0){
	    		$("#requestError").html("Please enter requester name");
	    		valid = false; 
	    	} 
		    if(valid == true) {
		    	$("#formId").submit();
		    } 
		    var timeout = setTimeout(function(){
		    	$("#ferror").html("");
		    	$("#emailError").html("");
		    	$('#requestError').html('');
		    }, 4000);
	    }
		
		function checkCommission(){
			var amount = $('#amount').val();
			if(amount.length<=0){
				amount = 0;
			}
			$.ajax({
				type : "POST",
	    		url:contextPath+"/Agent/GetCommission/"+amount,
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				
	   			}),
	   			success : function(response) {
	   				$('#commission').val(response.details);
	   			}
			});
		}
		function print(){
			//window.open(contextPath+"/Data",'_blank');
			$('#getreceipt').submit();
		}
	</script>

</body>
</html>
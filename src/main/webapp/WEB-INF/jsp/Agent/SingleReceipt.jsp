<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Receipt | Agent</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link rel="icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png" type="image/x-icon" />
    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/animate.min.css" rel="stylesheet"/>
    <!--  Paper Dashboard core CSS    -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/paper-dashboard.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/demo.css" rel="stylesheet" />
    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/themify-icons.css" rel="stylesheet">
	<script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 col-xs-12">
				<div class="panel panel-default" style="margin-top: 30%">
					<div class="panel-body">
						<div class="receipt_logo text-center">
							<img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/logo.png" alt="" height="50">
						</div>
						<div class="receipt_wrap">
							<div class="receipt_heading text-center">
								<h3>Load wallet Receipt</h3>
							</div>
							<hr>
							<div class="receipt_body row">
								<div class="col-md-5 col-md-offset-1 col-xs-6 left-side">
									<div class="">
										<h4>Date:</h4>
									</div>
									<div>
										<h4>Reference Number:</h4>
									</div>
									<div>
										<h4>Amount</h4>
									</div>
									<div>
										<h4>Agent Commission:</h4>
									</div>
									<div>
										<h4>Total Amount:</h4>
									</div>
								</div>
								<div class="col-md-6 col-xs-6 right-side">
									<div>
										<h4><fmt:formatDate pattern="dd-MM-yyyy" value="${data.created}" /></h4>
									</div>
									<div>
										<h4>${data.transaction.transactionRefNo}</h4>
									</div>
									<div>
										<h4>${data.transaction.amount}</h4>
									</div>
									<div>
										<h4>${data.transaction.commissionEarned}</h4>
									</div>
									<div>
										<h4>${data.transaction.amount+data.transaction.commissionEarned}</h4>
									</div>
									<div>
										<h4>${status}</h4>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript">
$(document).ready(function() {
    window.print();
    window.setTimeout('window.close();',500);
});

</script>
</html>

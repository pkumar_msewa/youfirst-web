<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<%
	response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
	response.setDateHeader("Expires", 0);
	response.setHeader("Pragma", "no-cache");
%>
<head>
<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Load Wallet List | Agent</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link rel="icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png" type="image/x-icon" />
    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/animate.min.css" rel="stylesheet"/>
    <!--  Paper Dashboard core CSS    -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/paper-dashboard.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/demo.css" rel="stylesheet" />
    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/themify-icons.css" rel="stylesheet">
    
    <!-- Datatables -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
</head>
<body>
	<div class="wrapper">
		<jsp:include page="LeftMenu.jsp"></jsp:include>
		<div class="main-panel">
			<jsp:include page="Header.jsp"></jsp:include>
			<div class="content">
				<div class="container-fluid">
					<div class="row">
			            <div class="col-12">
			              <div class="page-title-box">
			                <h4 class="page-title float-left">Load Wallet List</h4>
			                <div class="clearfix"></div>
			              </div>
			            </div>
					</div>
			         <div class="row">
													<div class="col-md-4 col-sm-4 col-xs-4">
														<form
															action="${pageContext.request.contextPath}/Agent/SummaryReport"
															method="post">
															<div class="form-row">
																<div class="col-sm-8">
																	<div id="" class="pull-left" style="cursor: pointer;">
																		<label class="sr-only" for="filterBy">Filter
																			By:</label> <input id="reportrange" name="Daterange"
																			class="form-control" readonly="readonly" />
																	</div>
																</div>
																<div class="col-sm-3">
																	<button class="btn btn-primary" onclick="fetchlist()"
																		type="submit">Filter</button>
																</div>
															</div>
														</form>
													</div>
													<div class="col-md-4 col-sm-4 col-xs-4">
													
													<p style="color:red">${errorMesg}</p>

													</div>
												</div><br>
					<div class="row">
						<div class="col-12">
							<div class="card-box table-responsive">
								<table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
											<th>Sl No</th>
											<th>File Name</th>
											<th>Requested Corporate</th>
											<th>Success Creation</th>
											<th>Partial Creation</th>
											<th>Failed Creation</th>
											<th>Category</th>
											<th>Request Date</th>
											<th>Processed</th>
											<th>Status</th>
										</tr>
                                        </thead>
                                       <tbody>
                                        <c:forEach items="${CatalogueList}" var="card"
									   varStatus="loopCounter">
								<tr>
									<td>${loopCounter.count}</td>
									<td>${card.absPath}</td>
										<td> <c:out value="${card.agent.username}" default="" escapeXml="true" /></a></td>
										<td>${card.successCreation}</td>
												<td>${card.partialCreation}</td>
												<td>${card.failedCreation}</td>
												<td> <c:out value="${card.categoryType}" default="" escapeXml="true" /></td>
												<td> <c:out value="${card.created}" default="" escapeXml="true" />
									</td>
									<td> <c:out value="${card.schedulerStatus}" default="" escapeXml="true" /></td>
								<c:choose>
								<c:when test="${card.reviewStatus==true}">
								<td><h6>File Approved</h6> </td>
								</c:when>
								<c:when test="${card.fileRejectionStatus==true}">
								<td><h6>File Rejected</h6> </td>
								</c:when>
								<c:otherwise>
								<td><h6>Pending</h6> </td>
								</c:otherwise>
								</c:choose>
								
								</tr>
							</c:forEach>
                                    </tbody>
                                    </table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<form action="${pageContext.request.contextPath}/Agent/GetReceipt" id="getreceipt" method="post" target="_blank">
		<input type="hidden" name="loadWalletId" id="loadWalletId" value="">
	</form>
	
    <script src="${pageContext.request.contextPath}/resources/corporate/assets/js/bootstrap.min.js" type="text/javascript"></script>
	  <!-- DateRange picker -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    
    <!-- Datatables -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
    
     <script type="text/javascript">	
            $(document).ready(function() {
                $('#datatable').DataTable({
                    responsive: true
                });

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
                
                
                var timeout = setTimeout(function(){
            		$("#stst").html("");
            	}, 3000); 
                
                
            } );

        </script>

    <script>
		$(function() {

			var start1 = '${startRange}';
			var end1  = '${endRange}';
			if(start1!=null  && start1!=""  && end1!=null && end1!=""){
				var start =start1 ;
				var end = end1;
				
			}else{
				var start = moment().subtract(29, 'days');
				var end = moment();
			}
			console.log(start1+":"+end1);
			function cb(start, end) {
				$('#reportrange').html(
						start.format('MM-dd-yyyy') + ' - '
								+ end.format('MM-dd-yyyy'));
			}

			$('#reportrange').daterangepicker(
					{
						startDate : start,
						endDate : end,
						locale : {
							format : 'YYYY-MM-DD'
						},
						dateLimit : {
							"days" : 30
						},
						ranges : {
							'Today' : [ moment(), moment() ],
							'Yesterday' : [ moment().subtract(1, 'days'),
									moment().subtract(1, 'days') ],
							'Last 7 Days' : [ moment().subtract(6, 'days'),
									moment() ],
							'Last 30 Days' : [ moment().subtract(29, 'days'),
									moment() ],
							'This Month' : [ moment().startOf('month'),
									moment().endOf('month') ],
							'Last Month' : [
									moment().subtract(1, 'month').startOf(
											'month'),
									moment().subtract(1, 'month')
											.endOf('month') ]
						}
					}, cb);

			cb(start, end);

		});
	</script>
	
	  
 <script>
 
 function clickMe(value, id){
	 console.log(value);
	 $('#corp'+id).val(value);
	 $('#corpSub'+id).submit();
 }
 
 
 function getByCorporate(username){
	 if(username!=null && username !=""){
		 $('#cId').val(username);
			$('#byCorporate').submit();		 
	 }
 }
 
 
 </script>
	
</body>
</html>
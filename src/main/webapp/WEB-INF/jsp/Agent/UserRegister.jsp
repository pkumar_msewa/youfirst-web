<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<%
	response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
	response.setDateHeader("Expires", 0);
	response.setHeader("Pragma", "no-cache");
%>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Agent | User Registration</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link rel="icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png" type="image/x-icon" />
    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/animate.min.css" rel="stylesheet"/>
    <!--  Paper Dashboard core CSS    -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/paper-dashboard.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/demo.css" rel="stylesheet" />
    <!--  Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/themify-icons.css" rel="stylesheet">

    <link href="${pageContext.request.contextPath}/resources/corporate/assets/dropify/css/dropify.css" rel="stylesheet">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />


    <style type="text/css">
    fieldset {
	    padding: .35em 1.625em .75em;
	    margin: 0 2px;
	    border: 1px solid silver;
	    margin-bottom: 15px;
	}
	legend {
		width: auto;
	}
	.input-group {
		display: flex;
	}
	.btn, .navbar .navbar-nav > li > a.btn {
		border-radius: 0;
		padding: 19px 19px;
	}
	.modelButton {
		padding: 5px;
	}
    </style>
</head>
<body oncopy="return false" oncut="return false" onpaste="return false">
	<div class="wrapper">
		<jsp:include page="LeftMenu.jsp"></jsp:include>
		<div class="main-panel">
			<jsp:include page="Header.jsp"></jsp:include>
			<div class="content">
				<div class="container-fluid">
					<div class="row">
                        <div class="col-md-10">
                            <div class="page-title-box">
                                <h4 class="page-title float-left">Add User</h4>
                                <div class="clearfix"></div>
                            </div>
                        </div>
					</div>
					<c:if test="${not empty error}">
						<center><strong id="errormsg" style="color:red; font-size:16px;">${error}</strong></center>
					</c:if>
					<c:if test="${not empty successMsg}">
						<center><strong id="errormsg" style="color:green; font-size:16px;">${successMsg}</strong></center>
					</c:if>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<form action="${pageContext.request.contextPath}/Agent/UserRegister" method="post" id="formId">
											<fieldset style="display:block;" id="userDetails">
		                                        <legend>User Details</legend>
		                                        <div class="col-sm-12">
		                                        <div class="col-sm-6">
		                                        	<div class="form-group">
									                    <label for="name">First Name:</label>
									                    <input type="text" class="form-control" id="firstName" name="firstName" maxlength="30" placeholder="Enter Your FirstName" autocomplete="off" onkeypress="return isAlphKey(event);">
									                    <span id="firstError" style="color:red; font-size:12px;"></span>
								                    </div>
		                                        </div>
		                                        <div class="col-sm-6">
		                                        	<div class="form-group">
			                            				<label for="name">Last Name:</label>
									                    <input type="text" class="form-control" id="lastName" name="lastName" maxlength="30" placeholder="Enter Your LastName" autocomplete="off" onkeypress="return isAlphKey(event);">
									                    <span id="lastError" style="color:red; font-size:12px;"></span>
								                    </div>
		                                        </div>
		                                        </div>
		                                         <div class="col-sm-12">
		                                        <div class="col-sm-6">
		                                        	<div class="form-group">
									                    <label for="email">Email address:</label>
									                    <input type="email" class="form-control" id="email" name="email" maxlength="50" placeholder="Enter Your Email" autocomplete="off" >
									                    <span id="mailError" style="color:red; font-size:12px;"></span>
								                    </div> 
		                                        </div>
						                        <div class="col-sm-6">
						                        	<div class="form-group">
									                    <label for="dob">Date of Birth:</label>
									                    <input type="text" class="form-control datepicker" id="datepicker" name="dateOfBirth" placeholder="Enter DOB" readonly="readonly" autocomplete="off">
									                	<span id="doberror" style="color:red; font-size:12px;"></span>
								                    </div>
						                        </div>
						                        </div>
						                         <div class="col-sm-12">
		                                        <div class="col-sm-6">
		                                        	<div class="form-group">
									                    <label for="phone">Phone:</label>
									                    <input type="text" class="form-control" id="phone" name="contactNo" placeholder="Enter Your Number"  maxlength="10" autocomplete="off" onkeypress="return isNumberKey(event)">
									                    <span id="ferror" style="color:red; font-size:12px;"></span>
								                    </div>
		                                        </div>
							                    </div>
		                                   	</fieldset>
	                                   	<div class="cta-actions text-center">
			                              	<center><button class="btn signLog_btn" type="button" onclick="validateform()" id="register" style="display:block;">CREATE ACCOUNT</button></center>
			                          	</div>
		                          	</form>
		                          	<form action="${pageContext.request.contextPath}/Agent/UserKycRegister" method="post" id="kycFormId" enctype="multipart/form-data">
	                                    <fieldset id="addressDiv" style="display:none;">
	                                    	<legend>Address</legend>
	                                    	<div class="col-sm-6">
	                                        	<div class="form-group">
								                    <label for="name">Address 1:</label>
								                    <input type="text" class="form-control" id="address1" name="address1" maxlength="30" placeholder="Enter Your Address1" autocomplete="off" onkeypress="return isAlphNumberKey(event);">
								                    <span id="address1Error" style="color:red; font-size:12px;"></span>
							                    </div>
	                                        </div>
	                                        <div class="col-sm-6">
	                                        	<div class="form-group">
								                    <label for="name">Address 2:</label>
								                    <input type="text" class="form-control" id="address2" name="address2" maxlength="30" placeholder="Enter Your Address2" autocomplete="off" onkeypress="return isAlphNumberKey(event);">
								                    <span id="address2Error" style="color:red; font-size:12px;"></span>
							                    </div>
	                                        </div>
	                                        <div class="col-sm-6">
	                                        	<div class="form-group">
								                    <label for="name">City:</label>
								                    <input type="text" class="form-control" id="city" name="city" maxlength="30" placeholder="Enter Your City" autocomplete="off" onkeypress="return isAlphKey(event);">
								                    <span id="cityError" style="color:red; font-size:12px;"></span>
							                    </div>
	                                        </div>
	                                        <div class="col-sm-6">
	                                        	<div class="form-group">
								                    <label for="name">State:</label>
								                    <input type="text" class="form-control" id="state" name="state" maxlength="30" placeholder="Enter Your State" autocomplete="off" onkeypress="return isAlphKey(event);">
								                    <span id="stateError" style="color:red; font-size:12px;"></span>
							                    </div>
	                                        </div>
	                                        <div class="col-sm-6">
	                                        	<div class="form-group">
								                    <label for="name">Country:</label>
								                    <input type="text" class="form-control" id="country" name="country"  value="India" maxlength="5" placeholder="Enter Your country" autocomplete="off" onkeypress="return isAlphKey(event);" readonly="readonly">
								                    <span id="countryError" style="color:red; font-size:12px;"></span>
							                    </div>
	                                        </div>
	                                        <div class="col-sm-6">
	                                        	<div class="form-group">
								                    <label for="name">Pin Code:</label>
								                    <input type="text" class="form-control" id="pinCode" name="pinCode" maxlength="6" placeholder="Enter Your pin" autocomplete="off" onkeypress="return isNumberKey(event);">
								                    <span id="pinError" style="color:red; font-size:12px;"></span>
							                    </div>
	                                        </div>
	                                    </fieldset>
	                                    <fieldset id="idDetailsDiv" style="display:none;">
	                                    	<div class="col-sm-6">
	                                        	<div class="form-group">
								                    <label for="name">Aadhaar No:</label>
								                    <input type="text" class="form-control" id="aadhaarIdNo" name="aadhaarIdNo" maxlength="12" placeholder="Enter Your Aadhaar No" autocomplete="off" onkeypress="return isNumberKey(event);">
								                    <span id="aadhaarIdNoError" style="color:red; font-size:12px;"></span>
							                    </div>
	                                        </div>
	                                        <div class="col-sm-6">
	                                        	<div class="form-group">
								                    <label for="name">Pan No:</label>
								                    <input type="text" class="form-control" id="panIdNo" name="panIdNo" maxlength="10" placeholder="Enter Your Pan No" autocomplete="off" onkeypress="return isAlphNumberKey(event);">
								                    <span id="panIdNoError" style="color:red; font-size:12px;"></span>
							                    </div>
	                                        </div>
	                                    </fieldset>
	                                    <fieldset id="kycDiv" style="display:none;">
		                                    <legend>Upload Documents</legend>
		                                        <div class="col-sm-4">
		                                          	<div class="form-group">
			                                            <!-- <div class="fileUpload blue-btn btn width100">
			                                                <span>Upload Your Aadhaar Card Front Side</span>
			                                                <input type="file" class="uploadlogo" name="aadharImage1"  id="aadhar_Image1"/>
			                                            </div> -->
			                                            <label>Upload Aadhaar(Front Side)</label>
			                                            <input type="file" class="dropify" id="aaadharCardImg1" data-default-file="" data-allowed-file-extensions="png jpg jpeg" 
			                                            	data-max-file-size="2M" name="aadharImage1"/>
			                                            <span class="error" id="error_aaadharCardImg1" style="color: red;"></span>
			                                        </div>
		                                        </div>
		                                        <div class="col-sm-4">
		                                          	<div class="form-group">
			                                            <!-- <div class="fileUpload blue-btn btn width100">
			                                                <span>Upload Your Aadhaar Card Back Side</span>
			                                                <input type="file" class="uploadlogo" name="aadharImage2"  id="aadhar_Image2"/>
			                                            </div> -->
			                                            <label>Upload Aadhaar(Back Side)</label>
			                                            <input type="file" class="dropify" data-default-file="" data-allowed-file-extensions="png jpg jpeg" 
			                                            	data-max-file-size="2M" name="aadharImage2" id="aaadharCardImg2"/>
			                                            <span class="error" id="error_aaadharCardImg2" style="color: red;"></span>
			                                        </div>
		                                        </div>
		                                        <div class="col-sm-4">
		                                          	<div class="form-group">
			                                            <!-- <div class="fileUpload blue-btn btn width100">
			                                                <span>Upload Your PAN Card</span>
			                                                <input type="file" class="uploadlogo" name="panCardImage" id="pan_Image" />
			                                            </div> -->
			                                            <label>Upload PAN/Passport/Voter Card</label>
			                                            <input type="file" class="dropify" data-default-file="" data-allowed-file-extensions="png jpg jpeg" 
			                                            	data-max-file-size="2M" name="panCardImage" id="panCardImg"/>
			                                         	<span class="error" id="error_panCardImg" style="color: red;"></span>
			                                        </div>
	                                            </div>
	                                            <input type="hidden" value="${id}" id="userId" name="userId">
	                                        </fieldset>
	                                        <div class="cta-actions text-center" id="kycSubmitBtn">
				                              	<center><button class="btn signLog_btn" type="button" onclick="validatekycform()" id="kycSubmit" style="display:none">CREATE ACCOUNT</button></center>
				                          	</div>
			                          	</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal -->
	  <div class="modal fade" id="otpModal" role="dialog">
	    <div class="modal-dialog modal-sm">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">OTP</h4>
	        </div>
	        <div class="modal-body">
	        	<input type="text" class="form-control" id="otp" name="otp" placeholder="Enter Your OTP" autocomplete="off"  maxlength="6">
	        	<input type="hidden" value="${id}" id="userId">
	        	<span id="error_otp" style="color: red;"></span>
	        </div>
	        <div class="modal-footer">
	        	<span id="successMsg" style="color: green;"></span>
	        	<button type="button" class="btn btn-default modelButton" onclick="resendOtp();">Resend OTP</button>
	          	<button type="button" class="btn btn-default modelButton" onclick="otpValidation();">Submit</button>
	        </div>
	      </div>
	    </div>
	  </div>
	
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script> -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/corporate/assets/dropify/js/dropify.min.js"></script>

	<!-- dropify initialization -->
	<script>
		$('.dropify').dropify();
	</script>
	
	
	<script>
		var success = "${success}";
		var contextPath ="${pageContext.request.contextPath}";
		$(document).ready(function(){
			console.log("success "+success.length);
			if(success.length > 0) {
				$('#error_otp').html('Otp sent to mobile no '+'${username}');
				$('#otpModal').modal('show');
			}
		});
		function isNumberKey(evt){
		    var charCode = (evt.which) ? evt.which : evt.keyCode
		    return !(charCode > 31 && (charCode < 48 || charCode > 57));
		}	
		
		function isAlphNumberKey(evt){
		    var k = (evt.which) ? evt.which : evt.keyCode
		    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
		}
		
		function isAlphKey(evt){
		    var charCode = (evt.which) ? evt.which : evt.keyCode
		    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
		}
		
		$("#idType").change(function(){
		    //alert("The text has been changed.");
		    var idNo  = $('#idNo').val();
	    	var idType  = $('#idType').val();
	    	if (idType == 'Aadhaar Card') {
	    		$('#idNo').attr('maxlength','12');
	    	}
	    	if (idType == 'Pan Card') {
	    		$('#idNo').attr('maxlength','10');
	    	}
	    	if (idType == 'Voter Id') {
	    		$('#idNo').attr('maxlength','10');
	    	}
		}); 
		
	
		
		function validateform(){
	    	var valid = true;
	    	var pattern = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|in)";
	    	var emailPattern = "^[a-zA-Z0-9]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
	    	var passwordPattern = "[a-zA-z0-9]"; //pattern for password
	    	var aadharPattern = "[0-9]{12}";
	    	var panPattern= "[A-Z]{5}[0-9]{4}[A-Z]{1}";
	    	var voterCardPattern="[A-Z]{3}[0-9]{7}";
	    	var firstName=$("#firstName").val();
	    	var lastName  = $('#lastName').val();
	    	var contactNo  = $('#phone').val();
	    	var email = $('#email').val() ;
	    	var dateOfBirth  = $('#datepicker').val();
	    	//var password  = $('#password').val();
	    	//var pinCode  = $('#pinCode').val();
	    	var pinCodePattern = "[0-9]{6}";
	    	var idNo  = $('#idNo').val();
	    	var idType  = $('#idType').val();
	    	var contactNoPattern = "[7-9]{1}[0-9]{9}";
	    	//var cpassword = $('#cpassword').val();
	    	var passportPattern ="^[A-Z][0-9]{8}$";
	    	console.log("email:: "+email);
	    	
	    	if(firstName.length <= 0){
	    		$("#firstError").html("Please enter your first name");
	    		valid = false;
	    	}
	    	if(lastName.length <= 0){
	    		$("#lastError").html("Please enter your last name");
	    		valid = false;
	    	}
	    	
	    	if(contactNo.length <=0){
	    		$("#ferror").html("Please enter your contact no");
	    		valid = false;
	    	}
	    	
    		if(contactNo.length == 10) {
    			if(!contactNo.match(contactNoPattern)) {
	    			console.log("invalid contact no");
	    			$("#ferror").html("Please enter valid contact number");
		    		valid = false; 
	    		}
    		} else {
    			console.log("else invalid contact no block");
    			$("#ferror").html("Please enter 10 digit contact number");
	    		valid = false; 
    		}
	    	
	    	if(email.length  <= 0){
	    		$("#mailError").html("Please enter your email Id ");
	    		valid = false; 
	    	}else if(!email.match(emailPattern)) {
	    		console.log("inside mail")
	    		$("#mailError").html("Enter valid email Id");
	    		valid = false;	
	    	}
	    	
	    	var today = new Date(dateOfBirth);
	    	var day = today.getDate();
	    	var month = today.getMonth()+1;
	    	var year = today.getFullYear();
	    	if(dateOfBirth.length  <= 0){
	    		$("#doberror").html("Please select date of birth");
	    		valid = false; 
	    	} else if(!isValidDate(dateOfBirth)){
	    		console.log("wrong date format");
	    		valid = false;
	    		$("#doberror").html("Please choose the proper format.(yyyy-mm-dd)");
	    	}else if(!(new Date(year+18, month-1, day) <= new Date())) {
				$("#doberror").html("You must be at least 18 years old to sign up");
				valid = false;
	    	}
	    	console.log("valid: "+valid);

	    if(valid == true) {
	    	$("#formId").submit();
	    } 
	    
	    var timeout = setTimeout(function(){
	        $("#firstError").html("");
	    	$("#lastError").html("");
	    	$("#ferror").html("");
	    	$("#mailError").html("");
	    	$("#codeError").html("");
	    	$("#passError").html("");
	    	$("#doberror").html("");
	    	$("#idTyeError").html("");
	    	$("#idNoError").html("");
	    	$("#cpassError").html("");
	    }, 3000);
	    }
		
		function isValidDate(dateString) {
			if(dateString.length>0){
				  var regEx = /^\d{4}-\d{2}-\d{2}$/;
				  if(!dateString.match(regEx)) return false;  // Invalid format
				  var d = new Date(dateString);
				  if(!d.getTime() && d.getTime() !== 0) return false; // Invalid date
				  return d.toISOString().slice(0,10) === dateString;
			}
		}
		
		function otpValidation(){
			var valid = true;
			var otp = $('#otp').val();
			var userId = $('#userId').val();
			if(otp.length <= 0){
				valid = false;
				$('#error_otp').html('Please enter 6 digit otp.');
			}
			if(valid){
				$.ajax({
	    			type : "POST",
		    		url:contextPath+"/Agent/OtpValidation/"+otp+"/"+userId,
		    		dataType:"json",
		   			contentType : "application/json",
		   			data : JSON.stringify({
		   			}),
		   			success : function(response) {
		   				if(response.code == 'S00'){
		   					console.log('success');
		   					$('#userDetails').css('display','none');
		   					$('#register').css('display','none');
		   					$('#kycDiv').css('display','block');
		   					$('#kycSubmit').css('display','block');
		   					$('#otpModal').modal('hide');
		   					$('#addressDiv').css('display','block');
		   					$('#idDetailsDiv').css('display','block');
		   				} else if (response.code == 'F04') {
	   						console.log('error');
	   						$('#error_otp').html('Please enter valid otp.');
	   					}
	   				}
	   			});
			}
			var timeout = setTimeout(function(){
		        $("#error_otp").html("");
		    }, 3000);
		}
		
		function resendOtp(){
			var valid = true;
			var userId = $('#userId').val();
			if(valid){
				$.ajax({
	    			type : "POST",
		    		url:contextPath+"/Agent/ResendOTP/"+userId,
		    		dataType:"json",
		   			contentType : "application/json",
		   			data : JSON.stringify({
		   			}),
		   			success : function(response) {
		   				if(response.code == 'S00'){
		   					console.log('success');
		   					$('#successMsg').html('Resend otp has been sent to your mobile');
		   				} else if (response.code == 'F04') {
	   						$('#error_otp').html('Please enter valid otp.');
	   					}
	   				}
	   			});
			}
			var timeout = setTimeout(function(){
		        $("#error_otp").html("");
		    }, 3000);
		}
		
		function validatekycform(){
			var valid = true;
			var aadharPattern = "[0-9]{12}";
	    	var panPattern= "[A-Z]{5}[0-9]{4}[A-Z]{1}";
			var aaadharCardImg1 = $('#aaadharCardImg1').val();
			var pinCodePattern = "[0-9]{6}";
			var address1 = $('#address1').val();
			if(address1.length<=0){
				valid = false;
				$('#address1Error').html('Please enter address1');
			}
			
			
			
			var address2 = $('#address2').val();
			if(address2.length<=0){
				valid = false;
				$('#address2Error').html('Please enter address2');
			}
			
			var city = $('#city').val();
			if(city.length<=0){
				valid = false;
				$('#cityError').html('Please enter city');
			}
			
			var state = $('#state').val();
			if(state.length<=0){
				valid = false;
				$('#stateError').html('Please enter state');
			}
			
			var state = $('#state').val();
			if(state.length<=0){
				valid = false;
				$('#stateError').html('Please enter state');
			}
			
			var pinCode = $('#pinCode').val();
			if(pinCode.length  <= 0){
	    		$("#pinError").html("Please enter pincode");
	    		valid = false; 
	    	} else if(pinCode.length == 6) {
	    		if(!pinCode.match(pinCodePattern)) {
	    			valid = false;
	    			$('#pinError').html("Please enter valid pincode");
	    		}
	    	} else {
	    		valid = false;
	    		$('#pinError').html("Please enter 6 digit pincode");
	    	}
			
			var aadhaarIdNo = $('#aadhaarIdNo').val();
			if(aadhaarIdNo.length  <= 0){
	    		$("#aadhaarIdNoError").html("Please enter aadhaar no");
	    		valid = false; 
	    	} else if(aadhaarIdNo.length == 12) {
	    		if(!aadhaarIdNo.match(aadharPattern)) {
	    			valid = false;
	    			$('#aadhaarIdNoError').html("Please enter valid aadhaar no");
	    		}
	    	} else {
	    		valid = false;
	    		$('#aadhaarIdNoError').html("Please enter 12 digit aadhaar no");
	    	}
			
			var panIdNo = $('#panIdNo').val();
			if(panIdNo.length  <= 0){
	    		$("#panIdNoError").html("Please enter pan no");
	    		valid = false; 
	    	} else if(panIdNo.length == 10) {
	    		if(!panIdNo.match(panPattern)) {
	    			valid = false;
	    			$('#panIdNoError').html("Please enter valid pan no");
	    		}
	    	} else {
	    		valid = false;
	    		$('#panIdNoError').html("Please enter 10 digit pan no");
	    	}
			
			if(aaadharCardImg1.length<=0){
				valid = false;
				$('#error_aaadharCardImg1').html('Please upload aadhaar card front image');
			}
			var aaadharCardImg2 = $('#aaadharCardImg2').val();
			if(aaadharCardImg2.length<=0){
				valid = false;
				$('#error_aaadharCardImg2').html('Please upload aadhaar card back image');
			}
			var panCardImg = $('#panCardImg').val();
			if(panCardImg.length<=0){
				valid = false;
				$('#error_panCardImg').html('Please upload pan card image');
			}
			if(valid){
				$('#kycFormId').submit();
				var spinner="Please Wait <img src='${pageContext.request.contextPath}/resources/spinner.gif' height='20' width='20'>"
					$('#kycSubmit').html(spinner);
					$('#kycSubmit').attr("disabled","disabled");
			}
			var timeout = setTimeout(function(){
		        $("#error_aaadharCardImg1").html("");
		        $("#error_aaadharCardImg2").html("");
		        $("#error_panCardImg").html("");
		        $('#address1Error').html("");
		        $('#panIdNoError').html("");
		        $('#aadhaarIdNoError').html("");
		        $('#pinError').html("");
		        $('#stateError').html("");
		        $('#cityError').html("");
		        $('#address1Error').html("");
		        $('#address2Error').html("");
		        $('#countryError').html("");
		    }, 3000);
		}
		
	</script>
	
	<script>
	     var today, datepicker;
	     today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
	     datepicker = $('#datepicker').datepicker({
	     uiLibrary: 'bootstrap4',
	     format: 'yyyy-mm-dd',
	     maxDate: today
     });
 	</script>
	
	
	
</body>
</html>
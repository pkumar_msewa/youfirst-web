<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page isELIgnored="false"%>
	<%
	    response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
	    response.setDateHeader("Expires", 0);
	    response.setHeader("Pragma", "no-cache");
	%>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Digiosk Cards | Agent Login</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png" type="image/x-icon" />

  <style>
    body{
      background: url("${pageContext.request.contextPath}/resources/corporate/assets/img/login_bg.png");
      background-repeat: no-repeat;
	  background-position: center;
	  background-size: cover;
	  height: 100vh;
    }

    .mt-1 {
      margin-top: 1%;
    }

    .mt-2 {
      margin-top: 2%;
    }

    .mt-3 {
      margin-top: 3%;
    }

    .mt-5 {
      margin-top: 40%;
    }

    .mb-1 {
      margin-bottom: 1%;
    }

    .mb-2 {
      margin-bottom: 2%;
    }

    .mb-3 {
      margin-bottom: 3%;
    }

    .mb-10 {
      margin-bottom: 10%;
    }

    .fgtLink {
      color: #42164e;
    }

    .fgtLink:hover, .fgtLink:focus {
      color: #42164e;
      text-decoration: none;
    }

    .fgtLink:focus {
      outline: none;
    }

    .log{
      width: 30%;
      margin-bottom:10%;
    }

    .btn-custom{
      color: white;
      background: linear-gradient(to right, #2b1858, #e41824);
      background-repeat: no-repeat;
      border-radius: 6px;
    }

    .btn-custom:hover, .btn-custom:focus{
      color: white;
    }

    .btn-custom:focus{
      outline: none;
    }

    .error{
      color: red;
      font-size: 12px;
    }

    .box {
      background: #ffffffcf;
      box-shadow: 0px 0px 20px 6px #efefef;
      border-radius: 10px;
      padding: 20px 25px;
    }

    .footer {
      position: fixed;
      width: 100%;
      padding: 10px;
      bottom: 0;
      background: #ffffffb8;
    }
    </style>
</head>
<body>
  <div class="container">
    <div class="col-md-4 col-md-offset-4">
	    <div class="box mt-5">
        <form id="loginForm" class="form-auth-small"  action="${pageContext.request.contextPath}/Agent/Home" method="post" class="login">
    
          <!-- <div class="col-md-6 ">
            <img src="${pageContext.request.contextPath}/resources/corporate/assets/img/login_1.jpg" class="img-responsive">
          </div> -->
          <center><img src="${pageContext.request.contextPath}/resources/digiosk-assets/img/logo.png" class="img-responsive log"></center>
          <center><span id="loginErrorMsg" class="error">${loginMsg}</span></center>
          <div class="form-group">
            <label>Username:</label>
            <input type="email" name="username" id="loginUsername" class="form-control" placeholder="Username" maxlength="50" required>
            <span class="error" id="error_loginUsername"></span>
          </div>
          <div class="form-group mb-10">
            <label>Password</label>
            <input type="password" name="password" id="loginPassword" class="form-control" placeholder="Password" maxlength="8" required>
            <small style="float: right;"><a href="#" data-toggle="modal" data-target="#myModal" class="fgtLink">Forgot Password?</a></small>
            <span class="error" id="error_loginPassword"></span>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="col-sm-6">
                <center><button type="reset" class="btn btn-custom btn-block">Reset</button></center>
              </div>
              <div class="col-sm-6">
                <center><button type="button" class="btn btn-custom btn-block" onclick="loginValidation();">Login</button></center>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="footer">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-6 col-md-offset-6">
            <div class="text-right">
              <span>2017 © Copyright iMoney.</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
	
	<!-- for forget password modal -->
	<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Forget Password</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Email Id</label>
			      <input type="email" name="email" id="email" class="form-control" maxlength="50" placeholder="Enter Email-Id" required>
			      <span id="error_email" class="error"></span>
		      </div>
    		  <div class="form-group">
    		  	<span id="error" class="error"></span>
    		  </div>
          <div class="form-group">
            <center><button id="submit" type="button" class="btn btn-custom" onclick="forgetPassword();">Submit</button></center>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
  
  <!-- for otp modal -->
	<div class="modal fade" id="otpModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">OTP</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>OTP</label>
			      <input type="text" name="otp" id="otp" class="form-control" placeholder="Enter the OTP" maxlength="6" required>
			      <span id="error_otp" class="error"></span>
			      <input type="hidden" value="" id="userName1">
		      </div>
		  
		      <div class="form-group">
            <label>New password</label>
			      <input type="password" name="newPassword" id="newPassword" class="form-control" placeholder="Enter the new Password" maxlength="6" required>
			      <span id="error_newPassword" class="error"></span>
		      </div>
		  
    		  <div class="form-group">
            <label>Confirm Password</label>
    			  <input type="password" name="confirmPassword" id="confirmPassword" class="form-control" placeholder="Enter the confirm password" maxlength="6" required>
    			  <span id="error_confirmPassword" class="error"></span>
    		  </div>

          <div class="form-group">
            <center><button id="button_otp" type="button" class="btn btn-custom" onclick="otp();">Submit</button></center>
          </div>
		  
    		  <div class="form-group">
    		  	<span id="invalid_resp" class="error"></span>
    		  </div>
        </div>
      </div>
    </div>
  </div>
  
  
  
  <!-- for successful forget password modal -->
	<div class="modal fade" id="successModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Success</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
			  <span id="success" style="color:green; font-size: :16px;">Your password has been changed successfully.</span>
		  </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <!-- javascript starts here -->

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <script type="text/javascript">
  	var cont_path="${pageContext.request.contextPath}";
  	var spinnerUrl = "Please wait <img src='/resources/images/spinner.gif' height='20' width='20'>";
  	
  	$('#myModal').on('hidden.bs.modal', function () {
	    $(this).find('#email').val("");
	    $(this).find('#error').html("");
	    $(this).find('#submit').html("Submit");
	    $(this).find('#submit').removeClass("disabled");
	});
  	$('#otpModal').on('hidden.bs.modal', function () {
	    $(this).find('#otp').val("");
	    $(this).find('#newPassword').val("");
	    $(this).find('#confirmPassword').val("");
	    $(this).find('#button_otp').val("Submit");
	});
  	
  	function forgetPassword() {
  		var valid = true;
  		var email = $('#email').val();
  		var pattern = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|in)";
  		
  		if (email.length  <= 0) {
    		$("#error_email").html("Please enter your email Id ");
    		valid = false; 
    	} else if(!email.match(pattern)) {
    		console.log("inside mail")
    		$("#error_email").html("Enter valid email Id");
    		valid = false;	
    	}
  		if (valid == true) {
  			$('#submit').html(spinnerUrl);
  			$("#submit").addClass("disabled");
  			$.ajax({
  				type:"POST",
        	    contentType : "application/json",
        	    url: cont_path+"/Agent/ForgetPassword",
        	    dataType : 'json',
    			data : JSON.stringify({
    				"username" : "" + email + "",
    			}),
    			success : function(response) {
      				if (response.code.includes("S00")) {
						$('#otpModal').modal('show');
						$('#myModal').modal('hide');
						$('#userName1').val(email);
      				} else {
    					$('#error').html(response.message);
    					$('#submit').html("Submit");
    					$("#submit").removeClass("disabled");
    				}
      			}
  			});
  		}
  		var timeout = setTimeout(function(){
	        $("#error_email").html("");
	        $("#error").html("");
	    }, 3000);
  	}
  	
  	
  	function otp() {
  		var valid = true;
  		var otp = $('#otp').val();
  		var userName1 = $('#userName1').val();
  		var newPassword = $('#newPassword').val();
  		var confirmPassword = $('#confirmPassword').val();
  		if (otp.length  <= 0) {
    		$("#error_otp").html("Please enter your OTP ");
    		valid = false; 
    	}
  		if (newPassword.length  <= 0) {
    		$("#error_newPassword").html("Please enter your new password ");
    		valid = false; 
    	}
  		if (confirmPassword.length  <= 0) {
    		$("#error_confirmPassword").html("Please enter your confirm password ");
    		valid = false; 
    	}
  		
  		if (newPassword != confirmPassword) {
  			$("#error_confirmPassword").html("New Password and confirm password does not match.");
    		valid = false; 
  		}
  		if (valid == true) {
  			$('#button_otp').html(spinnerUrl);
  			$("#button_otp").addClass("disabled");
  			$.ajax({
  				type:"POST",
        	    contentType : "application/json",
        	    url: cont_path+"/Agent/ForgetPasswordWithOtp",
        	    dataType : 'json',
    			data : JSON.stringify({
    				"key" : "" + otp + "",
    				"username" : "" + userName1 + "",
    				"newPassword" : "" + newPassword + "",
    				"confirmPassword" : "" + confirmPassword + ""
    			}),
    			success : function(response) {
    				if (response.code.includes("S00")) {
    					$('#successModal').modal('show');
    					$('#otpModal').modal('hide');
    				} else {
    					$('#invalid_resp').html(response.message);
    					$('#button_otp').html("Submit");
    					$("#button_otp").removeClass("disabled");
    				}
    			}
  			});
  		}
  		var timeout = setTimeout(function(){
	        $("#error_otp").html("");
	        $("#error_newPassword").html("");
	        $("#error_confirmPassword").html("");
	        $('#invalid_resp').html("");
	    }, 3000);
  	}
  	
  	$('#loginForm input').keydown(function(e) {
  	    if (e.keyCode == 13) {
  	    	loginValidation();
  	    }
  	});
  	
  	function loginValidation() {
  		var valid = true;
  		var pattern = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|in)";
  		var emailPattern = "^[a-zA-Z0-9]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
  		var loginUsername = $('#loginUsername').val();
  		var loginPassword = $('#loginPassword').val();
  		if (loginUsername.length <= 0) {
  			$('#error_loginUsername').html('Please enter your user name.');
  			valid = false;
  		} /* else if(!loginUsername.match(emailPattern)) {
  			console.log("invalid pattern");
  			$('#error_loginUsername').html('Please enter your valid email id');
  			valid = false;
  		} */
  		
  		if (loginPassword.length <= 0) {
  			$('#error_loginPassword').html('Please enter your password.');
  			valid = false;
  		} else if (loginPassword.length > 8) {
  			$('#error_loginPassword').html('Please enter your 8 digit password.');
  			valid = false;
  		}
  		
  		if (valid == true) {
  			$('#loginForm').submit();
  		}
  		var timeout = setTimeout(function(){
	        $("#error_loginUsername").html("");
	    	$("#error_loginPassword").html("");
	    	$('#loginErrorMsg').text("");
	    }, 3000);  		
  	}
  </script>
  
</body>
</html>

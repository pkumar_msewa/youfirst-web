<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
    response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
    response.setDateHeader("Expires", 0);
    response.setHeader("Pragma", "no-cache");
%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Agent Dashboard</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link rel="icon" href="${pageContext.request.contextPath}/resources/digiosk-assets/img/favicon.png" type="image/x-icon" />
    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/animate.min.css" rel="stylesheet"/>
    <!--  Paper Dashboard core CSS    -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/paper-dashboard.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/demo.css" rel="stylesheet" />
    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/themify-icons.css" rel="stylesheet">
</head>
<body>

<div class="wrapper">
<jsp:include page="LeftMenu.jsp"></jsp:include>

<!--==================nav===========================-->
    <div class="main-panel">
       <jsp:include page="Header.jsp"></jsp:include>
<!--==================end nav===========================-->

<!--==================header===========================-->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4 col-sm-4">
                        <div class="card">
                        <a href="${pageContext.request.contextPath}/Agent/UserList">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-warning text-center">
                                            <i class="ti-user"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Registered Users</p>
                                            ${userCount}
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <!-- <i class="ti-reload"></i> Updated now -->
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4">
                        <div class="card">
                        <a href="${pageContext.request.contextPath}/Agent/LoadCardList">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-success text-center">
                                            <i class="ti-wallet"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Total Transaction</p>
                                            ${totalAmount}
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <!-- <i class="ti-calendar"></i> Last day -->
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-danger text-center">
                                            <i class="ti-credit-card"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Total Physical Cards</p>
                                            ${physicalCardCount}
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <!-- <i class="ti-timer"></i> In the last hour -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<!--==================end header===========================-->
           
                <div class="row">
                    <div class="col-md-6">
                        <div class="card" style="margin-top: 50px;">
                            <div class="content">
                                <!-- <div id="chartHours" class="ct-chart"></div> -->
 								<div id="check" class="check_it"></div>
                                <div class="footer"> 
                                <hr>	
								<b>				
                                  Last 6 months IMoney User Data</b>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card" style="margin-top: 50px;">
                            <div class="content">
 								<div id="check1" class="check_it1"></div>
 								<div class="footer"> 
                                <hr>	
								<b>				
                                  Last 6 months IMoney Transactions Data</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                
                <div class="copyright pull-right">
					2017 © Copyright iMoney.
                </div>
            </div>
        </footer>

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="${pageContext.request.contextPath}/resources/corporate/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/resources/corporate/assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/resources/corporate/assets/js/bootstrap-checkbox-radio.js"></script>
	<script src="${pageContext.request.contextPath}/resources/corporate/assets/js/chartist.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/corporate/assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
	<script src="${pageContext.request.contextPath}/resources/corporate/assets/js/paper-dashboard.js"></script>
	<script src="${pageContext.request.contextPath}/resources/corporate/assets/js/demo.js"></script>
	<script type="text/javascript">
    	$(document).ready(function(){
        	demo.initChartist();
        	
        	var month=${Month};
        	var userValues=${UserValues};
        	
        	new Chartist.Line('.check_it', {
      		  labels: month,
      		  series: [
      		    userValues,
      		  ]
      		}, {
      		  fullWidth: true,
      		  chartPadding: {
      		    right: 40
      		  }
      		});
        	
        	
        	var monthData=${monthData};
        	var valueData=${valueData};
        	new Chartist.Line('.check_it1', {
        		  labels: monthData,
        		  series: [
        		    valueData,
        		  ]
        		}, {
        		  fullWidth: true,
        		  chartPadding: {
        		    right: 40
        		  }
        		});
        	
    	});
	</script>

</html>

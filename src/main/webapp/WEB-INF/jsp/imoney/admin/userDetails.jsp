
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
<meta name="keywords"
	content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
<meta name="author" content="pixelstrap">
<link rel="icon" href="${pageContext.request.contextPath}/resources/assets/images/favicon.png" type="image/x-icon" />
<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets/images/favicon.png"
	type="image/x-icon" />
<title>Home</title>

<!--Google font-->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700"
	rel="stylesheet">

<!-- Font Awesome -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.css">

<!-- ico-font -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/icofont/icofont.css">

<!-- Themify icon -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/themify-icons/themify-icons.css">
<!-- Flag icon -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/flag-icon.css">

<!-- prism css -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/prism.css">

<!-- Owl css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/owlcarousel.css">

<!-- Bootstrap css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.css">

<!-- App css -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/style.css">

<!-- Responsive css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/responsive.css">

<!-- linearicons css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/linearicons/style.css">
<!-- mystyle  css-->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/mystyle.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<style>
	/* The Modal (background) */
	.modal {
	    display: none; /* Hidden by default */
	    position: fixed; /* Stay in place */
	    padding-top: 70px; /* Location of the box */
	    
	    left: 0;
	    top: 0;
	    width: 100%; /* Full width */
	    height: 100%; /* Full height */
	    overflow: auto; /* Enable scroll if needed */
	    background-color: rgb(0,0,0); /* Fallback color */
	    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}
	
	/* Modal Content */
	.modal-content {
	    background-color: #fefefe;
	    margin: auto;
	    padding: 20px;
	    padding-left: 60px;
	    
	    border: 1px solid #888;
	    width: 80%;
	}
	
	#InfoModalID .modal-content {
		padding: 0;
		width: 100%;
	}
	
	/* The Close Button */
	.close {
	    color: #aaaaaa;
	    float: right;
	    font-size: 28px;
	    font-weight: bold;
	}
	
	.close:hover,
	.close:focus {
	    color: #000;
	    text-decoration: none;
	    cursor: pointer;
	}
	
	.modal-backdrop.show{
	z-index:0;
	}
	
	</style>

</head>
<body>

	<!-- Loader starts -->
	<div class="loader-wrapper">
		<div class="loader bg-white">
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
			<h4>
				Have a great day at work today <span>&#x263A;</span>
			</h4>
		</div>
	</div>
	<!-- Loader ends -->

	<!--page-wrapper Start-->
	<div class="page-wrapper">
		<jsp:include page="/WEB-INF/jsp/imoney/admin/header.jsp"></jsp:include>
		<div class="page-body-wrapper">
			<jsp:include page="/WEB-INF/jsp/imoney/admin/leftMenu.jsp"></jsp:include>

			<div class="page-body">
				<!-- Container-fluid starts -->
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-12">
							<div class="card height-equal equal-height-lg"
								style="margin-top: 15px;">
								<div class="card-header">
									<h5>User Details</h5>
									<div class="card-header-right">
										<a href="#" class="icon-bar"><i class="lnr lnr-magnifier"></i></a>
										<a href="#" class="icon-bar" onclick="downloadReport();"><i class="lnr lnr-download"></i></a>
									</div>
								</div>
								<input type="hidden" id="paginationId" value="1">

							<%-- 	<form id="dformId" method="post"
									action="${pageContext.request.contextPath}/Download/DownloadUserReport">
									<input type="hidden" id="downloadReportId" name="dateRange"
										value="" class="form-control" />
								</form> --%>

								<div class="row">
										<div class="col-md-4">
										<label>Select Daterange*</label>
											<div id="" style="cursor: pointer;"> 
												<input id="reportrange" name="toDate" class="form-control" style="background-color:white"
													readonly="readonly" />
											</div>
										</div>
										<div class="col-md-1" >
											<button class="btn btn-primary" style="margin-top: 50%;"  type="button" onclick="getUserListBasedOnRole(0);">Filter</button>
										</div>
										<div class="col-md-4">
		                                    <label>Find By Username*</label>
			                                    <input id="usernameId" name="userName" class="form-control" maxlength="100"  placeholder="Enter Username"/>
			                               
										</div>
										<div class="col-md-1" >
											 <button class="btn btn-primary" onclick="getByUserName()" style="margin-top: 50%;" type="button" >Search</button>
										</div>

								</div>
								<div class="card-body">
									<div class="tabble" style="overflow-x: scroll;">
		            					<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
		            					<thead>
	                                          <tr><th>S.No</th>
	                                          <th>Customer ID</th>
													<th>Title</th>
													<th>First&nbsp;Name</th>
													<th>Last&nbsp;Name</th>
													<th>Email</th>
													<th>Mobile</th>
													<th>Date&nbsp;Of&nbsp;Birth</th>
													<th>Created&nbsp;Date</th>
<!-- 													<th>Id&nbsp;Type</th> -->
<!-- 													<th>Id&nbsp;Number</th> -->
													<th>Status</th>
													<th>KYC&nbsp;Status</th>
													<th>Balance</th>
												</tr>
	                                    </thead>
	                                    <tbody id="genericUserList" style="height: 100px;overflow-y: auto;">
	                                    </tbody>
		            				</table>
		            				<nav>
										<ul class="pagination" id="paginationn"></ul>
									</nav>
	            				</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<!-- Container-fluid Ends -->
	</div>
	<!--Page Body Ends-->

	<div class="modal" tabindex="-1" role="dialog" id="InfoModalID">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Customer Details</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <div class="row">
				<div class="col-6" style="border-right: 1px solid #efefef">
					<div class="row mb-2">
						<div class="col-5"><h6>First Name:</h6></div>
						<div class="col-7"><strong id="fnameId"></strong></div>					
					</div>
					<div class="row mb-2">
						<div class="col-5"><h6>Last Name:</h6></div>
						<div class="col-7"><strong id="lnameId"></strong></div>					
					</div>
					<div class="row mb-2">
						<div class="col-5" ><h6>Email Id:</h6></div>
						<div class="col-7"><strong id="emailId"></strong></div>					
					</div>
					<div class="row mb-2">
						<div class="col-5"><h6>KYC Status:</h6></div>
						<div class="col-7"><strong id="acctTypeId"></strong></div>					
					</div>
					<div class="row mb-2">
						<div class="col-5"><h6>User Status</h6></div>
						<div class="col-7"><strong id="userStatusId"></strong></div>					
					</div>
				</div>
				<div class="col-6">
					<div class="row mb-2">
						<div class="col-5"><h6>Wallet Balance.:</h6></div>
						<div class="col-7"><strong id="balId"></strong></div>					
					</div>
					<div class="row mb-2">
						<div class="col-5"><h6>Card Number:</h6></div>
						<div class="col-7"><strong id="cardNoId" ></strong></div>					
					</div>
					<div class="row mb-2">
						<div class="col-5"><h6>Mobile:</h6></div>
						<div class="col-7"><strong id="mobileNoId" ></strong></div>					
					</div>
					<div class="row mb-2">
						<div class="col-5"><h6>Card Holder:</h6></div>
						<div class="col-7"><strong id="cardHolderId"></strong></div>					
					</div>	
					<div class="row mb-2">
						<div class="col-6"><button class="btn btn-sm btn-block btn-warning" type="button">BLOCK USER</button></div>
						<div class="col-6"><button class="btn btn-sm btn-block btn-success" type="button">LOCK ACCOUNT</button></div>					
					</div>	
				</div>        	
	        </div>
	      </div>
	    </div>
	  </div>
	</div>

	
	<!--page-wrapper Ends-->

	<!-- latest jquery-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/popper.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/bootstrap.js"></script>
	<!-- Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/chart/Chart.min.js"></script>
	<!-- Morris Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/raphael.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/morris.js"></script>
	<!-- owlcarousel js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/owlcarousel/owl.carousel.js"></script>
	<!-- Sidebar jquery-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/sidebar-menu.js"></script>
	<!--Sparkline  Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/sparkline/sparkline.js"></script>
	<!--Height Equal Js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/height-equal.js"></script>
	<!-- prism js -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/prism/prism.min.js"></script>
	<!-- clipboard js -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/clipboard/clipboard.min.js"></script>
	<!-- custom card js  -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/custom-card/custom-card.js"></script>
	<!-- Theme js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/script.js"></script>
	<!-- <script src="${pageContext.request.contextPath}/resources/assets/js/theme-customizer/customizer.js"></script> -->
	<!-- <script src="${pageContext.request.contextPath}/resources/assets/js/chat-sidebar/chat.js"></script> -->
	<!-- Counter js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.waypoints.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.counterup.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/counter-custom.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/notify/bootstrap-notify.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/notify/index.js"></script>
	 <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    
<!--     pagination -->

<script
		src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.min.js"></script>
    


	<script type="text/javascript">
		var contextPath="${pageContext.request.contextPath}";
		var value = "${value}";
		var status ="${status}";
		if(value == null){
			value='All';
		}
		var role = "${role}";
		$(document).ready(function(){
    		getUserList(0);
    	});
		$(function() {
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		    function cb(start, end) {
		        $('#reportrange').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
		    }
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'DD/MM/YYYY'
		        },
		        dateLimit: {
		            "days": 60
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		    cb(start, end);
		});
		
		function getUserList(page) {
    		var trHtml='';
    		var data;
    		$.ajax({
    			type : "POST",
	    		url:contextPath+"/Master/GenericUserList/"+value,
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"page" : page,
	   				"size" : 10,
	   			}),
	   			success : function(response) {
					$('#genericUserList').empty();
					if (response.details.content.length > 0) {
						for (var i = 0; i < response.details.content.length; i++) {
							data = response.details.content[i];
							trHtml = '<tr title="click here" style="cursor: pointer;" <a href="#" onclick="openNav(\''+data.username+ '\',\''+data.userDetail.firstName+ '\',\''
									+data.userDetail.lastName+ '\',\''+data.userDetail.email+ '\',\''+data.accountDetail.accountType.name+ '\');"></>">';
	   						trHtml = trHtml + '<td>' +(i+1)+ '</td>';
	   						trHtml = trHtml + '<td>' +data.userHashId+ '</td>';
	   						var gender='';
	   						if(data.userDetail.gender !=null){
	   							if(data.userDetail.gender == 'M'){
	   								gender = "Male";
	   								trHtml = trHtml + '<td>Mr.</td>';
	   							}else{
	   								trHtml = trHtml + '<td>Mrs</td>';
	   								gender = "Female";
	   							}
	   						}else{
	   							trHtml = trHtml + '<td>-</td>';
	   							gender = "-";
	   						}
	   						trHtml = trHtml + '<td>'+data.userDetail.firstName+'</td>';
	   						var name = '';
	   						if(data.userDetail.lastName != null) {
	   							trHtml = trHtml + '<td>'+data.userDetail.lastName+'</td>';
	   						} else {
	   							trHtml = trHtml + '<td>-</td>';
	   						}
	   						trHtml = trHtml + '<td>'+data.userDetail.email+'</td>';
	   						trHtml = trHtml + '<td>' +data.userDetail.contactNo+ '</td>';
	   						if(data.userDetail.dateOfBirth !=null){
       							trHtml = trHtml + '<td>' +data.userDetail.dateOfBirth+ '</td>';
       						}else{
       							trHtml = trHtml + '<td>-</td>';
       						}
	   						trHtml = trHtml + '<td>' +data.userDetail.created+'</td>';
// 	   						if(data.userDetail.idType !=null){
//        							trHtml = trHtml + '<td>' +data.userDetail.idType+ '</td>';
//        						}else{
//        							trHtml = trHtml + '<td>-</td>';
//        						}if(data.userDetail.idNo !=null){
//        							trHtml = trHtml + '<td>' +data.userDetail.idNo+ '</td>';
//        						}else{
//        							trHtml = trHtml + '<td>-</td>';
//        						}
	   						trHtml = trHtml + '<td>' +data.mobileStatus+ '</td>';
	   						trHtml = trHtml + '<td>' +data.accountDetail.accountType.name+ '</td>';
	   						trHtml = trHtml + '<td>' +data.accountDetail.balance+ '</td>';
	   						trHtml = trHtml + '</tr>';
	   						$('#genericUserList').append(trHtml);
						}
						$(function () {
	   	   					$('#paginationn').twbsPagination({
	    					 	totalPages: response.totalPage,
	    					 	visiblePages: 10,
	    		         	 	onPageClick: function (event, page) {
	    		         	 		if($('#paginationId').val() !=1){
	    		         	 			getUserList(page-1);
	    		         	 		}else{
					                $('#paginationId').val(2);
	    		         	 		}
	    			         	}
				 			});
		   				});
					} else {
						trHtml = '<tr>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td>No data found</td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '</tr>';
						$('#genericUserList').append(trHtml);
					}
	   			}
    		});
    	}
		
		function getUserListBasedOnRole(page) {
    		var trHtml='';
			var userRole='User'; 
    		var data;
    		var dateRange = $('#reportrange').val();
    		var userType = $('#userType').val();
    		$('#tableHeader0').css('display','block');
    		$.ajax({
    			type : "POST",
	    		url:contextPath+"/Master/GenericUserListBasedOnRole",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"page" : page,
	   				"size" : 10,
	   				"role" : "" + userRole + "",
	   				"dateRange" : "" + dateRange + "",
	   				"value" : "" + value + "",
	   				"status" : "" + status + "",
	   			}),
	   			success : function(response) {
					$('#genericUserList').empty();
					if (response.details.content.length > 0) {
						for (var i = 0; i < response.details.content.length; i++) {
							data = response.details.content[i];
							trHtml = '<tr title="click here" style="cursor: pointer;" <a href="#" onclick="openNav(\''+data.username+ '\',\''+data.userDetail.firstName+ '\',\''
							+data.userDetail.lastName+ '\',\''+data.userDetail.email+ '\',\''+data.accountDetail.accountType.name+ '\');"></> >';
	   						trHtml = trHtml + '<td>' +(i+1)+ '</td>';
	   						trHtml = trHtml + '<td>' +data.userHashId+ '</td>';
	   						var gender='';
	   						if(data.userDetail.gender !=null){
	   							if(data.userDetail.gender == 'M'){
	   								gender = "Male";
	   								trHtml = trHtml + '<td>Mr.</td>';
	   							}else{
	   								trHtml = trHtml + '<td>Mrs</td>';
	   								gender = "Female";
	   							}
	   						}else{
	   							trHtml = trHtml + '<td>-</td>';
	   							gender = "-";
	   						}
	   						trHtml = trHtml + '<td>'+data.userDetail.firstName+'</td>';
	   						var name = '';
	   						if(data.userDetail.lastName != null) {
	   							trHtml = trHtml + '<td>'+data.userDetail.lastName+'</td>';
	   						} else {
	   							trHtml = trHtml + '<td>-</td>';
	   						}
	   						trHtml = trHtml + '<td>'+data.userDetail.email+'</td>';
	   						trHtml = trHtml + '<td>' +data.userDetail.contactNo+ '</td>';
	   						
	   						if(data.userDetail.dateOfBirth !=null){
       							trHtml = trHtml + '<td>' +data.userDetail.dateOfBirth+ '</td>';
       						}else{
       							trHtml = trHtml + '<td>-</td>';
       						}
	   						
	   						
	   						trHtml = trHtml + '<td>' +data.userDetail.created+'</td>';
// 	   						if(data.userDetail.idType !=null){
//        							trHtml = trHtml + '<td>' +data.userDetail.idType+ '</td>';
//        						}else{
//        							trHtml = trHtml + '<td>-</td>';
//        						}if(data.userDetail.idNo !=null){
//        							trHtml = trHtml + '<td>' +data.userDetail.idNo+ '</td>';
//        						}else{
//        							trHtml = trHtml + '<td>-</td>';
//        						}
	   						trHtml = trHtml + '<td>' +data.mobileStatus+ '</td>';
	   						trHtml = trHtml + '<td>' +data.accountDetail.accountType.name+ '</td>';
	   						trHtml = trHtml + '<td>' +data.accountDetail.balance+ '</td>';
	   						trHtml = trHtml + '</tr>';
	   						$('#genericUserList').append(trHtml);
						}
						$(function () {
	   	   					if(response.totalPage > 0) {
	   	   						$('#paginationn').twbsPagination('destroy');
		   	   					$('#paginationn').twbsPagination({
		    					 	totalPages: response.totalPage,
		    					 	visiblePages: 10,
		    		         	 	onPageClick: function (event, page) {
		    		         	 		if($('#paginationId').val() !=1){
		    		         	 			fetchDataByRole(page-1);
		    		         	 		}else{
						                $('#paginationId').val(2);
		    		         	 		}
		    			         	}
					 			});
	   	   					}
		   				});
					} else {
						trHtml = '<tr>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td>No data found</td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '</tr>';
						$('#genericUserList').append(trHtml);
						$('#paginationn').twbsPagination('destroy');
					}
	   			}
    		});
    	}
		
		
		
		function fetchDataByRole(page) {
    		var trHtml='';
    		var data;
    		var dateRange = $('#reportrange').val();
    		var userType = $('#userType').val();
    		$('#tableHeader0').css('display','block');
    		$.ajax({
    			type : "POST",
	    		url:contextPath+"/Master/GenericUserListBasedOnRole",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"page" : page,
	   				"size" : 10,
	   				"role" : "User",
	   				"dateRange" : "" + dateRange + "",
	   				"value" : "" + value + "",
	   				"status" : "" + status + "",
	   			}),
	   			success : function(response) {
					$('#genericUserList').empty();
					if (response.details.content.length > 0) {
						for (var i = 0; i < response.details.content.length; i++) {
						data = response.details.content[i];
						trHtml = '<tr title="click here" style="cursor: pointer;" <a href="#" onclick="openNav(\''+data.username+ '\',\''+data.userDetail.firstName+ '\',\''
						+data.userDetail.lastName+ '\',\''+data.userDetail.email+ '\',\''+data.accountDetail.accountType.name+ '\');"></> >';
   						trHtml = trHtml + '<td>' +(i+1)+ '</td>';
   						trHtml = trHtml + '<td>' +data.userHashId+ '</td>';
   						var gender='';
   						if(data.userDetail.gender !=null){
   							if(data.userDetail.gender == 'M'){
   								gender = "Male";
   								trHtml = trHtml + '<td>Mr.</td>';
   							}else{
   								trHtml = trHtml + '<td>Mrs</td>';
   								gender = "Female";
   							}
   						}else{
   							trHtml = trHtml + '<td>-</td>';
   							gender = "-";
   						}
   						trHtml = trHtml + '<td>'+data.userDetail.firstName+'</td>';
   						var name = '';
   						if(data.userDetail.lastName != null) {
   							trHtml = trHtml + '<td>'+data.userDetail.lastName+'</td>';
   						} else {
   							trHtml = trHtml + '<td>-</td>';
   						}
   						trHtml = trHtml + '<td>'+data.userDetail.email+'</td>';
   						trHtml = trHtml + '<td>' +data.userDetail.contactNo+ '</td>';
   						
   						if(data.userDetail.dateOfBirth !=null){
   							trHtml = trHtml + '<td>' +data.userDetail.dateOfBirth+ '</td>';
   						}else{
   							trHtml = trHtml + '<td>-</td>';
   						}
   						
   						trHtml = trHtml + '<td>' +data.userDetail.created+'</td>';
//    						if(data.userDetail.idType !=null){
//    							trHtml = trHtml + '<td>' +data.userDetail.idType+ '</td>';
//    						}else{
//    							trHtml = trHtml + '<td>-</td>';
//    						}if(data.userDetail.idNo !=null){
//    							trHtml = trHtml + '<td>' +data.userDetail.idNo+ '</td>';
//    						}else{
//    							trHtml = trHtml + '<td>-</td>';
//    						}
   						trHtml = trHtml + '<td>' +data.mobileStatus+ '</td>';
   						trHtml = trHtml + '<td>' +data.accountDetail.accountType.name+ '</td>';
   						trHtml = trHtml + '<td>' +data.accountDetail.balance+ '</td>';
   						trHtml = trHtml + '</tr>';
   						$('#genericUserList').append(trHtml);
						}
					} else {
						trHtml = '<tr>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td>No data found</td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '</tr>';
						$('#genericUserList').append(trHtml);
						$('#paginationn').twbsPagination('destroy');
					}
	   			}
    		});
    	}
		
		
		function formatDate(d){
        	d=new Date(d);
        	var month = d.getMonth();
        	var day = d.getDate();
        	month = month + 1;
        	month = month + "";
        	if (month.length == 1)
        	{
        	month = "0" + month;
        	}
        	day = day + "";
        	if (day.length == 1)
        	{
        	day = "0" + day;
        	}
        	return d.getFullYear()+ '-' +month + '-' + day ;
       	}
		
		
		function getByUserName(){
			var trHtml='';
    		var data;
    		var username = $('#usernameId').val();
    		$('#paginationn').twbsPagination('destroy');
    		if(username!=null && username !=''){
        		$('#tableHeader0').css('display','block');
        		$.ajax({
        			type : "POST",
    	    		url:contextPath+"/Master/GenericUserListBasedOnRole",
    	    		dataType:"json",
    	   			contentType : "application/json",
    	   			data : JSON.stringify({
    	   				"username" : "" + username + "",
    	   			}),
    	   			success : function(response) {
    					$('#genericUserList').empty();
    					if (response !=null) {
    						data = response.details;
    						trHtml = '<tr title="click here" style="cursor: pointer;" <a href="#" onclick="openNav(\''+data.username+ '\',\''+data.userDetail.firstName+ '\',\''
							+data.userDetail.lastName+ '\',\''+data.userDetail.email+ '\',\''+data.accountDetail.accountType.name+ '\');"></> >';
       						trHtml = trHtml + '<td>' +(1)+ '</td>';
       						trHtml = trHtml + '<td>' +data.userHashId+ '</td>';
       						var gender='';
       						if(data.userDetail.gender !=null){
       							if(data.userDetail.gender == 'M'){
       								gender = "Male";
       								trHtml = trHtml + '<td>Mr.</td>';
       							}else{
       								trHtml = trHtml + '<td>Mrs</td>';
       								gender = "Female";
       							}
       						}else{
       							trHtml = trHtml + '<td>-</td>';
       							gender = "-";
       						}
       						trHtml = trHtml + '<td>'+data.userDetail.firstName+'</td>';
       						var name = '';
       						if(data.userDetail.lastName != null) {
       							trHtml = trHtml + '<td>'+data.userDetail.lastName+'</td>';
       						} else {
       							trHtml = trHtml + '<td>-</td>';
       						}
       						trHtml = trHtml + '<td>'+data.userDetail.email+'</td>';
       						trHtml = trHtml + '<td>' +data.userDetail.contactNo+ '</td>';
       						
       						if(data.userDetail.dateOfBirth !=null){
       							trHtml = trHtml + '<td>' +data.userDetail.dateOfBirth+ '</td>';
       						}else{
       							trHtml = trHtml + '<td>-</td>';
       						}
       						
       						trHtml = trHtml + '<td>' +data.userDetail.created+'</td>';
       						
       						
//        						if(data.userDetail.idType !=null){
//        							trHtml = trHtml + '<td>' +data.userDetail.idType+ '</td>';
//        						}else{
//        							trHtml = trHtml + '<td>-</td>';
//        						}if(data.userDetail.idNo !=null){
//        							trHtml = trHtml + '<td>' +data.userDetail.idNo+ '</td>';
//        						}else{
//        							trHtml = trHtml + '<td>-</td>';
//        						}
       						trHtml = trHtml + '<td>' +data.mobileStatus+ '</td>';
       						trHtml = trHtml + '<td>' +data.accountDetail.accountType.name+ '</td>';
	   						trHtml = trHtml + '<td>' +data.accountDetail.balance+ '</td>';
       						trHtml = trHtml + '</tr>';
       						$('#genericUserList').append(trHtml);
    						
    					} else {
    						trHtml = '<tr>';
    						trHtml = trHtml + '<td></td>';
    						trHtml = trHtml + '<td></td>';
    						trHtml = trHtml + '<td></td>';
    						trHtml = trHtml + '<td>No data found</td>';
    						trHtml = trHtml + '<td></td>';
    						trHtml = trHtml + '<td></td>';
    						trHtml = trHtml + '</tr>';
    						$('#genericUserList').append(trHtml);
    					}
    	   			}
        		});
    		}else{
    			$('#errUser').html("Please enter username")
    		}
		}
	</script>
	
	<script>
		function openNav(username,firstName,lastName,email,kycStatus) {
   		  
   		  $('#InfoModalID').modal('show');
			$('#fnameId').html(firstName);
				$('#lnameId').html(lastName);
				$('#emailId').html(email);
				$('#acctTypeId').html(kycStatus);
				
				$('#cardNoId').html('');
				$('#mobileNoId').html('');
				$('#cardHolderId').html('');
				$('#balId').html('');

				if (username != null && username != '') {
					$('#tableHeader0').css('display', 'block');
					$
							.ajax({
								type : "POST",
								url : contextPath + "/Master/WalletDetails",
								dataType : "json",
								contentType : "application/json",
								data : JSON.stringify({
									"username" : "" + username + "",
								}),
								success : function(response) {
									if (response != null) {
										if (response.balance != '0') {
											$('#balId').html("0.0 INR");
// 											$('#balId').html(
// 													response.balance + " INR");
										} else {
											$('#balId').html("0.00 INR");
										}if(response.cardNumber !=null && response.cardNumber!=''){
											$('#cardNoId').html(response.cardNumber);	
										}else{
											$('#cardNoId').html('NA');
										}if(response.cardHolder!=null && response.cardHolder!=''){
											$('#cardHolderId').html(response.cardHolder);
										}else{
											$('#cardHolderId').html('NA');
										}
										$('#mobileNoId').html(username);
										;
									}
								}
							});
				}
			}
		</script>
		<script>
		function downloadReport(){
			var daterange = $('#reportrange').val();
			$('#downloadReportId').val(daterange);
			if($('#downloadReportId').val() !=''){
				console.log($('#downloadReportId').val());
				$('#dformId').submit();
			}
		}
		
		</script>

</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<title>I-Money</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" crossorigin="anonymous">

	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

	<!-- header Section -->
	<nav class="navbar navbar-expand-lg navbar-light ">
	  	<div class="container">
	  		<a class="navbar-brand" href="#"><img src="${pageContext.request.contextPath}/resources/assets/images/logo.png" alt="Fingoole" style="width: 138px;"></a>
	  	</div>
	</nav>

	<!-- body Section -->
	<section>
		<div class="container">

			<div class="row">
				<div class="col-12">
					<div class="tab-content" id="pills-tabContent">
					  	<div class="tab-pane fade show active" id="safety_policy" role="tabpanel" aria-labelledby="pills-home-tab">
					  		<div class="fig_wrapper">
					  			<div class="fig_heading">
						  			<h5 class="title" style="text-transform: uppercase;">GENERAL TERMS & CONDITIONS</h5>
						  		</div>
						  		<div class="fig_body">
						  			<p class="text-justify">
						  				<b>Ishanvi IT Ecomm Private Limited</b> (hereinafter referred to as "<b>I-Money</b>") having its registered office at 351, Aggarwal Cyber Plaza 1, Netaji Subhash Place, New Delhi - 110034.
						  			</p>

						  			<p class="text-justify">
						  				The words Account/Wallet shall for the purpose of this agreement mean and include <b>I-Money</b> wallet or account held by the Customer for availing the services of <b>I-Money</b>;
						  			</p>

						  			<p class="text-justify">The words We/Us/Our shall for the purpose of this agreement mean and include <b>I-Money</b>.</p>

						  			<p class="text-justify">The words Customer/User/You/Applicant shall for the purpose of this agreement mean and include "Customer". The use of <b>I-Money</b> Account and Services by the Customer shall be subject to acceptance of the terms and conditions as detailed herein below ("<b>Terms and Conditions</b>").</p>

						  			<p class="text-justify">By applying for use of <b>I-Money</b> Wallet/Account, the Customer acknowledges that the Customer has read, understood and agrees to be bound by these Terms and Conditions defined hereunder and as may be amended from time to time by the Bank and /or <b>I-Money</b>.</p>

						  			<h5>1. DEFINITIONS</h5>

						  			<ul>
						  				<li class="text-justify"><strong>"Account Opening Form"/ "AOF"</strong> shall mean the form that <strong>I-Money</strong> may require to be filled by eligible Customer in writing for the purpose of "KYC" along with necessary supporting documents at Agent locations and/or such other locations as may be intimated by <strong>I-Money</strong> from time to time, for availing <strong>I-Money</strong> Services.</li>
						  				<!-- =========================================== -->
						  				<li class="text-justify"><strong>"Know Your Customer /KYC"</strong> shall mean the various norms, rules, laws, guidelines and statutes issued by RBI from time to time under which <strong><em>I-Money</em></strong> may be required to procure personal identification details along with supporting documents from the Customer. Such information or documents may be required at the time of submission of AOF and/or at a later date, for availing and/or continuation of the <strong><em>I-Money</em></strong> Wallet.</li>
						  				<li class="text-justify"><strong>"Agent/s"</strong> shall mean the retail agents appointed by <strong><em>I-Money</em></strong> to support the enrolment process of an Applicant and to facilitate the transactions of <strong><em>I-Money</em></strong> Wallet.</li>
						  				<li class="text-justify"><strong>"Applicant"</strong> shall mean an individual person above the age of 18 (eighteen) years, who wish to use <strong><em>I-Money</em></strong> Wallet.</li>
						  				<li class="text-justify"><strong>"Business Correspondent" or "BC"</strong> shall mean authorised business correspondent of the bank i.e. <strong><em>I-Money</em></strong> and wherever applicable, shall include its Agents.</li>
						  				<li class="text-justify"><strong>"Bill Payment"</strong> shall mean the transactions wherein the Customer uses <strong><em>I-Money</em></strong> to make payments towards utility bills, merchant payments and other such bill payments.</li>
						  				<li class="text-justify"><strong>"Bank"</strong> shall mean any Banking Institution, a company incorporated under the Companies Act, 1956 and a banking company within the meaning of Banking Regulations Act, 1949.</li>
						  				<li class="text-justify"><strong>"Charges"</strong> shall mean all charges pertaining to the Transactions and more particularly specified in clause 12 hereof.</li>
						  				<li class="text-justify"><strong>"Customer"</strong> shall mean an Applicant who is found to be eligible by the Bank or <strong><em>I-Money</em></strong> to use <strong><em>I-Money</em></strong> Wallet as per RBI regulations and has opened a mobile wallet Account. For the purpose of this agreement the words you/User/Beneficiary, shall have the same meaning as assigned to Customer.</li>
						  				<li class="text-justify"><strong>"Force Majeure Event"</strong> shall have the meaning ascribed to it in clause 21 hereof.</li>
						  				<li class="text-justify"><strong>"I-Money Wallet" or "Prepaid Payment Account"</strong> shall mean the semi closed pre-paid instrument issued by any Bank and which is co-branded with Ishanvi IT Ecomm Pvt Ltd in the form of virtual payment account comprising one reloadable pre-paid payment instrument which can be used on website or merchant site(s) (a) to make payment for goods and services purchased through merchants (b) for transfer of funds from one customer(s) pre-paid payment account to another pre-paid payment account or (c) for any other purposes as may be specified from time to time.</li>
						  				<li class="text-justify"><strong>"Merchant / Merchant Establishment"</strong> shall mean and include any outlet/ service provider who have been authorized by <strong><em>I-Money</em></strong> to accept payment for goods or services using <strong><em>I-Money</em></strong> Wallet.</li>
						  				<li class="text-justify"><strong>"Mobile Application(s)"</strong> shall mean the software application(s) which inter alia, enables <strong><em>I-Money</em></strong> Wallet account through the mobile-device.</li>
						  				<li class="text-justify"><strong>"Mobile PIN"</strong> shall mean a secret password that would enable the Customer to secure access and operate their Mobile Wallet/Account.</li>
						  				<li class="text-justify">"Mobile Wallet Account" shall mean and include <strong><em>I-Money</em></strong> Wallet or Prepaid Account.</li>
						  				<li class="text-justify"><strong>"Mobile Wallet Services" or "Services"</strong> shall mean use of Mobile Wallet and include prepaid mobile/DTH recharge; purchasing services, payment of bills, travel booking, payment for online and offline shopping and such other services which may be added from time to time (all such services are individually or collectively are referred as Service or Services as they case may be).</li>
						  				<li class="text-justify"><strong>"RBI"</strong> shall mean the Reserve Bank of India.</li>
						  				<li class="text-justify"><strong>"RBI Guidelines"</strong> shall mean the applicable guidelines, regulations, notifications and instructions issued by RBI in relation to the issuance and operation of pre-paid payment instruments in India and operation of <strong><em>I-Money</em></strong> Account and mobile wallet Services and all other guidelines and instructions inclusive of their respective amendments as may be issued and notified by RBI from time to time including The Payment and Settlement Systems Act, 2007 & regulations made there under.</li>
						  				<li class="text-justify"><strong>"Single-Sign-On Services"</strong> shall mean the services provided by <strong><em>I-Money</em></strong> to Customers through I-Money Wallet, or otherwise. For avoidance of doubt, the <strong><em>I-Money</em></strong> Balance is issued by any Bank but can be accessed through Single-Sign-On Services</li>
						  				<li class="text-justify">"Threshold Balance" shall mean such amounts as may be specified by <strong><em>I-Money</em></strong> from time to time, over and above which the Customer shall have the option to transfer money from the <strong><em>I-Money</em></strong> Wallet to the Mobile Money Account in terms of Clause 13.</li>
						  				<li class="text-justify"><strong>"Transaction/s"</strong> shall mean any credit or debit of money balances in the Customer's mobile wallet Account including but not limited to money transfer payments and receipts, payments for goods and services, utility payments, deposits and withdrawals.</li>
						  				<li class="text-justify"><strong>"Website"</strong> shall mean the website www.I-Moneypay.com and any other website as may be notified by <strong><em>I-Money</em></strong> which are owned, established and maintained by I-Money.</li>
						  			</ul>

						  			<h5>2. INTERPRETATION</h5>
						  			<ul>
						  				<li class="text-justify">All references to singular include plural and vice versa and the word "includes" should be construed as "without limitation".</li>
						  				<li class="text-justify">Words importing any gender include the other gender.</li>
						  				<li class="text-justify">Reference to the Terms and Conditions shall mean and include the Terms and Conditions of <strong><em>I-Money</em></strong> Wallet.</li>
						  				<li class="text-justify">Reference to any statute, ordinance or other law includes all regulations and other instruments and all consolidations, amendments, re-enactments or replacements for the time being in force.</li>
						  				<li class="text-justify">All headings, bold typing and italics (if any) have been inserted for convenience of reference only and do not define limit or affect the meaning or interpretation of these Terms.</li>
						  			</ul>
						  			<h5>3. REGISTRATION OF <em>I-Money</em> WALLET / PREPAID PAYMENT ACCOUNT</h5>
						  			<ul>
						  				<li class="text-justify"><strong><em>I-Money</em></strong> Wallet will be issued by the Bank and / or BC to Customer on the request of the Customer and pursuant to the Customer making an application for registration of <strong><em>I-Money</em></strong> Wallet and agreeing to terms and conditions as prescribed by the Bank and/ or BC in this regard, provided the Customer has fulfilled the eligibility and Know Your Customer ("KYC") criteria and provided all relevant information and/or documentation request by the Bank and /or BC.</li>
						  				<li class="text-justify">The Bank and / or BC reserves the right to reject any application made for issuing a Prepaid Payment Account without assigning any reason.</li>
						  				<li class="text-justify">In order to acquire, register, create and use a Prepaid Payment Account, the Bank and / or BC may require you to submit certain personal information, such as your name, mobile number, e-mail address, contact address, date of birth etc. You agree that the Data you provide to the Bank and / or BC upon registration and at all other times will be true, accurate, current and complete. You shall immediately inform the Bank or BC about change in Data along with such proof of change.</li>
						  				<li class="text-justify">You hereby authorize the Bank and / or BC to make any inquiries, directly or through third parties, that the Bank and / or BC may consider necessary to validate your identity and/or authenticate your identity and Prepaid Payment Account information. This may include asking you for further information and/or documentation about your account usage or identity, or requiring you to confirm identification by furnishing KYC documentation, ownership of your email address, telephone number or financial instruments, among others. This process is for internal verification purposes. You agree and understand that the Data as entered by you shall always be maintained by Bank`s authorized Business Correspondent(s).</li>
						  				<li class="text-justify">The collection, verification, audit and maintenance of correct and updated Customer information is a continuous process and the Bank and / or BC reserve the right, at any time, to take steps necessary to ensure compliance with all relevant and applicable KYC requirements.</li>
						  				<li class="text-justify">The Bank and / or BC reserves the right to discontinue the Services/ reject applications for Prepaid Payment Account Services at any time if there are discrepancies in information and/or documentation provided by You or if the information/documentation provided by you is found to be incorrect or wrong. In such an event, the Bank and /or BC reserve the right to forfeit the balance therein to the extent and in accordance with Applicable Laws.</li>
						  			</ul>

						  			<h5>TRASANCTION LIMITS UNDER PREPAID PAYMENT ACCOUNT SERVICES</h5>

						  			<ul>
						  				<li class="text-justify">The amount that can be transacted in the Prepaid Payment Account is governed by Applicable Laws including rules, regulations and guidelines laid down by RBI which include monthly limits, transaction limits and balance limits on the Prepaid Payment Account. Subject to any change in Applicable Law including guidelines/notifications issued by RBI from time to time the limitations stated hereunder may be reviewed and modified at the discretion of the Bank without prior intimation to the Customer:</li>
						  				<p class="text-justify">Following limits have been permitted by RBI for any Prepaid Payment Account:</p>
						  				<ul>
						  					<li class="text-justify">Limited KYC Customer - The maximum monetary value that can be stored at any point of time in a single card is INR 10,000/- (Rupees Ten Thousand only). The limit on per user card issue will be as per RBI laid guidelines.</li>
							  				<li class="text-justify">Full KYC Customer (Reloadable Card/Wallet) - The balance in the Prepaid Payment Account should not exceed INR 1,00,000/- (Rupees one lac only) at any point of time. Maximum transaction value is limited to INR 1,00,000/- limit during the month and maximum reloadable balance in a month not more than INR 5,00,000/-. There may be change from time to time and customer to customer as per discretion of the Bank and / or BC.</li>
							  				<li class="text-justify">The maximum monetary value that can be transferred from any Prepaid Payment Account to any Bank account in a single Transaction is INR 25,000/- (Rupees Twenty Five Thousand only). The maximum monetary value that can be transferred from one Prepaid Payment Account to any Bank account in a single month is INR 25,000/- (Rupees Twenty Five Thousand only).</li>
						  				</ul>
						  				<li class="text-justify">Prepaid Payment Account is valid for purchase of Products and Transactions throughout India in Indian Rupees only.</li>
						  				<li class="text-justify">Prepaid Payment Account or Services there under is not transferable unless required by operation of law.</li>
						  				<li class="text-justify">The Customer shall be able to use the Prepaid Payment Account only to the extent of the amount loaded onto the Prepaid Payment Account.</li>
						  				<li class="text-justify">The Prepaid Payment Account shall be activated subject to such minimum amount being loaded on the Prepaid Payment Account as may be specified by the Bank and / or BC from time to time.</li>
						  				<li class="text-justify">The Customer or any other person permitted to load the Prepaid Payment Account may credit the Prepaid Payment Account through any of the methods prescribed from time to time.</li>
						  				<li class="text-justify">The Customer may be able to carry out all or any kind of Transactions, as may be available from time to time</li>
						  				<li class="text-justify">Cash withdrawal is permissible from the Prepaid Payment Account subject to terms and conditions as may be defined by the Bank and / or BC.</li>
						  				<li class="text-justify">No interest will be payable by the Bank and / or BC to Customers on the available balance reflected on the <strong><em>I-Money</em></strong> Wallet.</li>
						  				<li class="text-justify">The Customer is permitted to maintain and operate only one Prepaid Payment Account. Any suspected non-conformity with this requirement shall be just cause for the suspension/ discontinuation of any/all Prepaid Payment Accounts associated with the Customer.</li>
						  				<li class="text-justify">The Bank and / or BC may further as per its discretion introduce appropriate controls over the usage of the Prepaid Payment Account.</li>

						  			</ul>

						  			<h5>5. Wallet Charges & Validity</h5>

						  			<ul>
						  				<li class="text-justify">You shall pay the Service Charges prescribed by I-Money in the form and manner prescribed for such payment. <strong><em>I-Money</em></strong> may at its discretion, change, amend, increase, or reduce the Service Charges without prior intimation to the Customer.</li>
						  				<li class="text-justify">Any value in your <strong><em>I-Money</em></strong> Wallet that is utilized towards making payments for any Transaction shall be automatically debited from your <strong><em>I-Money</em></strong> Wallet. <strong><em>I-Money</em></strong> responsibility is limited to the debiting of your <strong><em>I-Money</em></strong> Wallet and the subsequent payment to any Merchant Establishment that you might transact with. I-Money does not endorse, promote or warrant any goods and/or services that might be bought/availed or proposed to be bought/availed using <strong><em>I-Money</em></strong> Wallet.</li>
						  				<li class="text-justify"><strong><em>I-Money</em></strong> reserves the right to levy charges/ charge commission upon any amounts loaded to <strong><em>I-Money</em></strong> Wallet or any amounts spent/utilized by you using <strong><em>I-Money</em></strong> Wallet.</li>
						  			</ul>

						  			<h5>6. TERMS OF USAGE</h5>
						  			<ul>
						  				<li class="text-justify">The Customer shall at all times ensure that the Prepaid Payment Account credentials are kept safe.</li>
						  				<li class="text-justify">The Customer will be responsible for the security of the Prepaid Payment Account and shall take all steps towards ensuring the safekeeping thereof. The Customer shall not disclose his/her/its password to anyone verbally or in writing nor record it elsewhere. The Customer shall ensure the safety and confidentiality of its login id and password and shall keep I-MONEY indemnified in case of misuse of the same.</li>
						  				<li class="text-justify">The Customer will be liable for all charges incurred on the Prepaid Payment Account until the Prepaid Payment Account is reported for closure. You shall, immediately intimate the Bank and / or BC of the occurrence of any fraud, hacking or unauthorised use and the Bank and / or BC may, after due investigations, suspend or terminate the Prepaid Payment Account. However the Bank and / or BC shall not be liable for any such unauthorised usage or access of the Prepaid Payment Account and it shall be solely Customers responsibility to ensure privacy and confidentiality of Prepaid Payment Account details.</li>
						  				<li class="text-justify">On creation of Prepaid Payment Account, You will have the opportunity to use various interactive aspects through which you can communicate with us and share information. Do not use inappropriate language; make gratuitous personal criticisms or comments.</li>
						  				<li class="text-justify">You agree and acknowledge that you are solely responsible for any Information that you submit on the Website or Prepaid Payment Account or transmit to our Team and/or other users of the Website / Prepaid Payment Account.</li>
						  				<li class="text-justify">You agree and acknowledge that you will not post, distribute, or reproduce in any way any copyrighted material, trademarks, or other proprietary information without obtaining prior written consent of the owner of such proprietary rights and will not submit any content or material that infringes, misappropriates or violates the intellectual property, publicity, privacy or other rights of any party.</li>
						  				<li class="text-justify">You agree that you will not provide / post any Information that falsely expresses or implies that such content or material is sponsored or endorsed by the Bank and / or BC.</li>
						  				<li class="text-justify">You agree that you will not provide / post any information that is unlawful or that promotes or encourages illegal activity.</li>
						  				<li class="text-justify">You understand and agree that the Bank and / or BC may (but is not obligated to) review and delete any posted Information that in the sole discretion of the Bank and / or BC violates these Terms or which might be offensive, illegal, or violate the rights of, harm, or threaten the safety of other users of the Prepaid Payment Account or Website and/or any other person.</li>
						  				<li class="text-justify">You acknowledge and agree that the Bank and / or BC do not and shall not be required to actively monitor nor exercise any editorial control whatsoever over the content of any message or other material or information created, obtained or accessible through the Services. The Bank and / or BC do not endorse, verify or otherwise certify the contents of any comments or other material or information made by you. You are solely responsible for the contents of your communications and may be held legally liable or accountable for the content of your comments or other material or information.</li>
						  				<li class="text-justify">You agree that you will only provide / post information that you believe to be true and you will not purposely provide false or misleading information.</li>
						  				<li class="text-justify">The following is a list of the kind of content and communications that are illegal or prohibited on/through the Website or Prepaid Payment Account. The Bank and / or BC reserve the right to investigate and take appropriate legal action in its sole discretion against anyone who violates this provision, including without limitation, removing the offending communication from the Services and terminating the membership of such violators or blocking your use of the Services, Prepaid Payment Account and/or the Website. You may not post content that:</li>
						  				<ul>
						  					<li class="text-justify">belongs to another person and to which You do not have any right to;</li>
							  				<li class="text-justify">is grossly harmful, harassing, blasphemous defamatory, obscene, pornographic, pedophilic, libelous, invasive of another's privacy, hateful, or racially, ethnically objectionable, disparaging, relating or encouraging money laundering or gambling, or otherwise unlawful in any manner whatever;</li>
							  				<li class="text-justify">harm minors in any way; infringes any patent, trademark, copyright or other proprietary rights;</li>
							  				<li class="text-justify">violates any law for the time being in force;</li>
							  				<li class="text-justify">deceives or misleads the addressee about the origin of such messages or communicates any information which is grossly offensive or menacing in nature;</li>
							  				<li class="text-justify">impersonates another person; contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer resource;</li>
							  				<li class="text-justify">threatens the unity, integrity, defence, security or sovereignty of India, friendly relations with foreign states, or public order or causes incitement to the commission of any cognizable offence or prevents investigation of any offence or is insulting any other nation.</li>
						  				</ul>
						  				<li class="text-justify">You agree that you will notify the Bank and / or BC at <strong>cs@I-Moneypay.com</strong> upon coming across any objectionable content on the Prepaid Payment Account or Website and the Bank and / or BC shall use best efforts to remove such objectionable content within the time period prescribed under Applicable Law.</li>
						  				<li class="text-justify">The Customer shall inform the Bank and / or BC in writing at <strong>cs@I-Moneypay.com</strong> within seven (7) days, if any discrepancies exist in the Transactions/ particulars of the Prepaid Payment Account on any statement / records that is made available to the Customer. If the Bank and / or BC do not receive any information to the contrary within seven (7) days, the statement and the transactions shall be deemed to be correct and unconditionally and irrevocably binding on you. All records of your instructions and such other details (including but not limited to payments made or received) maintained by the Bank and / or BC, in electronic or documentary form pursuant to the Terms and Conditions herein, shall be deemed to be conclusive evidence of such instructions and such other details. In case of any dispute relating to the time of reporting and/ or Transaction/s made on the Prepaid Payment Account or Website or any other matter in relation to the said Prepaid Payment Account, the Bank and / or BC shall reserve the right to ascertain the time and/ or the authenticity of the disputed Transaction.</li>
						  				<li class="text-justify">You must ensure the availability of sufficient funds before executing any Transaction from the Prepaid Payment Account.</li>
						  				<li class="text-justify">You agree to adhere to all Applicable Laws and all such regulations, guidelines and rules prescribed from time to time by the Bank, RBI and any other regulatory body.</li>
						  				<li class="text-justify">You hereby agree and acknowledge that the Prepaid Payment Account is issued, loaded, withdrawn, terminated, closed down, suspended by the Bank and / or BC only.</li>
						  				<li class="text-justify">You agree that you will not use the Prepaid Payment Account for payment of any illegal/unlawful purchases/purposes.</li>
						  				<li class="text-justify">You shall be bound to comply with the terms and conditions and all the policies stipulated by the Bank and / or BC from time to time.</li>
						  				<li class="text-justify">You shall not use the Services, Prepaid Payment Account or Website for any purpose that might be construed as contrary or repugnant to any Applicable Laws, regulations, guidelines, judicial dicta, the Banks policies or public policy or for any purpose that might negatively prejudice the goodwill of the Bank and / or BC.</li>
						  				<li class="text-justify">You agree and accept full responsibility for any wrongful use of the Prepaid Payment Account which is in contravention of these terms and conditions. You shall indemnify the Bank and BC and their respective director, officers, personnel, contractors and agents, to make good any loss, damage, penalties, claims, demand, interest or any other financial charges (including lawyer fees) that the aforesaid persons may incur and or suffer whether directly or indirectly as a result of your committing violations of these terms and conditions.</li>
						  				<li class="text-justify">The Bank and / or BC reserve the absolute discretion and liberty to decline or honor the authorization request on the Prepaid Payment Account without assigning any reason thereto.</li>
						  				<li class="text-justify">The Customer acknowledges and understands that the Services are linked to internet connection (and in case of mobile, mobile phone connection) and the Bank and / or BC shall not be responsible and you will be solely responsible for all liability arising from including but not limited to any loss or interruption of the Services or the unavailability of Services due to a mobile or internet not supporting Prepaid Payment Account or Website or Merchant site(s).</li>
						  				<li class="text-justify">The Customer acknowledge that the information submitted by the Customer for availing the Services or information submitted while using the Services may be shared with third parties inter alia, to facilitate the provision of the Services.</li>
						  				<li class="text-justify">The Bank and / or BC may request the Customer to submit additional KYC information/documents as part of ongoing monitoring and due diligence.</li>
						  				<li class="text-justify">The Bank and / or BC may at their sole discretion, utilize the services of external service providers/or agents and on such terms as required or necessary, in relation to <strong><em>I-Money</em></strong> Wallet and/or Services.</li>
						  				<li class="text-justify">Merchant or Merchant Establishment may, at its sole discretion, make several promotional offers such as issuance of vouchers, announcing discounts,etc. You expressly agree and acknowledge that I-MONEY shall not be responsible or liable in any manner in respect of any such promotional or other offers made by Merchant or Merchant Establishment. I-MONEY disclaims all liability arising out of any such offer issued by Merchant or Merchant Establishment and availed by the User.</li>
						  			</ul>
						  			

						  			<h5>7. TRANSACTION AND LOADING OF PREPAID PAYMENT ACCOUNT</h5>
						  			<ul>
						  				<li class="text-justify">You can use multiple funding sources for loading money in the Prepaid Payment Account. These sources could be but not limited to Cash, Credit Cards, Debit Cards, Net Banking, and transfer from another Prepaid Payment Account.</li>
						  				<li class="text-justify">The Prepaid Payment Account may also be loaded by transfer of refund money of Transactions carried out using services of Merchant(s) or such other manner as is acceptable to the Bank. The Prepaid Payment Account may also be loaded by marketing promotions / schemes / incentives such as cash back, gifts, etc. by BC, Merchant or third parties or such other manner as is acceptable to the Bank and / or BC.</li>
						  				<li class="text-justify">The Bank and / or BC may impose charges/ fees etc., payable by the Customer for availing the said Prepaid Payment Account and funds shall be loaded on the Prepaid Payment Account after deduction of the applicable charges/ fees etc.</li>
						  				<li class="text-justify">In order to manage risk, the Bank and / or BC may limit the funding sources available for your use to fund any particular Transaction.</li>
						  				<li class="text-justify">The Bank and / or BC may monitor each Transaction made into Your Prepaid Payment Account to monitor high-risk & fraudulent transactions. If Your Transaction is classified as a high-risk Transaction or is suspected of fraud, the Bank and / or BC will place a hold on the Transaction and may ask you for more information on you and your funding source. The Bank and / or BC will conduct a review and accordingly the Bank and / or BC will either clear or cancel the Transaction, as per Applicable Law. If the Transaction is cleared, Bank and / or BC will notify you and update Your Prepaid Payment Account. Otherwise, the Bank and / or BC will cancel the Transaction and the funds may be forfeited. The said funds will be refundable only to source account upon valid demand raised by holder of source account. The Bank and / or BC will notify you by email and/or in the account history tab of Your Prepaid Payment Account if the Transaction is cancelled.</li>
						  				<li class="text-justify">When you load the Prepaid Payment Account, you are liable to the Bank and / or BC for the full amount of the load plus any fees, if the load is later invalidated for any reason including but not limited to chargeback, reversal of Transaction, dispute by the owner of funding source of a Transaction, you agree to allow the Bank and / or BC to recover any amounts due to the Bank and / or BC by debiting your Prepaid Payment Account. If there are insufficient funds in your Prepaid Payment Account to cover your liability, you agree to reimburse the Bank and / or BC through other means. If the Bank and / or BC are unable to recover the funds from your primary funding source, the Bank and / or BC may attempt to contact you and/or recover the funds from your alternate funding sources, or may at their discretion, can take appropriate legal actions to collect the amount due, to the extent allowed by Applicable Law. In addition, the Bank and / or BC reserve the right to suspend or terminate Your Prepaid Payment Account.</li>
						  			</ul>

						  			<h5>8. PAYMENT THROUGH PREPAID PAYMENT ACCOUNT</h5>
						  			<p class="text-justify">The Customer may also partially pay for the Products offered by the Merchant from its Prepaid Payment Account and pay the remaining amount using other payment mechanism such as debit card, credit card, net banking, etc.</p>

						  			<h5>9. TRANSACTIONS THROUGH PREPAID PAYMENT ACCOUNT</h5>

						  			<ul>
						  				<li class="text-justify">Customer can choose to withdraw the funds available in his Prepaid Payment Account to any bank account by way of IMPS/NEFT in accordance with guidelines of RBI. However, the Bank and / or BC may deny such withdrawal in any of the events as mentioned in terms and conditions.</li>
						  				<li class="text-justify">The Bank and / or BC reserve the right to delay withdrawals while screening for risk, or request you provide additional information to verify your identity and may limit the amount you can withdraw until the information is verified.</li>
						  				<li class="text-justify">Any withdrawal found to be suspicious will be held back and reversed into Prepaid Payment Account. The Prepaid Payment Account will also be suspended for operations and no Transactions will be possible pending an investigation. A notification will be given to you if you are the subject of an investigation. If you are able to provide a justification for the withdrawal to the satisfaction of the Bank and / or BC, your account will be removed from suspension and you would be free to transact using Your Prepaid Payment Account.</li>
						  				<li class="text-justify">In the case of no information being provided by you for a suspended Prepaid Payment Account, the Prepaid Payment Account will continue to be suspended till its validity and the amount will be forfeited in compliance with guidelines of RBI.</li>
						  			</ul>

						  			<h5>10. DATA USAGE</h5>

						  			<ul>
						  				<li class="text-justify">Except for information's that you provide /submit, all of the information available on or through Prepaid Payment Account, Services and/or the Website, including without limitation, text, photographs, graphics and video and audio content, is owned by us and our licensors and is protected by copyright, trademark, patent and trade secret laws, other proprietary rights and international treaties. You acknowledge that the Services and any underlying technology or software used in connection with the Services contain the <strong><em>I-Money</em></strong> proprietary information. We give you permission to use the aforementioned content for personal, non-commercial purposes only and do not transfer any intellectual property rights to you by virtue of permitting your use of the Services. You may print, download, and store information from the Website for your own convenience, but you may not copy, distribute, republish, sell, or exploit any of the content, or exploit the Website in whole or in part, for any commercial gain or purpose whatsoever. Except as is expressly and unambiguously provided herein, we do not grant you any express or implied rights.</li>
						  				<li class="text-justify">You agree that the terms and conditions mentioned on Merchant(s) sites are independent of these terms and conditions mentioned herein.</li>
						  			</ul>

						  			<h5>11. GENERAL CONDITIONS</h5>

						  			<ul>
						  				<li class="text-justify"><strong><em>I-Money</em></strong> Services can only be availed by a person who has attained the age of 18 (eighteen) years and is competent to contract.</li>
						  				<li class="text-justify">To avail Prepaid Payment Account Services from any <strong><em>I-Money</em></strong> Agent location, the Customers are required to approach the authorized retail agents appointed by <strong><em>I-Money</em></strong>.</li>
						  				<li class="text-justify">Customer should ensure the receipt of notification of confirmation of all Transactions from <strong><em>I-Money</em></strong> conducted at Agent locations.</li>
						  				<li class="text-justify">The Customer shall immediately call the customer call centre on the number listed on the Website and register a complaint and shall subsequently also register such complaint in writing with the Agent in the event of any loss, and/or theft in/ of the mobile phone connection with the I-MONEY Wallet, failing which the Bank and /or BC will not be liable for any unauthorized Transactions on <strong><em>I-Money</em></strong> Account of the Customer.</li>
						  				<li class="text-justify">Customer shall promptly inform the Bank and/ or BC about any changes in his/her permanent or communication address and provide the supporting document(s) that the Bank and / or BC may require from time to time.</li>
						  				<li class="text-justify">Customer shall not assign or transfer <strong><em>I-Money</em></strong> Account or the Services or otherwise grants to any third party a legal or equitable interest over it.</li>
						  				<li class="text-justify">Customer grants express authority to the Bank and / or BC for carrying out Transactions and instructions authenticated by providing the Mobile PIN.</li>
						  				<li class="text-justify">Customer shall be the sole and exclusive owner of the Mobile PIN and the Customer accepts sole responsibility for use, confidentiality and protection of the customer's Mobile PIN. The Customer shall not disclose the Mobile PIN to any other person and shall not respond to any unauthorized SMS/ e-mail/ phone call in which the Mobile PIN is asked for. The Bank and / or BC shall, in no manner whatsoever, be held responsible or liable, if the Customer incurs any loss as a result of the Mobile PIN being disclosed/ shared by the Customer with any third person or in any other manner whereby the security of the Mobile PIN is compromised.</li>
						  				<li class="text-justify">Customer must ensure the availability of sufficient funds including service charges thereon, if any in <strong><em>I-Money</em></strong> Account before initiating any Transaction.</li>
						  				<li class="text-justify">Customer acknowledges that any information provided to the Bank and / or BC with the intention of securing I-Money Account shall vest with <strong><em>I-Money</em></strong> and may be used by the Bank and/ or BC at its discretion, for any purpose consistent with any applicable law or regulation and privacy policy and/or statement displayed on its website.</li>
						  				<li class="text-justify">Any information submitted by the Customer while availing the <strong><em>I-Money</em></strong> Wallet may be shared with third parties by the Bank and / or BC to facilitate the provision of <strong><em>I-Money</em></strong> Services and any other additional services.</li>
						  				<li class="text-justify">The Customer shall not use <strong><em>I-Money</em></strong> Services for any purpose that might be construed as contrary or repugnant to any applicable law, public policy or for any purpose that is contrary to the Bank and / or BC policy or might prejudice the goodwill of the Bank and / or BC.</li>
						  				<li class="text-justify">The Customer confirms that he/she holds only one active I-Money account and does not hold multiple active <strong><em>I-Money</em></strong> Account in violation of the applicable laws and regulations.</li>
						  			</ul>

						  			<h5>12. RESERVATIONS</h5>

						  			<ul>
						  				<li class="text-justify">The Bank and / or BC reserve the right to discontinue the Services and/or reject the AOF at any time at their sole discretion, if there are discrepancies in the information provided by the Customer for the purpose of KYC. In such cases, the Bank and / or BC reserve the right to take appropriate action as they may deem fit and proper without incurring any liability in any manner whatsoever.</li>
						  				<li class="text-justify">The Bank and / or BC reserve the right to suspend and/or discontinue <strong><em>I-Money</em></strong> Services at any time, without giving prior intimation to the Customer, for any one or all of the following reasons, including but not limited to: </li>
						  				<ul>
						  					<li class="text-justify">For any suspected violation of any rules, regulations, orders, directions, notifications issued by RBI from time to time or for any violation of these Terms and Conditions.</li>
							  				<li class="text-justify">For any discrepancy or suspected discrepancy in the particular(s) or documentation or AOF provided by the Customer.</li>
							  				<li class="text-justify">To combat potential fraud, sabotage, wilful destruction, threat to national security or for any other force majeure reasons (more particularly detailed in clause 12 herein below) etc.</li>
							  				<li class="text-justify">In order to comply with any applicable laws and regulations. </li>
							  				<li class="text-justify">For any technical failure, modification, upgradation, variation, relocation, repair, and/or maintenance due to any emergency or for any other technical reasons.</li>
							  				<li class="text-justify">For any transmission deficiencies caused by topographical and geographical constraints/limitations.</li>
							  				<li class="text-justify">On account of ineligibility of the Customer under any criteria as mandated by the Bank and / or BC. However prior to such suspension/ discontinuance of the Prepaid Payment Account Services, the Customer, at the sole discretion of the Bank and / or BC, may be provided with the ability to transfer and/ or utilize the balances lying in  <strong><em>I-Money</em></strong> Account.</li>
						  				</ul>
						  				<li class="text-justify">In the event of occurrence of Transactions that may be construed as dubious or undesirable, the Bank and / or BC reserve the right to freeze operations in such I-Money Accounts and /or close <strong><em>I-Money</em></strong> Account including reporting to authorities as may be required as per applicable regulations and as may be deemed fit and proper.</li>
						  				<li class="text-justify">In case of network failure or for any other reason beyond the control of the Bank and / or BC, there could be delay or failure to complete the Transaction. The Bank and / or BC reserve the right to cancel the Transaction in case of any network failure. The Bank / BC shall not be responsible for any kind of losses that may occur due to such delay of failure to complete Transactions.</li>
						  				<li class="text-justify">The Bank and / or BC reserve(s) the right to reject the AOF and any documents of the Customer without providing any reason. The Bank and / or BC reserve the right to retain such AOF and documents and photographs submitted along with it.</li>
						  			</ul>

						  			<h5>13. CHARGES</h5>

						  			<ul>
						  				<li class="text-justify">Usage and operation of Prepaid Payment Account is subject to payment of service charges prescribed by the Bank and / or BC and as set out on the Website from time to time. Charges shall be exclusive of all taxes as applicable which will be charged extra.</li>
						  				<li class="text-justify">The Bank and / or BC have the right to levy Charges including but not limited to, charges on Transaction, periodic maintenance, Services etc. in accordance with applicable laws.</li>
						  				<li class="text-justify">Customer unconditionally and irrevocably authorizes the Bank and / or BC to debit his / her Prepaid Payment Account from time to time with applicable transaction charges and fees plus taxes for the issue and use of Prepaid Payment Account as and when required by the Bank and / or BC.</li>
						  			</ul>

						  			<h5>14. SUSPENSION OR DISCONTINUANCE OF SERVICES</h5>
						  			<p class="text-justify">The Bank and / or BC reserve the right, without prior notice and at its sole discretion, to suspend, restrict, discontinue or deny access to or Your use of Services provided by the Bank and / or BC :</p>
						  			<ul>
						  				<li class="text-justify">In case any of the documents furnished towards identity, address proof and other KYC requirements are found to be fake / forged / defective</li>
						  				<li class="text-justify">In case customer is not eligible to maintain or hold a Prepaid Payment Account as per Indian Laws.</li>
						  				<li class="text-justify">In case of unsatisfactory conduct of the Prepaid Payment Account</li>
						  				<li class="text-justify">In case of no transaction has been done on the Prepaid Payment Account by the Customer after expiry of six months or such other period as specified by the Bank and / or BC.</li>
						  				<li class="text-justify">If you use or the Prepaid Payment Account is used or suspected to be used to defraud any person or entity</li>
						  				<li class="text-justify">If you use the Prepaid Payment Account to engage in any unlawful activities including without limitation those which would constitute the infringement of intellectual property rights, a civil liability or a criminal offence;</li>
						  				<li class="text-justify">If you engage in any activities that would otherwise create any liability for the Bank or for any of its contractors or agents;</li>
						  				<li class="text-justify">For any suspected discrepancy in the particular(s), online application, documentation provided by the Customer;</li>
						  				<li class="text-justify">Any Force Majeure reasons;</li>
						  				<li class="text-justify">If the same is due to technical failure, modification, upgradation, variation, relocation, repair, and/or maintenance due to any emergency or for any technical reasons;</li>
						  				<li class="text-justify">If the same is due to technical failure, modification, upgradation, variation, relocation, repair, and/or maintenance due to any emergency or for any technical reasons;</li>
						  				<li class="text-justify">If the Bank and / or BC believe, in its reasonable opinion, that cessation/ suspension is necessary.</li>
						  				<p class="text-justify">All notices to be issued to the Customers under this Clause shall be deemed to have been sent and received on the contact information provided by the customer to the Bank and / or BC.</p>
						  			</ul>

						  			<h5>15. INDEMINITY</h5>

						  			<ul>
						  				<li class="text-justify">The Customer shall be liable to the Bank and / or BC for losses, expenses or damages and agree to indemnify, defend and hold harmless the Bank and / or BC and /or Agents harmless from any and all claims, losses, damages, liabilities, costs and expenses, including and without limitation legal fees and expenses arising out of or related to use or misuse of Prepaid Payment Account Services or website, violation of any of the terms and conditions or any breach of any representations, warranties and covenants made by the Customer under this Agreement.</li>
						  				<li class="text-justify">The Customer shall indemnify the Bank and / or BC against any fraud or any loss or damage suffered by the Bank and / or BC due to the failure on the part of the Customer to communicate correct permanent or communication address and/or failure on the part of the Customer to communicate any change/alteration in the said permanent or communication address.</li>
						  			</ul>

						  			<h5>16. TERMINATION</h5>

						  			<ul>
						  				<li class="text-justify">The Customer may request for closure of <strong><em>I-Money</em></strong> Account and Services thereof any time by giving a written notice at <em>cs@I-Moneypay.com</em> at least 15 (fifteen) working days to the Bank and / or BC. The termination shall take effect on the completion of the fifteenth day. The user will remain responsible for any Transactions made through <strong><em>I-Money</em></strong> Account until the time of such termination.</li>
						  				<li class="text-justify">Termination will be effective subject to payment of all amounts outstanding on the Prepaid Payment Account. No fees charged, if any, to Customer shall be refunded in the event of termination, suspension or discontinuance of Services.</li>
						  				<li class="text-justify">The Bank and / or BC may also restrict, terminate or suspend the use of Prepaid Payment Account at any time without prior notice if the Bank and / or BC reasonably believe it necessary for business or security reasons. The Prepaid Payment Account must not be used after these Terms and Conditions end or while use of Prepaid Payment Account is suspended.</li>
						  				<li class="text-justify">The Bank and / or BC shall, upon adequate verification, block/suspend/close the Prepaid Payment Account and terminate all facilities in relation thereto following the receipt of such intimation and shall not be liable for any inconvenience caused to the Customer in this regard.</li>
						  				<li class="text-justify">There may be expiry date for the Prepaid Payment Account as may be decided by the Bank and / or BC in accordance with the guidelines of RBI.</li>
						  				<li class="text-justify">Any value in your Prepaid Payment Account to be utilized in the following manner:</li>
						  				<ul>
							  				<li class="text-justify">Within 6 months from the date of your last Transaction or</li>
							  				<li class="text-justify">Within 6 months from the date of activation; whichever is later.</li>
							  				<li class="text-justify">Any value in Prepaid Payment Account which is not utilized or withdrawn in the aforesaid manner may stand forfeited at the discretion of the Bank and / or BC. The Bank and / or BC will send thirty (30) days advance communication to Customer before any forfeiture of outstanding amount in the Prepaid Payment Account by SMS at the mobile number and/or by email at email id which is provided by Customer for use of the Services. It is the responsibility of the Customer to ensure that the information provided by the Customer including the email id and the mobile number is updated at all times.</li>
							  			</ul>
						  			</ul>

						  			<h5>17. ADDITIONAL TERMS</h5>

						  			<ul>
						  				<li class="text-justify">The Bank and / or BC make no express or implied warranty, guarantee, representation or undertaking whatsoever regarding the services, which are not expressly mentioned herein.</li>
						  				<li class="text-justify">The Bank and / or BC shall not be responsible for any acts or omissions of any third party including distributors/retailers/Merchants etc. with regard to services which are not expressly authorized by The Bank and / or BC.</li>
						  				<li class="text-justify">The Bank and / or BC shall not be liable to the Customer or any other person for any incorrect information provided by the Customer to BC pertaining to the Prepaid Payment Account Services, any delays, loss of business, profit, revenue or goodwill, anticipated savings, damages, fees costs, expense, etc. or for any indirect or consequential loss, howsoever arising, on account of unavailability/usage of Prepaid Payment Account Services or otherwise.</li>
						  				<li class="text-justify">The Bank and / or BC shall not be responsible in any way for the products or for any site from any Merchant Establishment from which they are purchased, or for any charges, taxes or other duties relating to the Transactions. The Merchant Establishments are solely responsible for all information in relation to the products, for the products themselves and their supply and sale to the Customer.</li>
						  				<li class="text-justify">Any dispute with or complaint against any Merchant Establishment must be directly resolved by the Customer with the Merchant Establishment. It is clarified that The Bank and / or BC shall not be responsible or liable for any deficiency in goods and/or services purchased using the <strong><em>I-Money</em></strong> Wallet. This exclusion of liability shall apply even for goods and/or services made available by <strong><em>I-Money</em></strong> under promotional schemes. Customer is instructed to satisfy itself regarding the quality, quantity and fitness of any good and/or service before purchasing the same.</li>
						  			</ul>

						  			<h5>18. LIMITAION OF LAIBILITY</h5>
						  			<strong>GIFT VOUCHERS</strong>
						  			<ul>
						  				<li class="text-justify"><strong><em>I-Money</em></strong> is acting an agent for sale of gift vouchers of various merchant(s). Gift vouchers are available for sale in digital form only.</li>
						  				<li class="text-justify"><strong><em>I-Money</em></strong> also sales its own gift vouchers (known as <strong><em>I-Money</em></strong> Gift Cards)</li>
						  				<li class="text-justify">Customer agrees and understands while purchasing gift voucher of any particular merchant(s) that terms and conditions of that merchant(s) shall be applicable with respect of those gift vouchers.Customer agrees and understands that validity of <strong><em>I-Money</em></strong> Gift Card has limited validity only. In case the same remain unutilised after expiry of validity period, I-Money reserve the right to forfeit the same without any information to customer. <strong><em>I-Money</em></strong> reserve the right to change the validity period of <strong><em>I-Money</em></strong> Gift Cards.</li>
						  			</ul>

						  			<h5>19. CHANGE OF TERMS</h5>

						  			<ul>
						  				<li class="text-justify">The Bank and / or BC reserve the right, at its sole discretion to alter, modify or amend these Terms and Conditions from time to time and the same shall be updated and displayed on Website or Prepaid Payment Account.</li>
						  				<li class="text-justify">The Bank and / or BC may modify, terminate and/or suspend Prepaid Payment Account Services anytime with or without prior notice, due to any changes in internal policies, rules, regulations and laws set by relevant authorities and/or regulators.</li>
						  			</ul>

						  			<h5>20. SEVERABILITY</h5>

						  			<p class="text-justify">If any part of these Terms and Conditions are adjudged illegal or inoperable for any reason, the same shall be severed from the remainder of this document and only that portion of this document that is specifically adjudged illegal or inoperable shall cease to govern the relationship between the Bank / BC and the Customer.</p>

						  			<h5>21. OWNERSHIP AND PROPRIETARY RIGHTS</h5>

						  			<p class="text-justify">The Customer agrees that he/she shall have no claims/rights of whatsoever nature in the intellectual property rights arising out of and in connection with <strong><em>I-Money</em></strong> Wallet and Services thereto. The Customer further undertakes that he/she shall not attempt to modify, alter, obscure, translate, disassemble, decompile or reverse engineer the software underlying mobile banking or create any derivative product based on the software.</p>

						  			<h5>22. FORCE MAJEURE</h5>

						  			<ul>
						  				<li class="text-justify">The Bank and / or BC shall inform the Customer of the existence of a Force Majeure Event due to which the Bank and / or BC may be unable to provide the services in full or in part. "Force Majeure Event" under this agreement shall mean any event due to any cause beyond the reasonable control of the Bank and / or BC, including, without limitation, unavailability of any communication system, breach or virus in the processes or payment mechanism, sabotage, fire, flood, explosion, acts of God, civil commotion, strikes or industrial action of any kind, riots, insurrection, war, acts of government, computer hacking, unauthorized access to computer data and storage device, computer crashes, breach of security and encryption, etc.</li>
						  				<li class="text-justify">The Bank and / or BC shall inform the Customer of the existence of a Force Majeure Event due to which the Bank and / or BC may be unable to provide the services in full or in part. "Force Majeure Event" under this agreement shall mean any event due to any cause beyond the reasonable control of the Bank and / or BC, including, without limitation, unavailability of any communication system, breach or virus in the processes or payment mechanism, sabotage, fire, flood, explosion, acts of God, civil commotion, strikes or industrial action of any kind, riots, insurrection, war, acts of government, computer hacking, unauthorized access to computer data and storage device, computer crashes, breach of security and encryption, etc.</li>
						  			</ul>

						  			<h5>23. CUSTOMER COVENANTS NOT TO :</h5>

						  			<ul>
						  				<li class="text-justify">impersonate any person or entity, or make any false claim;</li>
						  				<li class="text-justify">access the Prepaid Payment Accounts of any other Customer without athorization/permission;</li>
						  				<li class="text-justify">perform any other similar fraudulent activity;</li>
						  				<li class="text-justify">infringe Banks and / or BC 's or any third party's intellectual property rights, rights of publicity or privacy;</li>
						  				<li class="text-justify">use the Services in a manner that results in or may result in complaints, disputes, reversals, chargebacks, fees, penalties and other liability to the Bank and / or BC , any third party or You;</li>
						  				<li class="text-justify">purchase of any product or commodity which may be illegal or forbidden by law or may result in a crime using <strong><em>I-Money</em></strong> Wallet, in such a case the Bank and / or BC shall not be liable and it shall be the sole responsibility of the Customer;</li>
						  			</ul>

						  			<h5>24. JURISDICTION</h5>

						  			<p class="text-justify">The laws of India shall govern these terms. In case of any dispute arising out of or in connection with <strong><em>I-Money</em></strong> Wallet the Customer shall approach the grievance redressal forum in accordance with the grievance redressal policy as laid out on the Website. The Customer hereby agrees that any legal action or proceedings arising out of these Terms and Conditions shall be brought in the courts or tribunals at Mumbai in India and irrevocably submit themselves to the jurisdiction of such courts and tribunals. The Bank and / or BC, however, in its absolute discretion, may commence any legal action or proceedings arising out of the terms in any other court, tribunal or other appropriate forum, and the user hereby consents to that jurisdiction. Any provision of these terms, which is prohibited or unenforceable in any jurisdiction, shall, as to such jurisdiction, be ineffective to the extent of prohibition or unenforceability but shall not invalidate the remaining provisions of the Terms or affect such provision in any other jurisdiction.</p>

						  			<h5>25. NOTICES</h5>

						  			<p class="text-justify">Notices in respect the Services and facilities of Prepaid Payment Account may be either through email or on the address given by <strong><em>I-Money</em></strong> on its Website, or on the address mentioned below:</p>

						  			<p class="text-justify">351, Aggarwal Cyber Plaza 1, Netaji Subhash Place, New Delhi – 110034.</p>

						  			<p class="text-justify">Email address <a href="mailto:ops@I-Money.co.in">ops@I-Money.co.in</a></p>

						  			<p class="text-justify">The Bank and / or BC would be deemed to have fulfilled its legal obligation to deliver to the Customer any communication or document if such communication or document is sent via electronic means or SMS or through notification on registered mobile number. Failure to advise the Bank and / or BC of any difficulty in opening a document or reading the communication within twenty-four (24) hours after delivery shall serve as an affirmation regarding the acceptance of the document / communication.</p>

						  			<p class="text-justify">THIS DOCUMENT IS PUBLISHED IN COMPLIANCE OF AND SHALL BE GOVERNED BY INDIAN LAW, INCLUDING BUT NOT LIMITED TO (I) THE PAYMENT AND SETTLEMENT SYSTEMS ACT, 2007 AND APPLICABLE RULES, REGULATIONS AND GUIDELINES MADE THEREUNDER INCLUDING POLICY GUIDELINES ON ISSUANCE AND OPERATION OF PRE-PAID PAYMENT INSTRUMENT IN INDIA; (II) THE INFORMATION TECHNOLOGY ACT, 2000, THE RULES, REGULATIONS, GUIDELINES AND CLARIFICATIONS FRAMED THEREUNDER INCLUDING THE INFORMATION TECHNOLOGY (REASONABLE SECURITY PRACTICES AND PROCEDURES AND SENSITIVE PERSONAL INFORMATION) RULES, 2011, AND THE PROVISIONS OF RULE 3 (1) OF THE INFORMATION TECHNOLOGY (INTERMEDIARIES GUIDELINES) RULES, 2011; (III) THE INDIAN CONTRACT ACT, 1872; AND (IV) RESERVE BANK OF INDIA ACT, 1934 AND THE APPLICABLE RULES, REGULATIONS AND GUIDELINES MADE THEREUNDER FOR THE BANK TO ISSUE PRE-PAID PAYMENT INSTRUMENT AND FOR MONEY TRANSFER.</p>

						  			<p class="text-justify">THIS DOCUMENT IS AN ELECTRONIC RECORD IN THE FORM OF AN ELECTRONIC CONTRACT FORMED UNDER INFORMATION TECHNOLOGY ACT, 2000 AND RULES MADE THEREUNDER AND THE AMENDED PROVISIONS PERTAINING TO ELECTRONIC DOCUMENTS / RECORDS IN VARIOUS STATUTES AS AMENDED BY THE INFORMATION TECHNOLOGY ACT, 2000. THIS AGREEMENT DOES NOT REQUIRE ANY PHYSICAL, ELECTRONIC OR DIGITAL SIGNATURE. THIS DOCUMENT IS ALSO DISCLOSED IN COMPLIANCE WITH SECTION 21 OF PAYMENT AND SETTLEMENT SYSTEMS ACT, 2007 AND AS REQUIRED TO BE DISCLOSED UNDER POLICY GUIDELINES ON ISSUANCE AND OPERATION OF PRE-PAID PAYMENT INSTRUMENT IN INDIA.</p>

						  			<p class="text-justify">THIS AGREEMENT IS A LEGALLY BINDING DOCUMENT BETWEEN YOU, THE BANK AND BUSINESS CORRESPONDANT (THESE TERMS DEFINED HEREIN). THE TERMS OF THIS DOCUMENT WILL BE EFFECTIVE UPON YOUR ACCEPTANCE OF THE SAME (IN ELECTRONIC FORM OR BY MEANS OF AN ELECTRONIC RECORD OR OTHER MEANS) AND WILL GOVERN THE RELATIONSHIP BETWEEN YOU, THE BANK AND BUSINESS CORESPONDANT FOR THE USE OF <strong><em>I-Money</em></strong> WALLET AND SERVICES (DEFINED BELOW). IF ANY TERMS OF THIS DOCUMENT CONFLICT WITH ANY OTHER DOCUMENT/ELECTRONIC RECORD IN THIS BEHALF, THE TERMS AND CONDITIONS OF THIS AGREEMENT SHALL PREVAIL, UNTIL FURTHER CHANGE / MODIFICATIONS ARE NOTIFIED BY THE BANK AND / OR BUSINESS CORESPONDANT.</p>

						  			<strong>By availing <em>I-Money</em> Services the Customer accepts all the above mentioned terms and conditions and agrees to abide by the same.</strong>
						  		</div>
					  		</div>
					  	</div>
					  	
					</div>
				</div>
			</div>
		</div>
	</section>
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p><script>document.write(new Date().getFullYear())</script> &copy; Copyright <strong><em>I-Money</em></strong>. All Rights Reserved.</p>
				</div>
			</div>
		</div>
	</footer>

	<!-- Scripts -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" crossorigin="anonymous"></script>

	<!-- <script>
		function sticky_relocate() {
			var window_top = $(window).scrollTop();
			var div_top = $('#sticky-anchor').offset().top;
			if (window_top > div_top) {
				$('.menu').addClass('sticky');
			} else {
				$('.menu').removeClass('sticky');
			}
		}

		$(function() {
			$(window).scroll(sticky_relocate);
			sticky_relocate();
		})
	</script> -->
</body>
</html>
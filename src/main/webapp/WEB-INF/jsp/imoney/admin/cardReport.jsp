
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
<meta name="keywords"
	content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
<meta name="author" content="pixelstrap">
<link rel="icon" href="${pageContext.request.contextPath}/resources/assets/images/favicon.png" type="image/x-icon" />
<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets/images/favicon.png"
	type="image/x-icon" />
<title>Home</title>

<!--Google font-->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700"
	rel="stylesheet">

<!-- Font Awesome -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.css">

<!-- ico-font -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/icofont/icofont.css">

<!-- Themify icon -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/themify-icons/themify-icons.css">
<!-- Flag icon -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/flag-icon.css">

<!-- prism css -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/prism.css">

<!-- Owl css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/owlcarousel.css">

<!-- Bootstrap css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.css">

<!-- App css -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/style.css">

<!-- Responsive css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/responsive.css">

<!-- linearicons css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/linearicons/style.css">
<!-- mystyle  css-->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/mystyle.css">

<style>
	/* The Modal (background) */
	.modal {
	    display: none; /* Hidden by default */
	    position: fixed; /* Stay in place */
	    padding-top: 70px; /* Location of the box */
	    
	    left: 0;
	    top: 0;
	    width: 100%; /* Full width */
	    height: 100%; /* Full height */
	    overflow: auto; /* Enable scroll if needed */
	    background-color: rgb(0,0,0); /* Fallback color */
	    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}
	
	/* Modal Content */
	.modal-content {
	    background-color: #fefefe;
	    margin: auto;
	    padding: 20px;
	    padding-left: 60px;
	    
	    border: 1px solid #888;
	    width: 80%;
	}
	
	#InfoModalID .modal-content {
		padding: 0;
		width: 100%;
	}
	
	/* The Close Button */
	.close {
	    color: #aaaaaa;
	    float: right;
	    font-size: 28px;
	    font-weight: bold;
	}
	
	.close:hover,
	.close:focus {
	    color: #000;
	    text-decoration: none;
	    cursor: pointer;
	}
	
	/* .modal-backdrop.show{
	z-index:0;
	} */
	
	.sidenav {
  height: 89%;
  margin-top: 5%;
  width: 0;
  position: fixed;
  z-index: 4;
  top: 0;
  right: 0;
  background-color: white;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 60px;
}

.sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}

.sidenav a:hover {
  color: #f1f1f1;
}

.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}

@media (min-width: 992px)
.modal-lg {
    max-width: 900px;
}

.linksBlock {
	border: 1px solid #787878;
	height: 85px;
	margin: 0 auto;
	padding: 17px 0;
}

.relatedLinks a {
	color: #00508f;
}
	
	
	</style>

</head>
<body>

	<!-- Loader starts -->
	<div class="loader-wrapper">
		<div class="loader bg-white">
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
			<h4>
				Have a great day at work today <span>&#x263A;</span>
			</h4>
		</div>
	</div>
	<!-- Loader ends -->

	<!--page-wrapper Start-->
	<div class="page-wrapper">
		<jsp:include page="/WEB-INF/jsp/imoney/admin/header.jsp"></jsp:include>
		<div class="page-body-wrapper">
			<jsp:include page="/WEB-INF/jsp/imoney/admin/leftMenu.jsp"></jsp:include>

			<div class="page-body">
				<!-- Container-fluid starts -->
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-12">
							<div class="card height-equal equal-height-lg"
								style="margin-top: 15px;">
								<div class="card-header">
									<h5>Card Report</h5>
									<div class="card-header-right">
										<a href="#" class="icon-bar"><i
											class="lnr lnr-magnifier"></i></a> <a href="#"
											class="icon-bar"><i class="lnr lnr-download"></i></a>
									</div>
								</div>
								<div class="card-body">
									<div class="tabble" style="overflow-x: scroll;">
										<table id="example"
											class="table table-striped table-bordered dt-responsive nowrap"
											style="width: 100%">
											<thead>
												<tr>
													<th>S.No</th>
													<th>First&nbsp;Name</th>
													<th>Last&nbsp;Name</th>
													<th>Email</th>
													<th>Mobile</th>
													<th>Card&nbsp;Number</th>
													<th>Proxy&nbsp;Number</th>
													<th>Expiry&nbsp;Date</th>
													<th>Card&nbsp;Status</th>
													<th>Preferred&nbsp;Name</th>
													<th>Card&nbsp;Type</th>
													<th>Wallet&nbsp;Hash&nbsp;Id</th>
													<th>Card&nbsp;Hash&nbsp;Id</th>
													<th>Created&nbsp;Date</th>
												</tr>
											</thead>
											<tbody id="genericUserList"
												style="height: 100px; overflow-y: auto;">
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<!-- Container-fluid Ends -->
	</div>

	<div class="modal" tabindex="-1" role="dialog" id="InfoModalID">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Card Details</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <div class="row">
				<div class="col-6" style="border-right: 1px solid #efefef">
					<div class="row mb-2">
						<div class="col-5"><h6>First Name:</h6></div>
						<div class="col-7"><strong id="fnameId"> ABC</strong></div>					
					</div>
					<div class="row mb-2">
						<div class="col-5"><h6>Last Name:EF</h6></div>
						<div class="col-7"><strong id="lnameId">DEF</strong></div>					
					</div>
					<div class="row mb-2">
						<div class="col-5" ><h6>Email Id:</h6></div>
						<div class="col-7"><strong id="emailId">abc@imoney.com</strong></div>					
					</div>
					<div class="row mb-2">
						<div class="col-5"><h6>KYC Status:</h6></div>
						<div class="col-7"><strong id="acctTypeId">KYC</strong></div>					
					</div>
				</div>
				<div class="col-6 relatedLinks">
					<div class="row">
						<div class="col-4 p-0" id="hrefId">
							<a href="#">
								<div class="linksBlock text-center">
									<div class="linksIcon">
										<i class="fa fa-list-alt"></i>								
									</div>
									<div class="linksTxt">Transactions</div>
								</div>		
							</a>				
						</div>
						
						<div class="col-4 p-0" id="hrefId">
							<a href="#">
								<div class="linksBlock text-center">
									<div class="linksIcon">
										<i class="fa fa-list-alt"></i>								
									</div>
									<div class="linksTxt">Wallets</div>
								</div>		
							</a>				
						</div>
						
						<div class="col-4 p-0">
							<a href="#">
								<div class="linksBlock text-center">
									<div class="linksIcon">
										<i class="fa fa-file-text-o"></i>								
									</div>
									<div class="linksTxt">Documents</div>
								</div>	
							</a>					
						</div>
					</div>
				</div>        	
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
	
	<!--page-wrapper Ends-->

	<!-- latest jquery-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/popper.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/bootstrap.js"></script>
	<!-- Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/chart/Chart.min.js"></script>
	<!-- Morris Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/raphael.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/morris.js"></script>
	<!-- owlcarousel js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/owlcarousel/owl.carousel.js"></script>
	<!-- Sidebar jquery-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/sidebar-menu.js"></script>
	<!--Sparkline  Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/sparkline/sparkline.js"></script>
	<!--Height Equal Js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/height-equal.js"></script>
	<!-- prism js -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/prism/prism.min.js"></script>
	<!-- clipboard js -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/clipboard/clipboard.min.js"></script>
	<!-- custom card js  -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/custom-card/custom-card.js"></script>
	<!-- Theme js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/script.js"></script>
	<!-- <script src="${pageContext.request.contextPath}/resources/assets/js/theme-customizer/customizer.js"></script> -->
	<!-- <script src="${pageContext.request.contextPath}/resources/assets/js/chat-sidebar/chat.js"></script> -->
	<!-- Counter js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.waypoints.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.counterup.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/counter-custom.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/notify/bootstrap-notify.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/notify/index.js"></script>
	
<!-- 	=== pagination=== -->
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.min.js"></script>
	


<script type="text/javascript">
		var contextPath = "${pageContext.request.contextPath}";
		var value = "${value}";
		if (value == null) {
			value = 'All';
		}
		var role = "${role}";
		$(document).ready(function() {
			getCardList(0);
		});
		$(function() {
			var start = moment().subtract(29, 'days');
			var end = moment();
			function cb(start, end) {
				$('#reportrange').html(
						start.format('DD/MM/YYYY') + ' - '
								+ end.format('DD/MM/YYYY'));
			}
			$('#reportrange').daterangepicker(
					{
						startDate : start,
						endDate : end,
						locale : {
							format : 'DD/MM/YYYY'
						},
						dateLimit : {
							"days" : 60
						},
						ranges : {
							'Today' : [ moment(), moment() ],
							'Yesterday' : [ moment().subtract(1, 'days'),
									moment().subtract(1, 'days') ],
							'Last 7 Days' : [ moment().subtract(6, 'days'),
									moment() ],
							'Last 30 Days' : [ moment().subtract(29, 'days'),
									moment() ],
							'This Month' : [ moment().startOf('month'),
									moment().endOf('month') ],
							'Last Month' : [
									moment().subtract(1, 'month').startOf(
											'month'),
									moment().subtract(1, 'month')
											.endOf('month') ]
						}
					}, cb);
			cb(start, end);
		});

		function getCardList(page) {
			var trHtml = '';
			var data;
			var userId = $('#hiddenId').val();
			$.ajax({
						type : "POST",
						url : contextPath + "/Master/getAllCards",
						dataType : "json",
						contentType : "application/json",
						data : JSON.stringify({
							"page" : page,
							"size" : 10,
						}),
						success : function(response) {
							console.log("response: " + response);
							console.log("details: " + response.details);
							console.log("length: " + response.details.length);
							$('#genericUserList').empty();
							if (response.details != null
									&& response.details != 'null'
									&& response.details.length > 0) {
								for (var i = 0; i < response.details.length; i++) {
									trHtml = '';
									var data = response.details[i];
									trHtml = '<tr title="click here" style="cursor: pointer;" <a href="#" onclick="openNav();"></>">';
									trHtml = trHtml + '<td>' + (i + 1)
											+ '</td>';
									trHtml = trHtml + '<td>' + data.firstName
											+ '</td>';
									trHtml = trHtml + '<td>' + data.lastName
											+ '</td>';
									trHtml = trHtml + '<td>' + data.email
											+ '</td>';
									trHtml = trHtml + '<td>' + data.mobile
											+ '</td>';
									trHtml = trHtml + '<td>' + data.cardNo
											+ '</td>';
									trHtml = trHtml + '<td>' + data.proxyNo
											+ '</td>';
									trHtml = trHtml + '<td>' + data.expiryDate
											+ '</td>';
									trHtml = trHtml + '<td>' + data.cardStatus
											+ '</td>';
									trHtml = trHtml + '<td>'
											+ data.preferredName + '</td>';
									trHtml = trHtml + '<td>' + data.cardType
											+ '</td>';
									trHtml = trHtml + '<td>'
											+ data.walletHashId + '</td>';
									trHtml = trHtml + '<td>' + data.cardHashId
											+ '</td>';
									trHtml = trHtml + '<td>' + data.created
											+ '</td>';
									trHtml = trHtml + '</tr>';
									$('#genericUserList').append(trHtml);
								}
								$(function() {
									$('#paginationn').twbsPagination({
										totalPages : response.totalPage,
										visiblePages : 10,
										onPageClick : function(event, page) {
											if ($('#paginationId').val() != 1) {
												getCardList(page - 1);
											} else {
												$('#paginationId').val(2);
											}
										}
									});
								});
							} else {
								trHtml = '<tr>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td>No data found</td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '</tr>';
								$('#genericUserList').append(trHtml);
							}
						}
					});
		}

		function formatDate(d) {
			d = new Date(d);
			var month = d.getMonth();
			var day = d.getDate();
			month = month + 1;
			month = month + "";
			if (month.length == 1) {
				month = "0" + month;
			}
			day = day + "";
			if (day.length == 1) {
				day = "0" + day;
			}
			return d.getFullYear() + '-' + month + '-' + day;
		}
	</script>

	<script>
		function openNav(firstName, lastName, email, id) {
			// 			$('#upperBlock').css('margin-top', '-17%');
			/* $('#mySidenav').css('width', '350px');
			$('#overlay').css('display', 'block'); */

			console.log(id);

			$('#InfoModalID').modal('show');
			
			
			$('#fnameId').html(firstName);
			$('#lnameId').html(lastName);
			$('#eamilId').html(email);
			$('#acctTypeId').html(kycStatus);
			$('#hrefId')
					.html(
							'<a href="${pageContext.request.contextPath}/Master/getTransaction/'+id+'"><i class="fa fa-list">&nbsp;Transaction</i></a>');
			$('#hrefCardId')
					.html(
							'<a href="${pageContext.request.contextPath}/Master/getCards/'+id+'"><i class="fa fa-list">&nbsp;Cards</i></a>');

			$('#cardNoId').html('');
			$('#mobileNoId').html('');
			$('#cardHolderId').html('');
			$('#balId').html('');

			if (username != null && username != '') {
				$('#tableHeader0').css('display', 'block');
				$.ajax({
					type : "POST",
					url : contextPath + "/Master/WalletDetails",
					dataType : "json",
					contentType : "application/json",
					data : JSON.stringify({
						"username" : "" + username + "",
					}),
					success : function(response) {
						if (response != null) {
							if (response.balance != '0') {
								$('#balId').html(response.balance + " INR");
							} else {
								$('#balId').html("0.00 INR");
							}
							if (response.cardNumber != null
									&& response.cardNumber != '') {
								$('#cardNoId').html(response.cardNumber);
							} else {
								$('#cardNoId').html('NA');
							}
							if (response.cardHolder != null
									&& response.cardHolder != '') {
								$('#cardHolderId').html(response.cardHolder);
							} else {
								$('#cardHolderId').html('NA');
							}
							$('#mobileNoId').html(username);
							;
						}
					}
				});
			}
		}
	</script>

</body>
</html>
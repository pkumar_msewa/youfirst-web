
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
<meta name="keywords"
	content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
<meta name="author" content="pixelstrap">
<link rel="icon" href="${pageContext.request.contextPath}/resources/assets/images/favicon.png" type="image/x-icon" />
<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets/images/favicon.png"
	type="image/x-icon" />
<title>Home</title>

<!--Google font-->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700"
	rel="stylesheet">

<!-- Font Awesome -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.css">

<!-- ico-font -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/icofont/icofont.css">

<!-- Themify icon -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/themify-icons/themify-icons.css">
<!-- Flag icon -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/flag-icon.css">

<!-- prism css -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/prism.css">

<!-- Owl css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/owlcarousel.css">

<!-- Bootstrap css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.css">

<!-- App css -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/style.css">

<!-- Responsive css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/responsive.css">

<!-- linearicons css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/linearicons/style.css">
<!-- mystyle  css-->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/mystyle.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />


<style>
	/* The Modal (background) */
	.modal {
	    display: none; /* Hidden by default */
	    position: fixed; /* Stay in place */
	    padding-top: 70px; /* Location of the box */
	    
	    left: 0;
	    top: 0;
	    width: 100%; /* Full width */
	    height: 100%; /* Full height */
	    overflow: auto; /* Enable scroll if needed */
	    background-color: rgb(0,0,0); /* Fallback color */
	    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}
	
	/* Modal Content */
	.modal-content {
	    background-color: #fefefe;
	    margin: auto;
	    padding: 20px;
	    padding-left: 60px;
	    
	    border: 1px solid #888;
	    width: 80%;
	}
	
	#InfoModalID .modal-content {
		padding: 0;
		width: 100%;
	}
	
	/* The Close Button */
	.close {
	    color: #aaaaaa;
	    float: right;
	    font-size: 28px;
	    font-weight: bold;
	}
	
	.close:hover,
	.close:focus {
	    color: #000;
	    text-decoration: none;
	    cursor: pointer;
	}
	
	.modal-backdrop.show{
	z-index:0;
	}
	
	</style>

</head>
<body>

	<!-- Loader starts -->
	<div class="loader-wrapper">
		<div class="loader bg-white">
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
			<h4>
				Have a great day at work today <span>&#x263A;</span>
			</h4>
		</div>
	</div>
	<!-- Loader ends -->

	<!--page-wrapper Start-->
	<div class="page-wrapper">
		<jsp:include page="/WEB-INF/jsp/imoney/admin/header.jsp"></jsp:include>
		<div class="page-body-wrapper">
			<jsp:include page="/WEB-INF/jsp/imoney/admin/leftMenu.jsp"></jsp:include>

			<div class="page-body">
				<!-- Container-fluid starts -->
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-12">
							<div class="card height-equal equal-height-lg"
								style="margin-top: 15px;">
								<div class="card-header">
									<h5>Card Details</h5>
								</div>
								<input type="hidden" id="paginationId" value="1">

								<form id="dformId" method="post"
									action="${pageContext.request.contextPath}/Download/DownloadUserReport">
									<input type="hidden" id="downloadReportId" name="dateRange"
										value="" class="form-control" />
								</form>
								<div class="col-md-12">
									<div class="row" style="margin: 0;">
										<div class="col-3">
											<label>Select Daterange*</label>
											<div id="" style="cursor: pointer;">
												<input id="reportrange" name="toDate" class="form-control"
													style="background-color: white" readonly="readonly" />
											</div>
										</div>
										<div class="col-2">
											<button class="btn btn-primary" style="margin-top: 22%;"
												type="button" onclick="getCardListBasedOnFilter(0);">Filter</button>
										</div>
										<div class="col-3">
											<label>Find By Contact Number*</label> <input id="usernameId"
												name="userName" class="form-control" maxlength="100"
												onkeypress="return isNumberKey(event);"
												placeholder="Enter Contact Number" />

										</div>
										<div class="col-1">
											<button class="btn btn-primary" onclick="getByUserName(0)"
												style="margin-top: 50%;" type="button">Search</button>
										</div>

									</div>
								</div>
								<div class="card-body">
									<div class="tabble" style="overflow-x: scroll;">
		            					<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
		            					<thead>
		            					

												<tr>
													<th>S.No</th>
													<th>First&nbsp;Name</th>
													<th>Last&nbsp;Name</th>
													<th>Email</th>
													<th>Mobile</th>
													<th>Proxy&nbsp;Number</th>
													<th>Card&nbsp;Status</th>
													<th>Card&nbsp;Type</th>
													<th>Created&nbsp;Date</th>
												</tr>
											</thead>
											<tbody id="genericUserList"
												style="height: 100px; overflow-y: auto;">
											</tbody>
										</table>
										<nav>
										<ul class="pagination" id="paginationn"></ul>
									</nav>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<!-- Container-fluid Ends -->
	</div>


	<div class="modal" tabindex="-1" role="dialog" id="InfoModalID">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Card Details</h5>
	        
	        <div class="col-6" style="text-align: center;">
								<span id="errorMesgId"></span>
							</div>
	        
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
				<div class="modal-body">
					<input type="hidden" id="tableRowCountId" value=""> <input
						type="hidden" id="cardHashValId" value=""> <input
						type="hidden" id="mobileNoId" value=""> <input
						type="hidden" id="cardTypeValId" value="">
					<div class="row">
						<div class="col-6" style="border-right: 1px solid #efefef">
							<div class="cardWrapper">
								<img alt="card" class="img-fluid"
									src="${pageContext.request.contextPath}/resources/assets/images/UI_card.png">
								<div class="cardInfo">
									<div class="cardNum">
										<strong id="cardNumId"></strong>
									</div>
									<div class="expdt">
										<strong id="expdtId"></strong>
									</div>
									<div class="cardNme">
										<strong id="cardNameId"></strong>
									</div>
								</div>
							</div>
						</div>
						<div class="col-6">
							<div class="row mb-2">
								<div class="col-5">
									<h6>Card Type.:</h6>
								</div>
								<div class="col-7">
									<strong id="cardTypeId"></strong>
								</div>
							</div>
							<div class="row mb-2">
								<div class="col-5">
									<h6>Date Issued:</h6>
								</div>
								<div class="col-7">
									<strong id="dateIssueId"></strong>
								</div>
							</div>
							<div class="row mb-2">
								<div class="col-5">
									<h6>Email:</h6>
								</div>
								<div class="col-7">
									<strong id="emailId"></strong>
								</div>
							</div>
							<div class="row mb-2">
								<div class="col-5">
									<h6>Card Status:</h6>
								</div>
								<div class="col-7">
									<strong id="statusId"></strong>
								</div>
							</div>
							<div class="row mb-2">
								<div class="col-5">
									<h6>Card Balance:</h6>
								</div>
								<div class="col-7">
									<strong id="balanceId"><i class="fa fa-inr"></i>&nbsp;</strong>
								</div>
							</div>
							<div class="row mb-2">
								<!-- 						<div class="col-6"><button class="btn btn-sm btn-block btn-warning" type="button">SUSPEND CARD</button></div> -->
								<div class="col-6" id="lockDivId" style="display: none;">
									<button class="btn btn-sm btn-block btn-danger"
										onclick="blockLockUnlockCard('suspend')" id="lockCardId"
										type="button">
										<strong>LOCK CARD</strong>
									</button>
								</div>
								<div class="col-6" id="unlockDivId" style="display: none;">
									<button class="btn btn-sm btn-block btn-success"
										onclick="blockLockUnlockCard('unblock')" id="unlockCardId"
										type="button">
										<strong>UNLOCK CARD</strong>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	  </div>
	</div>
	
	<!--page-wrapper Ends-->

	<!-- latest jquery-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/popper.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/bootstrap.js"></script>
	<!-- Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/chart/Chart.min.js"></script>
	<!-- Morris Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/raphael.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/morris.js"></script>
	<!-- owlcarousel js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/owlcarousel/owl.carousel.js"></script>
	<!-- Sidebar jquery-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/sidebar-menu.js"></script>
	<!--Sparkline  Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/sparkline/sparkline.js"></script>
	<!--Height Equal Js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/height-equal.js"></script>
	<!-- prism js -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/prism/prism.min.js"></script>
	<!-- clipboard js -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/clipboard/clipboard.min.js"></script>
	<!-- custom card js  -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/custom-card/custom-card.js"></script>
	<!-- Theme js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/script.js"></script>
	<!-- <script src="${pageContext.request.contextPath}/resources/assets/js/theme-customizer/customizer.js"></script> -->
	<!-- <script src="${pageContext.request.contextPath}/resources/assets/js/chat-sidebar/chat.js"></script> -->
	<!-- Counter js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.waypoints.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.counterup.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/counter-custom.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/notify/bootstrap-notify.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/notify/index.js"></script>
	 <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
	
<!-- 	=== pagination=== -->
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.min.js"></script>
	


<script type="text/javascript">
		var contextPath = "${pageContext.request.contextPath}";
		var value = "${value}";
		if (value == null) {
			value = 'All';
		}
		var role = "${role}";
		$(document).ready(function() {
			getCardList(0);
		});
		$(function() {
			var start = moment().subtract(29, 'days');
			var end = moment();
			function cb(start, end) {
				$('#reportrange').html(
						start.format('DD/MM/YYYY') + ' - '
								+ end.format('DD/MM/YYYY'));
			}
			$('#reportrange').daterangepicker(
					{
						startDate : start,
						endDate : end,
						locale : {
							format : 'DD/MM/YYYY'
						},
						dateLimit : {
							"days" : 360
						},
						ranges : {
							'Today' : [ moment(), moment() ],
							'Yesterday' : [ moment().subtract(1, 'days'),
									moment().subtract(1, 'days') ],
							'Last 7 Days' : [ moment().subtract(6, 'days'),
									moment() ],
							'Last 30 Days' : [ moment().subtract(29, 'days'),
									moment() ],
							'This Month' : [ moment().startOf('month'),
									moment().endOf('month') ],
							'Last Month' : [
									moment().subtract(1, 'month').startOf(
											'month'),
									moment().subtract(1, 'month')
											.endOf('month') ]
						}
					}, cb);
			cb(start, end);
		});

		function getCardList(page) {
			var trHtml = '';
			var data;
			var userId = $('#hiddenId').val();
			$.ajax({
						type : "POST",
						url : contextPath + "/Master/getAllCards",
						dataType : "json",
						contentType : "application/json",
						data : JSON.stringify({
							"page" : page,
							"size" : 10,
						}),
						success : function(response) {
							console.log("response: " + response);
							console.log("details: " + response.details);
							console.log("length: " + response.details.length);
							$('#genericUserList').empty();
							if (response.details != null
									&& response.details != 'null'
									&& response.details.length > 0) {
								for (var i = 0; i < response.details.length; i++) {
									trHtml = '';
									var data = response.details[i];
									trHtml = '<tr title="click here" style="cursor: pointer;" <a href="#" onclick="openNav(\''+data.cardType+ '\',\''+data.created+ '\',\''
									+data.cardStatus+ '\',\''+data.email+ '\',\''+data.balance+ '\',\''+data.preferredName+ '\',\''+data.cardNo+ '\',\''+data.expiryDate+ '\',\''+data.cardHashId+ '\',\''+i+ '\',\''+data.mobile+ '\');"></>">';
									trHtml = trHtml + '<td>' + (i + 1)
											+ '</td>';
									trHtml = trHtml + '<td>' + data.firstName
											+ '</td>';
									trHtml = trHtml + '<td>' + data.lastName
											+ '</td>';
									trHtml = trHtml + '<td>' + data.email
											+ '</td>';
									trHtml = trHtml + '<td>' + data.mobile
											+ '</td>';
									trHtml = trHtml + '<td>' + data.cardNo
											+ '</td>';
									trHtml = trHtml + '<td id=cardStatus'+i+'>' + data.cardStatus
											+ '</td>';
									trHtml = trHtml + '<td>' + data.cardType
											+ '</td>';
									trHtml = trHtml + '<td>' + data.created
											+ '</td>';
									trHtml = trHtml + '</tr>';
									$('#genericUserList').append(trHtml);
								}
								$(function() {
									$('#paginationn').twbsPagination({
										totalPages : response.totalPage,
										visiblePages : 10,
										onPageClick : function(event, page) {
											if ($('#paginationId').val() != 1) {
												getCardList(page - 1);
											} else {
												$('#paginationId').val(2);
											}
										}
									});
								});
							} else {
								trHtml = '<tr>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td>No data found</td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '<td></td>';
								trHtml = trHtml + '</tr>';
								$('#genericUserList').append(trHtml);
							}
						}
					});
		}
		function getCardListBasedOnFilter(page) {
    		var trHtml='';
			var userRole='User'; 
    		var data;
    		var dateRange = $('#reportrange').val();
    		var userType = $('#userType').val();
    		$('#tableHeader0').css('display','block');
    		$('#paginationn').twbsPagination('destroy');
    		$.ajax({
    			type : "POST",
	    		url:contextPath+"/Master/getAllCards",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"page" : page,
	   				"size" : 10,
	   				"dateRange" : "" + dateRange + "",
	   			}),
	   			success : function(response) {
					$('#genericUserList').empty();
					if (response.details.length > 0) {
						for (var i = 0; i < response.details.length; i++) {
														trHtml = '';
														var data = response.details[i];
														trHtml = '<tr title="click here" style="cursor: pointer;" <a href="#" onclick="openNav(\''+data.cardType+ '\',\''+data.created+ '\',\''
														+data.cardStatus+ '\',\''+data.email+ '\',\''+data.balance+ '\',\''+data.preferredName+ '\',\''+data.cardNo+ '\',\''+data.expiryDate+ '\',\''+data.cardHashId+ '\',\''+i+ '\',\''+data.mobile+ '\');"></>">';
														trHtml = trHtml + '<td>' + (i + 1)
																+ '</td>';
														trHtml = trHtml + '<td>' + data.firstName
																+ '</td>';
														trHtml = trHtml + '<td>' + data.lastName
																+ '</td>';
														trHtml = trHtml + '<td>' + data.email
																+ '</td>';
														trHtml = trHtml + '<td>' + data.mobile
																+ '</td>';
														trHtml = trHtml + '<td>' + data.expiryDate
																+ '</td>';
														trHtml = trHtml + '<td id=cardStatus'+i+'>' + data.cardStatus
																+ '</td>';
														trHtml = trHtml + '<td>' + data.cardType
																+ '</td>';
														trHtml = trHtml + '<td>' + data.created
																+ '</td>';
														trHtml = trHtml + '</tr>';
														$('#genericUserList').append(trHtml);
													}
													$(function() {
														$('#paginationn').twbsPagination({
															totalPages : response.totalPage,
															visiblePages : 10,
															onPageClick : function(event, page) {
																if ($('#paginationId').val() != 1) {
																	fetchDataByFilter(page - 1);
																} else {
																	$('#paginationId').val(2);
																}
															}
														});
													});
												} else {
													trHtml = '<tr>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td>No data found</td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '</tr>';
													$('#genericUserList').append(trHtml);
												}
											}
										});
							}
		
		
		
		function fetchDataByFilter(page) {
    		var trHtml='';
    		var data;
    		var dateRange = $('#reportrange').val();
    		var userType = $('#userType').val();
    		$('#tableHeader0').css('display','block');
    		$.ajax({
    			type : "POST",
	    		url:contextPath+"/Master/getAllCards",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"page" : page,
	   				"size" : 10,
	   				"role" : "User",
	   				"dateRange" : "" + dateRange + "",
	   				"value" : "" + value + "",
	   				"status" : "" + status + "",
	   			}),
	   			success : function(response) {
	   				$('#genericUserList').empty();
					if (response.details.length > 0) {
						for (var i = 0; i < response.details.length; i++) {
														trHtml = '';
														var data = response.details[i];
														trHtml = '<tr title="click here" style="cursor: pointer;" <a href="#" onclick="openNav(\''+data.cardType+ '\',\''+data.created+ '\',\''
														+data.cardStatus+ '\',\''+data.email+ '\',\''+data.balance+ '\',\''+data.preferredName+ '\',\''+data.cardNo+ '\',\''+data.expiryDate+ '\',\''+data.cardHashId+ '\',\''+i+ '\',\''+data.mobile+ '\');"></>">';
														trHtml = trHtml + '<td>' + (i + 1)
																+ '</td>';
														trHtml = trHtml + '<td>' + data.firstName
																+ '</td>';
														trHtml = trHtml + '<td>' + data.lastName
																+ '</td>';
														trHtml = trHtml + '<td>' + data.email
																+ '</td>';
														trHtml = trHtml + '<td>' + data.mobile
																+ '</td>';
														trHtml = trHtml + '<td>' + data.expiryDate
																+ '</td>';
														trHtml = trHtml + '<td id=cardStatus'+i+'>' + data.cardStatus
																+ '</td>';
														trHtml = trHtml + '<td>' + data.cardType
																+ '</td>';
														trHtml = trHtml + '<td>' + data.created
																+ '</td>';
														trHtml = trHtml + '</tr>';
														$('#genericUserList').append(trHtml);
													}
												} else {
													trHtml = '<tr>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td>No data found</td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '<td></td>';
													trHtml = trHtml + '</tr>';
													$('#genericUserList').append(trHtml);
												}
											}
    		});
    	}
		
		function formatDate(d) {
			d = new Date(d);
			var month = d.getMonth();
			var day = d.getDate();
			month = month + 1;
			month = month + "";
			if (month.length == 1) {
				month = "0" + month;
			}
			day = day + "";
			if (day.length == 1) {
				day = "0" + day;
			}
			return d.getFullYear() + '-' + month + '-' + day;
		}
	
	function getByUserName(page){
			var trHtml='';
    		var data;
    		var username = $('#usernameId').val();
    		$('#paginationn').twbsPagination('destroy'); 
    		if(username!=null && username !=''){
        		$('#tableHeader0').css('display','block');
        		$.ajax({
        			type : "POST",
    	    		url:contextPath+"/Master/getAllCards",
    	    		dataType:"json",
    	   			contentType : "application/json",
    	   			data : JSON.stringify({
    	   				"page" : page,
    	   				"size" : 10,
    	   				"username" : "" + username + "",
    	   			}),
    	   			success : function(response) {
						console.log("response: " + response);
						console.log("details: " + response.details);
						console.log("length: " + response.details.length);
						$('#genericUserList').empty();
						if (response.details != null
								&& response.details != 'null'
								&& response.details.length > 0) {
							for (var i = 0; i < response.details.length; i++) {
								trHtml = '';
								var data = response.details[i];
								trHtml = '<tr title="click here" style="cursor: pointer;" <a href="#" onclick="openNav(\''+data.cardType+ '\',\''+data.created+ '\',\''
								+data.cardStatus+ '\',\''+data.email+ '\',\''+data.balance+ '\',\''+data.preferredName+ '\',\''+data.cardNo+ '\',\''+data.expiryDate+ '\',\''+data.cardHashId+ '\',\''+i+ '\',\''+data.mobile+ '\');"></>">';
								trHtml = trHtml + '<td>' + (i + 1)
										+ '</td>';
								trHtml = trHtml + '<td>' + data.firstName
										+ '</td>';
								trHtml = trHtml + '<td>' + data.lastName
										+ '</td>';
								trHtml = trHtml + '<td>' + data.email
										+ '</td>';
								trHtml = trHtml + '<td>' + data.mobile
										+ '</td>';
								trHtml = trHtml + '<td>' + data.expiryDate
										+ '</td>';
								trHtml = trHtml + '<td id=cardStatus'+i+'>' + data.cardStatus
										+ '</td>';
								trHtml = trHtml + '<td>' + data.cardType
										+ '</td>';
								trHtml = trHtml + '<td>' + data.created
										+ '</td>';
								trHtml = trHtml + '</tr>';
								$('#genericUserList').append(trHtml);
							}
							$(function() { 	
								$('#paginationn').twbsPagination({
									totalPages : response.totalPage,
									visiblePages : 10,
									onPageClick : function(event, page) {
											getByUserNameAndPage(page - 1);
									}
								});
							});
						} else {
							trHtml = '<tr>';
							trHtml = trHtml + '<td></td>';
							trHtml = trHtml + '<td></td>';
							trHtml = trHtml + '<td></td>';
							trHtml = trHtml + '<td></td>';
							trHtml = trHtml + '<td></td>';
							trHtml = trHtml + '<td></td>';
							trHtml = trHtml + '<td>No data found</td>';
							trHtml = trHtml + '<td></td>';
							trHtml = trHtml + '<td></td>';
							trHtml = trHtml + '<td></td>';
							trHtml = trHtml + '<td></td>';
							trHtml = trHtml + '<td></td>';
							trHtml = trHtml + '<td></td>';
							trHtml = trHtml + '</tr>';
							$('#genericUserList').append(trHtml);
						}
					}
				});
	
    		}else{
    			$('#errUser').html("Please enter username")
    		}
		}
	
	
	function getByUserNameAndPage(page){
		var trHtml='';
		var data;
		var username = $('#usernameId').val();
		if(username!=null && username !=''){
    		$('#tableHeader0').css('display','block');
    		$.ajax({
    			type : "POST",
	    		url:contextPath+"/Master/getAllCards",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"page" : page,
	   				"size" : 10,
	   				"username" : "" + username + "",
	   			}),
	   			success : function(response) {
					console.log("response: " + response);
					console.log("details: " + response.details);
					console.log("length: " + response.details.length);
					$('#genericUserList').empty();
					if (response.details != null
							&& response.details != 'null'
							&& response.details.length > 0) {
						for (var i = 0; i < response.details.length; i++) {
							trHtml = '';
							var data = response.details[i];
							trHtml = '<tr title="click here" style="cursor: pointer;" <a href="#" onclick="openNav(\''+data.cardType+ '\',\''+data.created+ '\',\''
							+data.cardStatus+ '\',\''+data.email+ '\',\''+data.balance+ '\',\''+data.preferredName+ '\',\''+data.cardNo+ '\',\''+data.expiryDate+ '\',\''+data.cardHashId+ '\',\''+i+ '\',\''+data.mobile+ '\');"></>">';
							trHtml = trHtml + '<td>' + (i + 1)
									+ '</td>';
							trHtml = trHtml + '<td>' + data.firstName
									+ '</td>';
							trHtml = trHtml + '<td>' + data.lastName
									+ '</td>';
							trHtml = trHtml + '<td>' + data.email
									+ '</td>';
							trHtml = trHtml + '<td>' + data.mobile
									+ '</td>';
							trHtml = trHtml + '<td>' + data.expiryDate
									+ '</td>';
							trHtml = trHtml + '<td id=cardStatus'+i+'>' + data.cardStatus
									+ '</td>';
							trHtml = trHtml + '<td>'
									+ data.preferredName + '</td>';
							trHtml = trHtml + '<td>' + data.cardType
									+ '</td>';
							trHtml = trHtml + '<td>' + data.cardHashId
									+ '</td>';
							trHtml = trHtml + '<td>' + data.created
									+ '</td>';
							trHtml = trHtml + '</tr>';
							$('#genericUserList').append(trHtml);
						}
					} else {
						trHtml = '<tr>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td>No data found</td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '<td></td>';
						trHtml = trHtml + '</tr>';
						$('#genericUserList').append(trHtml);
					}
				}
			});

		}else{
			$('#errUser').html("Please enter username")
		}
	}
	

	
	function openNav(cardType, created, cardStatus, email, balance, nameOnCard,
			cardNo, expDate, cardHashId, rowCount , mobile) {
		console.log(cardHashId + ": " + rowCount +":"+mobile +":"+cardType);
		$('#errorMesgId').html("")
		if (cardStatus == 'Active') {
			jQuery('#unlockDivId').css("display", "none");
			jQuery('#lockDivId').css("display", "block");
		} else {
			jQuery('#lockDivId').css("display", "none")
			jQuery('#unlockDivId').css("display", "block")
		}
		$('#cardHashValId').val(cardHashId);
		$('#tableRowCountId').val(rowCount);
		$('#mobileNoId').val(mobile);
		$('#cardTypeValId').val(cardType);
		
		$('#InfoModalID').modal('show');
		$('#cardTypeId').html(cardType);
		$('#dateIssueId').html(created);
		$('#emailId').html(email);
		$('#statusId').html(cardStatus);
		$('#balanceId').html(balance);
		$('#cardNumId').html("");
		$('#expdtId').html("");
		$('#cardNameId').html("");
		
		
		if (cardHashId != null && cardHashId != '') {
			$.ajax({
						type : "POST",
						url : contextPath + "/Master/cardDetails",
						dataType : "json",
						contentType : "application/json",
						data : JSON.stringify({
							"id" : "" + cardHashId + "",
						}),
						success : function(response) {
							console.log(response.details);
							if (response.details != null && response.details.code == 'S00') {
								var cardNumber = response.details.cardNumber;
								var cardNumber = cardNumber.substr(cardNumber.length - 4);
								console.log(cardNumber);
								$('#cardNumId').html("XXXX XXXX XXXX "+cardNumber);
								$('#expdtId').html(response.details.expiryDate);
								$('#cardNameId').html(response.details.holderName);
								$('#balanceId').html(response.details.balance);
							}else{
								$('#cardNumId').html("XXXX XXXX XXXX XXXX");
								$('#expdtId').html("XX XXXX");
								$('#cardNameId').html("XXXX XXXX");
							}
						}
					});
		}
		
	}
</script>
	
	
	<script>
		function blockLockUnlockCard(actionType) {
			var cardHashId = $('#cardHashValId').val();
			var rowCount = $('#tableRowCountId').val();
			var mobile = $('#mobileNoId').val(); 
			var cardType = $('#cardTypeValId').val(); 
			
			console.log(cardHashId +": "+actionType);
			
			var confMesg='';
			if(actionType == 'suspend'){
				confMesg = 'LOCK'
			}else if(actionType == 'unblock'){
				confMesg = 'UNLOCK'
			}
			
			if(confirm('Confirm '+confMesg+' Card')){
			console.log(rowCount+""+cardHashId+""+actionType+": "+mobile);
			if (actionType != '' && cardHashId != '') {
				$.ajax({
					type : "POST",
					url : "${pageContext.request.contextPath}/Master/blockLockUnlockCard",
					dataType : "json",
					contentType : "application/json",
					data : JSON.stringify({
						"type" : "" + actionType + "",
						"cardHashId" : "" + cardHashId + "",
						"username" : "" + mobile + "",
						"cardType" : "" + cardType + "",
					}),
					success : function(response) {
						if (response.code != null && response.code == 'S00') {
							$('#errorMesgId').html("<strong id='errorMesgId' style='color:green'>"+ response.message + "</strong>")
							if(actionType!=null && actionType!='' && actionType == 'suspend'){
								$('#statusId').html("Locked");
								jQuery('#unlockDivId').css("display","block");
								jQuery('#lockDivId').css("display","none");
								$('#cardStatus'+rowCount).html('Locked');
							}else{
								$('#statusId').html("Active");
								$('#cardStatus'+rowCount).html('Active');
								jQuery('#unlockDivId').css("display","none")
								jQuery('#lockDivId').css("display","block")
							}
						} else {
							$('#errorMesgId').html("<strong id='errorMesgId' style='color:red'>"+ response.message + "</strong>")
						}
					}
				});
			}
			}else{
				console.log("dismissed");
			}
		}
		
		function isNumberKey(evt){
		    var charCode = (evt.which) ? evt.which : evt.keyCode
		    return !(charCode > 31 && (charCode < 48 || charCode > 57));
		}
		
	</script>

</body>
</html>
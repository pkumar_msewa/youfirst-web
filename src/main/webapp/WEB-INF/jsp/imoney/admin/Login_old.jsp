<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
	<title>youFirst</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/login/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/login/util.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/login/main.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/login/mystyle.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/login/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/login/font-awesome.css">
	
<!--===============================================================================================-->
</head>
<body style="background-color: #666666;">
			
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form" method="post"
									action="${pageContext.request.contextPath}/Master/Home" id="formId">
									
					<span class="login100-form-title p-b-43">
						Welcome to YouFirst Admin Portal
					</span>
					<p id="ajaxError" style="color:red"></p>
					 <c:if test="${not empty ERRORMSG}">
								<label id="errorMesg" style="margin-left: 20%; color:red" >${ERRORMSG}</label>
							</c:if>
					<input type="hidden" name="otp" id="otpId">
					
					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="text" name="username" id="emailId" required="required" placeholder="Email">
						<span class="focus-input100"></span>
					</div>
					
					
					<div class="wrap-input100 validate-input" data-validate="Password is required">
						<input class="input100" type="password" id="passId" name="password" placeholder="Password" maxlength="8">
						<span class="focus-input100"></span>
					</div>

					<div class="flex-sb-m w-full p-t-3 p-b-32">
						<!-- <div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
							<label class="label-checkbox100" for="ckb1">
								Remember me
							</label>
						</div> -->

						<div>
							<a href="#"  onclick="forgetPassword();" class="txt1" data-toggle="modal" data-target="#exampleModal">
								Forgot Password?
							</a>
						</div>
					</div>


					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="button"  id="loginBtn" 
							onclick="verifyLogin();">Log in</button>
					</div>
				</form>
				<div class="login100-more" style="background-image: url('${pageContext.request.contextPath}/resources/assets/images/bg-01.png');">
					<div class="Admin">
                         <div class="img_logo"></div>
						<p><h2>Welcome to YouFirst Admin Portal</h2></p>

					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="otpModal" class="modal fade" role="dialog" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content" style="margin-top: 40%; margin-left: 10%;  margin-right: 10%;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 align="center" class="modal-title" style="margin-left: 25%;">OTP Verification</h4>
				</div>
				<div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6" align="center" style="margin-left: 25%;" >
								<div class="form-group" align="center">
									 <input type="password" id="otpNo"
										placeholder="Enter OTP" class="form-control" maxlength="6"
										onkeypress="return isNumberKey(event);">
								</div>
								<p id="otpError" style="color: red"></p>
								<p id="otpMesg" style="color: red; margin-left: -3%;"></p>
							</div>
						</div>
						<div class="row">
						<div class="col-md-3"></div>
								<div class="col-md-3">
										<button class="btn btn-info" type="button"
										onclick="verifyLogin();">Resend</button>
								</div>
								<div class="col-md-3" align="right">
									<button class="btn btn-info" type="button"
										onclick="formSubmit();">Submit</button>
								</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</div>
	
<div id="fpModal" class="modal fade" role="dialog" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content" style="margin-top: 40%; margin-left: 10%;  margin-right: 10%;">
				<div class="modal-header">
				<h4 align="center" class="modal-title" style="margin-left: 25%;">Forgot Password</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-8" align="center" style="margin-left: 17%;" >
								<div class="form-group" align="center">
									 <input type="text" id="loginEmail"
										placeholder="Enter Email" class="form-control"">
								</div>
								<p id="fpError" style="color: red"></p>
							</div>
						</div>
						<div class="row">
						<div class="col-md-3"></div>
								<div class="col-md-3" align="center" style="margin-left: 10%;">
									<button class="btn btn-info" type="button" id="fpBtnId"
										onclick="getForgetPassOtp();">Submit</button>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>

<div id="fpModalOtp" class="modal fade" role="dialog" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content" style="margin-top: 30%; margin-left: 10%;  margin-right: 10%;">
		<div class="modal-header">
		<h4 align="center" class="modal-title" style="margin-left: 25%;">Forgot Password</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6" align="center" style="margin-left: 25%;" >
								<div class="form-group" align="center">
									 <input type="password" id="fpOtp"
										placeholder="Enter OTP" class="form-control" maxlength="6" 
										onkeypress="return isNumberKey(event);">
								</div>
								<p id="fpOtpError" style="color: red"></p>
								<div class="form-group" align="center">
									 <input type="password" id="epId"
										placeholder="Enter Password" class="form-control" maxlength="15">
								</div>
								<p id="fpEpError" style="color: red"></p>
								<div class="form-group" align="center">
									 <input type="password" id="cpId"
										placeholder="Confirm Password" class="form-control" maxlength="15">
								</div>
								<p id="fpCpError" style="color: red"></p>
								<p id="fpOtpMesg" style="color: green"></p>
							</div>
						</div>
						<div class="row">
						<div class="col-md-3"></div>
								<div class="col-md-3" align="right" style="margin-left: 12%;">
									<button class="btn btn-info" type="button" id="updatePassBtn"
										onclick="updatePassword();">Submit</button>
								</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</div>
	
	
<!--===============================================================================================-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/jquery-3.2.1.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/popper.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/bootstrap.js"></script>
	
<!--===============================================================================================-->
<!-- 	<script src="vendor/animsition/js/animsition.min.js"></script> -->
<!--===============================================================================================-->
<!-- 	<script src="vendor/bootstrap/js/popper.js"></script> -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/login/bootstrap.min.js"></script>
<!--===============================================================================================-->


  <script type="text/javascript">
    
    function verifyLogin(){
    	$("#otpMesg").html("");
    	var email= $('#emailId').val();
    	var password= $('#passId').val();
    	
    	var valid =true;
    	
    	if(email == null || email.length <=0){
    		valid = false;
    	}if(password == null || password.length <=0){
    		valid = false;
    	}
    	
    	if(valid){
    		$('#loginBtn').attr("disabled", "disabled");
    		$.ajax({
    			type : "POST",
	    		url:"${pageContext.request.contextPath}/Master/verifyLogin/",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"username" : "" + email + "",
	   				"password" : "" + password + "",
	   			}),
	   			success : function(response) {
	   				console.log(response);
	   				if(response.code == 'S00'){
	   					$('#otpMesg').html("OTP sent to Admin");
	   					$('#otpModal').modal('show');
	   				}else{
	   					$('#ajaxError').html(response.message);
	   					setTimeout(function(){
	   				    	$("#ajaxError").html("");
	   				    }, 4000);
	   				}
	   				$('#loginBtn').removeAttr("disabled", "disabled");
	   			}
    		});
    	}
    }
    
    
    function formSubmit(){
	var otp = $('#otpNo').val();
	var email= $('#emailId').val();
	var password= $('#passId').val();
	
	var valid =true;
	
	if(email == null || email.length <=0){
		$('#errorUser').html("Please enter username");
		valid = false;
	}if(password == null || password.length <=0){
		$('#errorPass').html("Please enter password");
		valid = false;
	}if(otp == null || otp.length <=0){
		$('#otpError').html("Please enter OTP");
		valid = false;
	}
	
	 var timeout = setTimeout(function(){
	    	$("#errorUser").html("");
	    	$("#errorPass").html("");
	    	$("#otpError").html("");
	    }, 4000);
	 
	if(valid){
		$('#otpId').val(otp);
		$('#formId').submit();
	}
	
    }
    
    
    function forgetPassword(){
    	$('#fpModal').modal('show');	
    }
    
    function getForgetPassOtp(){
    	$("#loginEmail").html("");
    	var email= $('#loginEmail').val();
    	var valid =true;
    	if(email == null || email.length <=0){
    		$('#fpError').html("Please enter a valid email")
    		valid = false;
    	}
    	if(valid){
    		$('#fpBtnId').attr("disabled", "disabled");
    		$.ajax({
    			type : "POST",
	    		url:"${pageContext.request.contextPath}/Master/ForgetPassword",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"username" : "" + email + ""
	   			}),
	   			success : function(response) {
	   				console.log(response);
	   				if(response.code == 'S00'){
	   					$('#fpOtpMesg').html("OTP sent to registered User ");
	   					$('#fpModal').modal('hide');	
	   					$('#fpModalOtp').modal('show');
	   				}else{
	   					$('#fpError').html(response.message);
// 	   					setTimeout(function(){
// 	   				    	$("#fpError").html("");
// 	   				    }, 3000);
	   				}
	   				$('#fpBtnId').removeAttr("disabled", "disabled");
	   			}
    		});
    	}
    	setTimeout(function(){
		    	$("#fpError").html("");
		    	$("#fpOtpMesg").html("");
		    }, 3000);
    }

    function updatePassword(){
    	var email= $('#loginEmail').val();
    	console.log(email);
    	var otp = $('#fpOtp').val();
    	var enterPass= $('#epId').val();
    	var confPass = $('#cpId').val();
    	
    	var valid =true;
    	if(otp == null || otp.length < 6){
    		$('#fpOtpError').html("Please enter valid Otp ")
    		valid = false;
    	}if(enterPass == null || enterPass.length <=0){
    		$('#fpEpError').html("Please enter password.")
    		valid = false;
    	}if(confPass == null || confPass.length <=0){
    		$('#fpCpError').html("Please confirm password.")
    		valid = false;
    	}else if(enterPass != confPass){
    		$('#fpCpError').html("Password mismatch.")
    		valid = false;
    	}
    	
    	
    	if(valid){
    		$('#updatePassBtn').attr("disabled", "disabled");
    		$.ajax({
    			type : "POST",
	    		url:"${pageContext.request.contextPath}/Master/ForgetPasswordWithOtp",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"username" : "" + email + "",
	   				"otp" : "" + otp + "",
	   				"password" : "" + enterPass + ""
	   			}),
	   			success : function(response) {
	   				console.log(response);
	   				if(response.code == 'S00'){
	   					$('#ajaxError').html("Password update successfully");
	   					$('#fpModalOtp').modal('hide');
	   					$('#fpModal').modal('hide');
	   					$('#fpOtp').val("");
	   			    	$('#epId').val("");
	   			    	 $('#cpId').val("");
	   					var otp = $('#fpOtp').val();
	   			    	var enterPass= $('#epId').val();
	   			    	var confPass = $('#cpId').val();
	   				}else{
	   					$('#fpCpError').html(response.message);
	   					setTimeout(function(){
	   				    	$("#fpError").html("");
	   				    }, 3000);
	   				}
	   				$('#updatePassBtn').removeAttr("disabled", "disabled");
	   			}
    		});
    	}setTimeout(function(){	
    		$('#fpOtpError').html("")
    		$('#fpEpError').html("")
    		$('#fpCpError').html("")
		    }, 3000);
    }
    
    </script>

<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

</body>
</html>
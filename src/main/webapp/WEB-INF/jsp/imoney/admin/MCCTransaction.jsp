
<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
<meta name="keywords"
	content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
<meta name="author" content="pixelstrap">
<link rel="icon" href="${pageContext.request.contextPath}/resources/assets/images/favicon.png" type="image/x-icon" />
<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets/images/favicon.png"
	type="image/x-icon" />
<title>Home</title>

<!--Google font-->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700"
	rel="stylesheet">

<!-- Font Awesome -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.css">

<!-- ico-font -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/icofont/icofont.css">

<!-- Themify icon -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/themify-icons/themify-icons.css">
<!-- Flag icon -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/flag-icon.css">

<!-- prism css -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/prism.css">

<!-- Owl css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/owlcarousel.css">

<!-- Bootstrap css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.css">

<!-- App css -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/style.css">

<!-- Responsive css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/responsive.css">

<!-- linearicons css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/linearicons/style.css">
<!-- mystyle  css-->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/mystyle.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>

<style>
	/* The Modal (background) */
	.modal {
	    display: none; /* Hidden by default */
	    position: fixed; /* Stay in place */
	    padding-top: 70px; /* Location of the box */
	    
	    left: 0;
	    top: 0;
	    width: 100%; /* Full width */
	    height: 100%; /* Full height */
	    overflow: auto; /* Enable scroll if needed */
	    background-color: rgb(0,0,0); /* Fallback color */
	    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}
	
	/* Modal Content */
	.modal-content {
	    background-color: #fefefe;
	    margin: auto;
	    padding: 20px;
	    padding-left: 60px;
	    
	    border: 1px solid #888;
	    width: 80%;
	}
	
	#InfoModalID .modal-content {
		padding: 0;
		width: 100%;
	}
	
	/* The Close Button */
	.close {
	    color: #aaaaaa;
	    float: right;
	    font-size: 28px;
	    font-weight: bold;
	}
	
	.close:hover,
	.close:focus {
	    color: #000;
	    text-decoration: none;
	    cursor: pointer;
	}
	
	/* .modal-backdrop.show{
	z-index:0;
	} */
	
	.sidenav {
  height: 89%;
  margin-top: 5%;
  width: 0;
  position: fixed;
  z-index: 4;
  top: 0;
  right: 0;
  background-color: white;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 60px;
}

.sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}

.sidenav a:hover {
  color: #f1f1f1;
}

.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}

@media (min-width: 992px)
.modal-lg {
    max-width: 900px;
}

.linksBlock {
	border: 1px solid #787878;
	height: 85px;
	margin: 0 auto;
	padding: 17px 0;
}

.relatedLinks a {
	color: #00508f;
}
	
	
	</style>

</head>
<body>

	<!-- Loader starts -->
	<div class="loader-wrapper">
		<div class="loader bg-white">
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
			<h4>
				Have a great day at work today <span>&#x263A;</span>
			</h4>
		</div>
	</div>
	<!-- Loader ends -->

	<!--page-wrapper Start-->
	<div class="page-wrapper">
		<jsp:include page="/WEB-INF/jsp/imoney/admin/header.jsp"></jsp:include>
		<div class="page-body-wrapper">
			<jsp:include page="/WEB-INF/jsp/imoney/admin/leftMenu.jsp"></jsp:include>

			<div class="page-body">
				<!-- Container-fluid starts -->
				<div class="page-body">
				<!-- Container-fluid starts -->
				<div class="container-fluid">
				
					<div class="row">
						<div class="col-lg-12">
							<div class="card height-equal equal-height-lg"
								style="margin-top: 15px;">
									<div class="card-header">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-6">
													<h5>Customer Transaction</h5>
												</div>
												<div class="col-md-6" align="right">
												<strong>Customer Mobile: ${customerId}</strong>
												</div>
											</div>
										</div>
									</div>
									<div class="card-body">
								<input type="hidden"  id="paginationId" value="1">

										<div class="row" style="display:block">
											<div class="tabble" style="overflow-x: scroll;">
												<table id="example"
													class="table table-striped table-bordered dt-responsive nowrap"
													style="width: 100%">
													<thead>
														<tr>
															<th>S.&nbsp; No</th>
															<th>Type</th>
															<th>Amount</th>
															<th>Balance</th>
															<!-- <th>Currency</th> -->
															<th>Description</th>
															<th>Indicator</th>
															<th>name</th>
															<th>Status</th>
															<th>Date</th>
														</tr>
													</thead>
													<tbody style="height: 100px; overflow-y: auto;">
													<c:if test="${userStatement!=null}">
														<c:forEach items="${userStatement}" var="list"
															varStatus="loopCounter">
															<tr>
																<td>${loopCounter.count}</td>
																<td>${list.type}</td>
																<td>${list.amount}</td>
																<td>${list.balance}</td>
																<%-- <td>${list.currency}</td> --%>
																<td>${list.description}</td>
																<td>${list.indicator}</td>
																<td>${list.name}</td>
																<td>${list.status}</td>
																<td>${list.date}</td>
															</tr>
														</c:forEach>
														</c:if>
														<c:if test="${userStatement == null}">
														<tr>
																<td>No Data found</td>
																</tr>
														</c:if>
													</tbody>
												</table>
											</div>
										</div>
									</div>
							</div>
						</div>
					</div>
				</div>

			</div>

			</div>
		</div>
		<!-- Container-fluid Ends -->
	</div>

	
	
	<!--page-wrapper Ends-->

	<!-- latest jquery-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/popper.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/bootstrap.js"></script>
	<!-- Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/chart/Chart.min.js"></script>
	<!-- Morris Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/raphael.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/morris.js"></script>
	<!-- owlcarousel js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/owlcarousel/owl.carousel.js"></script>
	<!-- Sidebar jquery-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/sidebar-menu.js"></script>
	<!--Sparkline  Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/sparkline/sparkline.js"></script>
	<!--Height Equal Js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/height-equal.js"></script>
	<!-- prism js -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/prism/prism.min.js"></script>
	<!-- clipboard js -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/clipboard/clipboard.min.js"></script>
	<!-- custom card js  -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/custom-card/custom-card.js"></script>
	<!-- Theme js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/script.js"></script>
	<!-- <script src="${pageContext.request.contextPath}/resources/assets/js/theme-customizer/customizer.js"></script> -->
	<!-- <script src="${pageContext.request.contextPath}/resources/assets/js/chat-sidebar/chat.js"></script> -->
	<!-- Counter js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.waypoints.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.counterup.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/counter-custom.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/notify/bootstrap-notify.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/notify/index.js"></script>
	
<!-- 	=== pagination=== -->
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	


<script type="text/javascript">
		$(document).ready(function(){
// 			getTransactionList(0);
			var table = $('#example').DataTable({
				lengthChange : false,
				buttons : [ 'excel', 'csv' ]
			});

			table.buttons().container().appendTo(
					'#example_wrapper .col-sm-6:eq(0)');
    	});
		$(function() {
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		    function cb(start, end) {
		        $('#reportrange').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
		    }
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'DD/MM/YYYY'
		        },
		        dateLimit: {
		            "days": 180
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		    cb(start, end);
		});
		
		function getTransactionList(page) {
    		var trHtml='';
    		var customerId =  '${customerId}';
    		var pagination = $('#paginationId').val();
    		console.log(customerId);
    		$.ajax({
    			type : "POST",
	    		url:"${pageContext.request.contextPath}/Master/mccTransaction",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"page" : page,
	   				"size" : 99,
	   				"customerId" : customerId,
	   			}),
	   			success : function(response) {
					$('#genericTransactionList').empty();
					console.log(response)
					if (response.details !=null && response.details.length > 0) {
						displayTableWithData(response);
						$(function () {
	   	   					$('#paginationn').twbsPagination({
	    					 	totalPages: response.totalPage,
	    					 	visiblePages: 10,
	    		         	 	onPageClick: function (event, page) {
	    		         	 		if(pagination != 1){
	    		         	 			getTransactionList(page-1);
	    		         	 		}else{
					                $('#paginationId').val(2);
	    		         	 		}
	    			         	}
				 			}); 
		   				});
					} else {
						displayNoneData();
					}
	   			}
    		});
    	}
		
		function getTransactionByFilter(page) {
    		var trHtml='';
    		var daterange = $('#reportrange').val();
    		var serviceType = $('#serviceTypeId').val();
    		var customerId = '${customerId}';
    		$.ajax({
    			type : "POST",
	    		url:"${pageContext.request.contextPath}/Master/mccTransaction",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"page" : page,
	   				"size" : 99,
	   				"daterange" :daterange,
	   				"customerId" : customerId,
	   			}),
	   			success : function(response) {
					$('#genericTransactionList').empty();
					console.log("response:"+response)
					if (response.details !=null && response.details.length > 0) {
						displayTableWithData(response);
						$(function () {
							$('#paginationn').twbsPagination('destroy');
	   	   					$('#paginationn').twbsPagination({
	    					 	totalPages: response.totalPage,
	    					 	visiblePages: 10,
	    		         	 	onPageClick: function (event, page) {
	    		         	 		if($('#paginationId').val() !=1){
	    		         	 			getFilterData(page-1);
	    		         	 		}else{
					                $('#paginationId').val(2);
	    		         	 		}
	    			         	}
				 			});
		   				});
					} else {
						displayNoneData();
					}
	   			}
    		});
    	}
		
		
		function getFilterData(page) {
    		var trHtml='';
    		var daterange = $('#reportrange').val();
    		var serviceType = $('#serviceTypeId').val();
    		var customerId = '${customerId}';
    		$.ajax({
    			type : "POST",
	    		url:"${pageContext.request.contextPath}/Master/mccTransaction",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"page" : page,
	   				"size" : 99,
	   				"daterange" :daterange,
	   				"customerId" : customerId,
	   			}),
	   			success : function(response) {
					$('#genericTransactionList').empty();
					console.log("response:"+response)
					if (response.details !=null && response.details.length > 0) {
						displayTableWithData(response);
					} else {
						displayNoneData();
					}
	   			}
    		});
    	}
		
		
		function displayTableWithData(response){
			var data;
			for (var i = 0; i < response.details.length; i++) {
				data = response.details[i];
				console.log(data);
				trHtml = '<tr>';
				trHtml = trHtml + '<td>' +(i+1)+ '</td>';
				trHtml = trHtml + '<td>' +data.TransactionDate+ '</td>';
				trHtml = trHtml + '<td>'+data.Description+'</td>';
				trHtml = trHtml + '<td>' +data.TransAmt+ '</td>';
				trHtml = trHtml + '<td>' +data.DrCrIndicator+ '</td>';
				trHtml = trHtml + '<td>' +data.MccCode+ '</td>';
				trHtml = trHtml + '<td>' +data.TerminalId+ '</td>';
				trHtml = trHtml + '<td>' +data.MerchantId+ '</td>';
				trHtml = trHtml + '<td>' +data.MerchantName+ '</td>';
				trHtml = trHtml + '<td>' +data.ProductCode+ '</td>';
				trHtml = trHtml + '<td>' +data.TransactionCode+ '</td>';
				trHtml = trHtml + '<td>' +data.TransRefNo+ '</td>';
				trHtml = trHtml + '<td>' +data.TransStatus+ '</td>';
				trHtml = trHtml + '</tr>';
				$('#genericTransactionList').append(trHtml);
			}
		}
		function displayNoneData(){
			$('#paginationn').twbsPagination('destroy');
			trHtml = '<tr>';
			trHtml = trHtml + '<td></td>';
			trHtml = trHtml + '<td></td>';
			trHtml = trHtml + '<td></td>';
			trHtml = trHtml + '<td></td>';
			trHtml = trHtml + '<td></td>';
			trHtml = trHtml + '<td>No data found</td>';
			trHtml = trHtml + '<td></td>';
			trHtml = trHtml + '<td></td>';
			trHtml = trHtml + '<td></td>';
			trHtml = trHtml + '<td></td>';
			trHtml = trHtml + '<td></td>';
			trHtml = trHtml + '</tr>';
			$('#genericTransactionList').append(trHtml);
		}
		
		
		
		function formatDate(d){
        	d=new Date(d);
        	var month = d.getMonth();
        	var day = d.getDate();
        	month = month + 1;
        	month = month + "";
        	if (month.length == 1)
        	{
        	month = "0" + month;
        	}
        	day = day + "";
        	if (day.length == 1)
        	{
        	day = "0" + day;
        	}
        	return d.getFullYear()+ '-' +month + '-' + day ;
       	}
	</script>

<script>
function submitDwonloadForm(){
	var daterange = $('#reportrange').val();
	var customerId = '${customerId}';
	$('#downloadDateId').val(daterange);
	$('#downloadCustomerId').val(customerId);
	var downloadDate=	$('#downloadDateId').val();
	var downloadCustomerId=	$('#downloadCustomerId').val();
	if(downloadDate!='' && downloadCustomerId !=''){
		$('#downloadFormId').submit();
	}
}
</script>

</body>
</html>
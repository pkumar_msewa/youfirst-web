<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>YouFirst</title>
    <link rel="icon" href="${pageContext.request.contextPath}/resources/assets/images/favicon.png" type="image/png">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/login/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/login/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/login/iofrm-style.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/login/iofrm-theme8.css">
</head>
<body>
    <div class="form-body">
        <!-- <div class="website-logo">
            <a href="index.html">
                <div class="logo">
                    <img class="logo-size" src="images/logo-light.svg" alt="">
                </div>
            </a>
        </div> -->
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">
                    <h5>Welcome to YouFirst Admin Portal</h5>
                   <!--  <p>Access to the most powerfull tool in the entire design and web industry.</p> -->
                    <img src="${pageContext.request.contextPath}/resources/assets/images/graphic4.svg" alt="">
                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <div class="website-logo-inside">
                            <a href="login.html">
                                <div class="logo_s">
                                   <img class="logo_s" src="${pageContext.request.contextPath}/resources/assets/images/youfirst.png" alt="logo" style="width: 180px;">
                                </div>
                            </a>
                        </div>
                        <div class="page-links">
                            <a href="login.html" class="active">Login</a><!-- <a href="register8.html">Register</a> -->
                        </div>
                       <form method="post" action="${pageContext.request.contextPath}/Master/Home" id="formId" >
                       <p id="ajaxError" style="color:red"></p>
					 <c:if test="${not empty ERRORMSG}">
								<label id="errorMesg" style="color:red; float: right" >${ERRORMSG}</label>
							</c:if>
					<input type="hidden" name="otp" id="otpId">
                            <input class="form-control" type="text" name="username" placeholder="Email ID" id="emailId">
                            <input class="form-control" type="password" name="password" placeholder="Password"  maxlength="8" minlength="6"id="passId">
                            <div class="form-button">
                                <button type="button" class="ibtn" id="loginBtn" 
							onclick="verifyLogin();">Login</button> <a href="javascript:void(0)" onclick="forgetPassword()">Forget password?</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<div id="otpModal" class="modal fade" role="dialog"
		data-backdrop="static">
		<div class="modal-dialog modal-sm modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">OTP Verification</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div>
					<div class="modal-body">
						<div class="content_wrapper">
							<div class="form-group">
								<input type="password" id="otpNo" placeholder="Enter OTP"
									class="form-control" maxlength="6"
									onkeypress="return isNumberKey(event);">
								<p id="otpError" class="error"></p>
								<p id="otpMesg" class="error"></p>
							</div>
							<div class="form-group text-center">
								<button class="btn btn-warning btn-sm" type="button"
									onclick="verifyLogin();">Resend</button>
								<button class="btn btn-primary btn-sm" type="button"
									onclick="formSubmit();">Submit</button>
							</div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>

	<div id="fpModal" class="modal fade" role="dialog"
		data-backdrop="static">
		<div class="modal-dialog modal-sm modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Forgot Password</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div>
					<div class="modal-body">
						<div class="content_wrapper">
							<div class="form-group">
									<input type="text" id="loginEmail" placeholder="Enter Email"
										class="form-control">
								<p id="fpError" class="error"></p>
								</div>
								<div class="form-group text-center">
								<button class="btn btn-primary btn-sm" type="button" id="fpBtnId"
									onclick="getForgetPassOtp();">Submit</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div id="fpModalOtp" class="modal fade" role="dialog"
		data-backdrop="static">
		<div class="modal-dialog modal-sm modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Generate New Password</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div>
					<div class="modal-body">
						<div class="content_wrapper">
							<div class="form-group">
								<input type="password" id="fpOtp" placeholder="Enter OTP"
									class="form-control" maxlength="6"
									onkeypress="return isNumberKey(event);">
								<p id="fpOtpError" class="error"></p>
							</div>
							<div class="form-group">
								<input type="password" id="epId" placeholder="Enter Password"
									class="form-control" maxlength="15">
								<p id="fpEpError" class="error"></p>
							</div>
							<div class="form-group">
								<input type="password" id="cpId" placeholder="Confirm Password"
									class="form-control" maxlength="15">
								<p id="fpCpError" class="error"></p>
							</div>
							<div class="text-center">
								<button class="btn btn-primary btn-sm" type="button"
									id="updatePassBtn" onclick="updatePassword();">Submit</button>
							</div>
							<p id="fpOtpMesg" class="text-success"></p>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>


	<script src="${pageContext.request.contextPath}/resources/assets/js/login/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/js/login/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/js/login/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/js/login/main.js"></script>

<script type="text/javascript">
    
    function verifyLogin(){
    	$("#otpMesg").html("");
    	var email= $('#emailId').val();
    	var password= $('#passId').val();
    	
    	var valid =true;
    	
    	if(email == null || email.length <=0){
    		valid = false;
    	}if(password == null || password.length <=0){
    		valid = false;
    	}
    	
    	if(valid){
    		$('#loginBtn').attr("disabled", "disabled");
    		$.ajax({
    			type : "POST",
	    		url:"${pageContext.request.contextPath}/Master/verifyLogin/",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"username" : "" + email + "",
	   				"password" : "" + password + "",
	   			}),
	   			success : function(response) {
	   				console.log(response);
	   				if(response.code == 'S00'){
	   					$('#otpMesg').html("OTP sent to Admin");
	   					$('#otpModal').modal('show');
	   				}else{
	   					$('#ajaxError').html(response.message);
	   					setTimeout(function(){
	   				    	$("#ajaxError").html("");
	   				    }, 4000);
	   				}
	   				$('#loginBtn').removeAttr("disabled", "disabled");
	   			}
    		});
    	}
    }
    
    
    function formSubmit(){
	var otp = $('#otpNo').val();
	var email= $('#emailId').val();
	var password= $('#passId').val();
	
	var valid =true;
	
	if(email == null || email.length <=0){
		$('#errorUser').html("Please enter username");
		valid = false;
	}if(password == null || password.length <=0){
		$('#errorPass').html("Please enter password");
		valid = false;
	}if(otp == null || otp.length <=0){
		$('#otpError').html("Please enter OTP");
		valid = false;
	}
	
	 var timeout = setTimeout(function(){
	    	$("#errorUser").html("");
	    	$("#errorPass").html("");
	    	$("#otpError").html("");
	    }, 4000);
	 
	if(valid){
		$('#otpId').val(otp);
		$('#formId').submit();
	}
	
    }
    
    
    function forgetPassword(){
    	$('#fpModal').modal('show');	
    }
    
    function getForgetPassOtp(){
    	$("#loginEmail").html("");
    	var email= $('#loginEmail').val();
    	var valid =true;
    	if(email == null || email.length <=0){
    		$('#fpError').html("Please enter a valid email")
    		valid = false;
    	}
    	if(valid){
    		$('#fpBtnId').attr("disabled", "disabled");
    		$.ajax({
    			type : "POST",
	    		url:"${pageContext.request.contextPath}/Master/ForgetPassword",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"username" : "" + email + ""
	   			}),
	   			success : function(response) {
	   				console.log(response);
	   				if(response.code == 'S00'){
	   					$('#fpOtpMesg').html("OTP sent to registered User ");
	   					$('#fpModal').modal('hide');	
	   					$('#fpModalOtp').modal('show');
	   				}else{
	   					$('#fpError').html(response.message);
// 	   					setTimeout(function(){
// 	   				    	$("#fpError").html("");
// 	   				    }, 3000);
	   				}
	   				$('#fpBtnId').removeAttr("disabled", "disabled");
	   			}
    		});
    	}
    	setTimeout(function(){
		    	$("#fpError").html("");
		    	$("#fpOtpMesg").html("");
		    }, 3000);
    }

    function updatePassword(){
    	var email= $('#loginEmail').val();
    	console.log(email);
    	var otp = $('#fpOtp').val();
    	var enterPass= $('#epId').val();
    	var confPass = $('#cpId').val();
    	
    	var valid =true;
    	if(otp == null || otp.length < 6){
    		$('#fpOtpError').html("Please enter valid Otp ")
    		valid = false;
    	}if(enterPass == null || enterPass.length <=0){
    		$('#fpEpError').html("Please enter password.")
    		valid = false;
    	}if(confPass == null || confPass.length <=0){
    		$('#fpCpError').html("Please confirm password.")
    		valid = false;
    	}else if(enterPass != confPass){
    		$('#fpCpError').html("Password mismatch.")
    		valid = false;
    	}
    	
    	
    	if(valid){
    		$('#updatePassBtn').attr("disabled", "disabled");
    		$.ajax({
    			type : "POST",
	    		url:"${pageContext.request.contextPath}/Master/ForgetPasswordWithOtp",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"username" : "" + email + "",
	   				"otp" : "" + otp + "",
	   				"password" : "" + enterPass + ""
	   			}),
	   			success : function(response) {
	   				console.log(response);
	   				if(response.code == 'S00'){
	   					$('#ajaxError').html("Password update successfully");
	   					$('#fpModalOtp').modal('hide');
	   					$('#fpModal').modal('hide');
	   					$('#fpOtp').val("");
	   			    	$('#epId').val("");
	   			    	 $('#cpId').val("");
	   					var otp = $('#fpOtp').val();
	   			    	var enterPass= $('#epId').val();
	   			    	var confPass = $('#cpId').val();
	   				}else{
	   					$('#fpCpError').html(response.message);
	   					setTimeout(function(){
	   				    	$("#fpError").html("");
	   				    }, 3000);
	   				}
	   				$('#updatePassBtn').removeAttr("disabled", "disabled");
	   			}
    		});
    	}setTimeout(function(){	
    		$('#fpOtpError').html("")
    		$('#fpEpError').html("")
    		$('#fpCpError').html("")
		    }, 3000);
    }
    
    </script>

<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>
</body>


</html>





    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
        <meta name="keywords" content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
        <meta name="author" content="pixelstrap">
        <link rel="icon" href="${pageContext.request.contextPath}/resources/assets/images/favicon.png" type="image/x-icon"/>
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets/images/favicon.png" type="image/x-icon"/>
        <title>Home</title>

        <!--Google font-->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">

        <!-- Font Awesome -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.css">

        <!-- ico-font -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/vendor/icofont/icofont.css">

        <!-- Themify icon -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/vendor/themify-icons/themify-icons.css">

        <!-- Flag icon -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/flag-icon.css">

        <!-- prism css -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/prism.css">

        <!-- Owl css -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/owlcarousel.css">

        <!-- Bootstrap css -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.css">

        <!-- App css -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/style.css">

        <!-- Responsive css -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/responsive.css">

        <!-- linearicons css -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/linearicons/style.css">
         <!-- mystyle  css-->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/mystyle.css">
    </head>
    <body>

    <!-- Loader starts -->
    <div class="loader-wrapper">
        <div class="loader bg-white">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <h4>Have a great day at work today <span>&#x263A;</span></h4>
        </div>
    </div>
    <!-- Loader ends -->

    <!--page-wrapper Start-->
    <div class="page-wrapper">

      <jsp:include page="/WEB-INF/jsp/imoney/admin/header.jsp"></jsp:include>
		<div class="page-body-wrapper">
			<jsp:include page="/WEB-INF/jsp/imoney/admin/leftMenu.jsp"></jsp:include>

                        <div class="page-body">
                        <!-- Container-fluid starts -->
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card height-equal equal-height-lg" style="margin-top: 15px;">
                                        <div class="card-header">
                                            <h5>Prefund Transaction</h5>
                                            <div class="card-header-right">
                                                <a href="#" class="icon-bar"><i class="lnr lnr-magnifier"></i></a> 
                                                <a href="#" class="icon-bar"><i class="lnr lnr-download"></i></a>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                            <div class="col-lg-12">
                                            <!-- <form class="form-inline">
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                    <input type="text" class="form-control" id="user" placeholder="Admin ID" name="Name">
                                                    </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                   <div class="form-group">
                                                   <input type="text" class="form-control" id="user" placeholder="First Name" name="Name">
                                                   </div>  
                                                   </div>
                                                    <div class="col-lg-3">
                                                     <div class="form-group">   
                                                     <input type="email" class="form-control" id="Email" placeholder="Middle Name" name="Email">
                                                     </div>
                                                     </div>
                                                    <div class="col-lg-3">
                                                     <div class="form-group">   
                                                     <input type="email" class="form-control" id="Email" placeholder="Last Name" name="Email">
                                                    </div>
                                                     </div>
                                                    <div class="col-lg-3">
                                                    <div class="form-group">
                                                    <input type="text" class="form-control" id="user" placeholder="Email" name="Name">
                                                    </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                    <div class="form-group">
                                                    <input class="datepicker form-control" data-date-format="mm/dd/yyyy" placeholder="Start Date">  
                                                     </div> 
                                                    </div>
                                                    <div class="col-lg-3">
                                                    <div class="form-group">
                                                    <input class="datepicker form-control" data-date-format="mm/dd/yyyy" placeholder="End Date">  
                                                     </div> 
                                                    </div>

                                                    
                                            </form>   --> 
                                            </div>
                                            </div>
                                            <section>
                                                <table class="table">
                                                    <thead>
                                                                <tr>
                                                                    <th>Name</th>
                                                                    <th>Date-Added</th>
                                                                    <th>Transaction Type Name</th>
                                                                    <th>Type</th>
                                                                    <th>Sub Type</th>
                                                                    <th>Amount</th>
                                                                    <th>Old Topup-Prefund value</th>
                                                                    <th>New-topup-Prefund</th>
                                                                 </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr  onclick="openModel();">
                                                                    <td>IMoney</td>
                                                                    <td>2018-11-06</td>
                                                                    <td>Purchase</td>
                                                                    <td>Fund</td>
                                                                    <td>debit</td>
                                                                    <td>-51.00</td>
                                                                    <td>256.141.50</td>
                                                                    <td>256.141.50</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>iPaisa</td>
                                                                    <td>2018-11-06</td>
                                                                    <td>Purchase</td>
                                                                    <td>Fund</td>
                                                                    <td>debit</td>
                                                                    <td>-51.00</td>
                                                                    <td>256.141.50</td>
                                                                    <td>256.141.50</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>iPaisa</td>
                                                                    <td>2018-11-06</td>
                                                                    <td>Purchase</td>
                                                                    <td>Fund</td>
                                                                    <td>debit</td>
                                                                    <td>-51.00</td>
                                                                    <td>256.141.50</td>
                                                                    <td>256.141.50</td>
                                                                </tr>
                                                            </tbody>
                                                </table>

                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- Container-fluid Ends -->
            </div>
            <!--Page Body Ends-->
        </div>
        <!--Page Body Ends-->

    </div>

    <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
              <h4 class="modal-title">user details</h4>
            </div>
            <div class="modal-body">
            
            <div class="row">
                        
                                                
                                    <div class="col-sm-4 ">
                            <div class="card">
                                <div class="card-body">
                                    <div class="stat-widget-dashboard">
                                        <div class="media">
                                            <img class="mr-3" src="${pageContext.request.contextPath}/resources/assets/images/dashboard-icons/document.png" alt="Generic placeholder image">
                                            <div class="media-body text-right">
                                                <h4 class="mt-0 counter font-primary">2569</h4>
                                                <span>New projects</span>
                                            </div>
                                        </div>
                                        <div class="dashboard-chart-container">
                                            <div id="line-chart-sparkline-dashboard1" class="flot-chart-placeholder line-chart-sparkline"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="stat-widget-dashboard">
                                        <div class="media">
                                            <img class="mr-3" src="${pageContext.request.contextPath}/resources/assets/images/dashboard-icons/document.png" alt="Generic placeholder image">
                                            <div class="media-body text-right">
                                                <h4 class="mt-0 counter font-primary">2569</h4>
                                                <span>New projects</span>
                                            </div>
                                        </div>
                                        <div class="dashboard-chart-container">
                                            <div id="line-chart-sparkline-dashboard1" class="flot-chart-placeholder line-chart-sparkline"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="stat-widget-dashboard">
                                        <div class="media">
                                            <img class="mr-3" src="${pageContext.request.contextPath}/resources/assets/images/dashboard-icons/document.png" alt="Generic placeholder image">
                                            <div class="media-body text-right">
                                                <h4 class="mt-0 counter font-primary">2569</h4>
                                                <span>New projects</span>
                                            </div>
                                        </div>
                                        <div class="dashboard-chart-container">
                                            <div id="line-chart-sparkline-dashboard1" class="flot-chart-placeholder line-chart-sparkline"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                     </div>
      

            



            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
          
        </div>
      </div>
    <!--page-wrapper Ends-->

    <!-- latest jquery-->
    <script src="${pageContext.request.contextPath}/resources/assets/js/jquery-3.2.1.min.js" ></script>

    <!-- Bootstrap js-->
    <script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/popper.min.js" ></script>
    <script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/bootstrap.js" ></script>

    <!-- Chart JS-->
    <script src="${pageContext.request.contextPath}/resources/assets/js/chart/Chart.min.js"></script>

    <!-- Morris Chart JS-->
    <script src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/raphael.js"></script>
    <script src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/morris.js"></script>

    <!-- owlcarousel js-->
    <script src="${pageContext.request.contextPath}/resources/assets/js/owlcarousel/owl.carousel.js" ></script>

    <!-- Sidebar jquery-->
    <script src="${pageContext.request.contextPath}/resources/assets/js/sidebar-menu.js" ></script>

    <!--Sparkline  Chart JS-->
    <script src="${pageContext.request.contextPath}/resources/assets/js/sparkline/sparkline.js"></script>

    <!--Height Equal Js-->
    <script src="${pageContext.request.contextPath}/resources/assets/js/height-equal.js"></script>

    <!-- prism js -->
    <script src="${pageContext.request.contextPath}/resources/assets/js/prism/prism.min.js"></script>

    <!-- clipboard js -->
    <script src="${pageContext.request.contextPath}/resources/assets/js/clipboard/clipboard.min.js" ></script>

    <!-- custom card js  -->
    <script src="${pageContext.request.contextPath}/resources/assets/js/custom-card/custom-card.js" ></script>

    <!-- Theme js-->
    <script src="${pageContext.request.contextPath}/resources/assets/js/script.js" ></script>
    <!-- <script src="${pageContext.request.contextPath}/resources/assets/js/theme-customizer/customizer.js"></script> -->
    <!-- <script src="${pageContext.request.contextPath}/resources/assets/js/chat-sidebar/chat.js"></script> -->
    <script src="${pageContext.request.contextPath}/resources/assets/js/dashboard-default.js" ></script>

    <!-- Counter js-->
    <script src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.waypoints.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.counterup.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/assets/js/counter/counter-custom.js"></script>

    <script src="${pageContext.request.contextPath}/resources/assets/js/notify/bootstrap-notify.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/assets/js/notify/index.js"></script>



    <script >

    function openModel(){

//         $('#myModal').modal('show');
    }



    </script>

    </body>
    </html>
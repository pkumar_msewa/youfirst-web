
<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
<meta name="keywords"
	content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
<meta name="author" content="pixelstrap">
<link rel="icon" href="${pageContext.request.contextPath}/resources/assets/images/favicon.png" type="image/x-icon" />
<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets/images/favicon.png"
	type="image/x-icon" />
<title>Home</title>

<!--Google font-->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700"
	rel="stylesheet">

<!-- Font Awesome -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.css">

<!-- ico-font -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/icofont/icofont.css">

<!-- Themify icon -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/themify-icons/themify-icons.css">
<!-- Flag icon -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/flag-icon.css">

<!-- prism css -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/prism.css">

<!-- Owl css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/owlcarousel.css">

<!-- Bootstrap css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.css">

<!-- App css -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/style.css">

<!-- Responsive css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/responsive.css">

<!-- linearicons css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/linearicons/style.css">
<!-- mystyle  css-->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/mystyle.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<style>
	/* The Modal (background) */
	.modal {
	    display: none; /* Hidden by default */
	    position: fixed; /* Stay in place */
	    padding-top: 70px; /* Location of the box */
	    
	    left: 0;
	    top: 0;
	    width: 100%; /* Full width */
	    height: 100%; /* Full height */
	    overflow: auto; /* Enable scroll if needed */
	    background-color: rgb(0,0,0); /* Fallback color */
	    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}
	
	/* Modal Content */
	.modal-content {
	    background-color: #fefefe;
	    margin: auto;
	    padding: 20px;
	    padding-left: 60px;
	    
	    border: 1px solid #888;
	    width: 80%;
	}
	
	#InfoModalID .modal-content {
		padding: 0;
		width: 100%;
	}
	
	/* The Close Button */
	.close {
	    color: #aaaaaa;
	    float: right;
	    font-size: 28px;
	    font-weight: bold;
	}
	
	.close:hover,
	.close:focus {
	    color: #000;
	    text-decoration: none;
	    cursor: pointer;
	}
	
	.modal-backdrop.show{
	z-index:0;
	}
	
	#editBtnId {
		padding: 2px 10px;
	}
	
	</style>

</head>
<body>

	<!-- Loader starts -->
	<div class="loader-wrapper">
		<div class="loader bg-white">
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
			<h4>
				Have a great day at work today <span>&#x263A;</span>
			</h4>
		</div>
	</div>
	<!-- Loader ends -->

	<!--page-wrapper Start-->
	<div class="page-wrapper">
		<jsp:include page="/WEB-INF/jsp/imoney/admin/header.jsp"></jsp:include>
		<div class="page-body-wrapper">
			<jsp:include page="/WEB-INF/jsp/imoney/admin/leftMenu.jsp"></jsp:include>

			<div class="page-body">
				<!-- Container-fluid starts -->
				<div class="container-fluid">
					<div class="row"> 	
						<div class="col-lg-12">
							<div class="card height-equal equal-height-lg"
								style="margin-top: 15px;">

								<div class="card-header">
										<h5>Transactions</h5>
								</div>

								<form id="downloadFormId"
									action="${pageContext.request.contextPath}/Download/DownloadTransaction/txn" method="POST">
								<input type="hidden" name="daterange"  id="downloadDateId">
								<input type="hidden" name="serviceTypeId"  id="downloadServiceId">
								</form>


								<div class="card-body">
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-3 col-sm-3 col-xs-3">
												<input type="hidden" id="paginationId" value="1">
													<label>Select Daterange*</label><br />
													<div style="cursor: pointer;">
														<input id="reportrange" name="toDate" class="form-control"
															style="background-color: white" readonly="readonly" />
													</div>
											</div>
											<div class="col-md-2.5">
												<div class="form-group">
													<label>Select Services*</label> <select
														class="form-control" id="serviceTypeId" name=""
														required>
														<option value="All">All</option>
														<c:if test="${serviceType !=null }">
															<c:forEach items="${serviceType}" var="serviceType">
																<option value="${serviceType.code}">${serviceType.service_name}</option>
															</c:forEach>
														</c:if>
													</select> <span id="error_dist" style="color: red;" class="error"></span>
												</div>
												<span id="err"
													style="color: red; position: fixed; margin-top: 27px;"></span>
											</div>
											<div class="col-md-2" style="margin-top:3%">
												<div class="form-group">
													<button class="btn btn-primary" onclick="getTransactionByFilter(0);" type="button">Search</button>
												</div>
											</div>
											<!-- <div class="col-md-3">
												<div class="form-group">
													<label>Find By Email*</label> <input id="usernameId"
														name="userName" class="form-control" maxlength="100"
														placeholder="Enter Email" />
														<span id="errUser" style="color: red; margin-top: -5%;"></span>
												</div>
											</div>
											<div class="col-md-1.5" style="margin-top:3%">
												<div class="form-group">
													<button class="btn btn-primary" onclick="getByUserName()"  type="button">Search</button>
												</div>
											</div> -->
										<!-- 	<div class="col-md-2" style="margin-top:3%"></div>
											<div class="col-md-1.5" style="margin-top:3%">
												<div class="form-group">
													<button class="btn btn-primary" onclick="submitDwonloadForm();"  type="button">Download <i class="fa fa-download" aria-hidden="true"></i></button>
												</div>
											</div> 
										</div> -->
									</div>


									<div class="tabble" style="overflow-x: scroll;">
		            					<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
		            					<thead>
	                                          <tr>
											<th>S.&nbsp;No</th>
											<th>Contact&nbsp;No</th>
											<th>Amount</th>
											<th>Service&nbsp;Name</th>
											<th>Description</th>
											<th>Created&nbsp;Date</th>
											<th>Retrieval&nbsp;Ref.&nbsp;No</th>
											<th>Transaction&nbsp;Ref.&nbsp;No</th>
											<th>Status</th>
										</tr>
	                                    </thead>
	                                    <tbody id="genericTransactionList" style="height: 100px;overflow-y: auto;">
	                                    </tbody>
		            				</table>
		            				<nav>
										<ul class="pagination" id="paginationn"></ul>
									</nav>
	            				</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<!-- Container-fluid Ends -->
	</div>
	<!--Page Body Ends-->

	
	<!--page-wrapper Ends-->

	<!-- latest jquery-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/popper.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/bootstrap.js"></script>
	<!-- Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/chart/Chart.min.js"></script>
	<!-- Morris Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/raphael.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/morris.js"></script>
	<!-- owlcarousel js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/owlcarousel/owl.carousel.js"></script>
	<!-- Sidebar jquery-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/sidebar-menu.js"></script>
	<!--Sparkline  Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/sparkline/sparkline.js"></script>
	<!--Height Equal Js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/height-equal.js"></script>
	<!-- prism js -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/prism/prism.min.js"></script>
	<!-- clipboard js -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/clipboard/clipboard.min.js"></script>
	<!-- custom card js  -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/custom-card/custom-card.js"></script>
	<!-- Theme js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/script.js"></script>
	<!-- <script src="${pageContext.request.contextPath}/resources/assets/js/theme-customizer/customizer.js"></script> -->
	<!-- <script src="${pageContext.request.contextPath}/resources/assets/js/chat-sidebar/chat.js"></script> -->
	<!-- Counter js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.waypoints.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.counterup.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/counter-custom.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/notify/bootstrap-notify.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/notify/index.js"></script>
	 <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    
<!--     pagination -->

<script
		src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.min.js"></script>
    


	<script type="text/javascript">
		$(document).ready(function(){
			getTransactionList(0);
    	});
		$(function() {
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		    function cb(start, end) {
		        $('#reportrange').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
		    }
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'DD/MM/YYYY'
		        },
		        dateLimit: {
		            "days": 180
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		    cb(start, end);
		});
		
		function getTransactionList(page) {
    		var trHtml='';
    		var pagination = $('#paginationId').val();
    		console.log(pagination);
    		$.ajax({
    			type : "POST",
	    		url:"${pageContext.request.contextPath}/Master/Transactions",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"page" : page,
	   				"size" : 10,
	   			}),
	   			success : function(response) {
					$('#genericTransactionList').empty();
					console.log(response.details);
					if (response.details !=null && response.details.content!=null && response.details.content.length > 0) {
						displayTableWithData(response);
						$(function () {
	   	   					$('#paginationn').twbsPagination({
	    					 	totalPages: response.totalPage,
	    					 	visiblePages: 10,
	    		         	 	onPageClick: function (event, page) {
// 	    		         	 		if(pagination != 1){
	    		         	 			getTransactionList(page-1);
// 	    		         	 		}else{
// 					                $('#paginationId').val(2);
// 	    		         	 		}
	    			         	}
				 			}); 
		   				});
					} else {
						displayNoneData();
					}
	   			}
    		});
    	}
		
		function getTransactionByFilter(page) {
    		var trHtml='';
    		var daterange = $('#reportrange').val();
    		var serviceType = $('#serviceTypeId').val();
    		$.ajax({
    			type : "POST",
	    		url:"${pageContext.request.contextPath}/Master/Transactions",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"page" : page,
	   				"size" : 10,
	   				"daterange" :daterange,
	   				"serviceTypeId" : serviceType
	   			}),
	   			success : function(response) {
					$('#genericTransactionList').empty();
					console.log("response:"+response)
					if (response.details !=null && response.details.content !=null && response.details.content.length > 0) {
						displayTableWithData(response);
						$(function () {
							$('#paginationn').twbsPagination('destroy');
	   	   					$('#paginationn').twbsPagination({
	    					 	totalPages: response.totalPage,
	    					 	visiblePages: 10,
	    		         	 	onPageClick: function (event, page) {
	    		         	 		if($('#paginationId').val() !=1){
	    		         	 			getFilterData(page-1);
	    		         	 		}else{
					                $('#paginationId').val(2);
	    		         	 		}
	    			         	}
				 			});
		   				});
					} else {
						displayNoneData();
					}
	   			}
    		});
    	}
		
		
		function getFilterData(page) {
    		var trHtml='';
    		var daterange = $('#reportrange').val();
    		var serviceType = $('#serviceTypeId').val();
    		$.ajax({
    			type : "POST",
	    		url:"${pageContext.request.contextPath}/Master/Transactions",
	    		dataType:"json",
	   			contentType : "application/json",
	   			data : JSON.stringify({
	   				"page" : page,
	   				"size" : 10,
	   				"daterange" :daterange,
	   				"serviceTypeId" : serviceType
	   			}),
	   			success : function(response) {
					$('#genericTransactionList').empty();
					console.log("response:"+response)
					if (response.details !=null && response.details.content !=null && response.details.content.length > 0) {
						displayTableWithData(response);
					} else {
						displayNoneData();
					}
	   			}
    		});
    	}
		
		
		
		function displayTableWithData(response){
			var data;
			for (var i = 0; i < response.details.content.length; i++) {
				data = response.details.content[i];
				trHtml = '<tr>';
				trHtml = trHtml + '<td>' +(i+1)+ '</td>';
				trHtml = trHtml + '<td>' +data.user.contactNo+ '</td>';
				trHtml = trHtml + '<td>'+data.amount+'</td>';
				trHtml = trHtml + '<td>' +data.service.service_name+ '</td>';
				trHtml = trHtml + '<td>' +data.description+ '</td>';
				trHtml = trHtml + '<td>' +data.created+ '</td>';
				trHtml = trHtml + '<td>' +data.rrn+ '</td>';
				trHtml = trHtml + '<td>' +data.transaction_ref_no+ '</td>';
				trHtml = trHtml + '<td>' +data.status+ '</td>';
				trHtml = trHtml + '</tr>';
				$('#genericTransactionList').append(trHtml);
			}
		}
		function displayNoneData(){
			$('#paginationn').twbsPagination('destroy');
			trHtml = '<tr>';
			trHtml = trHtml + '<td></td>';
			trHtml = trHtml + '<td></td>';
			trHtml = trHtml + '<td></td>';
			trHtml = trHtml + '<td>No data found</td>';
			trHtml = trHtml + '<td></td>';
			trHtml = trHtml + '<td></td>';
			trHtml = trHtml + '<td></td>';
			trHtml = trHtml + '</tr>';
			$('#genericTransactionList').append(trHtml);
		}
		
		
		
		function formatDate(d){
        	d=new Date(d);
        	var month = d.getMonth();
        	var day = d.getDate();
        	month = month + 1;
        	month = month + "";
        	if (month.length == 1)
        	{
        	month = "0" + month;
        	}
        	day = day + "";
        	if (day.length == 1)
        	{
        	day = "0" + day;
        	}
        	return d.getFullYear()+ '-' +month + '-' + day ;
       	}
	</script>

<script >

function editProfile(){
	alert("Feature is coming soon");
}

function submitDwonloadForm(){
	var daterange = $('#reportrange').val();
	var serviceType = $('#serviceTypeId').val();
	$('#downloadDateId').val(daterange);
	$('#downloadServiceId').val(serviceType);
	var downloadDate=	$('#downloadDateId').val();
	var downloadService=	$('#downloadServiceId').val();
	
	if(downloadDate!='' && downloadService !=''){
		$('#downloadFormId').submit();
	}
}

</script>

</body>
</html>
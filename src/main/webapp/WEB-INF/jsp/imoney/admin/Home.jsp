<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
<meta name="keywords"
	content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
<meta name="author" content="pixelstrap">
<link rel="icon"
	href="${pageContext.request.contextPath}/resources/assets/images/favicon.png"
	type="image/x-icon" />
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/assets/images/favicon.png"
	type="image/x-icon" />
<title>i-money</title>

<!--Google font-->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700"
	rel="stylesheet">

<!-- Font Awesome -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.css">

<!-- ico-font -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/icofont/icofont.css">

<!-- Themify icon -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/themify-icons/themify-icons.css">

<!-- Flag icon -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/flag-icon.css">

<!-- prism css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/prism.css">

<!-- Owl css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/owlcarousel.css">

<!-- Bootstrap css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.css">

<!-- App css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/style.css">

<!-- Responsive css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/responsive.css">

<!-- mystyle  css-->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/mystyle.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />


</head>
<body>

	<!-- Loader starts -->
	<div class="loader-wrapper">
		<div class="loader bg-white">
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
			<h4>
				Have a great day at work today <span>&#x263A;</span>
			</h4>
		</div>
	</div>
	<!-- Loader ends -->

	<!--page-wrapper Start-->
	<div class="page-wrapper">

		<!--Page Header Start-->
		<jsp:include page="/WEB-INF/jsp/imoney/admin/header.jsp"></jsp:include>
		<!--Page Header Ends-->

		<!--Page Body Start-->
		<div class="page-body-wrapper">
			<!--Page Sidebar Start-->
			<jsp:include page="/WEB-INF/jsp/imoney/admin/leftMenu.jsp"></jsp:include>
			<!--Page Sidebar Ends-->

			<div class="page-body">

				<div class="container-fluid">
					<div class="col-md-4 col-sm-4 col-xs-4">
						<form action="${pageContext.request.contextPath}/Master/HomeData"
							method="POST">
							<div class="form-row">
								<div class="col-md-9">
									<label>Select Daterange*</label>
									<div id="" style="cursor: pointer;">
										<input id="reportrange" name="dateRange" class="form-control"
											style="background-color: white" readonly="readonly" />
									</div>
								</div>
								<div class="col-md-1" style="margin-top: 9%">
									<button class="btn btn-primary" type="submit">Filter</button>
								</div>
							</div>
						</form>
					</div>
					<br />
					<div class="row">
						<div class="col-xl-3 col-lg-6 col-sm-6">
							<div class="card">
								<div class="card-body">
									<div class="stat-widget-dashboard">
										<div class="media">
											<img class="mr-3"
												src="${pageContext.request.contextPath}/resources/assets/images/dashboard-icons/document.png"
												alt="Generic placeholder image">
											<div class="media-body text-right">
												<h4 class="mt-0 counter font-primary">${dashboardData.totalUsers}</h4>
												<span>Total Users</span>
											</div>
										</div>
										<div class="dashboard-chart-container">
											<div id="line-chart-sparkline-dashboard1"
												class="flot-chart-placeholder line-chart-sparkline"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-lg-6 col-sm-6">
							<div class="card">
								<div class="card-body">
									<div class="stat-widget-dashboard">
										<div class="media">
											<img class="mr-3"
												src="${pageContext.request.contextPath}/resources/assets/images/dashboard-icons/operator.png"
												alt="Generic placeholder image">
											<div class="media-body text-right">
												<h4 class="mt-0 counter font-secondary">${dashboardData.activeUser}</h4>
												<span>Active Users</span>
											</div>
										</div>
										<div class="dashboard-chart-container">
											<div id="line-chart-sparkline-dashboard2"
												class="flot-chart-placeholder line-chart-sparkline"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-lg-6 col-sm-6">
							<div class="card">
								<div class="card-body">
									<div class="stat-widget-dashboard">
										<div class="media">
											<img class="mr-3"
												src="${pageContext.request.contextPath}/resources/assets/images/dashboard-icons/chat.png"
												alt="Generic placeholder image">
											<div class="media-body text-right">
												<h4 class="mt-0 counter font-success">${dashboardData.inactiveUsers}</h4>
												<span>Inactive Users</span>
											</div>
										</div>
										<div class="dashboard-chart-container">
											<div id="line-chart-sparkline-dashboard3"
												class="flot-chart-placeholder line-chart-sparkline"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-lg-6 col-sm-6">
							<div class="card">
								<div class="card-body">
									<div class="stat-widget-dashboard">
										<div class="media">
											<img class="mr-3"
												src="${pageContext.request.contextPath}/resources/assets/images/dashboard-icons/like.png"
												alt="Generic placeholder image">
											<div class="media-body text-right">
												<h4 class="mt-0 counter font-info">${dashboardData.blockedUsers}</h4>
												<span>Blocked Users</span>
											</div>
										</div>
										<div class="dashboard-chart-container">
											<div id="line-chart-sparkline-dashboard4"
												class="flot-chart-placeholder line-chart-sparkline"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-lg-6 col-sm-6">
							<div class="card">
								<div class="card-body">
									<div class="stat-widget-dashboard">
										<div class="media">
											<img class="mr-3"
												src="${pageContext.request.contextPath}/resources/assets/images/dashboard-icons/like.png"
												alt="Generic placeholder image">
											<div class="media-body text-right">
												<h4 class="mt-0 counter font-info">${dashboardData.totalWallets}</h4>
												<span>Total Wallets</span>
											</div>
										</div>
										<div class="dashboard-chart-container">
											<div id="line-chart-sparkline-dashboard4"
												class="flot-chart-placeholder line-chart-sparkline"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-lg-6 col-sm-6">
							<div class="card">
								<div class="card-body">
									<div class="stat-widget-dashboard">
										<div class="media">
											<img class="mr-3"
												src="${pageContext.request.contextPath}/resources/assets/images/dashboard-icons/like.png"
												alt="Generic placeholder image">
											<div class="media-body text-right">
												<h4 class="mt-0 counter font-info">${dashboardData.totalPCards}</h4>
												<span>Total Physical Cards</span>
											</div>
										</div>
										<div class="dashboard-chart-container">
											<div id="line-chart-sparkline-dashboard4"
												class="flot-chart-placeholder line-chart-sparkline"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-lg-6 col-sm-6">
							<div class="card">
								<div class="card-body">
									<div class="stat-widget-dashboard">
										<div class="media">
											<img class="mr-3"
												src="${pageContext.request.contextPath}/resources/assets/images/dashboard-icons/like.png"
												alt="Generic placeholder image">
											<div class="media-body text-right">
												<h4 class="mt-0 counter font-info">${dashboardData.totalVCards}</h4>
												<span>Total Virtual Cards</span>
											</div>
										</div>
										<div class="dashboard-chart-container">
											<div id="line-chart-sparkline-dashboard4"
												class="flot-chart-placeholder line-chart-sparkline"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-lg-6 col-sm-6">
							<div class="card">
								<div class="card-body">
									<div class="stat-widget-dashboard">
										<div class="media">
											<img class="mr-3"
												src="${pageContext.request.contextPath}/resources/assets/images/dashboard-icons/like.png"
												alt="Generic placeholder image">
											<div class="media-body text-right">
												<h4 class="mt-0 counter font-info">${dashboardData.blockedCards}</h4>
												<span>Blocked Cards</span>
											</div>
										</div>
										<div class="dashboard-chart-container">
											<div id="line-chart-sparkline-dashboard4"
												class="flot-chart-placeholder line-chart-sparkline"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-lg-6 col-sm-6">
							<div class="card">
								<div class="card-body">
									<div class="stat-widget-dashboard">
										<div class="media">
											<img class="mr-3"
												src="${pageContext.request.contextPath}/resources/assets/images/dashboard-icons/like.png"
												alt="Generic placeholder image">
											<div class="media-body text-right">
												<h4 class="mt-0 counter font-info">0</h4>
												<span>Transacted Users</span>
											</div>
										</div>
										<div class="dashboard-chart-container">
											<div id="line-chart-sparkline-dashboard4"
												class="flot-chart-placeholder line-chart-sparkline"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-lg-6 col-sm-6">
							<div class="card">
								<div class="card-body">
									<div class="stat-widget-dashboard">
										<div class="media">
											<img class="mr-3"
												src="${pageContext.request.contextPath}/resources/assets/images/dashboard-icons/like.png"
												alt="Generic placeholder image">
											<div class="media-body text-right">
												<h4 class="mt-0 counter font-info">0</h4>
												<span>V Card Txn Amount</span>
											</div>
										</div>
										<div class="dashboard-chart-container">
											<div id="line-chart-sparkline-dashboard4"
												class="flot-chart-placeholder line-chart-sparkline"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-lg-6 col-sm-6">
							<div class="card">
								<div class="card-body">
									<div class="stat-widget-dashboard">
										<div class="media">
											<img class="mr-3"
												src="${pageContext.request.contextPath}/resources/assets/images/dashboard-icons/like.png"
												alt="Generic placeholder image">
											<div class="media-body text-right">
												<h4 class="mt-0 counter font-info">0</h4>
												<span>P Card Txn Amount</span>
											</div>
										</div>
										<div class="dashboard-chart-container">
											<div id="line-chart-sparkline-dashboard4"
												class="flot-chart-placeholder line-chart-sparkline"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-lg-6 col-sm-6">
							<div class="card">
								<div class="card-body">
									<div class="stat-widget-dashboard">
										<div class="media">
											<img class="mr-3"
												src="${pageContext.request.contextPath}/resources/assets/images/dashboard-icons/like.png"
												alt="Generic placeholder image">
											<div class="media-body text-right">
												<h4 class="mt-0 counter font-info">0</h4>
												<span>Load Wallet Amount</span>
											</div>
										</div>
										<div class="dashboard-chart-container">
											<div id="line-chart-sparkline-dashboard4"
												class="flot-chart-placeholder line-chart-sparkline"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div>
							<a
								href="${pageContext.request.contextPath}/Master/TermsCondition">Terms
								& Condition</a>
						</div>
					</div>
				</div>
				<!-- Container-fluid Ends -->
			</div>
			<!--Page Body Ends-->
		</div>
		<!--Page Body Ends-->

	</div>
	<!--page-wrapper Ends-->

	<!-- latest jquery-->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/jquery-3.2.1.min.js"></script>

	<!-- Bootstrap js-->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/popper.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/bootstrap.js"></script>

	<!-- Chart JS-->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/chart/Chart.min.js"></script>

	<!-- Morris Chart JS-->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/raphael.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/morris.js"></script>

	<!-- owlcarousel js-->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/owlcarousel/owl.carousel.js"></script>

	<!-- Sidebar jquery-->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/sidebar-menu.js"></script>

	<!--Sparkline  Chart JS-->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/sparkline/sparkline.js"></script>

	<!--Height Equal Js-->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/height-equal.js"></script>

	<!-- prism js -->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/prism/prism.min.js"></script>

	<!-- clipboard js -->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/clipboard/clipboard.min.js"></script>

	<!-- custom card js  -->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/custom-card/custom-card.js"></script>

	<!-- Theme js-->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/script.js"></script>
	<!-- <script src="${pageContext.request.contextPath}/resources//assets/js/theme-customizer/customizer.js"></script> -->
	<!-- <script src="../assets/js/chat-sidebar/chat.js"></script> -->

	<!-- Counter js-->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.waypoints.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.counterup.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/counter/counter-custom.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/assets/js/notify/bootstrap-notify.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/notify/index.js"></script>
	<script type="text/javascript"
		src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>


	<script type="text/javascript">
		$(document).ready(
				function() {
					$(document).ready(
							function() {
								var table = $('#datatable').DataTable({
									lengthChange : false,
									buttons : [ 'excel', 'pdf', 'csv' ]
								});

								table.buttons().container().appendTo(
										'#datatable_wrapper .col-md-6:eq(0)');
							});
				});
	</script>

	<script>
		$(function() {

			var start = '${startDate}';
			var end = '${endDate}';

			if (start == null || start == '' || end == null || end == '') {
				start = moment().subtract(29, 'days');
				end = moment();
			}

			function cb(start, end) {
				var startTime = new Date(start).getTime();
				var endTime = new Date(end).getTime();
				console.log(startTime);
				$('#reportrangetime').val(startTime + '-' + endTime);
				$('#reportrange').html(
						start.format('DD/MM/YYYY') + ' - '
								+ end.format('DD/MM/YYYY'));
			}

			$('#reportrange').daterangepicker(
					{
						startDate : start,
						endDate : end,
						locale : {
							format : 'DD/MM/YYYY'
						},
						dateLimit : {
							"days" : 360
						},
						ranges : {
							'Today' : [ moment(), moment() ],
							'Yesterday' : [ moment().subtract(1, 'days'),
									moment().subtract(1, 'days') ],
							'Last 7 Days' : [ moment().subtract(6, 'days'),
									moment() ],
							'Last 30 Days' : [ moment().subtract(29, 'days'),
									moment() ],
							'This Month' : [ moment().startOf('month'),
									moment().endOf('month') ],
							'Last Month' : [
									moment().subtract(1, 'month').startOf(
											'month'),
									moment().subtract(1, 'month')
											.endOf('month') ]
						}
					}, cb);

			cb(start, end);

		});
	</script>

</body>
</html>
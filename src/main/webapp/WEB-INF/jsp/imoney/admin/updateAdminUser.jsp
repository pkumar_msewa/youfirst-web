<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
<meta name="keywords"
	content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
<meta name="author" content="pixelstrap">
<link rel="icon" href="${pageContext.request.contextPath}/resources/assets/images/favicon.png" type="image/x-icon" />
<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets/images/favicon.png"
	type="image/x-icon" />
<title>Home</title>

<!--Google font-->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700"
	rel="stylesheet">

<!-- Font Awesome -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.css">

<!-- ico-font -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/icofont/icofont.css">

<!-- Themify icon -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/themify-icons/themify-icons.css">
<!-- Flag icon -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/flag-icon.css">

<!-- prism css -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/prism.css">

<!-- Owl css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/owlcarousel.css">

<!-- Bootstrap css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.css">

<!-- App css -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/style.css">

<!-- Responsive css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/responsive.css">

<!-- linearicons css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/linearicons/style.css">
<!-- mystyle  css-->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/mystyle.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" type="text/css">
</head>
<body>

	<!-- Loader starts -->
	<div class="loader-wrapper">
		<div class="loader bg-white">
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
			<h4>
				Have a great day at work today <span>&#x263A;</span>
			</h4>
		</div>
	</div>
	<!-- Loader ends -->

	<!--page-wrapper Start-->
	<div class="page-wrapper">

		<jsp:include page="/WEB-INF/jsp/imoney/admin/header.jsp"></jsp:include>
		<div class="page-body-wrapper">
			<jsp:include page="/WEB-INF/jsp/imoney/admin/leftMenu.jsp"></jsp:include>

			<div class="page-body">
				<!-- Container-fluid starts -->
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-12">
							<div class="card height-equal equal-height-lg"
								style="margin-top: 15px;">
								<div class="card-header">
									<h5>Create Admin</h5>
									<div class="card-header-right"></div>
								</div>
								<div class="card-body">
									<div class="col-lg-12">
										<div class="row">
											<div class="col-md-8">
														<form  action="${pageContext.request.contextPath}/Master/CreateAdmin" id="formId"  method="post" >
										<div class="row">
												<c:choose>
													<c:when test="${not empty sucMsg}">
														<center style="color:green;">${sucMsg}</center>
													</c:when>
													<c:when test="${not empty errorMsg}">
														<center style="color:red;">${errorMsg}</center>
													</c:when>
												</c:choose>
												<fieldset>
													<legend>Create Admin</legend>
														<div class="row">
														<div class="col-6">
															<div class="form-group">
	                                                          <label>First Name*</label>
	                                                          <input type="text" name="firstName"  value="${user.firstName}" id="firstNameId" maxlength="15" 
	                                                          class="form-control agent" readonly/>
	                                                          <span id="error_firstName" style="color:red;" class="error"></span>
	                                                        </div>
														</div>
														<div class="col-6">
															<div class="form-group">
	                                                          <label>Last Name*</label>
	                                                          <input type="text" name="lastName"  value="${user.lastName}" id="lastNameId" maxlength="15" readonly class="form-control agent"/>
	                                                          <span id="error_lastName" style="color:red;" class="error"></span>
	                                                        </div>
														</div>
														<div class="col-6">
															<div class="form-group">
	                                                          <label for="fname">Email*</label>
																<input type="text" name="email"  value="${user.email}" id="emailId" maxlength="50" readonly class="form-control agent"/>
	                                                          <span id="error_email" style="color:red;" class="error"></span>
	                                                        </div>
														</div>
														<div class="col-6">
															<div class="form-group">
	                                                          <label for="fname">Contact*</label>
															<input type="text" name="contactNo"  value="${user.contactNo}" id="contactId" maxlength="10" readonly onkeypress="return isNumberKey(event);" class="form-control agent "/>
	                                                          <span id="error_contact" style="color:red;" class="error"></span>
	                                                        </div>
														</div>
														<div class="col-6">
															<div class="form-group">
	                                                          <label for="fname">Select Role</label>
	                                                        <input type="text" name="role"  value="Admin" id="roleId" class="form-control agent" readonly/>
	                                                          <span id="error_role"  style="color:red;"  class="error"></span>
	                                                        </div>
														</div>
														<div class="col-6">
														 <label>Select Menu Item</label>
														  <select id="menuItemId" name="menuItems"
															multiple="multiple">
															<c:if test="${menuList != null }">
															<c:forEach items="${menuList}" var="menu">
																<option value="${menu.id}">${menu.name}</option>
															</c:forEach>
															</c:if>
														</select> <span id="error_menu" style="color:red;" class="error"></span>
													</div>
													</div>
												</fieldset>
										</div>
										<center><button type="button" class="btn btn-primary mt-4" onclick="validateform()">Submit</button></center>
					</form>
                            </div>
                        </div>

												</div>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
				<!-- Container-fluid Ends -->
			</div>
			<!--Page Body Ends-->
		</div>
		<!--Page Body Ends-->

	</div>

	
	<!--page-wrapper Ends-->

	<!-- latest jquery-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/jquery-3.2.1.min.js"></script>

	<!-- Bootstrap js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/popper.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/bootstrap.js"></script>

	<!-- Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/chart/Chart.min.js"></script>

	<!-- Morris Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/raphael.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/morris.js"></script>

	<!-- owlcarousel js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/owlcarousel/owl.carousel.js"></script>

	<!-- Sidebar jquery-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/sidebar-menu.js"></script>

	<!--Sparkline  Chart JS-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/sparkline/sparkline.js"></script>

	<!--Height Equal Js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/height-equal.js"></script>

	<!-- prism js -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/prism/prism.min.js"></script>

	<!-- clipboard js -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/clipboard/clipboard.min.js"></script>

	<!-- custom card js  -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/custom-card/custom-card.js"></script>

	<!-- Theme js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/script.js"></script>
	<!-- <script src="${pageContext.request.contextPath}/resources/assets/js/theme-customizer/customizer.js"></script> -->
	<!-- <script src="${pageContext.request.contextPath}/resources/assets/js/chat-sidebar/chat.js"></script> -->
	<script src="${pageContext.request.contextPath}/resources/assets/js/dashboard-default.js"></script>

	<!-- Counter js-->
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.waypoints.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.counterup.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/counter/counter-custom.js"></script>

	<script src="${pageContext.request.contextPath}/resources/assets/js/notify/bootstrap-notify.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/notify/index.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>


	<script>

function validateform(){
	
	var valid= true;
	var pattern = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|in)";
	var firstName = $('#firstNameId').val();
	var lastName = $('#lastNameId').val();
	var  email= $('#emailId').val();
	var contact = $('#contactId').val();
	var role = $('#roleId').val();
	var menuItem = $('#menuItemId').val();
	
	
	if(firstName == null || firstName ==''){
		$('#error_firstName').html("Please enter first name");
		valid= false;
	}if(lastName ==null || lastName == ''){
		$('#error_lastName').html("Please enter last name");
		 valid= false;
	}if(email == null || email == ''){
		$('#error_email').html("Please enter email");
		 valid= false;
	}else if(!email.match(pattern)) {
		$("#error_email").html("Enter valid email Id");
		valid = false;	
	}if(contact ==null || contact ==''){
		$('#error_contact').html("Please enter contact");
		valid= false;
	}if(role ==null ||role ==''){
		$('#error_role').html("Please enter role");
		 valid= false;
	}if(menuItem ==null || menuItem ==''){
		$('#error_menu').html("Please select one menu item");
		 valid= false;
	}
	if(valid){
		$('#formId').submit();
	}
	
	var timeout = setTimeout(function(){
			$('#error_firstName').html("");
			$('#error_lastName').html("");
			$('#error_email').html("");
			$('#error_contact').html("");
			$('#error_role').html("");
			$('#error_menu').html("");
    }, 3000);
}

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}


</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#menuItemId').multiselect();
	});
</script>

</body>
</html>
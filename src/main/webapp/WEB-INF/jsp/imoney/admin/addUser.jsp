<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon"
	href="${pageContext.request.contextPath}/resources/assets/images/favicon.png"
	type="image/x-icon" />
<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>
<title>Add User</title>

<!--Google font-->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700"
	rel="stylesheet">

<!-- Font Awesome -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.css">

<!-- ico-font -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/icofont/icofont.css">

<!-- Themify icon -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/themify-icons/themify-icons.css">
<!-- Flag icon -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/flag-icon.css">

<!-- prism css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/prism.css">

<!-- Owl css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/owlcarousel.css">

<!-- Bootstrap css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.css">

<!-- App css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/style.css">

<!-- Responsive css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/responsive.css">

<!-- linearicons css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/linearicons/style.css">
<!-- mystyle  css-->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/mystyle.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css"
	rel="stylesheet" type="text/css" />

<style>
.sidenav {
	height: 89%;
	margin-top: 5%;
	width: 0;
	position: fixed;
	z-index: 4;
	top: 0;
	right: 0;
	background-color: white;
	overflow-x: hidden;
	transition: 0.5s;
	padding-top: 60px;
}

.sidenav a {
	padding: 8px 8px 8px 32px;
	text-decoration: none;
	font-size: 25px;
	color: #818181;
	display: block;
	transition: 0.3s;
}

.sidenav a:hover {
	color: #f1f1f1;
}

.sidenav .closebtn {
	position: absolute;
	top: 0;
	right: 25px;
	font-size: 36px;
	margin-left: 50px;
}

@media screen and (max-height: 450px) {
	.sidenav {
		padding-top: 15px;
	}
	.sidenav a {
		font-size: 18px;
	}
}

@media ( min-width : 992px) .modal-lg {
	max-width
	
	
	:
	
	
	900
	px
	;
	
	


}

.linksBlock {
	/* border: 1px solid #787878; */
	height: 85px;
	margin: 0 auto;
	padding: 17px 0;
}

.relatedLinks a {
	color: #00508f;
}
</style>

</head>
<body>

	<!-- Loader starts -->
	<div class="loader-wrapper">
		<div class="loader bg-white">
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
			<h4>
				Have a great day at work today <span>&#x263A;</span>
			</h4>
		</div>
	</div>
	<!-- Loader ends -->

	<!--page-wrapper Start-->
	<div class="page-wrapper">
		<jsp:include page="/WEB-INF/jsp/imoney/admin/header.jsp"></jsp:include>
		<div class="page-body-wrapper">
			<jsp:include page="/WEB-INF/jsp/imoney/admin/leftMenu.jsp"></jsp:include>

			<div class="page-body">
				<!-- Container-fluid starts -->
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-12">
							<div class="card height-equal" style="margin-top: 15px;">
								<div class="card-header">
									<h4>Add User</h4>
									<h2>${response.message}</h2>
								</div>
								<div class="card-body">
									<form
										action="${pageContext.request.contextPath}/Master/AddUser"
										id="formId" method="post" onload="addUserDetails()">
										<div class="form-row">
											<div class="col">
												<div class="form-group">
													<label>First Name</label> <input type="text"
														name="first_name" id="fName" class="form-control"
														maxlength="15" placeholder="First Name"
														required="required">
													<p style="color: red;" id="fNameErrorId"></p>
												</div>
											</div>
											<div class="col">
												<div class="form-group">
													<label>Middle Name</label> <input type="text"
														name="middle_name" id="mName" class="form-control"
														maxlength="15" placeholder="Middle Name">
												</div>
											</div>
										</div>
										<div class="form-row">
											<div class="col">
												<div class="form-group">
													<label>Last Name</label> <input type="text"
														name="last_name" id="lName" class="form-control"
														maxlength="10" placeholder="Last Name" required="required">
												</div>
											</div>
											<div class="col">
												<div class="form-group">
													<label>Mobile Number</label><input type="text"
														name="mobileNumber" id="mNumber" class="form-control"
														placeholder="Mobile Number"
														onkeypress="if ( isNaN(this.value + String.fromCharCode(event.keyCode) )) return false;"
														maxlength="10" required="required" />
													<p style="color: red;" id="mobNumErrorId"></p>
												</div>
											</div>
										</div>
										<div class="form-row">
											<div class="col">
												<div class="form-group">
													<label>Email Id</label> <input type="text" name="email"
														id="mail" class="form-control" placeholder="Email Id"
														required="required">
												</div>
											</div>
											
											<div class="col">
												<div class="form-group">
													<script type="text/javascript">
														$(".dateOfPurchased")
																.datepicker(
																		{
																			maxDate : "-1d",
																			minDate : new Date(
																					2007,
																					6,
																					12)

																		});
													</script>
													<p>
														Date <input type="text" id="datepicker" name="birthday">
													</p>



												</div>
											</div>
										</div>
										<div class="form-row">
											<div class="col">
												<div class="form-group">
													<label>Gender</label><br>
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" id="genderM"
															name="gender" id="inlineRadio1" value="option1">
														<label class="form-check-label" for="inlineRadio1">Female</label>
													</div>
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio"
															checked="checked" id="genderF" name="gender"
															id="inlineRadio2" value="option2"> <label
															class="form-check-label" for="inlineRadio2">Male</label>
													</div>
												</div>
											</div>
											<div class="form-row">
											<div class="col">
												<div class="form-group">
													<label>Select Role</label> <select class="form-control"
														id="selectrole" name="select_role" required="required">
														<option>-- Choose Select Role --</option>
														<option value="Role User">Role User</option>
														<option value="Role Distributer">Role Distributor</option>
														<option value="Role Agent">Role Agent</option>
													</select>
												</div>
											</div>
											<div class="col">
												<div class="form-group">
													<label>Password</label> <input type="password"
														maxlength="8" name="password" id="pswd"
														class="form-control" placeholder="Password"
														required="required">
													<p style="color: red;" id="passErrorId"></p>
												</div>
											</div>
										</div>
										
										<div class="form-row">
											<div class="col">
												<div class="form-group">
													<label>ID Type</label> <select class="form-control"
														id="idType" name="id_type" required="required">
														<option>-- Choose ID Type --</option>
														<option value="Aadhaar">Aadhaar</option>
														<option value="VoterId">VoterId</option>
														<option value="PAN">PAN</option>
														<option value="DL">DL</option>
														<option value="Passport">Passport</option>
													</select>
												</div>
											</div>
											
											<div class="col">
												<div class="form-group">
													<label>ID Number</label> <input type="text"
														name="id_number" id="idNum" class="form-control"
														maxlength="12" placeholder="ID Number" required="required">
												</div>
											</div>
										</div>
										
										
										<div class="form-group text-center">
											<button class="btn btn-primary" onclick="addUserDetails()">Submit</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<!-- Container-fluid Ends -->
	</div>
	<!--Page Body Ends-->

	<!--page-wrapper Ends-->

	<!-- latest jquery-->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap js-->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/popper.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/bootstrap.js"></script>
	<!-- Chart JS-->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/chart/Chart.min.js"></script>
	<!-- Morris Chart JS-->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/raphael.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/morris.js"></script>
	<!-- owlcarousel js-->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/owlcarousel/owl.carousel.js"></script>
	<!-- Sidebar jquery-->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/sidebar-menu.js"></script>
	<!--Sparkline  Chart JS-->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/sparkline/sparkline.js"></script>
	<!--Height Equal Js-->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/height-equal.js"></script>
	<!-- prism js -->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/prism/prism.min.js"></script>
	<!-- clipboard js -->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/clipboard/clipboard.min.js"></script>
	<!-- custom card js  -->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/custom-card/custom-card.js"></script>
	<!-- Theme js-->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/script.js"></script>
	<!-- <script src="${pageContext.request.contextPath}/resources/assets/js/theme-customizer/customizer.js"></script> -->
	<!-- <script src="${pageContext.request.contextPath}/resources/assets/js/chat-sidebar/chat.js"></script> -->
	<!-- Counter js-->
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.waypoints.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.counterup.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/counter/counter-custom.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/notify/bootstrap-notify.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/notify/index.js"></script>
	<script type="text/javascript"
		src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

	<!--     pagination -->
	<script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js"
		type="text/javascript"></script>

	<script>
		$('#datepicker').datepicker({
			uiLibrary : 'bootstrap4'
		});
	</script>
	<script type="text/javascript">
		function addUserDetails() {
			var fname = $("#fName").val();
			var mName = $("#mName").val();
			var lName = $("#lName").val();
			var mobile = $("#mNumber").val();
			var birthday = $("#bDay").val();
			var male = $("#genderM").val();
			var fMale = $("#genderF").val();

			var idType = $("idType").val();
			var idNum = $("#idNum").val();
			var password = $("#pswd").val();
			var selectrole=$('#selectrole');

			var reg = /^[6789]\d{9}$/;
			

			var valid = true;
			
			

			if (fname == '') {
				$("#fNameErrorId").html("Please enter the First Name")
				valid = false;
			}

			if (mobile.length != 10) {
				$("#mobNumErrorId")
						.html("Please enter the valid mobile Number")
				valid = false;
			} else if (!mobile.match(reg)) {
				$("#mobNumErrorId")
						.html("Please enter the valid mobile Number")
				valid = false;
			}

			if (password == '') {
				$("#passErrorId").html("Please enter the Password")
				valid = false;
			}

			if (valid == true) {
				$("#formId").submit();
			}
		}
	</script>

</body>
</html>
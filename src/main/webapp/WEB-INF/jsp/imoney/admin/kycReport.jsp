<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon"
	href="${pageContext.request.contextPath}/resources/assets/images/favicon.png"
	type="image/x-icon" />
<title>Kyc Report</title>

<!--Google font-->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700"
	rel="stylesheet">

<!-- Font Awesome -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.css">

<!-- ico-font -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/icofont/icofont.css">

<!-- Themify icon -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/vendor/themify-icons/themify-icons.css">

<!-- Flag icon -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/flag-icon.css">

<!-- prism css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/prism.css">

<!-- Owl css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/owlcarousel.css">

<!-- Bootstrap css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.css">

<!-- App css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/style.css">

<!-- Responsive css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/responsive.css">

<!-- linearicons css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/linearicons/style.css">
<!-- mystyle  css-->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/mystyle.css">

</head>
<body>

	<!-- Loader starts -->
	<div class="loader-wrapper">
		<div class="loader bg-white">
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
			<h4>
				Have a great day at work today <span>&#x263A;</span>
			</h4>
		</div>
	</div>
	<!-- Loader ends -->

	<!--page-wrapper Start-->
	<div class="page-wrapper">

		<jsp:include page="/WEB-INF/jsp/imoney/admin/header.jsp"></jsp:include>
		<div class="page-body-wrapper">
			<jsp:include page="/WEB-INF/jsp/imoney/admin/leftMenu.jsp"></jsp:include>
			<div class="page-body">
				<!-- Container-fluid starts -->
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-12">
							<div class="card height-equal equal-height-lg"
								style="margin-top: 15px;">
								<div class="card-header">
									<h5>KYC Report</h5>
									<div class="card-header-right">
										<a href="#" class="icon-bar"><i
											class="lnr lnr-magnifier"></i></a> <a href="#"
											class="icon-bar"><i class="lnr lnr-download"></i></a>
									</div>
								</div>
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-border table-striped">
											<thead>
												<tr>
													<th>Sl. No</th>
													<th>Date</th>
													<th>First name</th>
													<th>Last name</th>
													<th>Middle name</th>
													<th>Email</th>
													<th>Mobile Number</th>
													<th>Status</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>1</td>
													<td>2018-11-04</td>
													<td>abc</td>
													<td>def</td>
													<td>ghi</td>
													<td>john@example.com</td>
													<td>1234567890</td>
													<td>Kyc</td>
												</tr>
												<tr >
													<td>2</td>
													<td>2018-11-04</td>
													<td>abc</td>
													<td>def</td>
													<td>ghi</td>
													<td>john@example.com</td>
													<td>1234567890</td>
													<td>Non-Kyc</td>
												</tr>
												<tr >
													<td>3</td>
													<td>2018-11-04</td>
													<td>abc</td>
													<td>def</td>
													<td>ghi</td>
													<td>john@example.com</td>
													<td>1234567890</td>
													<td>Kyc</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<!-- Container-fluid Ends -->
	</div>
	<!--Page Body Ends-->
	</div>
	<!--Page Body Ends-->

	</div>

	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Reason for Rejection</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<form class="form-horizontal">
						<div class="form-group row">
							<label for="formGroupExampleInput"
								class="col-sm-4 col-form-label">Reason Type</label>
							<div class="col-sm-8">
								<select class="form-control" id="exampleFormControlSelect1">
									<option>-- Type --</option>
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="formGroupExampleInput"
								class="col-sm-4 col-form-label">Other:</label>
							<div class="col-sm-8">
								<textarea class="form-control" id="exampleFormControlTextarea1"
									rows="2"></textarea>
							</div>
						</div>
					</form>
					<div class="b_ok" style="text-align: center;">
						<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
					</div>


				</div>
			</div>
			<!--page-wrapper Ends-->

			<!-- latest jquery-->
			<script
				src="${pageContext.request.contextPath}/resources/assets/js/jquery-3.2.1.min.js"></script>

			<!-- Bootstrap js-->
			<script
				src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/popper.min.js"></script>
			<script
				src="${pageContext.request.contextPath}/resources/assets/js/bootstrap/bootstrap.js"></script>

			<!-- Chart JS-->
			<script
				src="${pageContext.request.contextPath}/resources/assets/js/chart/Chart.min.js"></script>

			<!-- Morris Chart JS-->
			<script
				src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/raphael.js"></script>
			<script
				src="${pageContext.request.contextPath}/resources/assets/js/morris-chart/morris.js"></script>

			<!-- owlcarousel js-->
			<script
				src="${pageContext.request.contextPath}/resources/assets/js/owlcarousel/owl.carousel.js"></script>

			<!-- Sidebar jquery-->
			<script
				src="${pageContext.request.contextPath}/resources/assets/js/sidebar-menu.js"></script>

			<!--Sparkline  Chart JS-->
			<script
				src="${pageContext.request.contextPath}/resources/assets/js/sparkline/sparkline.js"></script>

			<!--Height Equal Js-->
			<script
				src="${pageContext.request.contextPath}/resources/assets/js/height-equal.js"></script>

			<!-- prism js -->
			<script
				src="${pageContext.request.contextPath}/resources/assets/js/prism/prism.min.js"></script>

			<!-- clipboard js -->
			<script
				src="${pageContext.request.contextPath}/resources/assets/js/clipboard/clipboard.min.js"></script>

			<!-- custom card js  -->
			<script
				src="${pageContext.request.contextPath}/resources/assets/js/custom-card/custom-card.js"></script>

			<!-- Theme js-->
			<script
				src="${pageContext.request.contextPath}/resources/assets/js/script.js"></script>
			<!-- <script src="${pageContext.request.contextPath}/resources/assets/js/theme-customizer/customizer.js"></script> -->
			<!-- <script src="${pageContext.request.contextPath}/resources/assets/js/chat-sidebar/chat.js"></script> -->
			<script
				src="${pageContext.request.contextPath}/resources/assets/js/dashboard-default.js"></script>

			<!-- Counter js-->
			<script
				src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.waypoints.min.js"></script>
			<script
				src="${pageContext.request.contextPath}/resources/assets/js/counter/jquery.counterup.min.js"></script>
			<script
				src="${pageContext.request.contextPath}/resources/assets/js/counter/counter-custom.js"></script>

			<script
				src="${pageContext.request.contextPath}/resources/assets/js/notify/bootstrap-notify.min.js"></script>
			<script
				src="${pageContext.request.contextPath}/resources/assets/js/notify/index.js"></script>



			<script>
				function openModel() {

					$('#myModal').modal('show');
				}
			</script>
</body>
</html>